Overview
========

These are the tickets extracted from our trac instance, which used to
be run on a server dev.mutt.org.

* 'tickets' has a text file for each ticket, containing its history.
  The naming convention is 'ticketid-ticket_summary.txt'.
  The tickets are divided into 'closed' and 'open' subdirectories,
  reflecting their status at the time of export.

* 'attachments' has a subdirectory for each ticket that had
  attachments.  Note that a very small number of attachments were
  referenced in a ticket but not available in trac for some reason.


Why are these not in the gitlab ticket system?
==============================================

Importing them into gitlab proved practically impossible.  Their "web
api" has a spam filter cranked up to nosebleed levels.  Far too many
tickets were being rejected as spam.

After banging my head against the wall trying to use the web api for a
month, I gave up and wrote my own exporter.

However, even if it had been successful, importing using the web api
would have lost a great deal of information:
  * the submitter of the ticket and each comment
  * all dates
  * all attachments
  * the original ticket number

While the tickets might have been more easily manipulated, their
utility as a reference of mutt history would have been greatly
reduced.

-Kevin McCarthy
