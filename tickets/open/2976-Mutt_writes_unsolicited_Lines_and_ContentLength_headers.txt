Ticket:  2976
Status:  new
Summary: Mutt writes unsolicited "Lines" and "Content-Length" headers

Reporter: rtc
Owner:    mutt-dev

Opened:       2007-11-02 14:13:58 UTC
Last Updated: 2009-06-12 23:30:39 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
This annoyed me since the first day that I started using mutt: The unsolicted "Lines" and "Content-Length" headers that mutt adds to existing mailbox files. These should be kept up to date if they are already present, perhaps added to newly created messages, but not added to existing mailbox files.


--------------------------------------------------------------------------------
2007-11-02 14:14:19 UTC rtc
* Added attachment mutt-copy.patch
* Added comment:
Patch that solves the problem for me

--------------------------------------------------------------------------------
2007-11-02 15:48:31 UTC vinc17
* Added comment:
This is not a bug. They can be useful for Mutt and/or for the end user. Perhaps their creation (and removal?) should be controlled with a (possibly multi-valued) option.

* type changed to enhancement

--------------------------------------------------------------------------------
2007-11-02 17:24:29 UTC rtc
* Added comment:
The only use for such headers are messages in mboxcl2 format. Buch such messages cannot be read correctly without such headers, anyway. So such messages always contain these headers from the first time they are written. (In fact, mutt uses mboxcl2 format when writing sent messages into the outbox, which it shouldn't--it should write them in mboxrd format.) Please explain what utility for Mutt and/or the end user you mean if you see one that goes beyond that. I see it as incorrect behaviour if such headers are ever added by a mailer to already existing messages. But your proposal would be fine for me, too. It should be a trivial enhancement to the attached patch.

--------------------------------------------------------------------------------
2007-11-02 22:21:06 UTC Aron Griffis
* Added comment:
{{{
Mutt wrote:  [Fri Nov 02 2007, 01:24:30PM EDT]
> The only use for such headers are messages in mboxcl2 format.

The Lines header is used by mutt to show the number of lines in the
index.  Without the Lines header, mutt shows 0 until the message is
read, at which point it updates to the correct number of lines.  So
caching it between mailbox reads using the Lines header is desireable.
}}}

--------------------------------------------------------------------------------
2007-12-28 04:50:40 UTC rtc
* Added attachment mutt-copy2.patch
* Added comment:
updated patch with option

--------------------------------------------------------------------------------
2007-12-28 05:01:18 UTC rtc
* Added comment:
I now uploaded a new patch that introduces the option mboxrd_format to keep these headers from being added. In addition to the previous patch, it does that also on saving FCCs, and I implemented From_ escaping to prevent the issues arising out of this. I did not implement anything concerning unescaping these on reading mailboxes, but it wasn't done previously, anyway. (It might be annoying when postponing a message with From_s). If you want more fine-grained control over whether Content-Length and Lines should be added, please change the patch according to your desires; the necessary changes should be trivial.

--------------------------------------------------------------------------------
2009-06-12 23:30:39 UTC pdmef
* cc changed to 532766@bugs.debian.org
* Added comment:
See #3244 and debian 532766.
