Ticket:  3136
Status:  new
Summary: Please merge sendbox mode (Outbox for some IMAP servers)

Reporter: KiBi
Owner:    brendan

Opened:       2008-11-24 04:53:30 UTC
Last Updated: 2011-07-04 09:26:32 UTC

Priority:  minor
Component: IMAP
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Aron Griffis published a patch[1,2] so that mutt can be used as an IMAP client that is Outbox-aware. That's a Courier-IMAP[3] feature that allows for sending mails when they are added to a particular box (which defaults to INBOX.Outbox when this mode is enabled on the server).

That proves to be particularly useful when one is using remote IMAP through offlineimap, since there's no need to have a local MTA, to track network status and the like (I've been using postfix and postqueue -f for some years now). Just taking care of running offlineimap (for both incoming and outgoing mail at the same time) is then sufficient.

 1. http://n01senet.blogspot.com/2006/10/scratching-mutt-part-1-introducing.html
 2. http://n01senet.blogspot.com/2007/02/scratching-mutt-part-2-patch-and.html
 3. http://www.inter7.com/courierimap/INSTALL.html#imapsend

The original patch was designed for mutt 1.5.12 and said to apply on 1.5.13 as well. I've “ported” it to 1.5.18, and it looks like it now applies on both the pristine 1.5.18 release and the “patched” version as used in the Debian mutt-patched package.

I didn't really get the reason for keeping sent mails as unread, so I'm proposing a tiny modification.

(I'll try and use hg next time, I've still got to read the manual a bit more before before efficient enough.)

Mraw,[[BR]]
!KiBi.

--------------------------------------------------------------------------------
2008-11-24 04:53:56 UTC KiBi
* Added attachment mutt-pristine+sendbox.diff
* Added comment:
sendbox patch

--------------------------------------------------------------------------------
2008-11-24 04:55:03 UTC KiBi
* Added attachment mutt-pristine+sendbox+read.diff
* Added comment:
Mark sendbox'd messages as read.

--------------------------------------------------------------------------------
2008-11-24 05:10:35 UTC KiBi
* Added comment:
I meant to initially include the (locally-used-git) commit message along with the patch, but finally run filterdiff on it. Missing information is that Aron Griffis (whose mail you may find on his blog, not exposing it here) released this patch in 2006, under the GPLv2 license. I don't think my modifications are substantial enough to qualify, but in case you need it, same license (or any later version of the GPL as published by the FSF) for “my” updated patch.

Mraw,[[BR]]
!KiBi.

--------------------------------------------------------------------------------
2008-11-24 15:07:54 UTC Rocco Rutte
* Added comment:
{{{
Hi,

* Mutt wrote:


These patches were sent to mutt-dev already at least 2 times I think.
There was a conversion between me and Aron on #mutt where I mentioned
that I think some things can be improved before this makes it in. For
example that I think the sending interfaces (sendmail, SMTP, sendbox)
should technically look the same instead of 2 equal and one different. I
think this needs some more discussion.

See http://marc.info/?l=mutt-dev&m=121881393026700&w=2.

Rocco
}}}

--------------------------------------------------------------------------------
2009-01-04 20:22:13 UTC brendan
* milestone changed to 2.0

--------------------------------------------------------------------------------
2009-04-28 16:39:05 UTC agriffis
* Added comment:
Reworked patches posted here against tip (1.5.19+): http://www.mail-archive.com/mutt-dev@mutt.org/msg04919.html

--------------------------------------------------------------------------------
2011-07-04 09:25:36 UTC Nicolas.Estibals
* Added attachment mutt-resent-header-weeding.patch

--------------------------------------------------------------------------------
2011-07-04 09:25:45 UTC Nicolas.Estibals
* Added attachment mutt-sendbox.patch

--------------------------------------------------------------------------------
2011-07-04 09:25:52 UTC Nicolas.Estibals
* Added attachment mutt-write_postponed.patch

--------------------------------------------------------------------------------
2011-07-04 09:25:58 UTC Nicolas.Estibals
* Added attachment mutt-sendbox-bounce.patch

--------------------------------------------------------------------------------
2011-07-04 09:26:32 UTC Nicolas.Estibals
* Added comment:
I've reworked those patches against mutt 1.5.21. (see https://aur.archlinux.org/packages.php?ID=48506)

