Ticket:  3542
Status:  new
Summary: mutt sometimes fails to detect new messages in the current maildir

Reporter: AopicieR
Owner:    mutt-dev

Opened:       2011-10-08 09:02:58 UTC
Last Updated: 2011-10-10 02:07:29 UTC

Priority:  major
Component: maildir/mh
Keywords:  

--------------------------------------------------------------------------------
Description:
My inbox is a local maildir and I use "set timeout=1". Say my inbox is the current folder. Normally mutt will display any new mail to my inbox with a delay of at most one second. However when receiving a lot of new message more or less at the same time it sometimes happens that mutt will only detect some of them at all. The following command reproduces this rather reliably for me:
{{{
$ for (( i=1; i<=20; i++ )); do mail -s kBqYKThe5q $USER < /dev/null; done
}}}
If I run this command mutt will (in maybe 2 out of 5 cases) only detect one or two of the 20 new messages. The only way I have found to make it find all the new messages is to explicitly open my inbox again with "c!".

--------------------------------------------------------------------------------
2011-10-10 02:07:29 UTC vinc17
* cc changed to vincent@vinc17.org
* Added comment:
Possibly related to bug #3475.
