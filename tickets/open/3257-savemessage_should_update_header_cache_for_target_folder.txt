Ticket:  3257
Status:  new
Summary: <save-message> should update header cache for target folder

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-06-13 10:14:09 UTC
Last Updated: 2009-06-30 14:38:23 UTC

Priority:  minor
Component: header cache
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/436226

{{{
When I <save-message> a message to another folder, mutt does not
update the header cache of the destination folder. This means that
when I open the destination folder, mutt notices a change and has to
reindex the Maildir. At the time of <save-message>, mutt knows
exactly what change is being made and could update the destination's
header cache with much less effort than when opening the mailbox and
having to figure out what needs updating.

It would thus be nice if mutt updated the destination's header_cache
after successfully saving a message to another folder.
}}}

--------------------------------------------------------------------------------
2009-06-30 14:38:23 UTC pdmef
* component changed to header cache
