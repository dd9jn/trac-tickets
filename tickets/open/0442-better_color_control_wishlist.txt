Ticket:  442
Status:  new
Summary: better color control (wishlist)

Reporter: jim@neurosis.mit.edu
Owner:    mutt-dev

Opened:       2001-01-28 11:03:02 UTC
Last Updated: 2005-08-08 19:54:00 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2.5i
Severity: wishlist

-- Please type your report below this line

Mutt needs better color control.  For example, take the index.
There's no way to make the indicator bar only change the background
(to, say, blue) so that the foreground colors stay the same (including
seperate colors for the tree markers, etc).  There should also be ways
to only change one or the other color attribute, and there should be
ways to modify color attributes (eg, I want "bright" added to all colors
when a message is addressed to me).  Perhaps a more uniform scheme
(adding patterns and regexps to everything, not having special cases
for "index" colors) is in order.

Just an idea.

-- Mutt Version Information

Mutt 1.2.5i (2000-07-28)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.0 [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +USE_FCNTL  -USE_FLOCK
+USE_IMAP  -USE_GSS  +USE_SSL  +USE_POP  +HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +ENABLE_NLS
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
SHAREDIR="/usr/share/mutt"
SYSCONFDIR="/usr/share/mutt"
-ISPELL
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the muttbug utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-09 13:54:00 UTC brendan
* Added comment:
{{{
File under change-request
}}}
