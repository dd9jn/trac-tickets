Ticket:  2883
Status:  infoneeded_new
Summary: mutt gets confused when a message disappears from a maildir

Reporter: myon
Owner:    mutt-dev

Opened:       2007-04-24 11:01:35 UTC
Last Updated: 2009-06-30 14:26:00 UTC

Priority:  minor
Component: maildir/mh
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
----- Forwarded message from Mike O'Connor <stew@vireo.org> -----

Date: Wed, 18 Apr 2007 18:30:12 -0400
From: Mike O'Connor <stew@vireo.org>
Reply-To: Mike O'Connor <stew@vireo.org>, 419946@bugs.debian.org
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#419946: mutt gets confused when a message dissapears from a
	maildir

Package: mutt
Version: 1.5.13-3
Severity: normal

I use offlineimap to keep multiple computers in sync with a remote
imap server.  If offline imap deletes a file from a new directory in a
Maildir that mutt is currently viewing, mutt gets an error trying to
move the file from new to cur when running <sync-mailbox>.   The only
recourse is to quit mutt and restart. 

Here are the steps I can use to reproduce this:

Machine A and Machine B run the same offlineimap config and the same
mutt config.

I start offlineimap on both, which re-syncs with the server every 5
minutes.

I start mutt on both, and switch both machines to be viewing some
maildir which is synced by offlineimap which has new messages (which
equates to messages in that maildir's new directory).

On machine A i delete one of the messages which was new, then change
to a different folder (causing the message to actually be deleted).

The next time machine A's offlineimap syncs to the server, it deletes
the message on the server.

The next time machine B's offlineimap syncs to the server, it deletes
the message that machine A delted from the new directory of the folder
that machine B's mutt is viewing.

Then if i try to change folders or run :sync-mailbox on machine B, i
get an error in the status area: 

rename: No such file or directory (errno = 2)

stracing the running process reveals that it is, in fact trying to
rename a no-longer-existant file from the new directory to the cur
directory.

At this point, I can never change folders on Machine B without
quitting mutt and restarting it.

Thanks,

stew
}}}

--------------------------------------------------------------------------------
2007-04-24 22:41:43 UTC brendan
* milestone changed to 1.6
* priority changed to minor
* summary changed to mutt gets confused when a message disappears from a maildir

--------------------------------------------------------------------------------
2007-07-10 17:06:34 UTC lschiere
* Added comment:
I hit this bug when I use Mutt 1.5.13 with a local maildir, and delete a message via IMAP from any other client.  Oddly, this does not seem to happen *every* time this happens though.  I am not sure on the precise cause. 

--------------------------------------------------------------------------------
2007-07-12 11:55:19 UTC cameron
* Added comment:
This happens to me all the time.
I fairly frequently open the same maildir from two mutts.
Delete some messages from one mutt, read some in the other.
If these messages overlap, the second mutt to do a sync can't update
the overlapping messages because marking a message read renames it,
and of course deleting a message removes it.

Either way, mutt complains about being unable to find the file for the
message it wants to sync, saying "rename: No such file or directory (errno = 2)".

At that point I can't even quit from mutt since a quit requires a sync and that still fails,
and always have to kill that mutt.

It would be better if mutt coped better, probably by deciding that that message was as good as
deleted, removing it from the index and also from its list of things to sync.
It'll all be good next time the folder is opened.



--------------------------------------------------------------------------------
2008-05-29 12:25:23 UTC pdmef
* Added comment:
I tried to reproduce this as I can imagine mutt having trouble with disappearing messages. However, I can't. All I get is that the mailbox was externally modified. Can you still reproduce this with the latest version?

--------------------------------------------------------------------------------
2008-06-26 07:21:35 UTC brendan
* Added comment:
No response; cannot reproduce.

* resolution changed to worksforme
* status changed to closed

--------------------------------------------------------------------------------
2009-01-17 06:20:14 UTC stew
* cc changed to stew@vireo.org
* Added comment:
I'm still able to reproduce this bug easily.  The key thing missing from the original report is that mutt must have a reason to want to save the message that has been deleted.  So to reproduce, I go to a folder, open a previously unread message, close the message.  Then delete the file representing the message on disk.  Then when I return to mutt, and try to switch folders,  the error occurs as mutt tries to save the fact that this message has been read to disk.

* resolution changed to 
* status changed to new

--------------------------------------------------------------------------------
2009-01-17 08:33:48 UTC stew
* Added comment:
sorry, I should have mentioned previously that I'm currently reproducing the bug in mutt version  1.5.18-4 from Debian.

--------------------------------------------------------------------------------
2009-01-27 15:05:59 UTC pdmef
* Added comment:
I still cannot reproduce it, mutt removes the message from the index silently before I have a chance to sync. What are your settings for $mail_check and $timeout? Is there caching involved?

* Updated description:

{{{
----- Forwarded message from Mike O'Connor <stew@vireo.org> -----

Date: Wed, 18 Apr 2007 18:30:12 -0400
From: Mike O'Connor <stew@vireo.org>
Reply-To: Mike O'Connor <stew@vireo.org>, 419946@bugs.debian.org
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#419946: mutt gets confused when a message dissapears from a
	maildir

Package: mutt
Version: 1.5.13-3
Severity: normal

I use offlineimap to keep multiple computers in sync with a remote
imap server.  If offline imap deletes a file from a new directory in a
Maildir that mutt is currently viewing, mutt gets an error trying to
move the file from new to cur when running <sync-mailbox>.   The only
recourse is to quit mutt and restart. 

Here are the steps I can use to reproduce this:

Machine A and Machine B run the same offlineimap config and the same
mutt config.

I start offlineimap on both, which re-syncs with the server every 5
minutes.

I start mutt on both, and switch both machines to be viewing some
maildir which is synced by offlineimap which has new messages (which
equates to messages in that maildir's new directory).

On machine A i delete one of the messages which was new, then change
to a different folder (causing the message to actually be deleted).

The next time machine A's offlineimap syncs to the server, it deletes
the message on the server.

The next time machine B's offlineimap syncs to the server, it deletes
the message that machine A delted from the new directory of the folder
that machine B's mutt is viewing.

Then if i try to change folders or run :sync-mailbox on machine B, i
get an error in the status area: 

rename: No such file or directory (errno = 2)

stracing the running process reveals that it is, in fact trying to
rename a no-longer-existant file from the new directory to the cur
directory.

At this point, I can never change folders on Machine B without
quitting mutt and restarting it.

Thanks,

stew
}}}

--------------------------------------------------------------------------------
2009-06-01 04:22:30 UTC brendan
* Added comment:
I cannot reproduce this either. When I return to the index from the pager after having deleted the message (in new/), mutt simply reports that the mailbox has been modified and removes the message from the index. I'm closing this bug again.

* Updated description:

{{{
----- Forwarded message from Mike O'Connor <stew@vireo.org> -----

Date: Wed, 18 Apr 2007 18:30:12 -0400
From: Mike O'Connor <stew@vireo.org>
Reply-To: Mike O'Connor <stew@vireo.org>, 419946@bugs.debian.org
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#419946: mutt gets confused when a message dissapears from a
	maildir

Package: mutt
Version: 1.5.13-3
Severity: normal

I use offlineimap to keep multiple computers in sync with a remote
imap server.  If offline imap deletes a file from a new directory in a
Maildir that mutt is currently viewing, mutt gets an error trying to
move the file from new to cur when running <sync-mailbox>.   The only
recourse is to quit mutt and restart. 

Here are the steps I can use to reproduce this:

Machine A and Machine B run the same offlineimap config and the same
mutt config.

I start offlineimap on both, which re-syncs with the server every 5
minutes.

I start mutt on both, and switch both machines to be viewing some
maildir which is synced by offlineimap which has new messages (which
equates to messages in that maildir's new directory).

On machine A i delete one of the messages which was new, then change
to a different folder (causing the message to actually be deleted).

The next time machine A's offlineimap syncs to the server, it deletes
the message on the server.

The next time machine B's offlineimap syncs to the server, it deletes
the message that machine A delted from the new directory of the folder
that machine B's mutt is viewing.

Then if i try to change folders or run :sync-mailbox on machine B, i
get an error in the status area: 

rename: No such file or directory (errno = 2)

stracing the running process reveals that it is, in fact trying to
rename a no-longer-existant file from the new directory to the cur
directory.

At this point, I can never change folders on Machine B without
quitting mutt and restarting it.

Thanks,

stew
}}}
* resolution changed to worksforme
* status changed to closed

--------------------------------------------------------------------------------
2009-06-03 12:58:57 UTC stew
* Added comment:
I can still reproduce this.  Every time.  It doesn't work with a (N)ew message, only with (O)ld unread messages it seems.

Here are the steps, again.

1) Hit enter on a (O)ld message (a message that was (N)ew the LAST time I was in this maildir)
2) go to ~/Mail/$foldername/cur ; delete the corresponding message file
3) hit q in mutt to go back to the folder index.
4) hit "$".

I then get the message "rename: No such file or directory (errno = 2)" until I quit mutt and restart.  (I cannot change folders or sync).

> What are your settings for $mail_check and $timeout?
both of those have always been commented out in my config.

I currently am reproducing this with 1.5.19.

* resolution changed to 
* status changed to new

--------------------------------------------------------------------------------
2009-06-03 14:01:59 UTC Rocco Rutte
* Added comment:
{{{
Hi,

* Mutt wrote:

I still cannot reproduce it. After 3 I get the externally modified message.

Rocco
}}}

--------------------------------------------------------------------------------
2009-06-10 02:08:36 UTC brendan
* Added comment:
I couldn't reproduce from this recipe either -- I had the same experience as Rocco. Can you try with a newer mutt, eg one of the nightlies?

* Updated description:
{{{
----- Forwarded message from Mike O'Connor <stew@vireo.org> -----

Date: Wed, 18 Apr 2007 18:30:12 -0400
From: Mike O'Connor <stew@vireo.org>
Reply-To: Mike O'Connor <stew@vireo.org>, 419946@bugs.debian.org
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#419946: mutt gets confused when a message dissapears from a
	maildir

Package: mutt
Version: 1.5.13-3
Severity: normal

I use offlineimap to keep multiple computers in sync with a remote
imap server.  If offline imap deletes a file from a new directory in a
Maildir that mutt is currently viewing, mutt gets an error trying to
move the file from new to cur when running <sync-mailbox>.   The only
recourse is to quit mutt and restart. 

Here are the steps I can use to reproduce this:

Machine A and Machine B run the same offlineimap config and the same
mutt config.

I start offlineimap on both, which re-syncs with the server every 5
minutes.

I start mutt on both, and switch both machines to be viewing some
maildir which is synced by offlineimap which has new messages (which
equates to messages in that maildir's new directory).

On machine A i delete one of the messages which was new, then change
to a different folder (causing the message to actually be deleted).

The next time machine A's offlineimap syncs to the server, it deletes
the message on the server.

The next time machine B's offlineimap syncs to the server, it deletes
the message that machine A delted from the new directory of the folder
that machine B's mutt is viewing.

Then if i try to change folders or run :sync-mailbox on machine B, i
get an error in the status area: 

rename: No such file or directory (errno = 2)

stracing the running process reveals that it is, in fact trying to
rename a no-longer-existant file from the new directory to the cur
directory.

At this point, I can never change folders on Machine B without
quitting mutt and restarting it.

Thanks,

stew
}}}
* status changed to infoneeded_new

--------------------------------------------------------------------------------
2009-06-11 17:10:22 UTC brendan
* Added comment:
I tried this with debian mutt 1.5.18-6+b1, and couldn't reproduce with it either. I'm dropping this from the 1.6-blocking path.

* milestone changed to 

--------------------------------------------------------------------------------
2009-06-30 14:26:00 UTC pdmef
* component changed to maildir
