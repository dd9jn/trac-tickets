Ticket:  2664
Status:  new
Summary: Mutt should honnor Mail-Copies-To header

Reporter: vincent@vinc17.org
Owner:    mutt-dev

Opened:       2007-01-05 12:06:18 UTC
Last Updated: 2007-01-05 16:35:39 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
In mailing-lists, some users send their mail with:

  Mail-Copies-To: never

Though it is not standard (but neither is Mail-Followup-To, AFAIK) and it is primarily designed for Usenet (but mailing-lists are very similar), Mutt should support this header, defined in <http://www.newsreaders.com/misc/mail-copies-to.html>.
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2007-01-06 10:35:39 UTC rado
* Added comment:
{{{
A wish rather than a real bug?! (changed)
}}}
