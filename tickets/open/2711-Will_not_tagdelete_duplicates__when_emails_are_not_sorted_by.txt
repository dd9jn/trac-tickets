Ticket:  2711
Status:  reopened
Summary: Will not tag/delete duplicates (~=) when emails are not sorted by thread.

Reporter: schmmd@u.washington.edu
Owner:    mutt-dev

Opened:       2007-01-26 19:44:29 UTC
Last Updated: 2009-05-30 23:20:22 UTC

Priority:  minor
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
If I try to, for example, tag all duplicate emails and I am not viewing my emails in thread mode, "T~=" will not tag all of the duplicate emails.  However, if I sort my emails by thread and then run that command again, all duplicate emails are tagged.

--------------------------------------------------------------------------------
2007-01-27 13:54:36 UTC rado
* Added comment:
{{{
Fixed in 1.5.x versions, upgrade.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-01-27 13:54:37 UTC rado
* Added comment:
{{{
Upgrade to latest 1.5.x version.
}}}

--------------------------------------------------------------------------------
2008-12-04 16:14:45 UTC fabrivelas
* Added comment:
Replying to [comment:2 rado]:
> {{{
> Upgrade to latest 1.5.x version.
> }}}

This does not fix the problem for mutt version 1.5.18 (2008-05-17).

* resolution changed to 
* status changed to reopened

--------------------------------------------------------------------------------
2008-12-06 09:49:33 UTC pdmef
* Added comment:
It seems it's always been documented that ~= works with threading only, at least it mentions $duplicate_threads that points at threading.

Anyway, attached is a ugly patch that has O(n^2) complexity.

A real fix would need to separate the duplicate detection from threading code and hook into every sorting method because re-sorting already is done when the mailbox changes (append, mailbox polling, message removal).

* keywords changed to patch
* type changed to enhancement
* version changed to 1.5.18

--------------------------------------------------------------------------------
2008-12-06 09:50:02 UTC pdmef
* Added attachment fix-2711.diff

--------------------------------------------------------------------------------
2009-05-30 23:20:22 UTC brendan
* Updated description:
If I try to, for example, tag all duplicate emails and I am not viewing my emails in thread mode, "T~=" will not tag all of the duplicate emails.  However, if I sort my emails by thread and then run that command again, all duplicate emails are tagged.
