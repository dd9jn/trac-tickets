Ticket:  1437
Status:  new
Summary: Verify signatures in background

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2003-01-14 05:54:30 UTC
Last Updated: 2009-06-30 14:48:29 UTC

Priority:  trivial
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.0-5 / 2003-01-13
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#176473.
Please Cc all your replies to 176473@bugs.debian.org .]

From: Peter Palfrader <weasel@debian.org>
Subject: Verify signatures in background
Date: 

Hello Marco, hi Thomas,

it would be really neat if mutt could verify PGP Signatures in the
background.

Upon encountering an OpenPGP signed mail mutt should display it right
away, perhaps with »Still processing« in the standard PGP brackets and
launch GnuPG/whatever in the background.

Once the background process finishes the message should be redisplayed
with GnuPG's output that we very well know.

If the user moves to the next message before GnuPG could finish it
should be up to the user what is done by default
 (1) either let the command run (so that updating trustdb can finnish
     eventually). Once the command exited only the latest message should
     be tried to get verified.
 or
 (2) kill the process immediatly.

I favour option (1).

					yours,
					peter
[If you've downloaded my PGP key before please redownload it for I've
 changed cipher preferences: gpg --keyserver pgp.dtype.org --recv 94c09c7f]
-- 
 PGP signed and encrypted  |  .''`.  ** Debian GNU/Linux **
    messages preferred.    | : :' :      The  universal
                           | `. `'      Operating System
 http://www.palfrader.org/ |   `-    http://www.debian.org/



Received: (at submit) by bugs.guug.de; 9 Dec 2001 03:45:13 +0000
From aaronl@vitelus.com Sun Dec 09 04:45:13 2001
Received: from vitelus.com ([64.81.243.207] ident=aaronl)
	by trithemius.gnupg.org with esmtp (Exim 3.12 #1 (Debian))
	id 16Cutt-0006GY-00
	for <submit@bugs.guug.de>; Sun, 09 Dec 2001 04:45:13 +0100
Received: from aaronl by vitelus.com with local (Exim 3.33 #1 (Debian))
	id 16CusS-0001Z9-00
	for <submit@bugs.guug.de>; Sat, 08 Dec 2001 19:43:44 -0800
Date: Sat, 8 Dec 2001 19:43:44 -0800
From: Aaron Lehmann <aaronl@vitelus.com>
To: submit@bugs.guug.de
Subject: Please display pgp information incrementally
Message-ID: <20011208194344.A5918@vitelus.com>
Mime-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline
User-Agent: Mutt/1.3.20i

Package: mutt
Severity: wishlist

I like to use a tool called sigtrace which prints out a key trust path
between myself and the signer of a message. I set it up to run as part
of the pgp verification command in mutt. The problem this causes is
that it takes awhile to run, and in the meantime mutt blocks waiting
for it to complete. I would like to start reading a message while PGP
verification commands are still running. Ideally, a signed message
would start out in the pager as:

[-- PGP output follows (current time: Sat 08 Dec 2001 07:00:25 PM PST) --]
[-- End of PGP output --] 
...body of message...

and output from the PGP/gpg/sigtrace process would be filled in
between the first two lines as it was received on on the pipe,
preventing mutt from blocking as it waits for all of the output.

I don't know how difficult this would be. I suspect that it largely
hinges on the pager's ability to insert text at a point in the middle
of a message.


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2001-12-09 03:43:44 UTC Aaron Lehmann <aaronl@vitelus.com>
* Added comment:
{{{
Package: mutt
Severity: wishlist

I like to use a tool called sigtrace which prints out a key trust path
between myself and the signer of a message. I set it up to run as part
of the pgp verification command in mutt. The problem this causes is
that it takes awhile to run, and in the meantime mutt blocks waiting
for it to complete. I would like to start reading a message while PGP
verification commands are still running. Ideally, a signed message
would start out in the pager as:

[-- PGP output follows (current time: Sat 08 Dec 2001 07:00:25 PM PST) --]
[-- End of PGP output --] 
...body of message...

and output from the PGP/gpg/sigtrace process would be filled in
between the first two lines as it was received on on the pipe,
preventing mutt from blocking as it waits for all of the output.

I don't know how difficult this would be. I suspect that it largely
hinges on the pager's ability to insert text at a point in the middle
of a message.
}}}

--------------------------------------------------------------------------------
2001-12-09 03:50:04 UTC Aaron Lehmann <aaronl@vitelus.com>
* Added comment:
{{{
On Sat, Dec 08, 2001 at 07:43:44PM -0800, Aaron Lehmann wrote:
> I would like to start reading a message while PGP
> verification commands are still running.

I should point out that this makes sense even when not running some
wacky script. gpg itself takes a long time to verify signatures,
especially if you have a large keyring or instruct it to download
unknown keys from keyservers. I have a relatively small keyring, and
for certain keys GPG can take several seconds to verify a signature.
Mutt currently hangs while waiting for it to do so.
}}}

--------------------------------------------------------------------------------
2003-01-14 05:54:30 UTC Marco d'Itri <md@linux.it>
* Added comment:
{{{
Package: mutt
Version: 1.4.0-5 / 2003-01-13
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#176473.
Please Cc all your replies to 176473@bugs.debian.org .]

From: Peter Palfrader <weasel@debian.org>
Subject: Verify signatures in background
Date: 

Hello Marco, hi Thomas,

it would be really neat if mutt could verify PGP Signatures in the
background.

Upon encountering an OpenPGP signed mail mutt should display it right
away, perhaps with »Still processing« in the standard PGP brackets and
launch GnuPG/whatever in the background.

Once the background process finishes the message should be redisplayed
with GnuPG's output that we very well know.

If the user moves to the next message before GnuPG could finish it
should be up to the user what is done by default
(1) either let the command run (so that updating trustdb can finnish
    eventually). Once the command exited only the latest message should
    be tried to get verified.
or
(2) kill the process immediatly.

I favour option (1).

					yours,
					peter
[If you've downloaded my PGP key before please redownload it for I've
changed cipher preferences: gpg --keyserver pgp.dtype.org --recv 94c09c7f]
-- 
PGP signed and encrypted  |  .''`.  ** Debian GNU/Linux **
   messages preferred.    | : :' :      The  universal
                          | `. `'      Operating System
http://www.palfrader.org/ |   `-    http://www.debian.org/
}}}

--------------------------------------------------------------------------------
2003-10-08 04:55:06 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# solved by Lars Hecking
close 676
# no infos, no followups
close 1092
# reporter requested closing
close 1307
# unreproducible, no followups
close 1499
merge 907 1437
merge 1083 1176
severity 1562 wishlist
merge 1385 1562
retitle 1128 new subject breaks thread option
tags 64 patch
tags 123 patch
tags 1484 patch
tags 1489 patch
-- someone speaking Spanish should talk to #1572 reporter
}}}

--------------------------------------------------------------------------------
2003-10-08 04:55:07 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# solved by Lars Hecking
close 676
# no infos, no followups
close 1092
# reporter requested closing
close 1307
# unreproducible, no followups
close 1499
merge 907 1437
merge 1083 1176
severity 1562 wishlist
merge 1385 1562
retitle 1128 new subject breaks thread option
tags 64 patch
tags 123 patch
tags 1484 patch
tags 1489 patch
-- someone speaking Spanish should talk to #1572 reporter
}}}

--------------------------------------------------------------------------------
2005-09-04 21:20:58 UTC brendan
* Added comment:
{{{
change-request
}}}

--------------------------------------------------------------------------------
2009-06-30 14:48:29 UTC pdmef
* component changed to crypto
* Updated description:
{{{
Package: mutt
Version: 1.4.0-5 / 2003-01-13
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#176473.
Please Cc all your replies to 176473@bugs.debian.org .]

From: Peter Palfrader <weasel@debian.org>
Subject: Verify signatures in background
Date: 

Hello Marco, hi Thomas,

it would be really neat if mutt could verify PGP Signatures in the
background.

Upon encountering an OpenPGP signed mail mutt should display it right
away, perhaps with »Still processing« in the standard PGP brackets and
launch GnuPG/whatever in the background.

Once the background process finishes the message should be redisplayed
with GnuPG's output that we very well know.

If the user moves to the next message before GnuPG could finish it
should be up to the user what is done by default
 (1) either let the command run (so that updating trustdb can finnish
     eventually). Once the command exited only the latest message should
     be tried to get verified.
 or
 (2) kill the process immediatly.

I favour option (1).

					yours,
					peter
[If you've downloaded my PGP key before please redownload it for I've
 changed cipher preferences: gpg --keyserver pgp.dtype.org --recv 94c09c7f]
-- 
 PGP signed and encrypted  |  .''`.  ** Debian GNU/Linux **
    messages preferred.    | : :' :      The  universal
                           | `. `'      Operating System
 http://www.palfrader.org/ |   `-    http://www.debian.org/



Received: (at submit) by bugs.guug.de; 9 Dec 2001 03:45:13 +0000
From aaronl@vitelus.com Sun Dec 09 04:45:13 2001
Received: from vitelus.com ([64.81.243.207] ident=aaronl)
	by trithemius.gnupg.org with esmtp (Exim 3.12 #1 (Debian))
	id 16Cutt-0006GY-00
	for <submit@bugs.guug.de>; Sun, 09 Dec 2001 04:45:13 +0100
Received: from aaronl by vitelus.com with local (Exim 3.33 #1 (Debian))
	id 16CusS-0001Z9-00
	for <submit@bugs.guug.de>; Sat, 08 Dec 2001 19:43:44 -0800
Date: Sat, 8 Dec 2001 19:43:44 -0800
From: Aaron Lehmann <aaronl@vitelus.com>
To: submit@bugs.guug.de
Subject: Please display pgp information incrementally
Message-ID: <20011208194344.A5918@vitelus.com>
Mime-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline
User-Agent: Mutt/1.3.20i

Package: mutt
Severity: wishlist

I like to use a tool called sigtrace which prints out a key trust path
between myself and the signer of a message. I set it up to run as part
of the pgp verification command in mutt. The problem this causes is
that it takes awhile to run, and in the meantime mutt blocks waiting
for it to complete. I would like to start reading a message while PGP
verification commands are still running. Ideally, a signed message
would start out in the pager as:

[-- PGP output follows (current time: Sat 08 Dec 2001 07:00:25 PM PST) --]
[-- End of PGP output --] 
...body of message...

and output from the PGP/gpg/sigtrace process would be filled in
between the first two lines as it was received on on the pipe,
preventing mutt from blocking as it waits for all of the output.

I don't know how difficult this would be. I suspect that it largely
hinges on the pager's ability to insert text at a point in the middle
of a message.


>How-To-Repeat:
>Fix:
}}}
