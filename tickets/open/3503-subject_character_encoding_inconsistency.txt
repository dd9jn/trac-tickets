Ticket:  3503
Status:  infoneeded_new
Summary: subject character encoding inconsistency

Reporter: daniell
Owner:    mutt-dev

Opened:       2011-02-17 14:57:14 UTC
Last Updated: 2011-06-20 23:40:30 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When displaying a message with isolatin characters in it, in the index list it contains question marks, but in the pager, it is displayed alright:

Index:[[BR]]
Fwd: [suncustomers_hu] Febru?ri h?rlev?l[[BR]]


Pager:[[BR]]
Fwd: [suncustomers_hu] Februári hírlevél

My .muttrc's relevant lines:
{{{
set assumed_charset='us-ascii:iso-8859-1:iso-8859-2:utf-8'
set locale='hu_HU.ISO8859-2'
set send_charset='us-ascii:iso-8859-1:iso-8859-2:utf-8'
}}}

--------------------------------------------------------------------------------
2011-06-20 23:40:30 UTC me
* Added comment:
can you attach a sample message that demostrates the problem?

* status changed to infoneeded_new

--------------------------------------------------------------------------------
2011-06-21 11:07:39 UTC daniell
* Added attachment sample.mail
