Ticket:  3497
Status:  new
Summary: Mutt crashes when terminal window becomes too small

Reporter: tohojo
Owner:    mutt-dev

Opened:       2011-01-20 17:34:57 UTC
Last Updated: 2011-01-20 17:34:57 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When the terminal window containing mutt becomes too small (i.e. is resized), mutt crashed with a segfault. For the main window, this occurs when the terminal is less that two characters high, when a message is open it occurs below five lines and when composing an email (i.e. after having closed the editor, but before sending the email), it occurs below ~8 lines.

Especially the last crash is critical, as it leads to loss of data (the newly written email). It is also the only one that occurs for me in normal use. This happens because I am using a tiling window manager with a small terminal below mutt; when viewing an attachment (such as when confirming that the right file is attached), mutt is then temporarily demoted to be in the small window.

While I realise mutt is not very useful in a terminal that is less than eight characters high, I would expect it to survive without segfaulting; and either suspend drawing of the screen until the terminal becomes large enough, or at least exit gracefully without data loss.
