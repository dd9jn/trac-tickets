Ticket:  2141
Status:  assigned
Summary: wishlist: possible storing of attachments as separate files

Reporter: ildar@users.sourceforge.net
Owner:    mutt-dev

Opened:       2005-11-22 10:23:25 UTC
Last Updated: 2008-05-29 08:46:39 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
As a mail agent called The Bat!, I'd like mutt to have an option to store [some] attachments not in a mailbox, but in a directory as separate files. So that, mailboxes could shrink considerably, having only text and [several] references to other contents. The programm sould be tuned on what types of attachments to leave in mailbox and what types to extract.
If anyone could propose a patch as a 3rd party, this would be fine too.
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-11-23 04:27:11 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
You are free to save and then delete individual attachments from
mailboxes using mutt's receive-attachment menu.
-- 
Thomas Roessler			      <roessler@does-not-exist.org>






On 2005-11-22 11:23:25 +0100, ildar@users.sourceforge.net wrote:
> From: ildar@users.sourceforge.net
> To: Mutt Developers <mutt-dev@mutt.org>
> Date: Tue, 22 Nov 2005 11:23:25 +0100
> Subject: mutt/2141: wishlist: possible storing of attachments as separate files
> Reply-To: bug-any@bugs.mutt.org
> X-Spam-Level: 
> 
> >Number:         2141
> >Notify-List:    
> >Category:       mutt
> >Synopsis:       wishlist: possible storing of attachments as separate files
> >Confidential:   no
> >Severity:       normal
> >Priority:       medium
> >Responsible:    mutt-dev
> >State:          open
> >Keywords:       
> >Class:          sw-bug
> >Submitter-Id:   net
> >Arrival-Date:   Tue Nov 22 11:23:25 +0100 2005
> >Originator:     ildar@users.sourceforge.net
> >Release:        
> >Organization:
> >Environment:
> >Description:
> As a mail agent called The Bat!, I'd like mutt to have an option to store [some] attachments not in a mailbox, but in a directory as separate files. So that, mailboxes could shrink considerably, having only text and [several] references to other contents. The programm sould be tuned on what types of attachments to leave in mailbox and what types to extract.
> If anyone could propose a patch as a 3rd party, this would be fine too.
> >How-To-Repeat:
> >Fix:
> Unknown
> >Add-To-Audit-Trail:
> 
> >Unformatted:
> 
>
}}}

--------------------------------------------------------------------------------
2005-11-23 05:13:21 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Tue, Nov 22, 2005 at 11:23:25AM +0100, ildar@users.sourceforge.net wrote:

> >Severity:       normal
> >Priority:       medium

I don't have rights to change GNATS; could someone retag this with an
appropriate severity and priority?

-- 
Paul

        Now, now, my good man, this is no time for making enemies.
                               -- Voltaire, on his deathbed, in response to
                                     a priest asking that he renounce Satan
}}}

--------------------------------------------------------------------------------
2005-11-23 07:25:23 UTC tamo
* Added comment:
{{{
You are free to save and then delete individual attachments from
 On Tue, Nov 22, 2005 at 11:23:25AM +0100, ildar@users.sourceforge.net wrote:
Thomas kindly told him to use received-attachment menu ('v' key).
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-11-23 07:25:24 UTC tamo
* Added comment:
{{{
normal/medium -> minor/low

open -> feedback
}}}

--------------------------------------------------------------------------------
2008-05-29 08:46:39 UTC pdmef
* priority changed to minor
* type changed to enhancement
