Ticket:  3468
Status:  new
Summary: Unable to open mailbox sent in append-mode

Reporter: dkramer
Owner:    mutt-dev

Opened:       2010-11-04 14:35:32 UTC
Last Updated: 2010-11-04 14:35:32 UTC

Priority:  minor
Component: maildir/mh
Keywords:  

--------------------------------------------------------------------------------
Description:
I have an automated program that sends emails out automatically from our MRP system using the mutt command line. Occasionally we were getting Exit Code 1 and couldn't figure out why it was aborting. I downloaded 1.5.21 and compiled it with the debug option. I'm not sure why this is happening as the sent file exists. I have noticed that there is a sent.lock file in the sub directory with the sent folder, perhaps it isn't releasing the file lock?

I'm stumped and critical emails aren't going out . . . help! 

[2010-11-04 08:41:10] Mutt/1.5.21 (2010-09-15) debugging at level 5
[2010-11-04 08:41:10] Reading configuration file '/usr/local/etc/Muttrc'.
[2010-11-04 08:41:10] parse_attach_list: ldata = 0x80cdf64, *ldata = (nil)
[2010-11-04 08:41:10] parse_attach_list: added */.* [9]
[2010-11-04 08:41:10] parse_attach_list: ldata = 0x80cdf60, *ldata = (nil)
[2010-11-04 08:41:10] parse_attach_list: added text/x-vcard [7]
[2010-11-04 08:41:10] parse_attach_list: added application/pgp.* [2]
[2010-11-04 08:41:10] parse_attach_list: ldata = 0x80cdf60, *ldata = 0x87e30b8
[2010-11-04 08:41:10] parse_attach_list: skipping text/x-vcard
[2010-11-04 08:41:10] parse_attach_list: skipping application/pgp.*
[2010-11-04 08:41:10] parse_attach_list: added application/x-pkcs7-.* [2]
[2010-11-04 08:41:10] parse_attach_list: ldata = 0x80cdf5c, *ldata = (nil)
[2010-11-04 08:41:10] parse_attach_list: added text/plain [7]
[2010-11-04 08:41:10] parse_attach_list: ldata = 0x80cdf60, *ldata = 0x87e30b8
[2010-11-04 08:41:10] parse_attach_list: skipping text/x-vcard
[2010-11-04 08:41:10] parse_attach_list: skipping application/pgp.*
[2010-11-04 08:41:10] parse_attach_list: skipping application/x-pkcs7-.*
[2010-11-04 08:41:10] parse_attach_list: added message/external-body [4]
[2010-11-04 08:41:10] parse_attach_list: ldata = 0x80cdf58, *ldata = (nil)
[2010-11-04 08:41:10] parse_attach_list: added message/external-body [4]
[2010-11-04 08:41:10] Reading configuration file '/tmp/do_not_reply_muttrc'.
[2010-11-04 08:41:10] send.c:1214: mutt_mktemp returns "/tmp/mutt-d3-0-27625-655840911616609679".
''[2010-11-04 08:41:15] mutt_write_fcc(): unable to open mailbox /home/rbiggs/sent in append-mode, aborting.
''[2010-11-04 08:41:15] mutt_free_body: unlinking /tmp/mutt-d3-0-27625-655840911616609679.

