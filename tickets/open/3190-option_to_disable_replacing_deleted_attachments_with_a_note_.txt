Ticket:  3190
Status:  new
Summary: option to disable replacing deleted attachments with a note about their deletion

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-02-19 18:53:52 UTC
Last Updated: 2009-02-19 18:53:52 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Forwarding from: http://bugs.debian.org/516189

---
If I delete an attachment with mutt, it replaces it with
a pseudo-attachment, such as:

  Content-Type: message/external-body; access-type=x-mutt-deleted;
    expiration="Thu, 19 Feb 2009 19:09:30 +0100"; length=2077

  Content-Type: text/html; charset="utf-8"
  Content-Transfer-Encoding: quoted-printable

This makes sense in many cases, but some times this is not wanted.
It would be nice if mutt had an option that would control the
behaviour.

}}}
