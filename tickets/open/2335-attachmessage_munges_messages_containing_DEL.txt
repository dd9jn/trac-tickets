Ticket:  2335
Status:  new
Summary: <attach-message> munges messages containing DEL

Reporter: Alain Bench <veronatif@free.fr>
Owner:    mutt-dev

Opened:       2006-07-07 10:57:24 UTC
Last Updated: 2006-07-19 22:05:01 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Hi ALL.

    Compose:<attach-message> function extracts a message from a folder,
and attaches it as message/rfc822. The message is sent as-is. Verbatim
body, and nearly verbatim header, except removal of local only scope
lines as From_, (X-)Status:, Content-Length:, and Lines:.

    The attached mbox contains one message, a table of bytes from 20 to
FF, text/plain 8bit CP-1252. When I <attach-message> it, Mutt modifies
the message:

 -1) transcodes 8bit ==> QP
 -2) adds spurious field:

| Content-Disposition: inline; filename=mutt-$HOSTNAME-$UID-$PID-8

    The trigger seems to be the presence of control char 7F (DEL).
Effect (1) is unexpected, breaks the "verbatim" goal, and hurts the
whole purpose of sending such tables. But perhaps has some reason.
Effect (2) discloses private infos: Bad.


Bye!    Alain.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2006-07-18 22:06:18 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2006-07-07 11:57:24 +0200, Alain Bench wrote:

>     The attached mbox contains one message, a table of bytes
> from 20 to FF, text/plain 8bit CP-1252. When I
> <attach-message> it, Mutt modifies the message:
> 
>  -1) transcodes 8bit ==> QP

This transcoding is going to happen whenever mutt has to
send an 8bit message through a 7bit channel.

Blame the inventors of 8BITMIME.  Mutt is simply doing the best
it can under the circumstances.

-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2006-07-20 12:58:41 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hi Thomas!

 On Tuesday, July 18, 2006 at 23:05:01 +0200, Thomas Roessler wrote:

> This [8bit => QP] transcoding is going to happen whenever mutt has to
> send an 8bit message through a 7bit channel.

    Well no: Mutt by default can send all 80-FF hi-bit chars as 8bit,
with $allow_8bit=yes.

    An MTA might transcode later. But it will maybe QPify a simple mail
body, never a multipart part (unless it's a bad MTA). And never touch
inside an encapsulated attachment, as in my case. Anyway the transcoding
is already visible in my sent box, before any MTA. It's also visible
before sending: In compose menu, when I select the "[message/rfc822,
7bit, 1,0K]" attachment, and <view-attach> it, I can see:

| Content-Disposition: inline; filename=mutt-Ummon-1000-3152-7
| Content-Transfer-Encoding: quoted-printable


    If I remove the DEL 7F, then <attach-message> the same hi-bit table
and send: No QPification. And no spurious CDisp header added.


Bye!	Alain.
-- 
How to Report Bugs Effectively
<URL:http://www.chiark.greenend.org.uk/~sgtatham/bugs.html>
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:30 UTC 
* Added attachment fulltable-CP-1252.mbox.gz
* Added comment:
fulltable-CP-1252.mbox.gz
