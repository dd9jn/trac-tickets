Ticket:  2333
Status:  new
Summary: wish: display multiple ?variables at once

Reporter: Alain Bench <veronatif@free.fr>
Owner:    mutt-dev

Opened:       2006-07-07 00:05:28 UTC
Last Updated: 2007-04-07 14:21:25 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
    Hi ALL!

    I'd wish Mutt "set" command to be able to display the
value of several variables at once. Like example:

| :set ?realname ?from
| realname="John Doe" from="johnny@example.com"

    Usefull as poor-man's echo, as last statement in an
"identity" macro, and such. The following should also be
possible:

| :set locale=old-value
| [...]
| :set ?locale locale=new-value ?locale
| locale="old-value" locale="new-value"

    Usefull for tracking changes.


Bye!    Alain.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2006-07-08 14:17:55 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
* Fri Jul  7 2006 Alain Bench <veronatif@free.fr>
> | :set ?locale locale=new-value ?locale
> | locale="old-value" locale="new-value"
> 
>     Usefull for tracking changes.

See:
http://www10.plala.or.jp/sanrinsha/tamo/multiquery2.diff
or
http://www10.plala.or.jp/sanrinsha/tamo/multiquery.diff

-- 
tamo
}}}

--------------------------------------------------------------------------------
2006-07-09 05:33:19 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hi Tamotsu-san!

 On Friday, July 7, 2006 at 8:05:01 +0200, Tamotsu Takahashi wrote:

> * Fri Jul  7 2006 Alain Bench <veronatif@free.fr>
>>| :set ?locale locale=new-value ?locale
>>| locale="old-value" locale="new-value"
> http://www10.plala.or.jp/sanrinsha/tamo/multiquery2.diff

    Much thanks! I tried only v2: It works as expected. However there is
a small regression with 2 set commands in a row:

| :set ?locale ; set locale=fr_FR.euc-jisx0213

    ...does display, but wipes immediatly. While without multiquery2:

| :set ?locale ; set locale=fr_FR.euc-jisx0213
| locale="C"

    Specifically my <F142> display identity macro example doesn't work
anymore, and may become difficult to write in a portable way. And why
not push the thing further? What about:

| :set ?realname ; set ?from
| realname="John Doe"; from="johnny@example.com"

    ...to behave exactly as "set ?realname ?from" single command? After
all the user entering two echos in one command line logicaly expects
that. Only errors should overwrite:

| :set ?realname ; set ?typo
| typo: unknown variable


    Unrelated BTW: I just noticed that all Mutts 1.4/1.5 truncate the
display 2 columns before right side (display 78 characters on 80). I
know some terminals may misbahave on the bottom right (not mine), thus
keep 1 free column is safe. But why 2?

| :set from=789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.
| from="789.123456789.123456789.123456789.123456789.123456789.123456789.12345678


Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Folding@home project at <URL:http://folding.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2006-07-10 15:02:54 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
* Sat Jul  8 2006 Alain Bench <veronatif@free.fr>
> 
>  Hi Tamotsu-san!

Hi Alain,
Thanks for testing.


>   On Friday, July 7, 2006 at 8:05:01 +0200, Tamotsu Takahashi wrote:
>  
>  > * Fri Jul  7 2006 Alain Bench <veronatif@free.fr>
>  >>| :set ?locale locale=new-value ?locale
>  >>| locale="old-value" locale="new-value"
>  > http://www10.plala.or.jp/sanrinsha/tamo/multiquery2.diff
>  
>      Much thanks! I tried only v2: It works as expected. However there is
>  a small regression with 2 set commands in a row:
>  
>  | :set ?locale ; set locale=fr_FR.euc-jisx0213
>  
>      ...does display, but wipes immediatly. While without multiquery2:
>  
>  | :set ?locale ; set locale=fr_FR.euc-jisx0213
>  | locale="C"

Oops, I left an unnecessary line: "*err->data = '\0';" at
the beginning of parse_set.


>      Specifically my <F142> display identity macro example doesn't work
>  anymore, and may become difficult to write in a portable way. And why
>  not push the thing further? What about:
>  
>  | :set ?realname ; set ?from
>  | realname="John Doe"; from="johnny@example.com"
>  
>      ...to behave exactly as "set ?realname ?from" single command? After
>  all the user entering two echos in one command line logicaly expects
>  that. Only errors should overwrite:
>  
>  | :set ?realname ; set ?typo
>  | typo: unknown variable

Hmm, you can replace "size_t errlen = 0;" with
"size_t errlen = mutt_strlen (err->data);"

But I am not sure whether this is correct in any case.


>      Unrelated BTW: I just noticed that all Mutts 1.4/1.5 truncate the
>  display 2 columns before right side (display 78 characters on 80). I
>  know some terminals may misbahave on the bottom right (not mine), thus
>  keep 1 free column is safe. But why 2?
>  
>  | :set from=789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.
>  | from="789.123456789.123456789.123456789.123456789.123456789.123456789.12345678

I don't know why. But the related code is mutt_curses_message
(at the line 316 in curs_lib.c):
|  mutt_format_string (Errorbuf, sizeof (Errorbuf),
|                      0, COLS-2, 0, 0, Errorbuf, sizeof (Errorbuf), 0);
                          ^^^^^^

-- 
tamo
}}}

--------------------------------------------------------------------------------
2006-07-14 14:02:42 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Sunday, July 9, 2006 at 8:25:01 +0200, Tamotsu Takahashi wrote:

    [tamo/multiquery2.diff]
> unnecessary line: "*err->data = '\0';" at the beginning of parse_set.
> [...] replace "size_t errlen = 0;" with "size_t errlen = mutt_strlen
> (err->data);"

    Done both, and all seems well so far. I continue stress testing.


    Problem with long line truncation: Whatever screen size, the string
is truncated at 127 bytes, and displayed as an error, in red with a
beep. Example:

| :color error brightred default
| :set beep
| :set from=789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.
| :set ?from
| from="789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.1234567

    Perhaps would be preferable:

 -1) More than 127 bytes. Enough to avoid truncation on the widest
terminals.

 -2) Silent no error truncation, as without multiquery2.diff. Lack of
closing double quotes is enough visual hint. And COLS-2 truncation of
strings on 80 columns terminal is already silent. I mean user cares
about truncated display; not if it comes from columns or bytes.
Furthermore this truncation error prevents later commands to be
executed, and eventual later real errors to be reported:

| :set ?from from=123 typo=456

    This shouts the same "from=...long..." error, and does not change
$from. It should better change $from and shout error:

| typo: unknown variable


Bye!	Alain.
-- 
How to Report Bugs Effectively
<URL:http://www.chiark.greenend.org.uk/~sgtatham/bugs.html>
}}}

--------------------------------------------------------------------------------
2006-07-15 15:15:05 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
* Thu Jul 13 2006 Alain Bench <veronatif@free.fr>
>   On Sunday, July 9, 2006 at 8:25:01 +0200, Tamotsu Takahashi wrote:
>  
>      [tamo/multiquery2.diff]
>  > unnecessary line: "*err->data = '\0';" at the beginning of parse_set.
>  > [...] replace "size_t errlen = 0;" with "size_t errlen = mutt_strlen
>  > (err->data);"
>  
>      Done both, and all seems well so far. I continue stress testing.

Glad to hear that. Thanks a lot.


>      Perhaps would be preferable:
>  
>   -1) More than 127 bytes. Enough to avoid truncation on the widest
>  terminals.

Easy:

|--- commands.c~	Tue Jun 20 17:59:45 2006
|+++ commands.c	Fri Jul 14 14:56:35 2006
|@@ -583,7 +583,7 @@ void mutt_shell_escape (void)
| void mutt_enter_command (void)
| {
|   BUFFER err, token;
|-  char buffer[LONG_STRING], errbuf[SHORT_STRING];
|+  char buffer[LONG_STRING], errbuf[LONG_STRING];
|   int r;
| 
|   buffer[0] = 0;

Or, I can do it by making the functions use _real_ BUFFERs.
Currently the relevant functions don't allow realloc.


>   -2) Silent no error truncation, as without multiquery2.diff. Lack of
>  closing double quotes is enough visual hint. And COLS-2 truncation of
>  strings on 80 columns terminal is already silent. I mean user cares
>  about truncated display; not if it comes from columns or bytes.
>  Furthermore this truncation error prevents later commands to be
>  executed, and eventual later real errors to be reported:
>  
>  | :set ?from from=123 typo=456
>  
>      This shouts the same "from=...long..." error, and does not change
>  $from. It should better change $from and shout error:
>  
>  | typo: unknown variable

Sounds reasonable.
Try the attached patch, please.

-- 
tamo

--mvpLiMfbWzRoNl4x
Content-Type: text/plain; charset=us-ascii
Content-Disposition: attachment; filename="multiquery3.diff"

diff -r 26bdaa1790b3 init.c
--- a/init.c	Fri Jul 07 11:35:46 2006 +0900
+++ b/init.c	Fri Jul 07 14:04:05 2006 +0900
@@ -57,6 +57,19 @@
 	    _("Not available in this menu.")); \
 	  return (-1); \
 	} else
+
+#define APPEND_VALUE(prf) \
+  if (errlen) \
+  { \
+    if (errlen >= err->dsize) \
+      continue; /* silent truncation */ \
+    strfcpy (err->data + errlen, _("; "), err->dsize - errlen); \
+    errlen += strlen (_("; ")); \
+    if (errlen >= err->dsize) \
+      continue; /* silent truncation */ \
+  } \
+  errlen += prf; \
+  continue
 
 typedef struct myvar
 {
@@ -1788,10 +1803,11 @@ static void mutt_restore_default (struct
 
 static int parse_set (BUFFER *tmp, BUFFER *s, unsigned long data, BUFFER *err)
 {
   int query, unset, inv, reset, r = 0;
   int idx = -1;
   char *p, scratch[_POSIX_PATH_MAX];
   char* myvar;
+  size_t errlen = mutt_strlen (err->data);
 
   while (MoreArgs (s))
   {
@@ -1901,9 +1919,8 @@ static int parse_set (BUFFER *tmp, BUFFE
 
       if (query)
       {
-	snprintf (err->data, err->dsize, option (MuttVars[idx].data)
-			? _("%s is set") : _("%s is unset"), tmp->data);
-	return 0;
+	APPEND_VALUE (snprintf (err->data + errlen, err->dsize - errlen, option (MuttVars[idx].data)
+			? _("%s is set") : _("%s is unset"), tmp->data));
       }
 
       CHECK_PAGER;
@@ -1939,8 +1956,7 @@ static int parse_set (BUFFER *tmp, BUFFE
         {
           if ((val = myvar_get (myvar)))
           {
-            snprintf (err->data, err->dsize, "%s=\"%s\"", myvar, val);
-            break;
+            APPEND_VALUE (snprintf (err->data + errlen, err->dsize - errlen, "%s=\"%s\"", myvar, val));
           }
           else
           {
@@ -1948,19 +1964,21 @@ static int parse_set (BUFFER *tmp, BUFFE
             return (-1);
           }
         }
-	else if (DTYPE (MuttVars[idx].type) == DT_ADDR)
+	else
 	{
-	  _tmp[0] = '\0';
-	  rfc822_write_address (_tmp, sizeof (_tmp), *((ADDRESS **) MuttVars[idx].data), 0);
-	  val = _tmp;
+	  if (DTYPE (MuttVars[idx].type) == DT_ADDR)
+	  {
+	    _tmp[0] = '\0';
+	    rfc822_write_address (_tmp, sizeof (_tmp), *((ADDRESS **) MuttVars[idx].data), 0);
+	    val = _tmp;
+	  }
+	  else
+	    val = *((char **) MuttVars[idx].data);
+
+	  /* user requested the value of this variable */
+	  APPEND_VALUE (snprintf (err->data + errlen, err->dsize - errlen, "%s=\"%s\"",
+			MuttVars[idx].option, NONULL (val)));
 	}
-	else
-	  val = *((char **) MuttVars[idx].data);
-	
-	/* user requested the value of this variable */
-	snprintf (err->data, err->dsize, "%s=\"%s\"", MuttVars[idx].option,
-		  NONULL (val));
-	break;
       }
       else
       {
@@ -2019,9 +2037,8 @@ static int parse_set (BUFFER *tmp, BUFFE
       if (query || *s->dptr != '=')
       {
 	/* user requested the value of this variable */
-	snprintf (err->data, err->dsize, "%s=\"%s\"", MuttVars[idx].option,
-		  NONULL (ptr->pattern));
-	break;
+	APPEND_VALUE (snprintf (err->data + errlen, err->dsize - errlen, "%s=\"%s\"",
+		      MuttVars[idx].option, NONULL (ptr->pattern)));
       }
 
       if (option(OPTATTACHMSG) && !mutt_strcmp(MuttVars[idx].option, "reply_regexp"))
@@ -2121,8 +2138,8 @@ static int parse_set (BUFFER *tmp, BUFFE
 	    p = "unknown";
 	    break;
 	}
-	snprintf (err->data, err->dsize, "%s=%s", MuttVars[idx].option, p);
-	break;
+	APPEND_VALUE (snprintf (err->data + errlen, err->dsize - errlen, "%s=%s",
+		      MuttVars[idx].option, p));
       }
 
       CHECK_PAGER;
@@ -2146,8 +2163,8 @@ static int parse_set (BUFFER *tmp, BUFFE
       if (query || *s->dptr != '=')
       {
 	/* user requested the value of this variable */
-	snprintf (err->data, err->dsize, "%s=%d", MuttVars[idx].option, *ptr);
-	break;
+	APPEND_VALUE (snprintf (err->data + errlen, err->dsize - errlen, "%s=%d",
+		      MuttVars[idx].option, *ptr));
       }
 
       CHECK_PAGER;
@@ -2184,9 +2201,8 @@ static int parse_set (BUFFER *tmp, BUFFE
       {
 	char *vals[] = { "no", "yes", "ask-no", "ask-yes" };
 
-	snprintf (err->data, err->dsize, "%s=%s", MuttVars[idx].option,
-		  vals [ quadoption (MuttVars[idx].data) ]);
-	break;
+	APPEND_VALUE (snprintf (err->data + errlen, err->dsize - errlen, "%s=%s",
+		MuttVars[idx].option, vals [ quadoption (MuttVars[idx].data) ]));
       }
 
       CHECK_PAGER;
@@ -2254,11 +2270,11 @@ static int parse_set (BUFFER *tmp, BUFFE
       {
 	p = mutt_getnamebyvalue (*((short *) MuttVars[idx].data) & SORT_MASK, map);
 
-	snprintf (err->data, err->dsize, "%s=%s%s%s", MuttVars[idx].option,
+	APPEND_VALUE (snprintf (err->data + errlen, err->dsize - errlen, "%s=%s%s%s",
+		  MuttVars[idx].option,
 		  (*((short *) MuttVars[idx].data) & SORT_REVERSE) ? "reverse-" : "",
 		  (*((short *) MuttVars[idx].data) & SORT_LAST) ? "last-" : "",
-		  p);
-	return 0;
+		  p));
       }
       CHECK_PAGER;
       s->dptr++;

--mvpLiMfbWzRoNl4x--
}}}

--------------------------------------------------------------------------------
2006-07-20 16:06:04 UTC ab
* Added comment:
{{{
* Fri Jul  7 2006 Alain Bench <veronatif@free.fr>
 Hi Tamotsu-san!
 * Sat Jul  8 2006 Alain Bench <veronatif@free.fr>
  On Sunday, July 9, 2006 at 8:25:01 +0200, Tamotsu Takahashi wrote:
 
     Thank you very much, Tamo! multiquery3.diff together
with LONG_STRING seems good to me. Briliantly resisted huge
amount of stress tests, and always did the right thing. I
add the /patch/ tag. AB.
}}}
