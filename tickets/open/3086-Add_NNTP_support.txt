Ticket:  3086
Status:  new
Summary: Add NNTP support

Reporter: llucax
Owner:    mutt-dev

Opened:       2008-07-01 19:14:53 UTC
Last Updated: 2008-07-01 20:48:34 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
All newsreaders sucks, mutt could just suck less if it had NNTP support =)

Here is a widely used patch for it:
http://www.fiction.net/blong/programs/mutt/#nntp

It would be great if you merge it to upstream.

Thank you!

--------------------------------------------------------------------------------
2008-07-01 19:40:20 UTC brendan
* Added comment:
That patch is ancient. I think the one people use these days is here: http://mutt.org.ua/download/.

--------------------------------------------------------------------------------
2008-07-01 20:48:34 UTC llucax
* Added comment:
Yes, wrong patch, sorry about that!

Thanks for the correction.
