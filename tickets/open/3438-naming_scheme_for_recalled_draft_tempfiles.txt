Ticket:  3438
Status:  new
Summary: naming scheme for recalled draft tempfiles

Reporter: balderdash
Owner:    brendan

Opened:       2010-08-06 16:18:12 UTC
Last Updated: 2011-11-21 17:38:22 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
This is about mutt's interaction with Vim, but if anything should be changed in response to this, it's on mutt's end.  Vim automatically detects filenames that start with 'mutt' and end with any six characters that are either alphanums or - or  _,  and declares their filetype to be "mail".  I am guessing that this is because at least on IMAP, when one recalls a postponed message, mutt downloads it and names the tempfile in tmpdir 'mutt' plus six chars.  Therefore, Vim will set the filetype to mail when you edit a recalled draft message.  Wonderful.

Annoying side-effect: a mutt config file named, e.g., 'muttmacros', gets declared by Vim to be mail, which automatically sets annoying things (especially if one has added special autocommands for the mail filetype, like linebreaking stuff).  The filetype *should* be "muttrc". In that file, one might have all of ones macros separated out from the muttrc.  This happens to any mutt+6char filename, like muttrchook, muttemails, mutthooksrc, muttsource, muttimaprc, etc.

Now this can be avoided by not using those filenames, but as a matter of principle, mutt ought to name the recalled-draft-tempfiles according to some other system that's unlikely to get trampled on by a user's "normal" config filename.  Why not the usual tempfile naming system "mutt-hostname-bunchofnumbers"?  Or 'mutt' plus 18 chars instead of 6?

BTW, setting a Vim modeline in those files to override Vim's behavior does not work.

--------------------------------------------------------------------------------
2010-08-06 16:20:36 UTC balderdash
* milestone changed to 1.6

--------------------------------------------------------------------------------
2011-11-21 17:38:22 UTC brendan
* milestone changed to 2.0
