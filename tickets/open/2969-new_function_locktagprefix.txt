Ticket:  2969
Status:  new
Summary: new function: lock-tag-prefix

Reporter: cypherpunk
Owner:    mutt-dev

Opened:       2007-10-11 06:38:52 UTC
Last Updated: 2008-05-18 01:42:57 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I wanted a way to have a macro that:
 - when applied with tag-prefix, applies several commands to all tagged messages
 - when applied without tag-prefix, applies the same commands to the current message

So I added two functions to mutt: lock-tag-prefix and unlock-tag-prefix. The idea is that the said macro starts with <lock-tag-prefix> and ends with <unlock-tag-prefix>.

lock-tag-prefix lock the tag-prefix to "on", but only if it is in effect. unlock-tag-prefix unlocks it.

Please let me know if you apply this patch and require me to write the documentation, I'll try to write something sensible.

--------------------------------------------------------------------------------
2007-10-11 06:39:10 UTC cypherpunk
* Added attachment lionel_lock-tag-prefix

--------------------------------------------------------------------------------
2007-10-11 07:26:25 UTC Nicolas Rachinsky
* Added comment:
{{{
* Mutt <fleas@mutt.org> [2007-10-11 06:38 -0000]:
>  lock-tag-prefix lock the tag-prefix to "on", but only if it is in effect.
>  unlock-tag-prefix unlocks it.

What happens if the macro aborts before it reaches the
unlock-tag-prefix? Does it work with nested macros?
And you can't apply the tag-prefix to only some of the commands in the
macro with your patch, I think.

BTW:
There are two similar patches in the thread on mutt-dev starting with
<20060115193922.GA17110@domus.home.globnix.net>.

Nicolas
}}}

--------------------------------------------------------------------------------
2007-10-11 07:41:01 UTC Lionel Elie Mamane
* Added comment:
{{{
On Thu, Oct 11, 2007 at 07:26:26AM -0000, Mutt wrote:
>  * Mutt <fleas@mutt.org> [2007-10-11 06:38 -0000]:

>>  lock-tag-prefix lock the tag-prefix to "on", but only if it is in effect.
>>  unlock-tag-prefix unlocks it.

>  What happens if the macro aborts before it reaches the
>  unlock-tag-prefix?

The tag-prefix stays locked, and there is no visible cue to the user
that it is so. I don't know how to add such a visible cue.

> Does it work with nested macros?

I would think it does, but untested and I'm not a mutt expert.

>  And you can't apply the tag-prefix to only some of the commands in
>  the macro with your patch, I think.

That is mostly correct. You can apply it to some commands, and then
not apply it to the commands that come after that, but you cannot
apply to commands after that. I.e. you can do:

 <lock-tag-prefix>cmd1 cmd2 cmd3<unlock-tag-prefix>cmd4 cmd5 cmd6

and the tag-prefix will be applied to commands 1, 2 and 3, but not 4,
5 and 6.

>  There are two similar patches in the thread on mutt-dev starting with
>  <20060115193922.GA17110@domus.home.globnix.net>.

From skimming the first of these patches, it seems to me that to get
the effect I'm after, you'd have to do something like:

 <tag-push><tag-pop>cmd1<tag-pop>cmd2<tag-pop>cmd3

Yeah, I'd be happy with that, too.
}}}

--------------------------------------------------------------------------------
2007-10-11 08:41:28 UTC Nicolas Rachinsky
* Added comment:
{{{
* Mutt <fleas@mutt.org> [2007-10-11 07:41 -0000]:
>  >  What happens if the macro aborts before it reaches the
>  >  unlock-tag-prefix?
> 
>  The tag-prefix stays locked, and there is no visible cue to the user
>  that it is so. I don't know how to add such a visible cue.

I think this can cause hard to debug problems. If the user notes that
there is a problem, since the problem will look like strange
unreproducable behaviour.

>  > Does it work with nested macros?
> 
>  I would think it does, but untested and I'm not a mutt expert.

I just noted that the patches in the mentioned thread do not handle
nested macros well if more than one uses <tag-push>/<tag-set-tagflag>.
:(

>  >  There are two similar patches in the thread on mutt-dev starting with
>  >  <20060115193922.GA17110@domus.home.globnix.net>.
> 
>  From skimming the first of these patches, it seems to me that to get
>  the effect I'm after, you'd have to do something like:
> 
>   <tag-push><tag-pop>cmd1<tag-pop>cmd2<tag-pop>cmd3
> 
>  Yeah, I'd be happy with that, too.

Yes, that's the idea.

Nicolas

}}}

--------------------------------------------------------------------------------
2007-10-12 08:44:11 UTC Lionel Elie Mamane
* Added comment:
{{{
On Thu, Oct 11, 2007 at 08:41:28AM -0000, Mutt wrote:
>  * Mutt <fleas@mutt.org> [2007-10-11 07:41 -0000]:

>>>  What happens if the macro aborts before it reaches the
>>>  unlock-tag-prefix?

>>  The tag-prefix stays locked, and there is no visible cue to the
>>  user that it is so. I don't know how to add such a visible cue.

>  I think this can cause hard to debug problems.

Yes. I expect someone more at east in the mutt source code would have
no particular difficulty adding the said visual cue.

>>> Does it work with nested macros?

>>  I would think it does, but untested and I'm not a mutt expert.

>  I just noted that the patches in the mentioned thread do not handle
>  nested macros well if more than one uses
>  <tag-push>/<tag-set-tagflag>.  :(

In my previous statement, I had thought only of the nesting of a macro
that does not use (un)lock-tag-prefix in a macro that uses them; it
works in the sense that the commands in the inner macro correctly
apply to all tagged messages. If the inside nested macro uses
unlock-tag-prefix, then it will release the lock for the (rest of the)
outside macro, too. This is rather straightforward to fix; here is an
(untested) updated patch.

This will overflow at INT_MAX-2 levels of nesting; that should cover
most reasonable uses ;-)
}}}

--------------------------------------------------------------------------------
2007-10-12 08:46:25 UTC Lionel Elie Mamane
* Added comment:
{{{
On Thu, Oct 11, 2007 at 07:41:01AM -0000, Mutt wrote:

>>  And you can't apply the tag-prefix to only some of the commands in
>>  the macro with your patch, I think.

>  That is (...) correct.

I'd be interested in a scenario where this would be really useful,
though. A macro that would do that would do something on tagged
messages and something (else?) on the current message. This looks like
it would be confusing to the user.
}}}

--------------------------------------------------------------------------------
2007-10-12 14:52:56 UTC Nicolas Rachinsky
* Added comment:
{{{
* Mutt <fleas@mutt.org> [2007-10-12 08:46 -0000]:
>  I'd be interested in a scenario where this would be really useful,
>  though. A macro that would do that would do something on tagged
>  messages and something (else?) on the current message.

Hm.  Good question. I just considered it natural to enable the user to
do that. But I don't know an actual application for this feature.

> This looks like
>  it would be confusing to the user.

Not if the user himself wrote the macro to do that.

Nicolas
}}}

--------------------------------------------------------------------------------
2008-05-18 01:42:57 UTC brendan
* milestone changed to 2.0
* priority changed to minor
