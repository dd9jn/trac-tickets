Ticket:  885
Status:  new
Summary: storing number of new messages per box

Reporter: Aaron Brick <chizor@lithic.org>
Owner:    mutt-dev

Opened:       2001-11-21 22:44:43 UTC
Last Updated: 2007-04-07 14:23:50 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.20-1
Severity: wishlist

-- Please type your report below this line

hi,

i think a great addition to mutt would be an integer associated with each
mailbox, for storing hte number of new messages in it. it would be adjusted
every time mail arrived or was read or deleted, and would provide the user
with a high-level description of changes in mailboxes when they're choosing
from the list. seeing that there are 46876 new bytes in a mailbox is not
nearly as good as knowing that there are 2 new messages.

of course, this could be taken farther: we could keep track of the number of
messages in all different cases (new, unopened, replied, etc.). it would
provide some very helpful folder listings.

aaron brick.


-- Mutt Version Information

Mutt 1.3.20i (2001-07-24)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.2.17 [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-05 12:35:59 UTC brendan
* Added comment:
{{{
Refiled as change-request.
}}}
