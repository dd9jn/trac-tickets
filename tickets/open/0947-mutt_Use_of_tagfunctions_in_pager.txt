Ticket:  947
Status:  new
Summary: mutt: Use of tag-functions in pager

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2002-01-07 14:48:50 UTC
Last Updated: 2005-08-20 13:38:42 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.24-2
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#127957.
Please Cc all your replies to 127957@bugs.debian.org.]

From: Alexander Neumann <bugreport@bumpern.de>
Subject: mutt: Use of tag-functions in pager
Date: Sun, 06 Jan 2002 01:07:56 +0100

Package: mutt
Version: 1.3.24-2
Severity: wishlist

I would like to apply actions to tagged messages even in the pager. Today, this
isn't possible due to the nonexistance of this function in the pager. It would be
nice to integrate that.

Yours, Alexander

-- System Information
Debian Release: 3.0
Architecture: i386
Kernel: Linux sigsegv 2.4.16 #4 Tue Dec 4 14:08:31 CET 2001 i686
Locale: LANG=en_US, LC_CTYPE=en_US

Versions of packages mutt depends on:
ii  libc6            2.2.4-7                 GNU C Library: Shared libraries an
ii  libncurses5      5.2.20010318-3          Shared libraries for terminal hand
ii  libsasl7         1.5.27-2                Authentication abstraction library
ii  postfix [mail-tr 0.0.20011115.SNAPSHOT-1 A high-performance mail transport 



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-01-07 14:48:50 UTC Marco d'Itri <md@linux.it>
* Added comment:
{{{
Package: mutt
Version: 1.3.24-2
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#127957.
Please Cc all your replies to 127957@bugs.debian.org.]

From: Alexander Neumann <bugreport@bumpern.de>
Subject: mutt: Use of tag-functions in pager
Date: Sun, 06 Jan 2002 01:07:56 +0100

Package: mutt
Version: 1.3.24-2
Severity: wishlist

I would like to apply actions to tagged messages even in the pager. Today, this
isn't possible due to the nonexistance of this function in the pager. It would be
nice to integrate that.

Yours, Alexander

-- System Information
Debian Release: 3.0
Architecture: i386
Kernel: Linux sigsegv 2.4.16 #4 Tue Dec 4 14:08:31 CET 2001 i686
Locale: LANG=en_US, LC_CTYPE=en_US

Versions of packages mutt depends on:
ii  libc6            2.2.4-7                 GNU C Library: Shared libraries an
ii  libncurses5      5.2.20010318-3          Shared libraries for terminal hand
ii  libsasl7         1.5.27-2                Authentication abstraction library
ii  postfix [mail-tr 0.0.20011115.SNAPSHOT-1 A high-performance mail transport
}}}

--------------------------------------------------------------------------------
2005-08-09 10:54:11 UTC rado
* Added comment:
{{{
This is rather a wish than a bug. Should be user-configurable.
}}}

--------------------------------------------------------------------------------
2005-08-21 07:38:42 UTC rado
* Added comment:
{{{
Repeat for mutt-dev, reminder for sender:
This is rather a wish than a bug. Perhaps should be user-configurable via muttrc.
}}}
