Ticket:  3387
Status:  new
Summary: Mutt assumes that IMAP password and SSL client certificate password (if set) are the same

Reporter: timow
Owner:    mutt-dev

Opened:       2010-02-26 17:50:22 UTC
Last Updated: 2010-03-10 16:47:53 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
For accessing my IMAP account, I need to authenticate via an SSL client certificate and, additionally, a password set for the IMAP account.
Mutt (with OpenSSL binding) can only handle this if the certificate file is not password protected or is protected via the same password as the IMAP account.

I expect mutt to handle both passwords separately.

A debugging session has revealed that the password callback ssl_passwd_cb() in mutt_ssl.c calls mutt_account_getpass(account). mutt_account_getpass determines that the account is of type IMAP and returns the IMAP password. However, it would need to return the password for the SSL client certificate.

--------------------------------------------------------------------------------
2010-03-02 10:17:04 UTC timow
* Added attachment patch-1.5.20.tw.ssl_client_cert_pass.1
* Added comment:
Patch for a new configuration variable ssl_client_cert_pass

--------------------------------------------------------------------------------
2010-03-10 16:42:26 UTC timow
* Added attachment patch-1.5.20.tw.ssl_client_cert_pass.1.patch
* Added comment:
A patch that adds a configuration variable ssl_client_cert_pass (covering OpenSSL and GnuTLS)

--------------------------------------------------------------------------------
2010-03-10 16:47:53 UTC timow
* Added comment:
The patch patch-1.5.20.tw.ssl_client_cert_pass.1.patch hopefully fixes the issue. It adds a configuration variable ssl_client_cert_pass that is used if the private key is protected by a passphrase. If the variable is not set, mutt asks for the passphrase (and no longer uses, for example, the passphrase of an IMAP account for the key).

The patch covers both, OpenSSL and GnuTLS, bindings.
