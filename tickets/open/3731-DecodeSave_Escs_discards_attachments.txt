Ticket:  3731
Status:  new
Summary: Decode&Save (Esc+s) discards attachments

Reporter: viric
Owner:    mutt-dev

Opened:       2015-02-01 00:06:30 UTC
Last Updated: 2015-02-27 14:18:26 UTC

Priority:  major
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
I like PGP for the ciphering of communications, but I don't like the contents of my mailbox being ciphered, once it's at home.

I wanted to use the Decode&save functionality, but it silently discards any attachments (using PGP/MIME in this case) and keeps only the body.

To reproduce it, just have a ciphered letter with an attachment (also ciphered) in PGP/MIME, and use <Esc>s to save it decoded. The decoded version lacks the attachment.

--------------------------------------------------------------------------------
2015-02-14 10:41:14 UTC pebo
* Added comment:
{{{
On Sun, Feb 01, 2015 at 12:06:30AM -0000, Mutt wrote:

This was probably fixed in the meantime, I just tried to reproduce it
with HEAD + some unrelated patches (1.5.23+58, 385d7434c9d6) and all
encrypted attachments survived a <decrypt-save>.
}}}

--------------------------------------------------------------------------------
2015-02-27 14:06:01 UTC viric
* Added comment:
I just tried http://dev.mutt.org/hg/mutt/rev/71f12fef8c6f with gpgme, and the problem still happens to me.

The same thing happens without gpgme in 1.5.21, at least. I don't understand how pebo cannot reproduce it.

--------------------------------------------------------------------------------
2015-02-27 14:18:26 UTC viric
* Added comment:
AH! I had to use <decrypt-save>! Very confusing, as <decode-save> deciphers too.
