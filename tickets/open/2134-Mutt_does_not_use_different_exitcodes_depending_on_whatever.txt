Ticket:  2134
Status:  new
Summary: Mutt does not use different exit-codes depending on whatever

Reporter: utcke+mutt@informatik.uni-hamburg.de
Owner:    mutt-dev

Opened:       2005-11-10 16:56:59 UTC
Last Updated: 1970-01-01 00:00:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Hi,

I'm using mutt inside a script where glimpse selects messages from
various mailboxes and assembles them all in a temporary mailbox.

However, sometimes glimpse doesn't get it quite right, and mutt will
report:

/tmp/glimpse.21144/mboxfind.results is not a mailbox.

Which, indeed, it isn't.  However, my script will never know about
this, since mutt returns 0 as its exit code anyway, and happily delete
the file instead of doing something useful.

So, in short, I would really ask for different exit codes for mutt, at
least for the above case, but quite possibly for some other cases too
(everytime mutt exits even though I didn't ask it to exit, I guess...)

Help very much appreciated

Sven
>How-To-Repeat:
>Fix:
Unknown
}}}
