Ticket:  2944
Status:  new
Summary: mutt-1.5.16cvs: "bind map KEY noop" KEY rebound to macro does not activate despite listing against unbound in key bindings display.

Reporter: Yorudan
Owner:    mutt-dev

Opened:       2007-08-19 14:03:46 UTC
Last Updated: 2007-08-19 15:33:10 UTC

Priority:  major
Component: mutt
Keywords:  xref 1880, 2723, 2575

--------------------------------------------------------------------------------
Description:


{{{
Package: mutt
Version: 1.5.12-1ubuntu1.1
Severity: normal

-- Please type your report below this line

Mutt fails (in CVS as of 2007-08-19 as well as Ubuntu-6.10 packaged 1.5.12)
to accept that a key has been rebound to a macro, despite this binding being
shown in the help display.

To illustrate:

:bind index r noop
:macro index r "display-message ; reply"

when r is pressed in the index view, "Key is not bound." However, in the
listing of keybindings (produced with a ?) I see 

[...]
q           quit                   save changes to mailbox and quit
r           macro                  display-message; reply
s           save-message           save message/attachment to a file
u           undelete-message       undelete the current entry
[...]

I think this may be related to ticket #1880, opened 3 years ago but still
showing as unclosed, relating to the difference between binding to a null
operation, and removing a binding entirely. My speculation is that owing to
a binding to a noop, Mutt never falls through to check whether there is a
macro binding for that key.

The patch for 1.5.1 provided by David Cohen will no longer apply to Mutt.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Using built-in specs.
Target: i486-linux-gnu
Configured with: ../src/configure -v --enable-languages=c,c++,fortran,objc,obj-c++,treelang --prefix=/usr --enable-shared --with-system-zlib --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --enable-nls --program-suffix=-4.1 --enable-__cxa_atexit --enable-clocale=gnu --enable-libstdcxx-debug --enable-mpfr --enable-checking=release i486-linux-gnu
Thread model: posix
gcc version 4.1.2 20060928 (prerelease) (Ubuntu 4.1.1-13ubuntu5)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.16cvs (2007-07-25)
Copyright (C) 1996-2007 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.17-10-386 (i686)
ncurses: ncurses 5.5.20060422 (compiled with 5.5)
libidn: 0.6.3 (compiled with 0.6.3)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK   -USE_INODESORT   
-USE_POP  -USE_IMAP  -USE_SMTP  -USE_GSS  -USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.


--- Begin /usr/local/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb "<search>~b " "search in message bodies"
macro index,pager,attach,compose \cb "\
<enter-command> set my_pipe_decode=\$pipe_decode pipe_decode<Enter>\
<pipe-message> urlview<Enter>\
<enter-command> set pipe_decode=\$my_pipe_decode; unset my_pipe_decode<Enter>" \
"call urlview to extract URLs out of a message"
macro generic,pager <F1> "<shell-escape> less /usr/local/doc/mutt/manual.txt<Enter>" "show Mutt documentation"
macro index,pager y "<change-folder>?<toggle-mailboxes>" "show incoming mailboxes list"
bind browser y exit
attachments   +A */.*
attachments   -A text/x-vcard application/pgp.*
attachments   -A application/x-pkcs7-.*
attachments   +I text/plain
attachments   -A message/external-body
attachments   -I message/external-body
--- End /usr/local/etc/Muttrc
}}}

--------------------------------------------------------------------------------
2007-08-19 14:04:54 UTC Yorudan
* version changed to 1.5.16

--------------------------------------------------------------------------------
2007-08-19 15:33:10 UTC rado
* keywords changed to xref 1880, 2723, 2575
* summary changed to mutt-1.5.16cvs: "bind map KEY noop" KEY rebound to macro does not activate despite listing against unbound in key bindings display.
