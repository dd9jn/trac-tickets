Ticket:  3648
Status:  new
Summary: TLS-received emails are missorted when sorting by date-received

Reporter: hobarrera
Owner:    mutt-dev

Opened:       2013-07-08 17:08:24 UTC
Last Updated: 2013-11-15 04:39:41 UTC

Priority:  major
Component: browser
Keywords:  

--------------------------------------------------------------------------------
Description:
I recently enabled STARTTLS on :25 on my email server.
Now some of my emails are received using TLS, while others aren't.

mutt missorts emails that have been receive using TLS.
This is my (relevant) config:

{{{
set sort = threads
set sort_aux = last-date-received
}}}

Let me put this as a reference:

{{{
Received: from mail-ee0-x23b.google.com (mail-ee0-x23b.google.com [IPv6:2a00:1450:4013:c00::23b]);
        by mx1.ubertech.com.ar (OpenSMTPD) with ESMTPS id 43f3092d;
        TLS version=TLSv1/SSLv3 cipher=AES128-SHA bits=128 verify=NO;
        for <hugo@osvaldobarrera.com.ar>;
        Sun, 7 Jul 2013 17:27:24 +0000 (GMT)
}}}

With no exception, all emails which include the line starting in "TLS version..." (as the example above) are placed at the very top of the list, while all other emails are at the bottom.

Since I had no TLS in the past, all other 400 emails still in my inbox are properly sorted, while TLS-received ones (ie: the ones with the mentioned header) get placed at the very top of the list.

--------------------------------------------------------------------------------
2013-10-06 16:05:16 UTC me
* Added comment:
{{{
On Mon, Jul 08, 2013 at 05:08:25PM -0000, Mutt wrote:

Which mailbox format are you using?  The date received that Mutt 
uses comes from either the "From" field separator (mbox), or the 
timestamp on the file from the stat() system call.  Mutt does not 
parse the Received header field, so it has to be when the message 
it put into the mailbox where the problem is occurring.
}}}

--------------------------------------------------------------------------------
2013-11-15 04:39:41 UTC hobarrera
* Added comment:
{{{
On 2013-10-06 16:05, Mutt wrote:

I'm using maildir.
This sorts as expected:

set sort = threads              # Sort messages into threads
#set sort_aux = last-date

This does not:

set sort = threads              # Sort messages into threads
set sort_aux = last-date-received

For these last settings, these are to dates for the topmost emails in my INBOX:

2013-10-11
2013-09-25
2013-09-12
2013-11-14
2013-08-24
2013-08-29
2013-09-28
2013-08-22
2013-07-26
2013-07-26
2013-10-09
2013-10-10
2013-09-17
2013-09-08
2013-07-31
2013-10-24
2013-10-12
2013-10-15
2013-10-16
2013-08-27
2013-08-27
2013-10-21
2013-10-21
2013-10-22
2013-10-23
2013-08-12
2013-10-12

Can I avoid the timestamp being used? The timestamp won't usually match
the date it was received.
}}}
