Ticket:  919
Status:  new
Summary: Feature request with implementation: IMAP folder attributes UNSEEN and MESSAGES in folder_format

Reporter: ntyni+mutt@mappi.helsinki.fi
Owner:    mutt-dev

Opened:       2001-12-17 17:20:08 UTC
Last Updated: 2008-10-31 19:35:51 UTC

Priority:  trivial
Component: IMAP
Keywords:  IMAP,patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.24i
Severity: wishlist

-- Please type your report below this line
Here's something I've been wanting from Mutt: IMAP folder attributes 
UNSEEN and MESSAGES for the browser. Patch is against stock 1.3.24i.
I'd be very glad if something like this made its way to the official version.

The implementation could probably be more efficient (querying all the 
attributes from the server in a single go), but it doesn't seem to be too
slow for me.

--- ./doc/manual.sgml	2001/12/16 20:04:34	1.1
+++ ./doc/manual.sgml	2001/12/16 19:57:09
@@ -3572,7 +3572,9 @@
 <tag>&percnt;F  </tag>file permissions
 <tag>&percnt;g  </tag>group name (or numeric gid, if missing)
 <tag>&percnt;l  </tag>number of hard links
-<tag>&percnt;N  </tag>N if folder has new mail, blank otherwise
+<tag>&percnt;M  </tag>blank for non-IMAP mailboxes; IMAP: number of messages
+<tag>&percnt;N  </tag>N if folder has new mail, blank otherwise (IMAP: number of RECENT messages
+<tag>&percnt;U  </tag>blank for non-IMAP mailboxes; IMAP: number of UNSEEN messages
 <tag>&percnt;s  </tag>size in bytes
 <tag>&percnt;t  </tag>* if the file is tagged, blank otherwise
 <tag>&percnt;u  </tag>owner name (or numeric uid, if missing)
--- ./imap/imap.c	2001/12/16 13:27:17	1.1
+++ ./imap/imap.c	2001/12/16 13:34:57
@@ -1127,9 +1127,9 @@
   return result;
 }
 
-/* returns count of recent messages if new = 1, else count of total messages.
+/* returns count of recent messages if new = 1, count of unseen if new > 1,
+ * else count of total messages
  * (useful for at least postponed function)
- * Question of taste: use RECENT or UNSEEN for new?
  *   0+   number of messages in mailbox
  *  -1    error while polling mailboxes
  */
@@ -1180,7 +1180,7 @@
 	   mutt_bit_isset(idata->capabilities,STATUS))
   {				
     snprintf (buf, sizeof (buf), "STATUS %s (%s)", mbox,
-      new ? "RECENT" : "MESSAGES");
+      (new > 1 ? "UNSEEN" : (new ? "RECENT" : "MESSAGES")));
   }
   else
     /* Server does not support STATUS, and this is not the current mailbox.
--- ./buffy.h	2001/12/16 13:18:42	1.1
+++ ./buffy.h	2001/12/16 13:19:28
@@ -24,6 +24,8 @@
 #endif				/* BUFFY_SIZE */
   struct buffy_t *next;
   short new;			/* mailbox has new mail */
+  short unseen;			/* mailbox has unseen mail */
+  short messages;		/* total messages in mailbox */
   short notified;		/* user has been notified */
   short magic;			/* mailbox type */
   short newly_created;		/* mbox or mmdf just popped into existence */
--- ./buffy.c	2001/12/16 13:19:31	1.1
+++ ./buffy.c	2001/12/16 13:26:07
@@ -365,10 +365,16 @@
 	
 #ifdef USE_IMAP
       case M_IMAP:
-	if ((tmp->new = imap_mailbox_check (tmp->path, 1)) > 0)
-	  BuffyCount++;
-	else
-	  tmp->new = 0;
+        tmp->new = imap_mailbox_check (tmp->path, 1);
+        tmp->unseen = imap_mailbox_check (tmp->path, 2);
+        if (tmp->new > 0 || tmp->unseen > 0)
+          BuffyCount++;
+        else {
+          tmp->new = 0;
+          tmp->unseen = 0;
+        }
+        if ((tmp->messages = imap_mailbox_check (tmp->path, 0)) <= 0)
+          tmp->messages = 0;
 
 	break;
 #endif
--- ./browser.c	2001/12/16 13:30:07	1.1
+++ ./browser.c	2001/12/16 13:47:21
@@ -237,6 +237,24 @@
 	mutt_format_s (dest, destlen, fmt, "");
       break;
       
+    case 'M':
+#ifdef USE_IMAP
+      if (mx_is_imap (folder->ff->desc))
+      {
+	if (!optional)
+	{
+	  snprintf (tmp, sizeof (tmp), "%%%sd", fmt);
+	  snprintf (dest, destlen, tmp, folder->ff->messages);
+	}
+	else if (!folder->ff->messages)
+	  optional = 0;
+	break;
+      }
+#endif
+      snprintf (tmp, sizeof (tmp), "%%%sc", fmt);
+      snprintf (dest, destlen, tmp, ' ');
+      break;
+
     case 'N':
 #ifdef USE_IMAP
       if (mx_is_imap (folder->ff->desc))
@@ -254,7 +272,25 @@
       snprintf (tmp, sizeof (tmp), "%%%sc", fmt);
       snprintf (dest, destlen, tmp, folder->ff->new ? 'N' : ' ');
       break;
-      
+
+    case 'U':
+#ifdef USE_IMAP
+      if (mx_is_imap (folder->ff->desc))
+      {
+	if (!optional)
+	{
+	  snprintf (tmp, sizeof (tmp), "%%%sd", fmt);
+	  snprintf (dest, destlen, tmp, folder->ff->unseen);
+	}
+	else if (!folder->ff->unseen)
+	  optional = 0;
+	break;
+      }
+#endif
+      snprintf (tmp, sizeof (tmp), "%%%sc", fmt);
+      snprintf (dest, destlen, tmp, ' ');
+      break;
+
     case 's':
       if (folder->ff->st != NULL)
       {
@@ -300,7 +336,7 @@
 }
 
 static void add_folder (MUTTMENU *m, struct browser_state *state,
-			const char *name, const struct stat *s, int new)
+			const char *name, const struct stat *s, int new, int unseen, int messages)
 {
   if (state->entrylen == state->entrymax)
   {
@@ -324,6 +360,8 @@
   }
 
   (state->entry)[state->entrylen].new = new;
+  (state->entry)[state->entrylen].messages  = messages;
+  (state->entry)[state->entrylen].unseen = unseen;
   (state->entry)[state->entrylen].name = safe_strdup (name);
   (state->entry)[state->entrylen].desc = safe_strdup (name);
 #ifdef USE_IMAP
@@ -407,7 +445,7 @@
     tmp = Incoming;
     while (tmp && mutt_strcmp (buffer, tmp->path))
       tmp = tmp->next;
-    add_folder (menu, state, de->d_name, &s, (tmp) ? tmp->new : 0);
+    add_folder (menu, state, de->d_name, &s, (tmp) ? tmp->new : 0, (tmp) ? tmp->unseen : 0, (tmp) ? tmp->messages : 0);
   }
   closedir (dp);  
   browser_sort (state);
@@ -431,14 +469,14 @@
 #ifdef USE_IMAP
     if (mx_is_imap (tmp->path))
     {
-      add_folder (menu, state, tmp->path, NULL, tmp->new);
+      add_folder (menu, state, tmp->path, NULL, tmp->new, tmp->unseen, tmp->messages);
       continue;
     }
 #endif
 #ifdef USE_POP
     if (mx_is_pop (tmp->path))
     {
-      add_folder (menu, state, tmp->path, NULL, tmp->new);
+      add_folder (menu, state, tmp->path, NULL, tmp->new, tmp->unseen, tmp->messages);
       continue;
     }
 #endif
@@ -452,7 +490,7 @@
     strfcpy (buffer, NONULL(tmp->path), sizeof (buffer));
     mutt_pretty_mailbox (buffer);
 
-    add_folder (menu, state, buffer, &s, tmp->new);
+    add_folder (menu, state, buffer, &s, tmp->new, tmp->unseen, tmp->messages);
   }
   while ((tmp = tmp->next));
   browser_sort (state);
--- ./browser.h	2001/12/16 13:35:45	1.1
+++ ./browser.h	2001/12/16 13:42:38
@@ -31,6 +31,8 @@
   char *desc;
 
   unsigned short new;
+  unsigned short unseen;
+  unsigned short messages;
 #ifdef USE_IMAP
   char delim;
   
--- ./init.h	2001/12/16 20:03:37	1.1
+++ ./init.h	2001/12/16 20:02:41
@@ -533,7 +533,9 @@
   ** .dt %F  .dd file permissions
   ** .dt %g  .dd group name (or numeric gid, if missing)
   ** .dt %l  .dd number of hard links
-  ** .dt %N  .dd N if folder has new mail, blank otherwise
+  ** .dt %M  .dd blank for non-IMAP folders; IMAP: number of messages
+  ** .dt %N  .dd N if folder has new mail, blank otherwise (IMAP: number of RECENT messages)
+  ** .dt %U  .dd blank for non-IMAP folders; IMAP: number of UNSEEN messages
   ** .dt %s  .dd size in bytes
   ** .dt %t  .dd * if the file is tagged, blank otherwise
   ** .dt %u  .dd owner name (or numeric uid, if missing)


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011006 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.24i (2001-11-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.0 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-02-08 12:07:34 UTC rado
* Added comment:
{{{
Move to IMAP category
}}}

--------------------------------------------------------------------------------
2007-04-12 16:44:14 UTC brendan
* Updated description:
{{{
Package: mutt
Version: 1.3.24i
Severity: wishlist

-- Please type your report below this line
Here's something I've been wanting from Mutt: IMAP folder attributes 
UNSEEN and MESSAGES for the browser. Patch is against stock 1.3.24i.
I'd be very glad if something like this made its way to the official version.

The implementation could probably be more efficient (querying all the 
attributes from the server in a single go), but it doesn't seem to be too
slow for me.

--- ./doc/manual.sgml	2001/12/16 20:04:34	1.1
+++ ./doc/manual.sgml	2001/12/16 19:57:09
@@ -3572,7 +3572,9 @@
 <tag>&percnt;F  </tag>file permissions
 <tag>&percnt;g  </tag>group name (or numeric gid, if missing)
 <tag>&percnt;l  </tag>number of hard links
-<tag>&percnt;N  </tag>N if folder has new mail, blank otherwise
+<tag>&percnt;M  </tag>blank for non-IMAP mailboxes; IMAP: number of messages
+<tag>&percnt;N  </tag>N if folder has new mail, blank otherwise (IMAP: number of RECENT messages
+<tag>&percnt;U  </tag>blank for non-IMAP mailboxes; IMAP: number of UNSEEN messages
 <tag>&percnt;s  </tag>size in bytes
 <tag>&percnt;t  </tag>* if the file is tagged, blank otherwise
 <tag>&percnt;u  </tag>owner name (or numeric uid, if missing)
--- ./imap/imap.c	2001/12/16 13:27:17	1.1
+++ ./imap/imap.c	2001/12/16 13:34:57
@@ -1127,9 +1127,9 @@
   return result;
 }
 
-/* returns count of recent messages if new = 1, else count of total messages.
+/* returns count of recent messages if new = 1, count of unseen if new > 1,
+ * else count of total messages
  * (useful for at least postponed function)
- * Question of taste: use RECENT or UNSEEN for new?
  *   0+   number of messages in mailbox
  *  -1    error while polling mailboxes
  */
@@ -1180,7 +1180,7 @@
 	   mutt_bit_isset(idata->capabilities,STATUS))
   {				
     snprintf (buf, sizeof (buf), "STATUS %s (%s)", mbox,
-      new ? "RECENT" : "MESSAGES");
+      (new > 1 ? "UNSEEN" : (new ? "RECENT" : "MESSAGES")));
   }
   else
     /* Server does not support STATUS, and this is not the current mailbox.
--- ./buffy.h	2001/12/16 13:18:42	1.1
+++ ./buffy.h	2001/12/16 13:19:28
@@ -24,6 +24,8 @@
 #endif				/* BUFFY_SIZE */
   struct buffy_t *next;
   short new;			/* mailbox has new mail */
+  short unseen;			/* mailbox has unseen mail */
+  short messages;		/* total messages in mailbox */
   short notified;		/* user has been notified */
   short magic;			/* mailbox type */
   short newly_created;		/* mbox or mmdf just popped into existence */
--- ./buffy.c	2001/12/16 13:19:31	1.1
+++ ./buffy.c	2001/12/16 13:26:07
@@ -365,10 +365,16 @@
 	
 #ifdef USE_IMAP
       case M_IMAP:
-	if ((tmp->new = imap_mailbox_check (tmp->path, 1)) > 0)
-	  BuffyCount++;
-	else
-	  tmp->new = 0;
+        tmp->new = imap_mailbox_check (tmp->path, 1);
+        tmp->unseen = imap_mailbox_check (tmp->path, 2);
+        if (tmp->new > 0 || tmp->unseen > 0)
+          BuffyCount++;
+        else {
+          tmp->new = 0;
+          tmp->unseen = 0;
+        }
+        if ((tmp->messages = imap_mailbox_check (tmp->path, 0)) <= 0)
+          tmp->messages = 0;
 
 	break;
 #endif
--- ./browser.c	2001/12/16 13:30:07	1.1
+++ ./browser.c	2001/12/16 13:47:21
@@ -237,6 +237,24 @@
 	mutt_format_s (dest, destlen, fmt, "");
       break;
       
+    case 'M':
+#ifdef USE_IMAP
+      if (mx_is_imap (folder->ff->desc))
+      {
+	if (!optional)
+	{
+	  snprintf (tmp, sizeof (tmp), "%%%sd", fmt);
+	  snprintf (dest, destlen, tmp, folder->ff->messages);
+	}
+	else if (!folder->ff->messages)
+	  optional = 0;
+	break;
+      }
+#endif
+      snprintf (tmp, sizeof (tmp), "%%%sc", fmt);
+      snprintf (dest, destlen, tmp, ' ');
+      break;
+
     case 'N':
 #ifdef USE_IMAP
       if (mx_is_imap (folder->ff->desc))
@@ -254,7 +272,25 @@
       snprintf (tmp, sizeof (tmp), "%%%sc", fmt);
       snprintf (dest, destlen, tmp, folder->ff->new ? 'N' : ' ');
       break;
-      
+
+    case 'U':
+#ifdef USE_IMAP
+      if (mx_is_imap (folder->ff->desc))
+      {
+	if (!optional)
+	{
+	  snprintf (tmp, sizeof (tmp), "%%%sd", fmt);
+	  snprintf (dest, destlen, tmp, folder->ff->unseen);
+	}
+	else if (!folder->ff->unseen)
+	  optional = 0;
+	break;
+      }
+#endif
+      snprintf (tmp, sizeof (tmp), "%%%sc", fmt);
+      snprintf (dest, destlen, tmp, ' ');
+      break;
+
     case 's':
       if (folder->ff->st != NULL)
       {
@@ -300,7 +336,7 @@
 }
 
 static void add_folder (MUTTMENU *m, struct browser_state *state,
-			const char *name, const struct stat *s, int new)
+			const char *name, const struct stat *s, int new, int unseen, int messages)
 {
   if (state->entrylen == state->entrymax)
   {
@@ -324,6 +360,8 @@
   }
 
   (state->entry)[state->entrylen].new = new;
+  (state->entry)[state->entrylen].messages  = messages;
+  (state->entry)[state->entrylen].unseen = unseen;
   (state->entry)[state->entrylen].name = safe_strdup (name);
   (state->entry)[state->entrylen].desc = safe_strdup (name);
 #ifdef USE_IMAP
@@ -407,7 +445,7 @@
     tmp = Incoming;
     while (tmp && mutt_strcmp (buffer, tmp->path))
       tmp = tmp->next;
-    add_folder (menu, state, de->d_name, &s, (tmp) ? tmp->new : 0);
+    add_folder (menu, state, de->d_name, &s, (tmp) ? tmp->new : 0, (tmp) ? tmp->unseen : 0, (tmp) ? tmp->messages : 0);
   }
   closedir (dp);  
   browser_sort (state);
@@ -431,14 +469,14 @@
 #ifdef USE_IMAP
     if (mx_is_imap (tmp->path))
     {
-      add_folder (menu, state, tmp->path, NULL, tmp->new);
+      add_folder (menu, state, tmp->path, NULL, tmp->new, tmp->unseen, tmp->messages);
       continue;
     }
 #endif
 #ifdef USE_POP
     if (mx_is_pop (tmp->path))
     {
-      add_folder (menu, state, tmp->path, NULL, tmp->new);
+      add_folder (menu, state, tmp->path, NULL, tmp->new, tmp->unseen, tmp->messages);
       continue;
     }
 #endif
@@ -452,7 +490,7 @@
     strfcpy (buffer, NONULL(tmp->path), sizeof (buffer));
     mutt_pretty_mailbox (buffer);
 
-    add_folder (menu, state, buffer, &s, tmp->new);
+    add_folder (menu, state, buffer, &s, tmp->new, tmp->unseen, tmp->messages);
   }
   while ((tmp = tmp->next));
   browser_sort (state);
--- ./browser.h	2001/12/16 13:35:45	1.1
+++ ./browser.h	2001/12/16 13:42:38
@@ -31,6 +31,8 @@
   char *desc;
 
   unsigned short new;
+  unsigned short unseen;
+  unsigned short messages;
 #ifdef USE_IMAP
   char delim;
   
--- ./init.h	2001/12/16 20:03:37	1.1
+++ ./init.h	2001/12/16 20:02:41
@@ -533,7 +533,9 @@
   ** .dt %F  .dd file permissions
   ** .dt %g  .dd group name (or numeric gid, if missing)
   ** .dt %l  .dd number of hard links
-  ** .dt %N  .dd N if folder has new mail, blank otherwise
+  ** .dt %M  .dd blank for non-IMAP folders; IMAP: number of messages
+  ** .dt %N  .dd N if folder has new mail, blank otherwise (IMAP: number of RECENT messages)
+  ** .dt %U  .dd blank for non-IMAP folders; IMAP: number of UNSEEN messages
   ** .dt %s  .dd size in bytes
   ** .dt %t  .dd * if the file is tagged, blank otherwise
   ** .dt %u  .dd owner name (or numeric uid, if missing)


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011006 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.24i (2001-11-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.0 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}
* milestone changed to 2.0

--------------------------------------------------------------------------------
2008-10-31 19:35:51 UTC jukka
* cc changed to j+mutt@salmi.ch
