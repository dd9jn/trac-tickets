Ticket:  3536
Status:  new
Summary: patterns truncated at 1024 characters

Reporter: acorn
Owner:    mutt-dev

Opened:       2011-08-22 19:04:42 UTC
Last Updated: 2011-08-22 19:04:42 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Mutt seems to truncate patterns to 1024 characters.

This makes it difficult to filter on very long patterns.

To reproduce:

1) Send yourself 2 emails with these subjects:
     Axxxxxxxxxxxxxxxxxxxxxx1
     AyyyyyyyyyyyyyyyyyyyyyyB2

2) to see all emails type:
     l~A
   you should see both the emails

3) Use a long (>1024 char) limit pattern which should make the first email visible and the second invisible:
     l~s^A~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA.....................~sB

Notice that both emails are still visible.  The ~sB at the end of the pattern got truncated.

4) use l~A to see all emails again

5) use a similar pattern that is long but less than 1024 characters:

l~s^A~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA.....................~sB

Now only the 2nd email is visible.


(Unimportant Note: the above patterns match any subject that starts with A (~s^A) and contains A (~sA) and contains B (~sB).  The subjects must also be at least 22 characters long (~sA.....................).  The reason I used a pattern with .... is so that the truncation results in a valid pattern (truncation is in the middle of the dots).  If you use a long pattern that gets truncated in a strange location you will instead see a "missing parameter" error.  Here is an example:

l~s^A~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sA~sB

Thanks!
-Acorn
