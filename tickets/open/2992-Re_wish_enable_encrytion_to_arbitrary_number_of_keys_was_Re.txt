Ticket:  2992
Status:  new
Summary: Re: wish: enable encrytion to arbitrary number of keys (was: Re:

Reporter: Paul Walker <paul@black-sun.demon.co.uk>
Owner:    mutt-dev

Opened:       2007-11-23 12:53:32 UTC
Last Updated: 2008-05-18 02:14:21 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
On Fri, Nov 23, 2007 at 03:34:54AM -0500, Derek Martin wrote:

> While I don't really oppose this, it seems to me that the far saner
> way to deal with this is for the mailing list software to allow the
> subscribing users to upload their public key, and to make the mailing

It would make it easier in some cases, but you do lose security - anybody
who can somehow get themselves onto the list can read it, and anybody
compromising the mailing list host automatically gets to read everything. If
the senders do the encryption, that doesn't happen.

Whether most people need that level of security is another question, but
then again most people don't use encrypted mailing lists at all... :-)


--------------------------------------------------------------------------------
2007-11-23 12:53:33 UTC Paul Walker
* id changed to 2992

--------------------------------------------------------------------------------
2008-05-18 02:02:35 UTC brendan
* component changed to crypto
* Updated description:
On Fri, Nov 23, 2007 at 03:34:54AM -0500, Derek Martin wrote:

> While I don't really oppose this, it seems to me that the far saner
> way to deal with this is for the mailing list software to allow the
> subscribing users to upload their public key, and to make the mailing

It would make it easier in some cases, but you do lose security - anybody
who can somehow get themselves onto the list can read it, and anybody
compromising the mailing list host automatically gets to read everything. If
the senders do the encryption, that doesn't happen.

Whether most people need that level of security is another question, but
then again most people don't use encrypted mailing lists at all... :-)
* milestone changed to 2.0
* priority changed to minor
* type changed to enhancement

--------------------------------------------------------------------------------
2008-05-18 02:05:18 UTC brendan
* cc changed to pdmef@gmx.net
* Added comment:
(remove fleas from cc list)
