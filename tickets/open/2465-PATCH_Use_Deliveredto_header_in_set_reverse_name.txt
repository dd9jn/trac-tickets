Ticket:  2465
Status:  new
Summary: [PATCH] Use Delivered-to header in set_reverse_name()

Reporter: nate-mutt@refried.org
Owner:    mutt-dev

Opened:       2006-09-03 20:30:45 UTC
Last Updated: 2006-09-07 05:35:02 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
This patch makes use of the Delivered-to: header when building the From: address in set_reverse_name().  This is needed so that alternate addresses can be found in emails sent from mailing lists where the alternate address isn't in the normal To: or Cc: headers.
>How-To-Repeat:
>Fix:
Included
}}}

--------------------------------------------------------------------------------
2006-09-07 09:45:21 UTC Derek Martin <code@pizzashack.org>
* Added comment:
{{{
On Sun, Sep 03, 2006 at 09:30:45PM +0200, nate-mutt@refried.org wrote:
> This patch makes use of the Delivered-to: header when building the
> From: address in set_reverse_name().  This is needed so that
> alternate addresses can be found in emails sent from mailing lists
> where the alternate address isn't in the normal To: or Cc: headers.

I'm not sure if this is a particularly useful approach...  I have
mails delivered by two different MTAs, and none of them carry a
"Delivered-To" header...

-- 
Derek D. Martin
http://www.pizzashack.org/
GPG Key ID: 0x81CFE75D
}}}

--------------------------------------------------------------------------------
2006-09-07 11:26:49 UTC Kyle Wheeler <kyle-mutt-dev@memoryhole.net>
* Added comment:
{{{
On Thursday, September  7 at 04:45 AM, quoth Derek Martin:
> On Sun, Sep 03, 2006 at 09:30:45PM +0200, nate-mutt@refried.org 
> wrote:
> > This patch makes use of the Delivered-to: header when building the
> > From: address in set_reverse_name().  This is needed so that
> > alternate addresses can be found in emails sent from mailing lists
> > where the alternate address isn't in the normal To: or Cc: headers.
> 
> I'm not sure if this is a particularly useful approach...  I have
> mails delivered by two different MTAs, and none of them carry a
> "Delivered-To" header...

On MTAs that do add the header, it is also often not the same as the 
address that is exposed to the outside world. For example, since 
memoryhole.net is a virtual domain on my server, my mails all have 
Delivered-To: memoryhole.net-kyle-mutt-dev@memoryhole.net, even though 
that's definitely not my email address.

~Kyle
-- 
The whole art of government consists in the art of being honest.
                  -- Thomas Jefferson: Rights of British America, 1774

--CEUtFxTsmBsHRLs3
Content-Type: application/pgp-signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Comment: Thank you for using encryption!

iD8DBQFE/5+JBkIOoMqOI14RAq6BAKD+/DBsnDIJc9GMf8NE4PDo/JHhfwCgqo1q
jXNzIwFFwmfCqLEaEGY0TZg=JZC2
-----END PGP SIGNATURE-----

--CEUtFxTsmBsHRLs3--
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:32 UTC 
* Added attachment use-delivered-to.patch
* Added comment:
use-delivered-to.patch
