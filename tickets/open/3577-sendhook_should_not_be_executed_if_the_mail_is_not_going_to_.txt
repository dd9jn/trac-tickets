Ticket:  3577
Status:  new
Summary: send-hook should not be executed if the mail is not going to be sent

Reporter: Y_Plentyn
Owner:    mutt-dev

Opened:       2012-04-22 15:21:30 UTC
Last Updated: 2012-04-23 11:24:42 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from  http://bugs.debian.org/641457 :

{{{
I have a hook like this:

send-hook '~s keyword' "push '<edit-subject><kill-line>Thank you<enter>'"

The idea is that I want the subject to be automatically modified if it contains
a specific keyword. This works well when you actually reply to the mail (or
write a new mail).

But if you exit the text editor without having done any change, the confirmation
screen is not displayed but the "push" is still executed and the various letters
end up executing multiple commands that should not have been executed because
they were intended for the "Compose menu" and not the "Index menu".

So the send-hook/send2-hook should really only be executed if the mail is going
to be sent...

And ideally if a reply is cancelled, a push recorded in a reply-hook should
also be cancelled. Because my first try was to use a reply-hook for my need.
:-)

Cheers,
}}}

--------------------------------------------------------------------------------
2012-04-23 11:24:42 UTC vinc17
* Added comment:
I think this bug is invalid: as documented, send-hook is executed "after getting the initial list of recipients" (and before the editor is executed, so that the editor gets correct data). Thus Mutt cannot know whether the message will be modified or not at this time. The user apparently needs send2-hook, which "is matched every time a message is changed".

BTW, I don't think that "push" should be allowed in these hooks (or at least it should be discouraged), because the behavior depends on the context (options...). If the goal is to set some subject in a hook, there's the my_hdr command for that.
