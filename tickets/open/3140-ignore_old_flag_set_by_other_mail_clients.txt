Ticket:  3140
Status:  new
Summary: ignore old flag set by other mail clients

Reporter: adrian13
Owner:    brendan

Opened:       2008-12-14 14:33:59 UTC
Last Updated: 2009-01-03 18:58:46 UTC

Priority:  minor
Component: IMAP
Keywords:  patch

--------------------------------------------------------------------------------
Description:
I am occasionally using other mail clients to access my mailbox over IMAP. The IMAP server, however, adds to all new mails which have not been read the old flag (Status: O). I would prefer if all unread mails in mutt would all be marked as "New" and not some as "Old" and some as "New". I tried to get a patch included into dovecot but it was refused as it would violate the IMAP RFC.

The attached patch let's mutt ignore that flag completely, with a new config option called "ignore_old_flag". All "Old" flagged mails are now still flagged as "New".

--------------------------------------------------------------------------------
2008-12-14 14:34:30 UTC adrian13
* Added attachment ignore_old_flag.patch
* Added comment:
ignore_old_flag.patch

--------------------------------------------------------------------------------
2008-12-15 12:16:44 UTC Moritz Barsnick
* Added comment:
{{{
What about this good old option? It defaults to "yes". I always set it
to "no", because I also don't like the "Old" tag:

3.118. mark_old

   Type: boolean
   Default: yes

   Controls whether or not mutt marks new unread messages as old if you
   exit a mailbox without reading them. With this option set, the next
   time you start mutt, the messages will show up with an "O" next to
   them in the index menu, indicating that they are old.
}}}

--------------------------------------------------------------------------------
2008-12-15 12:28:51 UTC Moritz Barsnick
* Added comment:
{{{
Sorry,
I withdraw my previous comment.  I am probably wrong and you are right.

The manual section I quoted speaks of how mutt marks, not of how mutt
interprets.  You wanted to change the latter.
}}}

--------------------------------------------------------------------------------
2008-12-15 13:00:29 UTC adrian13
* Added comment:
Replying to [comment:2 Moritz Barsnick]:
> Sorry,
> I withdraw my previous comment.  I am probably wrong and you are right.
> 
> The manual section I quoted speaks of how mutt marks, not of how mutt
> interprets.  You wanted to change the latter.

Right ;-). I was also confused and trying to get mark_old to ignore old marked messages.
Unfortunately it is only tells mutt to not mark messages as "Old" upon leaving the mailbox and not how it treats already "Old" marked messages.

--------------------------------------------------------------------------------
2009-01-03 18:48:41 UTC brendan
* milestone changed to 2.0

--------------------------------------------------------------------------------
2009-01-03 18:58:46 UTC brendan
* Added comment:
I'm going to defer this until after 1.6. The approach you've taken is a bit hackish, I'm afraid. It might be better to ignore `Status: O` iff the server supports custom persistent IMAP flags (this is how mutt marks messages old). But I'm afraid that this might not be enough for some environments, so I think such a patch needs a testing window.

* component changed to IMAP
* owner changed to brendan
