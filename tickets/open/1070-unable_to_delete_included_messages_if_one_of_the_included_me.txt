Ticket:  1070
Status:  new
Summary: unable to delete included messages if one of the included message is pgp-signed/encrypted

Reporter: mss@mawhrin.net (Mikhail Sobolev)
Owner:    mutt-dev

Opened:       2002-02-26 10:23:18 UTC
Last Updated: 2009-06-30 14:19:11 UTC

Priority:  minor
Component: crypto
Keywords:  #498

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.25-2
Severity: normal

I encountered this while receiving a mailing list digest.  There was a
number of pgp-signed messages in it.  When I tried to delete some of the
attched messages (it did not matter if they were signed or not), I got
the message

    Deletion of attachments from PGP messages is unsupported.

This digest is not a PGP message at all.  I was able to reproduce this
(just forwarded a signed message to myself and then tried to delete it).

Maybe this is just by design or something?  For me, it was a bit unexpected.

Regards,

--
Misha

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011002 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.25i (2002-01-01)
Copyright (C) 1996-2001 Michael R. Elkins É ÄÒÕÇÉÅ.
Mutt ÒÁÓÐÒÏÓÔÒÁÎÑÅÔÓÑ âåú ëáëéè-ìéâï çáòáîôéê; ÄÌÑ ÐÏÌÕÞÅÎÉÑ ÂÏÌÅÅ
ÐÏÄÒÏÂÎÏÊ ÉÎÆÏÒÍÁÃÉÉ ××ÅÄÉÔÅ `mutt -vv'.
Mutt Ñ×ÌÑÅÔÓÑ Ó×ÏÂÏÄÎÙÍ ÐÒÏÇÒÁÍÍÎÙÍ ÏÂÅÓÐÅÞÅÎÉÅÍ.  ÷Ù ÍÏÖÅÔÅ
ÒÁÓÐÒÏÓÔÒÁÎÑÔØ ÅÇÏ ÐÒÉ ÓÏÂÌÀÄÅÎÉÉ ÏÐÒÅÄÅÌÅÎÎÙÈ ÕÓÌÏ×ÉÊ; ÄÌÑ ÐÏÌÕÞÅÎÉÑ
ÂÏÌÅÅ ÐÏÄÒÏÂÎÏÊ ÉÎÆÏÒÍÁÃÉÉ ××ÅÄÉÔÅ `mutt -vv'.

System: Linux 2.4.17 (i586) [using ncurses 5.2]
ðÁÒÁÍÅÔÒÙ ËÏÍÐÉÌÑÃÉÉ:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
þÔÏÂÙ Ó×ÑÚÁÔØÓÑ Ó ÒÁÚÒÁÂÏÔÞÉËÁÍÉ, ÉÓÐÏÌØÚÕÊÔÅ ÁÄÒÅÓ <mutt-dev@mutt.org>.
þÔÏÂÙ ÓÏÏÂÝÉÔØ ÏÂ ÏÛÉÂËÅ, ÉÓÐÏÌØÚÕÊÔÅ ÐÒÏÇÒÁÍÍÕ flea(1).

patch-1.3.24.rr.compressed.1
patch-1.3.24.appoct.2
patch-1.3.15.sw.pgp-outlook.1
patch-1.3.24.admcd.gnutls.1
Md.use_editor
Md.paths_mutt.man
Md.muttbug_no_list
Md.use_etc_mailname
Md.muttbug_warning
Md.gpg_status_fd
patch-1.3.25.cd.edit_threads.9.1
patch-1.2.xtitles.1
patch-1.3.23.1.ametzler.pgp_good_sign


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-06 22:53:42 UTC brendan
* Added comment:
{{{
Cross-reference with #498.
}}}

--------------------------------------------------------------------------------
2009-06-30 14:19:11 UTC pdmef
* component changed to crypto
* Updated description:
{{{
Package: mutt
Version: 1.3.25-2
Severity: normal

I encountered this while receiving a mailing list digest.  There was a
number of pgp-signed messages in it.  When I tried to delete some of the
attched messages (it did not matter if they were signed or not), I got
the message

    Deletion of attachments from PGP messages is unsupported.

This digest is not a PGP message at all.  I was able to reproduce this
(just forwarded a signed message to myself and then tried to delete it).

Maybe this is just by design or something?  For me, it was a bit unexpected.

Regards,

--
Misha

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011002 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.25i (2002-01-01)
Copyright (C) 1996-2001 Michael R. Elkins É ÄÒÕÇÉÅ.
Mutt ÒÁÓÐÒÏÓÔÒÁÎÑÅÔÓÑ âåú ëáëéè-ìéâï çáòáîôéê; ÄÌÑ ÐÏÌÕÞÅÎÉÑ ÂÏÌÅÅ
ÐÏÄÒÏÂÎÏÊ ÉÎÆÏÒÍÁÃÉÉ ××ÅÄÉÔÅ `mutt -vv'.
Mutt Ñ×ÌÑÅÔÓÑ Ó×ÏÂÏÄÎÙÍ ÐÒÏÇÒÁÍÍÎÙÍ ÏÂÅÓÐÅÞÅÎÉÅÍ.  ÷Ù ÍÏÖÅÔÅ
ÒÁÓÐÒÏÓÔÒÁÎÑÔØ ÅÇÏ ÐÒÉ ÓÏÂÌÀÄÅÎÉÉ ÏÐÒÅÄÅÌÅÎÎÙÈ ÕÓÌÏ×ÉÊ; ÄÌÑ ÐÏÌÕÞÅÎÉÑ
ÂÏÌÅÅ ÐÏÄÒÏÂÎÏÊ ÉÎÆÏÒÍÁÃÉÉ ××ÅÄÉÔÅ `mutt -vv'.

System: Linux 2.4.17 (i586) [using ncurses 5.2]
ðÁÒÁÍÅÔÒÙ ËÏÍÐÉÌÑÃÉÉ:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
þÔÏÂÙ Ó×ÑÚÁÔØÓÑ Ó ÒÁÚÒÁÂÏÔÞÉËÁÍÉ, ÉÓÐÏÌØÚÕÊÔÅ ÁÄÒÅÓ <mutt-dev@mutt.org>.
þÔÏÂÙ ÓÏÏÂÝÉÔØ ÏÂ ÏÛÉÂËÅ, ÉÓÐÏÌØÚÕÊÔÅ ÐÒÏÇÒÁÍÍÕ flea(1).

patch-1.3.24.rr.compressed.1
patch-1.3.24.appoct.2
patch-1.3.15.sw.pgp-outlook.1
patch-1.3.24.admcd.gnutls.1
Md.use_editor
Md.paths_mutt.man
Md.muttbug_no_list
Md.use_etc_mailname
Md.muttbug_warning
Md.gpg_status_fd
patch-1.3.25.cd.edit_threads.9.1
patch-1.2.xtitles.1
patch-1.3.23.1.ametzler.pgp_good_sign


>How-To-Repeat:
>Fix:
}}}
