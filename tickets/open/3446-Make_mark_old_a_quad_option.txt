Ticket:  3446
Status:  new
Summary: Make "mark_old" a quad option

Reporter: rata
Owner:    mutt-dev

Opened:       2010-08-13 22:14:08 UTC
Last Updated: 2010-11-06 05:37:39 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
(just almost c&p from[1], reporting in bugzilla as suggeted[2])

Hi,

The attached patch makes the "mark_old" option to be a quad option.
I find this useful because I don't always want to mark mails as old, just sometimes. And in fact, I only want mutt to ask me about it only on certain mailboxes. So making it a quad option lets me use mutt this way (this patch + a folder hook to set that option on some mailboxes)

After writing the patch, of course ;), I found that there was a third party patch that does this (and more things)[3]. I didn't find any reason why that patch was not applied, so I thought perhaps it was because for some of the other things that patch does and this patch could be applied anyways.

This is my first time writing a patch for mutt so, even in a so simple patch, I can have done something fundamentally wrong or something like that. My apologies if that is the case :)

Now about the patch itself.
There's one thing that I really doubt: the change to pop.c. I dont know what the correct thing to do is. I'm not sure if checking for ==M_YES is ok, or checking for != M_NO, or.. I don't know.

I've tried this with local mail only (so I didn't try the change to pop.c I guess) and have checked the patch with "check_sec.sh".

If the patch looks ok I can send a v2 perhaps changing the text to "Mark new unread mails as old?" and, if needed, rebase on top of current HEAD too ?




Thanks a lot,
Rodrigo


PS: I'm not subscribed to mutt-dev anymore, please let me know if I need to be.

[1]: http://marc.info/?l=mutt-dev&m=127585905107686&w=2
[2]: http://marc.info/?l=mutt-dev&m=128165837701038&w=2
[3]: http://www.schrab.com/aaron/mutt/

--------------------------------------------------------------------------------
2010-08-13 22:15:00 UTC rata
* Added attachment make-mark_old-quad.patch
* Added comment:
Make "mark_old" a quad option

--------------------------------------------------------------------------------
2010-08-13 22:16:40 UTC rata
* cc changed to rodrigo@sdfg.com.ar

--------------------------------------------------------------------------------
2010-08-13 22:22:11 UTC rata
* Added comment:
Sorry, there are some newlines missing and I screw it with the footnotes. And it seems I can't edit it, I hope it isn't too much trouble.




Thanks,

Rodrigo

--------------------------------------------------------------------------------
2010-11-06 05:37:39 UTC rata
* Added comment:
Ping ? This has been posted on June for the first time and never received a review. I will really appreciate if someone can review this.



Thanks,
Rodrigo
