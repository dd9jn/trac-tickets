Ticket:  795
Status:  new
Summary: mutt shows "T" in index even when the other recipient is the sender

Reporter: ma@dt.e-technik.uni-dortmund.de (Matthias Andree)
Owner:    mutt-dev

Opened:       2001-09-26 05:57:46 UTC
Last Updated: 2005-09-04 18:26:07 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.22.1i
Severity: wishlist

-- Please type your report below this line

When a sender puts his From: address on the Cc: list, and there is no other
recipient besides the mutt user, in the index, the letter "T" appears next to
the message.

It'd be nice if mutt recognized the sender Cc'd himself and displayed a "+",
"L" or " " (depending on context) instead of the "T".

-- Mutt Version Information

Mutt 1.3.22.1i (2001-08-30)
Copyright (C) 1996-2001 Michael R. Elkins und andere.
Mutt übernimmt KEINERLEI GEWÄHRLEISTUNG. Starten Sie `mutt -vv', um
weitere Details darüber zu erfahren. Mutt ist freie Software. 
Sie können es unter bestimmten Bedingungen weitergeben; starten Sie
`mutt -vv' für weitere Details.

System: Linux 2.4.10-ma1 [using ncurses 5.0]
Einstellungen bei der Compilierung:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, verwenden Sie bitte das Programm flea(1).



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-05 12:26:07 UTC brendan
* Added comment:
{{{
Refiled as change-request.
}}}
