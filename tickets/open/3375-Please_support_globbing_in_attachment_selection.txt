Ticket:  3375
Status:  new
Summary: Please support globbing in attachment selection

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2010-01-31 18:29:26 UTC
Last Updated: 2010-02-01 17:30:45 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/552149
---

{{{
I often want to attach several files to a mail.  It would help if mutt
supported globbing in the prompt for an attachment filename, so I could
say /path/to/*.patch.

}}}

Cheers
Antonio

--------------------------------------------------------------------------------
2010-02-01 17:30:45 UTC Simon Ruderich
* Added comment:
{{{
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

On Sun, Jan 31, 2010 at 06:29:26PM -0000, Mutt wrote:

As some kind of workaround you could change the file mask with
'm' to something like '.*.patch'. Then tag the messages with 't'
and use ;<enter> to attach them.

But globbing would be a much better solution.

Hope this helps,
Simon
- -- 
+ privacy is necessary
+ using gnupg http://gnupg.org
+ public key id: 0x92FEFDB7E44C32F9
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)

iQIcBAEBCAAGBQJLZr6zAAoJEJL+/bfkTDL57NUQAJqRMg3gXA5HhOFf1AhutzC9
AVR5icnahpZsnJz0crneHP13CZrqRJHq/m2vSNbrDIcaNb0mnXjwPJFNYJuxGCKD
+uJapXhlyVc2vtC9QO77sTjWXPvnGM8BFt+3NfHKbMMspmWIdjLAW2nrcHClW36Y
2UUF1EXmu1vXOZb61ua9oXfslIeFi8XD9ZiIWhMEDQVdB0fJplVNl2yIgtJtk+JD
pfAakuvabVaKSvzwVc9UKtSKbZoEYDqiN+Tt7qC4hg4/sWGCDQxNEshRWp4NzigT
jwNbxa3g8YqDCbgj82W40nz3eC7egjWbKax8KBYsx/rtHzUDtVHTLsoWE3Vnlmij
k/No2Jx8KBVHd2LW7VV7jTjyiQOuiEr2/2883d5yzLM6rtWF7D54cUzxIIdc166G
rgRFy36016Zk4Q3Bns7cqN1H1nkjUPB7q/+ZvuGyjaKSmOIlrPvdufMjuPX1sEji
Tblytd2SV9cfnDEm4hutAcusBGBmPotMHRw5UHeqViNpM1h+MN+ggkQdcH6fZYh5
NiADxV2sJtn0bZMG8FP9+FmCOr7Y1pM/375FLAwWrzFuHHcO5wrbD0FQwkv3lXKa
AvmzERMswy3RERmdaBwnE8eBruoT1xmZ5gLHGVbvaTYNTFEpjWNHZNwx+z2WNfq3
DNxZ0A1+VQwsFKxjsm/T
=qpJx
-----END PGP SIGNATURE-----
}}}
