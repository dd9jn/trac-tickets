Ticket:  1507
Status:  new
Summary: Mutt jumps into incorrect maildir

Reporter: dean@mail.vipersoft.co.uk
Owner:    mutt-dev

Opened:       2003-03-23 13:45:26 UTC
Last Updated: 1970-01-01 00:00:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.1i
Severity: normal

-- Please type your report below this line

When viewing the maildir list after leaving a directory, if 
new mail appears in another maildir, and you try to go into 
that second maildir, mutt actually goes into the first maildir.

To help combat the problem I have had to add this to my mutt
key bindings.

macro index "q" "<change-folder>=Postponed<enter><change-folder>?<tab>" "Browse folders"


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i586-pc-linux-gnu/2.95.3/specs
gcc version 2.95.3 20010315 (release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20 (i586) [using ncurses 5.2]
Compile options:
DOMAIN="vipersoft.co.uk"
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
-ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2003-03-25 22:53:05 UTC John Iverson <johni@flash.net>
* Added comment:
{{{
* On Tue, 25 Mar 2003, Michael Elkins wrote:

> On 2003-03-22, dean@mail.vipersoft.co.uk wrote:
> > When viewing the maildir list after leaving a directory, if 
> > new mail appears in another maildir, and you try to go into 
> > that second maildir, mutt actually goes into the first maildir.
> 
> Can you please explain in a little more detail what the problem is?  I
> can't figure out what you are trying to explain...

He may be experiencing the behavior discussed here:
http://www.mail-archive.com/mutt-users@mutt.org/msg31360.html

-- 
John
}}}
