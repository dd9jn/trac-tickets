Ticket:  975
Status:  new
Summary: pgp-hook confirmation relief and automatic key selection

Reporter: Dale Woolridge <dale-list-mutt-dev@woolridge.org>
Owner:    mutt-dev

Opened:       2002-01-14 16:54:07 UTC
Last Updated: 2007-03-27 02:47:32 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.25i
Severity: wishlist

-- Please type your report below this line

As has been noted before (bug #343), use of pgp-hook unnecessarily prompts
for confirmation about the key selected via the pgp-hook for a particular
recipient.  Furthermore, it is often the case with pgp-hook that a single
key is selected, yet a list of keys (usually 1 with multiple uids) is
presented for selection.  It has been noted that since the recipient argument
for pgp-hook is a regular expression, confirmation is necessary to avoid
false positives matching the hook.  Still, I believe it should be possible
to circumvent the hook confirmation if so desired.  Even one could avoid
the hook confirmation, a list of keys is always presented, which brings
me to the other (related) item.  Whether key matching is done via pgp-hook
or using recipient address(es), there are often cases where a single key
is listed (sometimes with multiple uids).  Again, the decision to show a
list of keys when there is only one (real) key should be up to the user.

This patch
    http://www.woolridge.org/mutt/patches/patch-1.3.25.dw.pgp-hook.3
implements the above desired features by adding two variables: pgp_confirmhook
and pgp_autoselectkey.  The names were selected in an attempt to be as similar
as possible to existing variables.  The patch also allows multiple pgp-hook's
per recipient, which I personally don't use, but it might be useful for other
people (something like bug #973 comes to mind).  Documentation for the
added variables is also included with the patch.


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /opt/gcc-2.95.2/lib/gcc-lib/i586-pc-linux-gnu/2.95.2/specs
gcc version 2.95.2 19991024 (release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.25i (2002-01-01)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.2.19 (i586) [using ncurses 4.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  -HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/var/qmail/bin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/opt/share/mutt"
SYSCONFDIR="/opt/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.3.25.dw.pgp-hook.3


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-03-27 20:47:32 UTC brendan
* Added comment:
{{{
dedupe
}}}
