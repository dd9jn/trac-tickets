Ticket:  3253
Status:  new
Summary: mutt: Please make "myself not in M-F-T to posts in subscribed lists" optional

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-06-13 09:15:13 UTC
Last Updated: 2009-06-13 09:15:13 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/426142
{{{
I like to get CCs of replies to posts I make to mailing lists. Mutt
hardcodes that I will not, in the Mail-Followup-To header it will
add. Here's the patch I use to change that. Use of "list" instead of
"subscribe" is not adequate for that because "lists" does not have the
effect of "subscribe" on %L and %T in index_format, nor on ~u in a
search/tag/limit pattern. It is desirable to keep that effect (because
I _am_ subscribed to the said mailing lists), but I just _do_ want CCs
in subthreads I participate in.

For general consumption, it should be changed to make it an option. Or
even better, have "subscribe", "list" and "subscribe-cc" so that the
choice is per-list.
}}}
