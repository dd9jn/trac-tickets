Ticket:  3006
Status:  new
Summary: mutt-1.5.17: MIME boundaries should not be displayed on the only text/plain part

Reporter: vasily@korytov.pp.ru
Owner:    mutt-dev

Opened:       2007-12-23 23:21:26 UTC
Last Updated: 2009-07-10 01:58:37 UTC

Priority:  major
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{
Package: mutt
Version: 1.5.17
Severity: normal

-- Please type your report below this line

Alpine, a successor of Pine MUA, by default sends 8-bit mail as MIME
multipart/mixed with only one text/plain part. For example:

,----
| [...]
| User-Agent: Alpine 0.99999 (DEB 796 2007-11-08)
| MIME-Version: 1.0
| Content-Type: MULTIPART/MIXED;
| BOUNDARY="-1463811583-823278545-1198451178=:5415"
|
|   This message is in MIME format.  The first part should be readable
|   text,
|   while the remaining parts are likely unreadable without MIME-aware
|   tools.
|
| ---1463811583-823278545-1198451178=:5415
| Content-Type: TEXT/PLAIN; format=flowed; charset=KOI8-R
| Content-Transfer-Encoding: 8BIT
|
| ôÅÓÔ
| ---1463811583-823278545-1198451178=:5415--
`----

Mutt displays this message correctly, but it prepends the display with a
text/PLAIN boundary. This style:

,----
| [...]
| User-Agent: Alpine 0.99999 (DEB 796 2007-11-08)
|
| [-- Attachment #1 --]
| [-- Type: text/PLAIN, Encoding: 8bit, Size: 0.1K --]
|
| ôÅÓÔ
`----

My suggestion is that Mutt really should not display this boundary if
the message has an only text/plain part.


-- System Information
System Version: FreeBSD hub.flexpro.ru 6.3-RC1 FreeBSD 6.3-RC1 #0: Mon Dec  3 13:15:16 MSK 2007     root@hub.flexpro.ru:/usr/src/sys/i386/compile/HUB  i386

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc -I/usr/local/include
Using built-in specs.
Configured with: FreeBSD/i386 system compiler
Thread model: posix
gcc version 3.4.6 [FreeBSD] 20060305

- CFLAGS
-O2 -fno-strict-aliasing -pipe -march=pentiumpro

-- Mutt Version Information

Mutt 1.5.17 (2007-11-01)
Copyright (C) 1996-2007 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: FreeBSD 6.3-RC1 (i386)
ncurses: ncurses 5.6.20061217 (compiled with 5.6)
libiconv: 1.9
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE
-USE_FCNTL  +USE_FLOCK   -USE_INODESORT
+USE_POP  +USE_IMAP  -USE_SMTP  +USE_GSS  +USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  +HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX  +COMPRESSED
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

vvv.quote
patch-1.5.0.ats.date_conditional.1
dgc.deepif.1
vvv.initials
rr.compressed

--- Begin /home/deskpot/.mutt/muttrc
set from="vasily@korytov.pp.ru"
set envelope_from
set use_from
alternates (noc|abuse|moscowusers)@flexpro.ru
set folder="{korytov.pp.ru}"
set imap_user="$from"
mailboxes =INBOX =INBOX.Root =INBOX.Announces =INBOX.Comments =INBOX.Junk
set spoolfile="=INBOX"
set mbox="=INBOX"
set postponed="=INBOX.Drafts"
set record="=INBOX.Sent"
set allow_8bit
set use_8bitmime
set send_charset="us-ascii:koi8-r:utf-8"
charset-hook windows-1251 cp1251
charset-hook us-ascii utf-8
unset crypt_autosign
unset crypt_autoencrypt
unset crypt_replysign
unset crypt_replyencrypt
unset crypt_replysignencrypted
set crypt_verify_sig="yes"
unset pgp_show_unusable
set editor="emacs -nw"
set edit_headers
set fast_reply
set mime_forward
set include="yes"
set abort_nosubject="no"
set abort_unmodified="yes"
set postpone="ask-yes"
set reply_to="yes"
set attribution="On %d, %n wrote:"
set pager_index_lines=4
unset arrow_cursor
set sort="threads"
set sort_aux="date-received"
set reply_regexp="^(re([\[0-9\]+])*:[ \t]*|re([\[0-9\]+])*:[ \t]re([\[0-9\]+])*:[ \t])"
set tilde
unset markers
set pager_context=0
set pager_stop
set quote_regexp="^([a-z]+[|>}#]|[ \t]+[a-z]+[|>}#]|[|>}#]|[ \t]+[|>}#])+"
set print="ask-no"
set move="ask-yes"
set delete="yes"
set mail_check=30
set timeout=30
set beep
set beep_new
unset mark_old
unset wait_key
unset help
unset confirmappend
ignore *		# this means "ignore all lines by default"
unignore From: Subject: To: Cc: Bcc: Date:
unignore Mail-Followup-To: Followup-To: Reply-To:
unignore User-Agent: X-Mailer:
unhdr_order *		# forget the previous settings
hdr_order From: Reply-To: Subject: To: Cc: Bcc: Date:
bind pager "\Cv" next-page
bind pager "\033v" previous-page
bind pager <BackSpace> previous-page
bind pager "\177" previous-page
color normal		white		black
color attachment	green		black
color bold		blue		white
color error		red		white
color hdrdefault	cyan		black
color indicator		white		red
color markers		red		black
color message		white		blue
color quoted		green		black
color quoted1		yellow		black
color quoted2		green		black
color quoted3		yellow		black
color signature		red		black
color status		white		blue
color tilde		cyan		black
color tree		red		black
color underline		yellow		white
color header		yellow		black \
	"^(((Delivery|Resent)-)?Date|X-Received): *"
color header		yellow		black \
	"^(Organization|Subject): *"
color header		green		black \
	"^(From|(X-)?Sender|Return-Path): *"
color header		green		black \
	"^(Reply|Mail-Followup|Return-Receipt|Errors)-To: *"
color header		green		black \
	"^Resent-(From|Sender|Reply-To): *"
color header		green		black \
	"^(((Delivered|X-Comment|Apparently)-)?To|(B)?Cc): *"
color header		green		black \
	"^Resent-(To|Cc|Bcc): *"
color header		red		black \
	"^(Message-ID|Resent-Message-ID|References|In-Reply-To): *"
color body		magenta		black \
	"((ftp|http)://|(www|ftp).|(news|URL):)[-a-zA-Z_0-9./~?=&:]+"
color body		magenta		black \
	"mailto:[-a-zA-Z_0-9][-a-zA-Z_0-9./:]+@[-a-zA-Z_0-9.]+"
color body		magenta		black \
	"[-a-z_0-9][-a-z_0-9./:]+@[-a-z_0-9.]+"
set alias_file="~/.mutt/aliases"
source ~/.mutt/aliases
set certificate_file="~/.mutt/certificates"
source ~/.mutt/pgp
--- End /home/deskpot/.mutt/muttrc


--- Begin /usr/local/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb "<search>~b " "search in message bodies"
macro index,pager \cb "<pipe-message> urlview<Enter>" "call urlview to extract URLs out of a message"
macro attach,compose \cb "<pipe-entry> urlview<Enter>" "call urlview to extract URLs out of a message"
macro generic,pager <F1> "<shell-escape> less /usr/local/share/doc/mutt/manual.txt<Enter>" "show Mutt documentation"
macro index,pager y "<change-folder>?<toggle-mailboxes>" "show incoming mailboxes list"
bind browser y exit
attachments   +A */.*
attachments   -A text/x-vcard application/pgp.*
attachments   -A application/x-pkcs7-.*
attachments   +I text/plain
attachments   -A message/external-body
attachments   -I message/external-body
--- End /usr/local/etc/Muttrc
}}}

--------------------------------------------------------------------------------
2007-12-26 17:20:34 UTC Marco d'Itri
* Added comment:
{{{
On Dec 24, Mutt <fleas@mutt.org> wrote:

>  Alpine, a successor of Pine MUA, by default sends 8-bit mail as MIME
>  multipart/mixed with only one text/plain part. For example:
Spam filters will love this...
}}}

--------------------------------------------------------------------------------
2008-07-09 12:46:23 UTC pdmef
* component changed to display

--------------------------------------------------------------------------------
2009-07-10 01:58:37 UTC bandoswuu
* Added comment:
thanks. I've updated it.
[http://www.jj54.com/ error fixes]
