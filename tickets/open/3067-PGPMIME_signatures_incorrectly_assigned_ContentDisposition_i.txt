Ticket:  3067
Status:  started
Summary: PGP/MIME signatures incorrectly assigned "Content-Disposition: inline"

Reporter: jrollins
Owner:    me

Opened:       2008-05-31 17:45:31 UTC
Last Updated: 2010-08-24 19:51:40 UTC

Priority:  minor
Component: crypto
Keywords:  openpgp mime content-disposition

--------------------------------------------------------------------------------
Description:
I believe that mutt is incorrectly assigning the Content-Disposition of PGP/MIME signature attachments as "inline".  This causes some MUAs (eg. gmail) to feel the need to display the attached signature inline, instead of as an attachment.  I believe that the Content-Disposition should instead be set to "attachment", since that is in fact what it is.

I notice, though, that the Content-Disposition is specifically hard coded in the mutt pgp.c source to be "DISPINLINE" instead of "DISPATTACH".  I wonder why this is, since it appears to be a deliberate choice.  This seems wrong to me, but I would be curious to hear a justification.

The patch is very very simple.  It involves changing [source:pgp.c@5410:73a180edce53#L1111] from:
{{{
  t->disposition = DISPINLINE;
}}}
to
{{{
  t->disposition = DISPATTACH;
}}}
I applied this patch to the Debian Lenny version 1.5.17 of mutt and it worked perfectly.  I haven't checked it with the current head of your repo, but I have no reason to suspect it wouldn't work.

If there is reasonable justification for keeping the default to be DISPINLINE, then maybe we could make a patch where the value of the Content-Disposition is set by a mutt variable, herefore allowing users to easily change it if need be.  I think that it should we set DISPATTACH, though, as PGP/MIME signatures should be attachments, not inline.

Thanks for maintaining such a great MUA.

--------------------------------------------------------------------------------
2008-06-12 06:08:57 UTC brendan
* Added comment:
I'm not sure I agree that the disposition should be attachment. Although the RFC doesn't specify one way or the other, t->disposition was set to inline by the author of mutt and the PGP/MIME RFC in [c4f80a425496], 8 years ago. Since the content-type is correct, enlightened mailers can do the right thing, and unenlightened mailers can display the signature instead of a somewhat meaningless attachment.

Personally I think it's too close to 1.6 to make this kind of change, but I'll keep the bug open for discussion. Hopefully others will chime in.

* component changed to crypto
* type changed to enhancement

--------------------------------------------------------------------------------
2008-06-12 14:09:35 UTC jmcclelland
* Added comment:
> unenlightened mailers can display the signature instead of a somewhat meaningless attachment.

I'm not sure an uninterpreted inline signature is more meaningful than an uninterpreted attachment. Without the signature being processed by gpg is all a bunch of gobblygook.

> Personally I think it's too close to 1.6 to make this kind of change, but I'll keep the bug open for discussion. Hopefully others will chime in.

Since I'm not really close the the development cycle of mutt - I don't have an opinion on ''when'' the change should be made and fully understand the need to make changes at the right time to avoid surprises.

However, I do think the change proposed by jrollins is a good one. I think the safest change would be to make it a .muttrc controlled variable that defaults to inline (so if people who actually rely on the old behavior upgrade they won't be screwed).

Thanks for the great work on mutt!!

--------------------------------------------------------------------------------
2008-06-12 14:18:35 UTC dkg
* Added comment:
Replying to [comment:1 brendan]:
> enlightened mailers can do the right thing, and unenlightened mailers can display the signature instead of a somewhat meaningless attachment.

The trouble with this approach is that the signature itself is meaningless unless you have an enlightened mailer.

So the question becomes: if you're corresponding with folks who have unenlightened mailers, do you want them to see a big chunk of random-looking gobbledy-gook?  or would you rather they see a well-defined, isolated attachment?  As unenlightened as some mailers can be, modern ones can basically all handle attachments and present them intelligibly to the user, even when they don't understand the content type.  For [http://lists.debian.org/debian-devel-announce/2008/05/msg00004.html example], the debian lists web interface doesn't (can't) interpret OpenPGP signatures, but it does isolate them to separate nodes instead of displaying their contents inline.  This seems like reasonable behavior.  If mutt can encourage it in other MUAs, it would be good.

A third option not yet mentioned would be to omit the `Content-Disposition:` header entirely for the signature sub-part.  [http://gnus.org my MUA] does this when it emits signed messages, and it seems to cause every MUA i've encountered to behave reasonably (mailers which don't know how to interpret the the `Content-Type: application/pgp-signature` isolate it to an attachment, and mailers which do know how to interpret it do the Right Thing).

* keywords changed to openpgp mime content-disposition

--------------------------------------------------------------------------------
2008-06-12 14:51:18 UTC jrollins
* Added comment:
Thanks a lot for your response, brendan.  I appreciate you keeping this ticket open to discussion as well.

While I think it may mean something that the RFC author set the disposition this way, I'm still not sure I think it's the correct approach.  The PGP/MIME signature is *supposed* to be an attachment, which is in the RFC.  It therefore seems that setting the disposition to be "inline", and therefore suggesting that MUAs display the signature inline, just doesn't make sense, at least not to me.

I also agree with dkg that displaying signature is more meaningless that leaving it as an attachment.  If the signature is not being processed by an OpenPGP tool, then it really is just a random string of characters, in which case it really doesn't make sense to display it.

I think either the suggestion to make it a config-controllable option, or removing the Content-Disposition header entirely are better than leaving the disposition hard coded in.

My broader worry is that the current setting might discourage some people from using or accepting PGP signatures because they might think it makes there email "look messy".  This might seem like a trite concern, but it is a reaction I've encountered many times because of this issue.

Thanks again for your time with this.

--------------------------------------------------------------------------------
2008-06-13 05:06:54 UTC Thomas Roessler
* Added comment:
{{{
On 2008-06-12 14:51:18 -0000, Mutt wrote:

>  While I think it may mean something that the RFC author set the
>  disposition this way, I'm still not sure I think it's the correct
>  approach.  

>  The PGP/MIME signature is *supposed* to be an attachment, which
>  is in the RFC.  It therefore seems that setting the disposition
>  to be "inline", and therefore suggesting that MUAs display the
>  signature inline, just doesn't make sense, at least not to me.

The PGP/MIME spec is silent on whether or not Content-Disposition
should be sent, or to what value.

The notion that the PGP/MIME signature is "supposed to be an
attachment", and that that somehow influences the choice of value
for Content-Disposition is bogus: The signature is yet another body
part (commonly called "attachment"), but that doesn't mean that
handling that part as an "attachment" in the sense of the
Content-Disposition header is somehow more or less reasonable.

My suspicion is that we added the current code to mutt as a hack to
make things work better with some other mailer -- maybe it was about
keeping Eudora from cluttering attachment directories, maybe it was
about something else.  I don't remember.

>  I think either the suggestion to make it a config-controllable
>  option, or removing the Content-Disposition header entirely are
>  better than leaving the disposition hard coded in.

A config-controlled option for this would be ridiculous, sorry.

Cheers,
}}}

--------------------------------------------------------------------------------
2008-06-13 19:17:31 UTC jmcclelland
* Added comment:

> The notion that the PGP/MIME signature is "supposed to be an
> attachment", and that that somehow influences the choice of value
> for Content-Disposition is bogus: 

That makes a lot of sense. Thanks for the explanation. Given the lack of
clarity in the spec, what do you think of leaving the Content-Disposition
header out entirely?

> A config-controlled option for this would be ridiculous, sorry.

I don't understand why that would be ridiculous. Can you explain or point me
to documentation that might help me understand that? Thanks.

--------------------------------------------------------------------------------
2008-06-16 10:35:57 UTC Thomas Roessler
* Added comment:
{{{
>> The notion that the PGP/MIME signature is "supposed to be an
>> attachment", and that that somehow influences the choice of
>> value for Content-Disposition is bogus:

>  That makes a lot of sense. Thanks for the explanation. Given the
>  lack of clarity in the spec, 

Please do not confuse "lack of clarity" with "it doesn't matter".
In this case, it simply doesn't matter.

> what do you think of leaving the Content-Disposition header out
> entirely?

As I said, I don't see a big downside, but suspect that the code for
that was put in for some reason that related to compatibility with
some other MUA's behavior.

>> A config-controlled option for this would be ridiculous, sorry.

>  I don't understand why that would be ridiculous. Can you explain
>  or point me to documentation that might help me understand that?
>  Thanks.

If anybody is supposed to understand what's going on with that
parameter, it's us.  Given that we're evidently confused about the
reason for the current setting, it strikes me as silly to expect
users of the software to have a better clue about that choice.

If you want to change it for your own install, feel free to comment
it out in the source.  But I don't think there's value to having an
additional option for it in the main distribution.

Cheers,
}}}

--------------------------------------------------------------------------------
2008-06-16 14:58:38 UTC jrollins
* Added comment:
In multi-part email, all of the body parts are commonly called
attachments, which should not influence how a given part is displayed.
This is what Thomas mentions above, and I completely agree with this
statement.  What *is* meant to influence how a given part is displayed
is the Content-Disposition header.  This is the point of this header,
to suggest to MUAs how a particular part should be displayed if the
MUA is unfamiliar it's MIME type.

OpenPGP signatures, as PGP/MIME type parts, should, by the most
enlightened MUAs, be processed as OpenPGP signatures, and the outcome
of that processing should be displayed to the user.

MUAs that are unfamiliar with or unable to process PGP/MIME parts
should look to the Content-Disposition header to determine how to
display the part.

MUAs that can not process signatures and are currently displaying
PGP/MIME parts from mutt inline (ie. gmail) are in fact doing the
right thing since they are honoring the Content-Disposition header as
set.  MUAs can of course choose to ignore the Content-Disposition and
display the part as they wish, but that is up to them, and they do so
at their own peril.

I think this argument shows clearly that parts with
Content-Disposition set to "inline" should be displayed inline, and
parts with Content-Disposition set to "attachment" should be displayed
as attachments.  Conversely, parts that should be displayed inline
should have Content-Disposition set to "inline", and parts that should
be displayed as attachments should have Content-Disposition set to
"attachment".

The question that remains is about how PGP/MIME parts should be
displayed by MUAs that can not process them.  Unprocessed PGP/MIME
parts are meaningless character strings, and should therefore never be
displayed inline.  Thus the Content-Disposition for PGP/MIME parts
should not be "inline".  I think jmcclelland, dkg, and I have all
argued for this fairly persuasively.  If this reasoning is correct,
then the Content-Disposition of PGP/MIME parts should be set to
"attachment" and what mutt is currently doing is wrong.

I understand that making a new user-configurable variable for this
would be too much work.  I therefore believe that the most logical
solution is to change to Content-Disposition for PGP/MIME parts to be
"attachment".  Since this is a fairly trivial change, I hope that we
can see it in a future version.

Thank you very much for your attention to this matter.

--------------------------------------------------------------------------------
2010-08-24 19:32:31 UTC me
* Added comment:
The pgp-signature body part was labelled inline simply because when I added the content-disposition support, I failed to support the case for "no preference" in which case the header field is omitted.

I agree with Thomas that it does not make sense to label it as "attachment" since it has no meaning apart from the multipart/signed.

I've attached a patch which makes Mutt be able to omit the Content-Disposition, and will now omit it for the multipart/signed with pgp-signature.

* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2010-08-24 19:33:07 UTC me
* Added attachment mutt-nodisp.diff
* Added comment:
patch to allow mutt to omit content-disposition

--------------------------------------------------------------------------------
2010-08-24 19:51:40 UTC me
* status changed to started
