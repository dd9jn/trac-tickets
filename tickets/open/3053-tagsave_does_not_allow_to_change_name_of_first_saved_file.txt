Ticket:  3053
Status:  new
Summary: tag-save does not allow to change name of first saved file

Reporter: ulrich_scholz
Owner:    mutt-dev

Opened:       2008-05-08 08:24:46 UTC
Last Updated: 2008-05-08 08:24:46 UTC

Priority:  minor
Component: mutt
Keywords:  tag-save

--------------------------------------------------------------------------------
Description:
tag-save followed by a folder allows to save several attachments to the same folder.  All but the first file names can be changed/adjusted before saving.  It's a defect that the first name cannot be changed.

version 1.5.11 
