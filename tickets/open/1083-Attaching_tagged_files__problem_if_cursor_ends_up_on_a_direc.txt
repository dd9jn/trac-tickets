Ticket:  1083
Status:  new
Summary: Attaching tagged files - problem if cursor ends up on a directory

Reporter: William Berriss <William.Berriss@vil.ite.mee.com>
Owner:    mutt-dev

Opened:       2002-03-05 07:07:01 UTC
Last Updated: 2005-10-19 17:13:20 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.25i
Severity: normal

-- Please type your report below this line

Hi

I have auto_tag turned on and auto advance to next message feature.
Bascially, when I go to attach 5 files say from a given directory,
I hit tab twice to open that dir and then hit the T key
for each of the files that I want to attach/ When done I just
hit ENTER and usually it works as expected.

However, if the last file that I hit T for has a directory
after it (in the list), and because my auto advance feature
puts the cursor on the line after the tagged file
(i.e the directory) then hitting ENTER takes me down into the directory.

viz.

17 *   -rw-r--r--  1 user1 group1     1956 Feb 27 15:42  wrapperprogram.dia
18 *   -rw-r--r--  1 user1 group1     17433 Mar 01 09:13 wrapperprogram.ps
19     drwxr-sr-x  2 user1 group1     4096 Mar 01 09:26  trash/

As you can see, I tag items 18 and 19 by hitting t and the cursor ends
up on item 19, OK. Then I hit enter to say attach these 2 and
mutt takes me to the directory trash/.  Perhaps it is supposed to
do this but it threw me at first. I guess the use of the enter key
to say FINISHED or to say ENTER DIR is the problem?

HTH

Will
--

-- Mutt Version Information

Mutt 1.3.25i (2002-01-01)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.9-13 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /home/berrisw/.muttrc
macro index \A "!abook\n" "Abook"
macro pager \A "!abook\n" "Abook"
set realname="William Berriss"
set sendmail="/usr/sbin/sendmail -oem -oi"
set spoolfile=imap://staff/INBOX 
set folder=imap://staff/INBOX 
set from="William.Berriss@vil.ite.mee.com"
set alternates="william.berriss@vil.ite.mee.com"
set history=100
set print_cmd="mpage -P -1 -f -o -bA4" ## -f = fold long lines
set abort_nosubject=ask-yes
set abort_unmodified=ask-yes
set alias_file="~/.mutt/.aliases"
source ~/.mutt/.aliases
source ~/.mutt/.aliases-vil
source ~/.mutt/.aliases-mitsubishi
source ~/.mutt/.aliases-private
set alias_format="%2n %-20t %-30a %r"
unset allow_8bit
unset arrow_cursor
unset ascii_chars
unset askbcc
set askcc
set attach_format="%u%D%t%2n %T%.90d%> [%.7m/%.10M, %.6e, %s] "
set attach_split
unset autoedit
set auto_tag
set beep
set beep_new
set bounce_delivered
set charset="iso-8859-1"
unset check_new
set collapse_unread
unset confirmappend
set confirmcreate
set copy=yes
set date_format="%a %b %d %H:%M (%Z)"
set default_hook="((~f %s) !~P) | (~P ~t %s)"
set delete=ask-yes
set edit_headers
set editor="vim -c 'set ruler formatoptions=tl expandtab tw=72 noautoindent' '+/^$' '+noh' "
set escape=~
set fast_reply
source ~/.mutt/.keybindings                     # my key bindingsi
source ~/.mutt/.fcc-save-hooks                     # my fcc and save hooks
folder-hook =[^I] exec collapse-all
set followup_to
set force_name
set forward_decrypt
set forward_format="Fwd: %s"
set forward_quote
set weed
unhdr_order *
hdr_order From Date: From: To: Cc: Bcc: Fcc: Subject:
set hdrs
set help
set hidden_host
set history=150
set hostname="staff.vil.ite.mee.com"
set imap_user=berrisw
unignore *
ignore *
unignore date delivery-date to from: subject cc importance pgp
set ignore_list_reply_to
set include=yes
set indent_string="> "
set index_format="%3C%?M?+& ?%Z %{%a %d %b %Y %H:%M} %-25.25F %-50.50s %?M?[%e/%E]&(%3c)?"
set ispell="aspell -e -c"
set mail_check=600
unset mark_old
set markers
set mask=".*"
set mbox_type=Maildir
unset metoo
set menu_scroll
unset mh_purge
set mime_forward=ask-yes
set move=no
set pager=builtin
set pager_context=1
set pager_format="%Z [%C/%m] %-20.20n %-50.50s %> [%e/%E mesgs] %l lines (%c)"
set pager_index_lines=12
set pager_stop
source ~/.mutt/gpg.rc           # Start up with GnuPG by default
set pgp_good_sign="good"
set pgp_entry_format="%4n %t%f %[%y/%m/%d] %4l/0x%k %-4a %2c %u"
unset pipe_decode
unset pipe_split
set postpone=ask-yes
set postponed="~/.mutt/.postponed"
set print=ask-no
unset prompt_after
set query_command = "mutt_ldap_query2.pl -s staff.vil.ite.mee.com -s ldap-cc.vil.ite.mee.com -b dc=vil,dc=com '%s' 2> /dev/null"
set quit=ask-yes
set quote_regexp="^([ \t]*[>%:|])+" # less inclusive, useful w/mail w/code
set read_inc=1
unset read_only
set recall=ask-no
set record="=spammers"
set reply_regexp="^((re|aw|antw.):[ \t]*)*"
unset reply_self
set reply_to=ask-yes
set reverse_alias
set reverse_name
unset save_address
set save_empty
set save_name
set shell="bash"
set sig_dashes
set signature="~/.signature"
set smart_wrap
set smileys="(>From)|(:[-^]?[][)(><}{|/DP])"
color body brightgreen white "<[Gg]>"
color body brightgreen white "<[Bb][Gg]>"
color body red default  " >?[;:][-^]?[][)(><}{|/DP][)}]*"
set sort=threads
set sort_aux=reverse-last-date-received
set sort_re
set status_chars=" *%A"
set status_format="%v@%h:%f (Sort=%s) [%M/%m(%P of %LB)] Limit=%V %> [postponed=%p|n=%n|u=%u|t=%t]"
set status_on_top
unset strict_threads
set suspend
set thorough_search
set tilde
set timeout=60
set tmpdir="~/tmp"
set to_chars="LTGCF"
unset uncollapse_jump
unset use_8bitmime
set use_domain
set use_from
set visual="vim"
set wait_key
set wrap_search
set write_inc=1
color body        magenta  default  "[^ <]*:\/\/[^ >]*" # for URLs
color normal            default         default
color hdrdefault        red             default
color header         green       default ^(From|Subject):
color signature         cyan            default
color indicator         black           white
color error             brightred       default
color status            green           default
color tree              magenta         default # the thread tree in the index menu
color tilde             magenta         default
color message           red            default
color markers           brightblue            default
color attachment        magenta default
color search            default         green   # how to hilite searches in the pager
color quoted            cyan            default # quoted text
color quoted1           magenta         default
color quoted2           red                     default
color quoted3           green           default
color quoted4           blue            default
color quoted5           brightcyan            default
color quoted6           magenta         default
color quoted7           red                     default
color quoted8           green           default
color quoted9           cyan            default
color underline         brightgreen default
color index             green default ~F
color body                      magenta         default "((ftp|http|https)://|(file|news):|www\\.)[-a-z0-9_.:]*[a-z0-9](/[^][{} \t\n\r\"<>()]*[^][{} \t\n\r\"<>().,:!])?/?"
color body                      green            default [-a-z_0-9.]+@[-a-z_0-9.]+    # e-mail addresses
unhdr_order * # clear system defaults
hdr_order Date: From: Old-Return-Path: Reply-To: Mail-Followup-To: To: Cc: Bcc: Delivered-To: Subject: In-Reply-To: X-Mailing-List: X-Operating-System: X-Mailer: User-Agent:
my_hdr X-Operating-System: `uname -sr` on an `uname -m`
source ~/.mutt/.scores
--- End /home/berrisw/.muttrc


--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/share/doc/mutt-1.2.5i/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/share/doc/mutt-1.2.5i/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/share/doc/mutt-1.2.5i/manual.txt\n" "Show Mutt documentation"
set pgp_decode_command="gpg %?p?--passphrase-fd 0? --no-verbose --batch --output - %f"
set pgp_verify_command="gpg --no-verbose --batch --output - --verify %s %f"
set pgp_decrypt_command="gpg --passphrase-fd 0 --no-verbose --batch --output - %f"
set pgp_sign_command="gpg --no-verbose --batch --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="gpg --no-verbose --batch --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="pgpewrap gpg -v --batch --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="pgpewrap gpg --passphrase-fd 0 -v --batch --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="gpg --no-verbose --import -v %f"
set pgp_export_command="gpg --no-verbose --export --armor %r"
set pgp_verify_key_command="gpg --no-verbose --batch --fingerprint --check-sigs %r"
set pgp_list_pubring_command="gpg --no-verbose --batch --with-colons --list-keys %r" 
set pgp_list_secring_command="gpg --no-verbose --batch --with-colons --list-secret-keys %r" 
set pgp_getkeys_command=""
color hdrdefault red default
color quoted brightblue default
color signature red default
color indicator brightyellow red
color error brightred default
color status yellow blue
color tree magenta default	# the thread tree in the index menu
color tilde magenta default
color message brightcyan default
color markers brightcyan default
color attachment brightmagenta default
color search default green	# how to hilite search patterns in the pager
color header brightred default ^(From|Subject):
color body magenta default "(ftp|http)://[^ ]+"	# point out URLs
color body magenta default [-a-z_0-9.]+@[-a-z_0-9.]+	# e-mail addresses
color underline brightgreen default
mono quoted bold
set ispell="/usr/bin/aspell --mode=email check"
--- End /etc/Muttrc


Received: (at submit) by bugs.guug.de; 2 May 2002 09:02:29 +0000
From frederic.gobry@smartdata.ch Thu May 02 11:02:29 2002
Received: from smartdatapc1.epfl.ch ([128.179.50.2] helo=rhin.smartdata.ch)
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 173CTt-0005j7-00
	for <submit@bugs.guug.de>; Thu, 02 May 2002 11:02:29 +0200
Received: (from fred@localhost)
	by rhin.smartdata.ch (8.11.1/8.11.1) id g42947W23513;
	Thu, 2 May 2002 11:04:07 +0200
Date: Thu, 2 May 2002 11:04:07 +0200
From: Frédéric Gobry <frederic.gobry@smartdata.ch>
Message-Id: <200205020904.g42947W23513@rhin.smartdata.ch>
Subject: mutt-1.3.99i: tag-<enter> does not behave as expected
To: submit@bugs.guug.de

Package: mutt
Version: 1.3.99i
Severity: normal

-- Please type your report below this line
When I want to attach several files to a mail, I usually tag the files and
type ;-<enter> to select the group of files as a whole. This works unless
the cursor is located on a directory entry when I hit ;-<enter>. In that
case, the action is not to select the tagged files, but to enter the
directory and discard the previous selection, which is rather annoying...

This is a small incoherency, but in such a great tool that it is very
outstanding :-)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/egcs-2.91.66/specs
gcc version egcs-2.91.66 19990314/Linux (egcs-1.1.2 release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.99i (2002-05-02)
Copyright (C) 1996-2001 Michael R. Elkins et autres.
Mutt ne fournit ABSOLUMENT AUCUNE GARANTIE ; pour les détails tapez `mutt -vv'.
Mutt est un logiciel libre, et vous êtes libre de le redistribuer
sous certaines conditions ; tapez `mutt -vv' pour les détails.

System: Linux 2.4.18 (i686) [using slang 10202]
Options de compilation :
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/local/stow/mutt-1.3.99/share/mutt"
SYSCONFDIR="/usr/local/stow/mutt-1.3.99/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Pour contacter les développeurs, veuillez écrire à <mutt-dev@mutt.org>.
Pour signaler un bug, veuillez utiliser l'utilitaire flea(1).


--- Begin /home/fred/.muttrc
set spoolfile = "=Inbox"
auto_view text/html
set nowait_key
unignore x-swish
set mbox_type = maildir
set editor = "emacsnw -f text-mode "
set reply_regexp = "^(r[eé]p?\.?([\[0-9\]+])*|aw)[ \t]*:?[ \t]*"
set folder = ~/Mail
color index white red ~F
set mime_forward = ask-yes
set forward_decode = yes
set pop_user = frederic.gobry
set pop_host = mail.smartdata.ch
set pop_delete = yes
account-hook . 'unset imap_user; unset imap_pass; unset tunnel'
account-hook imap://puck.ch 'set tunnel="ssh gobry@puck.ch /usr/sbin/imapd"'
folder-hook . 'set from = "Frederic Gobry <frederic.gobry@smartdata.ch>"'
folder-hook puck.ch 'set from = "Frederic Gobry <gobry@puck.ch>"'
set alias_file  = ~/.mail/aliases
set copy   = yes
set record = "=Outgoing"
set pipe_decode = no
set check_new = yes
unignore x-sfighter
color header brightred 	black 	^(x-sfighter)
unignore x-mailfolder
macro index U ":set pipe_decode=yes\n|urlviewer --mail\n :set pipe_decode=no\n" \
        "Parse URL in message"
macro pager U ":set pipe_decode=yes\n|urlviewer --mail\n :set pipe_decode=no\n" \
        "Parse URL in message"
macro index S "l~h x-sfighter:\\ -----------\n" \
	"Limit view to the potential Spams"
macro index N "l~h x-sfighter:\\ .*------------$ ~N\n" \
	"Limit view to the good messages"
macro index A "l~A\n"                               \
	"Restore global view"
set print_command = "a2ps --pretty-print=mail"
subscribe software@pluton 	\
	  gconf-list		\
	  ecb-list		\
	  all@smartdata.ch 	\
	  gnupg-users 		\
          pybliographer-general \
	  bluetooth-dev		\
	  pygtk			\
	  uclinux-dev		\
	  bugtraq@securityfocus \
	  lyx-fr		\
	  pgui-devel
mailboxes	=Mailing-Lists \
		=MaybeSpam
source ~/.mail/aliases
source ~/.mail/hooks
source ~/.mail/gpg.rc
--- End /home/fred/.muttrc


--- Begin /usr/local/stow/mutt-1.3.99/etc/Muttrc
ignore *
unignore from: date subject to cc
unignore organization organisation x-mailer: x-newsreader: 
unignore x-idiap-info: x-mailing-list:
unignore posted-to: reply-to:
hdr_order = From: Date To Cc Subject
set abort_nosubject=ask-yes
set abort_unmodified=ask-yes
set arrow_cursor
set noconfirmappend
set fast_reply
set noforward_decode
set move=no
set postponed="=postponed"
set reverse_alias
set sig_dashes
set smart_wrap
set sort=threads
set tilde
bind pager <up>   previous-line
bind pager <down> next-line
mono  status    underline
mono  indicator reverse
mono  header    standout     ^.
mono  quoted    bold
color normal    black white
color tree      white blue
color status    brightwhite blue
color indicator brightwhite blue
color signature white black
color quoted    blue white
color tilde     black black
color search    white red
color markers   blue white
color header white 	black 	^.*
color header brightyellow 	black 	^(From)
color header brightwhite 	black 	^(Subject)
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
set query_command = "annu --mutt '%s'"
set charset="iso-8859-1"
set send_charset="iso-8859-1"
--- End /usr/local/stow/mutt-1.3.99/etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-10-08 04:55:06 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# solved by Lars Hecking
close 676
# no infos, no followups
close 1092
# reporter requested closing
close 1307
# unreproducible, no followups
close 1499
merge 907 1437
merge 1083 1176
severity 1562 wishlist
merge 1385 1562
retitle 1128 new subject breaks thread option
tags 64 patch
tags 123 patch
tags 1484 patch
tags 1489 patch
-- someone speaking Spanish should talk to #1572 reporter
}}}

--------------------------------------------------------------------------------
2005-10-20 11:13:20 UTC ab
* Added comment:
{{{
Add mutt/1176 reporter to notifieds, deduppe and devirus
unformatted.
}}}
