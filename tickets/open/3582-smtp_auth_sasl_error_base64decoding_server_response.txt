Ticket:  3582
Status:  infoneeded_new
Summary: smtp_auth_sasl: error base64-decoding server response

Reporter: tczengming
Owner:    mutt-dev

Opened:       2012-05-22 06:24:15 UTC
Last Updated: 2012-07-07 06:44:03 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
mutt can't send email
version : mutt 1.5.21
host: smtp.163.com  (gmail test ok)
command like this:echo "testmail" | mutt -d 5 -s "test" -e 'set smtp_url="smtp://user:pas@smtp.163.com:25/"' to@sina.com
{{{
System: Linux 2.6.37.6-0.11-desktop (i686)
ncurses: ncurses 5.7.20101009 (compiled with 5.7)
compile options：
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
-USE_POP  -USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE 
}}}
log:
{{{
[2012-05-22 14:12:58] Mutt/1.5.21 (2010-09-15) debugging at level 5
[2012-05-22 14:12:58] Reading configuration file '/usr/local/etc/Muttrc'.
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b0, *ldata = (nil)
[2012-05-22 14:12:58] parse_attach_list: added */.* [9]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b4, *ldata = (nil)
[2012-05-22 14:12:58] parse_attach_list: added text/x-vcard [7]
[2012-05-22 14:12:58] parse_attach_list: added application/pgp.* [2]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b4, *ldata = 0x80f2a08
[2012-05-22 14:12:58] parse_attach_list: skipping text/x-vcard
[2012-05-22 14:12:58] parse_attach_list: skipping application/pgp.*
[2012-05-22 14:12:58] parse_attach_list: added application/x-pkcs7-.* [2]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b8, *ldata = (nil)
[2012-05-22 14:12:58] parse_attach_list: added text/plain [7]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b4, *ldata = 0x80f2a08
[2012-05-22 14:12:58] parse_attach_list: skipping text/x-vcard
[2012-05-22 14:12:58] parse_attach_list: skipping application/pgp.*
[2012-05-22 14:12:58] parse_attach_list: skipping application/x-pkcs7-.*
[2012-05-22 14:12:58] parse_attach_list: added message/external-body [4]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49bc, *ldata = (nil)
[2012-05-22 14:12:58] parse_attach_list: added message/external-body [4]
[2012-05-22 14:12:58] send.c:1214: mutt_mktemp returns "/tmp/mutt-linux-008-1000-28292-13932838271170144133".
[2012-05-22 14:12:58] sendlib.c:2692: mutt_mktemp returns "/tmp/mutt-linux-008-1000-28292-726912188748030617".
[2012-05-22 14:12:58] mwoh: buf[Subject: test] is short enough
[2012-05-22 14:12:59] send.c:988: mutt_mktemp returns "/tmp/mutt-linux-008-1000-28292-2054191190810729236".
[2012-05-22 14:12:59] mwoh: buf[Subject: test] is short enough
[2012-05-22 14:12:59] Connected to smtp.163.com:25 on fd=4
[2012-05-22 14:12:59] 4< 220 gz-t-163smtp1.163.com SMTP Server for Netease [466] 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:12:59] 4> EHLO linux-008.site
[2012-05-22 14:12:59] 4< 250-gz-t-163smtp1.163.com
[2012-05-22 14:12:59] 4< 250-mail
[2012-05-22 14:12:59] 4< 250-PIPELINING
[2012-05-22 14:12:59] 4< 250-8BITMIME
[2012-05-22 14:12:59] 4< 250-AUTH LOGIN PLAIN
[2012-05-22 14:12:59] 4< 250-AUTH=LOGIN PLAIN
[2012-05-22 14:12:59] 4< 250 STARTTLS 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:12:59] 4> STARTTLS
[2012-05-22 14:12:59] 4< 220 2.0.0 Start TLS 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:12:59] ssl_check_preauth: hostname check passed
[2012-05-22 14:12:59] X509_STORE_load_locations failed
[2012-05-22 14:12:59] X509_verify_cert: unable to get local issuer certificate (20)
[2012-05-22 14:12:59]  [/serialNumber=p0JiiH1Hm6SVOaDND51IWyf9sq-PK1Td/C=CN/ST=Guangdong/L=Guangzhou/O=Guangzhou NetEase Computer System Co., Ltd./OU=MAIL Dept./CN=*.163.com]
[2012-05-22 14:12:59] X509_STORE_load_locations failed
[2012-05-22 14:12:59] ssl_check_preauth: signer check passed
[2012-05-22 14:12:59] trusted: /C=US/O=GeoTrust, Inc./CN=GeoTrust SSL CA
[2012-05-22 14:12:59] ssl_check_preauth: hostname check passed
[2012-05-22 14:12:59] X509_STORE_load_locations failed
[2012-05-22 14:12:59] ssl_check_preauth: signer check passed
[2012-05-22 14:13:00] 4> EHLO linux-008.site
[2012-05-22 14:13:00] 4< 250-gz-t-163smtp1.163.com
[2012-05-22 14:13:00] 4< 250-mail
[2012-05-22 14:13:00] 4< 250-PIPELINING
[2012-05-22 14:13:00] 4< 250-8BITMIME
[2012-05-22 14:13:00] 4< 250-AUTH LOGIN PLAIN
[2012-05-22 14:13:00] 4< 250 AUTH=LOGIN PLAIN 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:13:00] SASL local ip: 192.168.0.111;45452, remote ip:113.108.225.9;25
[2012-05-22 14:13:00] External SSF: 128
[2012-05-22 14:13:00] External authentication name: tczengming
[2012-05-22 14:13:00] 4> AUTH LOGIN
[2012-05-22 14:13:00] 4< 334 VXNlcm5hbWU6 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:13:00] smtp_auth_sasl: error base64-decoding server response.
[2012-05-22 14:13:01] mutt_free_body: unlinking /tmp/mutt-linux-008-1000-28292-13932838271170144133.
}}}

--------------------------------------------------------------------------------
2012-07-07 06:20:14 UTC brendan
* Updated description:
mutt can't send email
version : mutt 1.5.21
host: smtp.163.com  (gmail test ok)
command like this:echo "testmail" | mutt -d 5 -s "test" -e 'set smtp_url="smtp://user:pas@smtp.163.com:25/"' to@sina.com
{{{
System: Linux 2.6.37.6-0.11-desktop (i686)
ncurses: ncurses 5.7.20101009 (compiled with 5.7)
compile options：
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
-USE_POP  -USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE 
}}}
log:
{{{
[2012-05-22 14:12:58] Mutt/1.5.21 (2010-09-15) debugging at level 5
[2012-05-22 14:12:58] Reading configuration file '/usr/local/etc/Muttrc'.
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b0, *ldata = (nil)
[2012-05-22 14:12:58] parse_attach_list: added */.* [9]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b4, *ldata = (nil)
[2012-05-22 14:12:58] parse_attach_list: added text/x-vcard [7]
[2012-05-22 14:12:58] parse_attach_list: added application/pgp.* [2]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b4, *ldata = 0x80f2a08
[2012-05-22 14:12:58] parse_attach_list: skipping text/x-vcard
[2012-05-22 14:12:58] parse_attach_list: skipping application/pgp.*
[2012-05-22 14:12:58] parse_attach_list: added application/x-pkcs7-.* [2]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b8, *ldata = (nil)
[2012-05-22 14:12:58] parse_attach_list: added text/plain [7]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49b4, *ldata = 0x80f2a08
[2012-05-22 14:12:58] parse_attach_list: skipping text/x-vcard
[2012-05-22 14:12:58] parse_attach_list: skipping application/pgp.*
[2012-05-22 14:12:58] parse_attach_list: skipping application/x-pkcs7-.*
[2012-05-22 14:12:58] parse_attach_list: added message/external-body [4]
[2012-05-22 14:12:58] parse_attach_list: ldata = 0x80e49bc, *ldata = (nil)
[2012-05-22 14:12:58] parse_attach_list: added message/external-body [4]
[2012-05-22 14:12:58] send.c:1214: mutt_mktemp returns "/tmp/mutt-linux-008-1000-28292-13932838271170144133".
[2012-05-22 14:12:58] sendlib.c:2692: mutt_mktemp returns "/tmp/mutt-linux-008-1000-28292-726912188748030617".
[2012-05-22 14:12:58] mwoh: buf[Subject: test] is short enough
[2012-05-22 14:12:59] send.c:988: mutt_mktemp returns "/tmp/mutt-linux-008-1000-28292-2054191190810729236".
[2012-05-22 14:12:59] mwoh: buf[Subject: test] is short enough
[2012-05-22 14:12:59] Connected to smtp.163.com:25 on fd=4
[2012-05-22 14:12:59] 4< 220 gz-t-163smtp1.163.com SMTP Server for Netease [466] 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:12:59] 4> EHLO linux-008.site
[2012-05-22 14:12:59] 4< 250-gz-t-163smtp1.163.com
[2012-05-22 14:12:59] 4< 250-mail
[2012-05-22 14:12:59] 4< 250-PIPELINING
[2012-05-22 14:12:59] 4< 250-8BITMIME
[2012-05-22 14:12:59] 4< 250-AUTH LOGIN PLAIN
[2012-05-22 14:12:59] 4< 250-AUTH=LOGIN PLAIN
[2012-05-22 14:12:59] 4< 250 STARTTLS 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:12:59] 4> STARTTLS
[2012-05-22 14:12:59] 4< 220 2.0.0 Start TLS 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:12:59] ssl_check_preauth: hostname check passed
[2012-05-22 14:12:59] X509_STORE_load_locations failed
[2012-05-22 14:12:59] X509_verify_cert: unable to get local issuer certificate (20)
[2012-05-22 14:12:59]  [/serialNumber=p0JiiH1Hm6SVOaDND51IWyf9sq-PK1Td/C=CN/ST=Guangdong/L=Guangzhou/O=Guangzhou NetEase Computer System Co., Ltd./OU=MAIL Dept./CN=*.163.com]
[2012-05-22 14:12:59] X509_STORE_load_locations failed
[2012-05-22 14:12:59] ssl_check_preauth: signer check passed
[2012-05-22 14:12:59] trusted: /C=US/O=GeoTrust, Inc./CN=GeoTrust SSL CA
[2012-05-22 14:12:59] ssl_check_preauth: hostname check passed
[2012-05-22 14:12:59] X509_STORE_load_locations failed
[2012-05-22 14:12:59] ssl_check_preauth: signer check passed
[2012-05-22 14:13:00] 4> EHLO linux-008.site
[2012-05-22 14:13:00] 4< 250-gz-t-163smtp1.163.com
[2012-05-22 14:13:00] 4< 250-mail
[2012-05-22 14:13:00] 4< 250-PIPELINING
[2012-05-22 14:13:00] 4< 250-8BITMIME
[2012-05-22 14:13:00] 4< 250-AUTH LOGIN PLAIN
[2012-05-22 14:13:00] 4< 250 AUTH=LOGIN PLAIN 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:13:00] SASL local ip: 192.168.0.111;45452, remote ip:113.108.225.9;25
[2012-05-22 14:13:00] External SSF: 128
[2012-05-22 14:13:00] External authentication name: tczengming
[2012-05-22 14:13:00] 4> AUTH LOGIN
[2012-05-22 14:13:00] 4< 334 VXNlcm5hbWU6 529b9177-52e3-4e81-a5e4-a21fdf2ced48
[2012-05-22 14:13:00] smtp_auth_sasl: error base64-decoding server response.
[2012-05-22 14:13:01] mutt_free_body: unlinking /tmp/mutt-linux-008-1000-28292-13932838271170144133.
}}}

--------------------------------------------------------------------------------
2012-07-07 06:44:03 UTC brendan
* Added comment:
This SMTP server appears to be in violation of the RFC:
{{{
[2012-05-22 14:13:00] 4< 334 VXNlcm5hbWU6 529b9177-52e3-4e81-a5e4-a21fdf2ced48
}}}
The bit after the base64 should not be there. From RFC 4954:
{{{
continue-req    = "334" SP [base64] CRLF
                        ;; Intermediate response to the AUTH
                        ;; command.
                        ;; This non-terminal complies with
                        ;; syntax defined by Reply-line [SMTP].
}}}
also from that RFC:
  A server challenge is sent as a 334 reply with the text part
  containing the [BASE64] encoded string supplied by the SASL
  mechanism.  This challenge MUST NOT contain any text other
  than the BASE64 encoded challenge.
...
  If the server cannot [BASE64] decode any client response, it
  MUST reject the AUTH command with a 501 reply (and an enhanced
  status code of 5.5.2).  If the client cannot BASE64 decode any
  of the server's challenges, it MUST cancel the authentication
  using the "*" response.  In particular, servers and clients
  MUST reject (and not ignore) any character not explicitly
  allowed by the BASE64 alphabet, and MUST reject any sequence
  of BASE64 characters that contains the pad character ('=')
  anywhere other than the end of the string (e.g., "=AAA" and
  "AAA=BBB" are not allowed).

This doesn't leave a lot of room to adapt to this particular buggy server while remaining RFC-compliant ourselves. Options might be to set smtp_authenticators="PLAIN" or if that also doesn't work, to use SASL initial response (I'm not sure whether we don't already do that with PLAIN though).

Please try setting `smtp_authenticators="PLAIN"` and report on how that worked out.

* status changed to infoneeded_new
