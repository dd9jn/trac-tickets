Ticket:  2056
Status:  new
Summary: Attempt re-connect when idle mailbox connection closes

Reporter: gale@sefer.org
Owner:    brendan

Opened:       2005-09-06 22:38:34 UTC
Last Updated: 2016-07-30 15:49:34 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
As requested by Brendan Cully in PR#2049:
In some environments - possibly dependent on particular IMAP
server software, firewalls, routers, etc. - the connection
to an IMAP mailbox can close when it remains idle for more
than a few minutes. That can happen when the user returns
to the mailbox after viewing a message for a long time.

Currently, mutt automatically closes the mailbox when
this happens. This makes mutt nearly unusable in this
type of IMAP/network environment.

The correct behavior is for mutt to attempt to
reconnect to the mailbox. Mutt should only close
the mailbox if the reconnect attempt - or several
attempts - fails.

This seems to be standard behavior for most IMAP email
clients.

--------------------------------------------------------------------------------
2007-04-01 21:46:31 UTC brendan
* Updated description:
As requested by Brendan Cully in PR#2049:
In some environments - possibly dependent on particular IMAP
server software, firewalls, routers, etc. - the connection
to an IMAP mailbox can close when it remains idle for more
than a few minutes. That can happen when the user returns
to the mailbox after viewing a message for a long time.

Currently, mutt automatically closes the mailbox when
this happens. This makes mutt nearly unusable in this
type of IMAP/network environment.

The correct behavior is for mutt to attempt to
reconnect to the mailbox. Mutt should only close
the mailbox if the reconnect attempt - or several
attempts - fails.

This seems to be standard behavior for most IMAP email
clients.

--------------------------------------------------------------------------------
2007-04-01 21:48:55 UTC brendan
* owner changed to brendan

--------------------------------------------------------------------------------
2007-04-03 18:09:10 UTC brendan
* milestone changed to 2.0

--------------------------------------------------------------------------------
2008-08-27 10:19:23 UTC kuno
* cc changed to kuno@frob.nl

--------------------------------------------------------------------------------
2009-05-22 05:26:29 UTC atrus
* cc changed to kuno@frob.nl, mutt-bugs@atrus.rifetech.com

--------------------------------------------------------------------------------
2009-06-13 19:36:05 UTC brendan
* cc changed to kuno@frob.nl, mutt-bugs@atrus.rifetech.com, 521732@bugs.debian.org

--------------------------------------------------------------------------------
2010-08-05 23:18:58 UTC me
* Added comment:
Ran into this problem recently.  My workaround was to set $imap_keepalive to some lower bound that keeps the connection open.

--------------------------------------------------------------------------------
2011-05-09 06:36:46 UTC petr
* cc changed to kuno@frob.nl, mutt-bugs@atrus.rifetech.com, 521732@bugs.debian.org, petr.mvd@gmail.com

--------------------------------------------------------------------------------
2011-07-18 02:32:21 UTC jrn
* cc changed to kuno@frob.nl, mutt-bugs@atrus.rifetech.com, 521732@bugs.debian.org, petr.mvd@gmail.com, jrnieder@gmail.com

--------------------------------------------------------------------------------
2016-07-30 15:49:34 UTC TenLeftFingers
* cc changed to kuno@frob.nl, mutt-bugs@atrus.rifetech.com, 521732@bugs.debian.org, petr.mvd@gmail.com, jrnieder@gmail.com, jarlathreidy@gmail.com
