Ticket:  3647
Status:  new
Summary: Possibility to bind the 'abort' command of the command line editor.

Reporter: ytterbium
Owner:    mutt-dev

Opened:       2013-07-03 06:34:47 UTC
Last Updated: 2013-07-03 06:40:07 UTC

Priority:  major
Component: user interface
Keywords:  keybind

--------------------------------------------------------------------------------
Description:
It could be interesting to have the possibility to rebind the "abort" function of the command line editor, actually bind to ^G. As this functionality has no assigned function, it is impossible to bind it, while we don't come all of emacs-world, and we could prefer for example the <esc> key to the ^G.


--------------------------------------------------------------------------------
2013-07-03 06:40:07 UTC ytterbium
* Added comment:
And I miss my escaping of the !^G.

So I rewrite my ticket here : 
It could be interesting to have the possibility to rebind the "abort" function of the command line editor, actually bind to !^G. As this functionality has no assigned function, it is impossible to bind it, while we don't come all of emacs-world, and we could prefer for example the <esc> key to the !^G. 
