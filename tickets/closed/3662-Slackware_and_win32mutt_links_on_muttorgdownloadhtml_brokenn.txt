Ticket:  3662
Status:  closed
Summary: Slackware and win32mutt links on mutt.org/download.html broken/need updating

Reporter: MichaelRay
Owner:    mutt-dev

Opened:       2013-10-23 19:38:02 UTC
Last Updated: 2013-10-23 19:59:31 UTC

Priority:  trivial
Component: doc
Keywords:  URL, slackware, win32mutt

--------------------------------------------------------------------------------
Description:

Hello,

The link to the Slackware version of mutt is broken and needs to be updated on page:
http://www.mutt.org/download.html

it lists:
http://www.slackware.com/pb/download.php?q=current/mutt-1.4.2.1i-i486-1

which should be:
http://packages.slackware.com/?r=slackware-current&p=mutt-1.5.22-i486-1.txz

Also, the geocities domain is gone:
http://www.geocities.com/win32mutt/index.html

But oocities has the same content:
http://www.oocities.org/win32mutt/index.html



--------------------------------------------------------------------------------
2013-10-23 19:59:31 UTC me
* Added comment:
Updated, thanks.

FYI, the web site is under revision control if you want to send pull requests directly https://bitbucket.org/mutt/www-mutt-org

* resolution changed to fixed
* status changed to closed
