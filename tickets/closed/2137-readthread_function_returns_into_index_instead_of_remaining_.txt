Ticket:  2137
Status:  closed
Summary: read-thread function returns into index instead of remaining in pager

Reporter: ps@twin.jikos.cz
Owner:    mutt-dev

Opened:       2005-11-18 23:40:10 UTC
Last Updated: 2010-08-05 23:48:50 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
problem with thread sorting mode. when reading message in pager, standard C^N or C^D work well, while C^R mark 
messages read as should, BUT ALSO bring me back to index from pager. The same with <Esc>+R.


--------------------------------------------------------------------------------
2006-02-08 09:47:50 UTC Sanda Pavel <ps@ucw.cz>
* Added comment:
{{{
hi,

i made small patch for mutt/2137 bug.
it works for me, but i dont know much 
about mutt internals - could somebody check
it after me ?

its diff-ed against mutt-1.5.11.

pavel

--d6Gm4EdcadzBjdND
Content-Type: text/plain; charset=us-ascii
Content-Disposition: attachment; filename="diff.txt"

--- pagerold.c	2005-09-13 17:59:55.000000000 +0200
+++ pager.c	2006-02-07 14:24:48.000000000 +0100
@@ -2298,6 +2298,35 @@
 	}
 	break;
 
+      case OP_MAIN_READ_THREAD:
+      case OP_MAIN_READ_SUBTHREAD:
+	//CHECK_MSGCOUNT	//these are in OP_MAIN_READ_THREAD in curs_main.c
+	//CHECK_VISIBLE		//should it be here ?
+	CHECK_MODE(IsHeader (extra));
+	CHECK_READONLY;
+
+#ifdef USE_IMAP
+//CHECK_IMAP_ACL(IMAP_ACL_DELETE);	//should be here ACL_SEEN ?
+#endif
+
+	r = mutt_thread_set_flag (extra->hdr, M_READ, 1,
+				  ch == OP_MAIN_READ_THREAD ? 0 : 1);
+
+	if (r != -1)
+	{
+	  if (option (OPTRESOLVE))
+	  {
+	    rc = ( ch == OP_MAIN_READ_THREAD ? OP_MAIN_NEXT_THREAD : OP_MAIN_NEXT_SUBTHREAD );  
+	    ch = -1;
+	  }
+
+	  if (!option (OPTRESOLVE) && PagerIndexLines)
+	    redraw = REDRAW_FULL;
+	  else
+	    redraw = REDRAW_STATUS | REDRAW_INDEX;
+	}
+	break;
+
       case OP_DISPLAY_ADDRESS:
 	CHECK_MODE(IsHeader (extra) || IsMsgAttach (extra));
         if (IsMsgAttach (extra))

--d6Gm4EdcadzBjdND--
}}}

--------------------------------------------------------------------------------
2010-08-05 23:29:48 UTC me
* Updated description:
problem with thread sorting mode. when reading message in pager, standard C^N or C^D work well, while C^R mark 
messages read as should, BUT ALSO bring me back to index from pager. The same with <Esc>+R.
* milestone changed to 

--------------------------------------------------------------------------------
2010-08-05 23:48:50 UTC me
* Added comment:
fixed in rev 57d4b3839172

* resolution changed to fixed
* status changed to closed
