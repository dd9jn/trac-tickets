Ticket:  1864
Status:  closed
Summary: mutt: 2 From headers in each saved mbox mail

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2004-04-22 18:42:14 UTC
Last Updated: 2005-07-26 07:28:24 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1-20040112+1
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#245218.
Please Cc all your replies to 245218@bugs.debian.org .]

From: Brian May <bam@debian.org>
Subject: mutt: 2 From headers in each saved mbox mail
Date: Thu, 22 Apr 2004 09:25:12 +1000

When pushing s to save a message from Maildir to mbox format, mutt
inserts 2 From headers (I mean From, not from:)

eg:

>head ~/spam
>From Chaney@ameritech.net Wed Apr 21 12:33:55 2004
>From Chaney@ameritech.net  Wed Apr 21 12:33:55 2004
Return-Path: <Chaney@ameritech.net>
X-Original-To: bam@snoopy.apana.org.au
Delivered-To: bam@snoopy.apana.org.au
Received: from localhost (snoopy.apana.org.au [127.0.0.1])
        by snoopy.apana.org.au (Postfix) with ESMTP id 431EBD82E5
        for <bam@snoopy.apana.org.au>; Wed, 21 Apr 2004 12:33:55 +1000 (EST)
Received: from snoopy.apana.org.au ([127.0.0.1])
        by localhost (snoopy [127.0.0.1]) (amavisd-new, port 10024)

This confuses formail, which thinks there are exactly two messages in the
mbox file:

>formail -s head < ~/spam
>From Chaney@ameritech.net Wed Apr 21 12:33:55 2004
 
>From Chaney@ameritech.net  Wed Apr 21 12:33:55 2004
Return-Path: <Chaney@ameritech.net>
X-Original-To: bam@snoopy.apana.org.au
Delivered-To: bam@snoopy.apana.org.au
Received: from localhost (snoopy.apana.org.au [127.0.0.1])
        by snoopy.apana.org.au (Postfix) with ESMTP id 431EBD82E5
        for <bam@snoopy.apana.org.au>; Wed, 21 Apr 2004 12:33:55 +1000 (EST)
Received: from snoopy.apana.org.au ([127.0.0.1])
        by localhost (snoopy [127.0.0.1]) (amavisd-new, port 10024)
        with ESMTP id 06257-10 for <bam@snoopy.apana.org.au>;

Although this too is kindof weird, as there are a lot more then two messages
in the mbox:

 >grep '^From ' ~/spam | wc  --lines
112

However, if I manually delete the excess from lines, it all works fine.

-- System Information:
Debian Release: testing/unstable
  APT prefers unstable
  APT policy: (500, 'unstable')
Architecture: i386 (i686)
Kernel: Linux 2.4.25
Locale: LANG=C, LC_CTYPE=C

Versions of packages mutt depends on:
ii  libc6                       2.3.2.ds1-11 GNU C Library: Shared libraries an
ii  libidn11                    0.4.1-1      GNU libidn library, implementation
ii  libncursesw5                5.4-3        Shared libraries for terminal hand
ii  libsasl2                    2.1.18-4     Authentication abstraction library
ii  postfix [mail-transport-age 2.0.19-1     A high-performance mail transport 

-- no debconf information


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-07-27 01:28:24 UTC brendan
* Added comment:
{{{
This should be fixed by the MMDF copy patch committed two days ago.
}}}

* resolution changed to fixed
* status changed to closed
