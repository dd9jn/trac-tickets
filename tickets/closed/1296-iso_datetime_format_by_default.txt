Ticket:  1296
Status:  closed
Summary: iso date/time format by default

Reporter: Eduardo Pérez Ureta <eperez@it.uc3m.es>
Owner:    mutt-dev

Opened:       2002-08-02 13:39:41 UTC
Last Updated: 2005-11-20 12:05:02 UTC

Priority:  minor
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: wishlist
Tags: patch

This patch changes the default date/time format in mutt to conform to ISO
date/time standard.

This date format is more used in the world, and is easier to read, to
parse and to sort.

For a detailed description and advantages:
ISO 8601 http://www.cl.cam.ac.uk/~mgk25/iso-time.html

If you also like using ISO date/time format by default you should apply
this patch.

Mutt should follow standards whenever it's a good idea and possible.
}}}

--------------------------------------------------------------------------------
2002-08-02 13:39:41 UTC Eduardo Pérez Ureta <eperez@it.uc3m.es>
* Added attachment mutt_datetime_iso.diff
* Added comment:
mutt_datetime_iso.diff

--------------------------------------------------------------------------------
2002-08-07 06:56:40 UTC Glenn Maynard <g_mutt@zewt.org>
* Added comment:
{{{
On Thu, Aug 01, 2002 at 10:39:41PM +0000, Eduardo Pérez Ureta wrote:
> This patch changes the default date/time format in mutt to conform to ISO
> date/time standard.
> 
> This date format is more used in the world, and is easier to read, to
> parse and to sort.

Easier to read?  That's entirely subjective.  I can read "Tue, Aug 06, 2002 
at 07:37:48PM EDT" much more quickly and naturally than "2002-08-06
19:38:30 EDT".  I much prefer "Aug" over "08", and I hate 24-hour time.

Where are these dates used where parsing and sorting is important?

> For a detailed description and advantages:
> ISO 8601 http://www.cl.cam.ac.uk/~mgk25/iso-time.html

Quoting:

"It is not intended as a replacement for language-dependent worded date
notations such as "24. Dezember 2001" (German) or "February 4, 1995" (US
English)."

This is a language-dependent worded date notation.

> If you also like using ISO date/time format by default you should apply
> this patch.
> 
> Mutt should follow standards whenever it's a good idea and possible.

But it shouldn't apply standards where they're not intended to be used.

> -# set date_format="!%a, %b %d, %Y at %I:%M:%S%p %Z"
> +# set date_format="!%Y-%m-%d %H:%M:%S %Z"

I don't know why this disables locale translation by default, though.
For quoting, it's debatable (what language your quote string is in
depends on the mail, not the locale language), but in general I'd
expect users to prefer seeing dates in their own language, not in
English.

> -# set index_format="%4C %Z %{%b %d} %-15.15L (%4l) %s"
> +# set index_format="%4C %Z %[%Y-%m-%d] %-15.15L (%4l) %s"

Likewise, "Aug 05" is much more readable to me than "2002-08-06", and
there's certainly no need for textual sorting or parsing of the index.

-- 
Glenn Maynard
}}}

--------------------------------------------------------------------------------
2002-08-07 08:05:15 UTC Glenn Maynard <g_mutt@zewt.org>
* Added comment:
{{{
On Tue, Aug 06, 2002 at 08:15:21PM -0400, Thomas Dickey wrote:
> > > This patch changes the default date/time format in mutt to conform to ISO
> > > date/time standard.
> > > 
> > > This date format is more used in the world, and is easier to read, to
> > > parse and to sort.
> > 
> > Easier to read?  That's entirely subjective.  I can read "Tue, Aug 06, 2002 
> > at 07:37:48PM EDT" much more quickly and naturally than "2002-08-06
> > 19:38:30 EDT".  I much prefer "Aug" over "08", and I hate 24-hour time.
> > 
> > Where are these dates used where parsing and sorting is important?
> 
> filenames for instance (I use dates in my patch filenames to make them sort
> to make it simple to apply them in succession from a script).
> 
> one would like to say databases, but only naive databases store dates in
> text form.

Of course those are places where you might want to use this format, but
I was referring to date_format and index_format.

-- 
Glenn Maynard
}}}

--------------------------------------------------------------------------------
2002-08-07 16:42:22 UTC "Thomas E. Dickey" <dickey@herndon4.his.com>
* Added comment:
{{{
On Tue, 6 Aug 2002, Glenn Maynard wrote:

> On Tue, Aug 06, 2002 at 09:17:19PM -0400, Thomas Dickey wrote:
> > > Of course those are places where you might want to use this format, but
> > > I was referring to date_format and index_format.
> >
> > Offhand (long day) the only thing that comes to mind is the cutoff date options
> > for retrieval from rcs/sccs/whatever.  Some tools do allow a variety of
> > formats, but taking locale into account would unnecessarily complicate things
> > and make the tool less reliable due to operator error.
>
> Maybe I'm having a long day, too, but I don't know what RCS has to do with
> Mutt ...

You were asking where people might want to use the ISO date format because
it was simple to parse, so I was trying to find some useful results ;-)

-- 
T.E.Dickey <dickey@herndon4.his.com>
http://invisible-island.net
ftp://invisible-island.net
}}}

--------------------------------------------------------------------------------
2002-08-08 00:00:16 UTC Glenn Maynard <g_mutt@zewt.org>
* Added comment:
{{{
On Wed, Aug 07, 2002 at 05:42:22AM -0400, Thomas E. Dickey wrote:
> > > > Of course those are places where you might want to use this format, but
> > > > I was referring to date_format and index_format.
> > >
> > > Offhand (long day) the only thing that comes to mind is the cutoff date options
> > > for retrieval from rcs/sccs/whatever.  Some tools do allow a variety of
> > > formats, but taking locale into account would unnecessarily complicate things
> > > and make the tool less reliable due to operator error.
> >
> > Maybe I'm having a long day, too, but I don't know what RCS has to do with
> > Mutt ...
>
> You were asking where people might want to use the ISO date format because
> it was simple to parse, so I was trying to find some useful results ;-)

Sorry, I was asking how it being simple to parse was any benefit in
date_format and index_format.  :)  (I think it's not at all.  I suppose
someone might try to parse quote headers, but that's a losing battle
anyway.)

There are plenty of places where this format has technical advantages,
but I don't think this is one of them; I think this one's just personal
preference and nothing more.

-- 
Glenn Maynard
}}}

--------------------------------------------------------------------------------
2003-12-26 14:41:33 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# feature, not bug
close 1731
tags 844 patch
tags 1004 patch
tags 1296 patch
tags 1488 patch
tags 1637 patch
merge 1644 1685
tags 1644 patch
tags 1644 fixed
tags 1716 patch
tags 1716 fixed
tags 1721 patch
tags 1734 patch
-- broken threading: is #1116 fixed?
}}}

--------------------------------------------------------------------------------
2005-08-24 12:36:24 UTC rado
* Added comment:
{{{
better defaults support
}}}

--------------------------------------------------------------------------------
2005-09-11 13:22:52 UTC ab
* Added comment:
{{{
Devirused and despammed unform. Ok: This is a wish for
default change. It seems clear to me from discussion that
it is not a sensible change. At least there will never be a
consensus for it. I'd like to close mutt/1296. Any opponent?
}}}

--------------------------------------------------------------------------------
2005-09-23 09:02:02 UTC rado
* Added comment:
{{{
about to close
}}}

* status changed to new

--------------------------------------------------------------------------------
2005-09-23 09:02:03 UTC rado
* Added comment:
{{{
Not against closing, and I agree there'll never be a solution to suit
everyone.
However, mutt is not only/ primarily used in US/UK, meaning
the am/pm for hours could be changed in favour of more widespread usage (i.e. 24h).
}}}

--------------------------------------------------------------------------------
2005-09-23 12:47:33 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Rado,

 On Thursday, September 22, 2005 at 4:02:02 PM +0200, Rado Smiljanic wrote:

> mutt is not only/ primarily used in US/UK, meaning the am/pm for hours
> could be changed in favour of more widespread usage (i.e. 24h).

    How much the am/pm thing seems ugly to us, as a default setting it
is consistent with other defaults and especially the English C locale.
Anyone using another language has to change this anyway. I think it
makes sense to keep 12h clock as default.

    Even out of defaults: In International places where English is
spoken, I send a custom English attribution with am/pm time.


Bye!	Alain.
-- 
When you want to reply to a mailing list, please avoid doing so with
Novell GroupWise. This lacks necessary references and breaks threads.
}}}

--------------------------------------------------------------------------------
2005-10-22 09:23:10 UTC rado
* Added comment:
{{{
Hello Rado,
No agreement possible, no authorative decision.
}}}

* status changed to closed

--------------------------------------------------------------------------------
2005-10-22 09:23:11 UTC rado
* Added comment:
{{{
I guess this won't be solved in my life time. ;)
}}}

--------------------------------------------------------------------------------
2005-10-22 09:24:45 UTC rado
* Added comment:
{{{
Sorry, didn't mean to close it
}}}

* resolution changed to wontfix
* status changed to closed

--------------------------------------------------------------------------------
2005-11-11 10:07:42 UTC Eduardo Pérez Ureta <eduardo.perez@uc3m.es>
* Added comment:
{{{
On 2005-09-22 19:47:33 +0200, Alain Bench wrote:
>     How much the am/pm thing seems ugly to us, as a default setting it
> is consistent with other defaults and especially the English C locale.
> Anyone using another language has to change this anyway. I think it
> makes sense to keep 12h clock as default.

I don't know if you really checked:

$ LC_ALL=C date +%X
17:57:23

$ LC_ALL=en_US date +%X
05:55:35 PM

You can see that the C locale uses 24-hour time format, but the en_US is
12-hour time format.

>     Even out of defaults: In International places where English is
> spoken, I send a custom English attribution with am/pm time.

You shouldn't, as the international date/time format is 24-hour:
http://www.cl.cam.ac.uk/~mgk25/iso-time.html
}}}

--------------------------------------------------------------------------------
2005-11-21 05:10:01 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Eduardo,

 El jueves 10 noviembre 2005 a las 19:45:04 +0100, Eduardo P=E9rez Ur=
eta escrib=EDa:

> On 2005-09-22 19:47:33 +0200, Alain Bench wrote:
>> How much the am/pm thing seems ugly to us, as a default setting it
>> is consistent with other defaults and especially the English C
>> locale.
> I don't know if you really checked:
>| $ LC_ALL=3DC date +%X
>| 17:57:23

    Yes, thanks. The thread continued long out of bug tracker on
mutt-dev only (for no valid reason), and a good soul informed me. I
replied:

| But saying "consistent" I didn't mean C [d_]t_fmt used 12h. I meant
| that by default the full system is English, and that English uses 1=
2h.
| Well, American and Canadian...


>> In International places where English is spoken, I send a custom
>> English attribution with am/pm time.
> You shouldn't

    You win retroactively: I 24hourized my international attribution.=
 My
own, not Mutt's default.


Bye!=09Alain.
--=20
When you want to reply to a mailing list, please avoid doing so with
iPlanet Messenger Express. This lacks necessary references and breaks=
 threads.
}}}
