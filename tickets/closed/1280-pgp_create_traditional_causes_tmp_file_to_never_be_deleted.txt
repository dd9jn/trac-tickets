Ticket:  1280
Status:  closed
Summary: pgp_create_traditional causes tmp file to never be deleted

Reporter: rmurray@cyberhqz.com (Ryan Murray)
Owner:    mutt-dev

Opened:       2002-07-21 08:22:40 UTC
Last Updated: 2005-08-02 02:02:11 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.28-2
Severity: wishlist

-- Please type your report below this line

set pgp_create_traditional.  Send a signed message.  A file will remain in
/tmp with the unsigned body.

-- System Information
Debian Release: 3.0
Kernel Version: Linux straylight 2.4.19-rc1 #1 SMP Wed Jun 26 20:59:16 PDT 2002 i586 unknown

Versions of the packages mutt depends on:
ii  libc6          2.2.5-10       GNU C Library: Shared libraries and Timezone
ii  libncurses5    5.2.20020112a- Shared libraries for terminal handling
ii  libsasl7       1.5.27-3       Authentication abstraction library.
exim	Not installed or no info
ii  postfix        1.1.11-0.woody A high-performance mail transport agent
	^^^ (Provides virtual package mail-transport-agent)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011002 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.28i (2002-03-13)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.19-rc1 (i586) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.tlr.mx_open_append.2
patch-1.3.28.cvs.indexsegfault
patch-1.3.27.bse.xtitles.1
patch-1.3.26.appoct.3
patch-1.3.15.sw.pgp-outlook.1
patch-1.3.27.admcd.gnutls.19
Md.use_editor
Md.paths_mutt.man
Md.muttbug_no_list
Md.use_etc_mailname
Md.muttbug_warning
Md.gpg_status_fd
patch-1.3.24.rr.compressed.1
patch-1.3.25.cd.edit_threads.9.1
patch-1.3.23.1.ametzler.pgp_good_sign


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 20:02:11 UTC brendan
* Added comment:
{{{
Appears to be fixed.
}}}

* resolution changed to fixed
* status changed to closed
