Ticket:  3665
Status:  closed
Summary: Encrypting postponed messages

Reporter: Christian
Owner:    kevin8t8

Opened:       2013-11-01 21:46:19 UTC
Last Updated: 2015-01-12 23:44:52 UTC

Priority:  major
Component: crypto
Keywords:  patch

--------------------------------------------------------------------------------
Description:
As per the subject. It has also been discussed on the mutt-users mailing list: [http://article.gmane.org/gmane.mail.mutt.user/41469]

--------------------------------------------------------------------------------
2013-11-06 03:05:03 UTC kevin8t8
* Added comment:
The initial patch that Christian Brabandt posted at <20130909203426.GD15757@256bit.org> is probably a good starting point.

mutt_prepare_template() is capable of decrypting a message and stripping a signature, since it's also used for resending a message.

One issue with the patch is that crypt_get_keys() returns a keylist for the *recipients* of the message.  I can only assume it "worked" for him because he has an "encrypt-to" setting in his gpg.conf.

I think we could either use the from address, or perhaps better, have a configuration variable like $postpone_encrypt_as (keyid).

In terms of the interface, should we have a configuration variable $postpone_encrypt (boolean|quadaoption), or just check if $msg->security has ENCRYPT set?  I'm leaning towards the configuration variable, since the user has to set $postpone_encrypt_as anyways, they should manually turn this on too.


* component changed to crypto
* owner changed to kevin8t8
* status changed to assigned
* type changed to enhancement

--------------------------------------------------------------------------------
2013-11-06 21:12:57 UTC kevin8t8
* Added attachment add-postpone-encrypt.patch

--------------------------------------------------------------------------------
2013-11-06 21:14:03 UTC kevin8t8
* Added comment:
Adding a patch implementing $postpone_encrypt and $postpone_encrypt_as options.

--------------------------------------------------------------------------------
2013-11-19 22:50:06 UTC kevin8t8
* keywords changed to patch

--------------------------------------------------------------------------------
2014-05-23 14:36:53 UTC rinni
* Added attachment config
* Added comment:
output of mutt -v

--------------------------------------------------------------------------------
2014-05-23 14:37:08 UTC rinni
* Added comment:
Hi,

I just tried the patch but it just gives me

{{{
usage: gpg [options] --encrypt [filename]
}}}


when I try to postpone the mail. I attached the output of ''mutt -v''




--------------------------------------------------------------------------------
2014-05-23 20:09:45 UTC kevin8t8
* Added comment:
The patch isn't doing anything different from a normal encrypt command - it's just using the postpone_encrypt_as keyid for the target.

What do you have postpone_encrypt_as set to?  Do you have the pgp settings set up (e.g. pgp_encrypt_only_command)?

--------------------------------------------------------------------------------
2014-05-24 21:55:50 UTC rinni
* Added comment:
I have in my config:

{{{
set pgp_sign_as = 0x7393982B
set postpone_encrypt_as = $pgp_sign_as

set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg --batch --quiet --no-verbose --output - --encrypt --textmode --armor --always-trust --encrypt-to %a -- -r %r -- %f"
}}}

and sending encrypted (or/and sigend) messages still works.

Postponing the mail works if I replace %a with my keyid. But from the manual[1] using %a should be valid.

[1]http://dev.mutt.org/doc/manual.html#pgp-decode-command

--------------------------------------------------------------------------------
2014-05-25 01:57:27 UTC kevin8t8
* Added comment:
It looks like the "--encrypt-to %a" is causing the problem.  The %a is empty in the invocation when postponing.

In my testing, with this pgp_encrypt_only_command setting, trying to send an encrypt only email (not signed) fails with the same message.

Looking at the source, it appears %a is only populated for the pgp_encrypt_sign_command; it is not populated for the pgp_encrypt_only_command. [See pgp_invoke_encrypt() and _mutt_fmt_pgp_command()].

Therefore, I don't believe it is (currently) legal to have the %a in that setting.  An alternative would be adding the encrypt-to setting in your gpg.conf.

--------------------------------------------------------------------------------
2014-05-25 11:09:38 UTC rinni
* Added comment:
Yes, using key-ids for "--encrypt-to" works.
So it's not a problem of your patch -- which is really great!

But from the documentation using "%a" should be valid[1] in all the $pgp_*_command variables, so docs or code should be changed I guess. I'll probably file a separate bug about that.

[1] http://dev.mutt.org/doc/manual.html#pgp-decode-command

--------------------------------------------------------------------------------
2015-01-12 23:44:52 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [8a2d9d352e2c08384764e5cc4b910627fe44c338]:
{{{
#!CommitTicketReference repository="" revision="8a2d9d352e2c08384764e5cc4b910627fe44c338"
Add option to encrypt postponed messages. (closes #3665)

This patch is based on Christian Brabandt's patch sent
to mutt-users.

Add two new configuration variables: $postpone_encrypt and
$postpone_encrypt_as.  When $postpone_encrypt is set and a message is
marked for encryption, the message will be encrypted using the key
specified in $postpone_encrypt_as before saving the message.

In this patch, $postpone_encrypt_as must be specified.  I experimented
with passing safe_strdup( NONULL (PostponeEncryptAs)) when unspecified,
but although gpg.conf has a default-key setting, I could not get it to
work properly. (pgpclassic gave an error message and gpgme sefaulted.)

Although not necessary, this patch turns off signing during encryption
of the postponed message (and turns it back on before saving), since
there is no need to sign the message yet.
}}}

* resolution changed to fixed
* status changed to closed
