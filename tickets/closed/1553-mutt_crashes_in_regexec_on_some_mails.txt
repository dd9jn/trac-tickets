Ticket:  1553
Status:  closed
Summary: mutt crashes in regexec() on some mails

Reporter: bee@pickle.me.uk
Owner:    mutt-dev

Opened:       2003-04-28 16:00:04 UTC
Last Updated: 2005-09-21 16:41:59 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.0-4
Severity: normal

-- Please type your report below this line

mutt crashes on the call to regexec() in resolve_types() for the pager, on attempting to display this line of an email (in the body):
From: «Íîâîñòè êàðàîêå» <xxx@beedesign.co.uk>

And only if I have this line in my configuration for colour display:
From: «Íîâîñòè êàðàîêå» <piotr@beedesign.co.uk>

It would seem this problem is likely in the libc on my system - compiling mutt with --with-regex causes it to disappear. Just wondering if there's anything I can do to determine if this definitely is the source.

Thanks,

Barnaby

-- System Information
Debian Release: testing/unstable
Kernel Version: Linux purple 2.4.18 #1 Sun Apr 13 17:26:15 BST 2003 i686 unknown unknown GNU/Linux

Versions of the packages mutt depends on:
ii  exim           3.36-6         An MTA (Mail Transport Agent)
ii  libc6          2.3.1-15       GNU C Library: Shared libraries and Timezone
ii  libncurses5    5.3.20021109-2 Shared libraries for terminal handling
ii  libsasl7       1.5.27-3.3     Authentication abstraction library.
ii  exim           3.36-6         An MTA (Mail Transport Agent)
	^^^ (Provides virtual package mail-transport-agent)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-linux/3.2.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,proto,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-objc-gc i386-linux
Thread model: posix
gcc version 3.2.3 20030316 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-04-28 16:00:04 UTC bee@pickle.me.uk
* Added comment:
{{{
Package: mutt
Version: 1.4.0-4
Severity: normal

-- Please type your report below this line

mutt crashes on the call to regexec() in resolve_types() for the pager, on attempting to display this line of an email (in the body):
From: «Íîâîñòè êàðàîêå» <xxx@beedesign.co.uk>

And only if I have this line in my configuration for colour display:
From: «Íîâîñòè êàðàîêå» <piotr@beedesign.co.uk>

It would seem this problem is likely in the libc on my system - compiling mutt with --with-regex causes it to disappear. Just wondering if there's anything I can do to determine if this definitely is the source.

Thanks,

Barnaby

-- System Information
Debian Release: testing/unstable
Kernel Version: Linux purple 2.4.18 #1 Sun Apr 13 17:26:15 BST 2003 i686 unknown unknown GNU/Linux

Versions of the packages mutt depends on:
ii  exim           3.36-6         An MTA (Mail Transport Agent)
ii  libc6          2.3.1-15       GNU C Library: Shared libraries and Timezone
ii  libncurses5    5.3.20021109-2 Shared libraries for terminal handling
ii  libsasl7       1.5.27-3.3     Authentication abstraction library.
ii  exim           3.36-6         An MTA (Mail Transport Agent)
	^^^ (Provides virtual package mail-transport-agent)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-linux/3.2.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,proto,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-objc-gc i386-linux
Thread model: posix
gcc version 3.2.3 20030316 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.
}}}

--------------------------------------------------------------------------------
2004-04-10 05:47:11 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Barnaby, and thanks for the report.

On Monday, April 28, 2003 at 12:00:04 AM +0100, Barnaby Gray wrote:

> ii  libc6          2.3.1-15       GNU C Library: Shared libraries and Timezone
> mutt crashes on the call to regexec() [...] It would seem this problem
> is likely in the libc on my system

   You are very probably right. If you use an UTF-8 locale, please
check if Glibc regex bug #216512 applies to your case (there are various
shell or C test programs) on
<URL:http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=216512>.


Bye!	Alain.
-- 
A: Because it messes up the order in which people normally read text.
Q: Why is top-posting such a bad thing?
}}}

--------------------------------------------------------------------------------
2005-08-25 13:58:06 UTC rado
* Added comment:
{{{
Any progress on this front? Does it apply to current mutt?
Have you found what is causing this (mutt or something else)?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-25 13:58:07 UTC rado
* Added comment:
{{{
Need update.
}}}

--------------------------------------------------------------------------------
2005-09-22 10:41:59 UTC rado
* Added comment:
{{{
no response for 4 weeks (2y after first rfc by Alain).
}}}

* resolution changed to fixed
* status changed to closed
