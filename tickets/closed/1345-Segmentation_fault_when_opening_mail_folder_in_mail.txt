Ticket:  1345
Status:  closed
Summary: Segmentation fault when opening mail folder in ~/mail

Reporter: Vernon Erle Ikeda <vern@hay.haya.qc.ca>
Owner:    mutt-dev

Opened:       2002-09-22 06:32:09 UTC
Last Updated: 2005-08-02 02:15:55 UTC

Priority:  critical
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: important

-- Please type your report below this line

   When opening mail folders in ~/mail, mutt crashes
(Segmentation Error).  

	Vernon


    Vernon Erle Ikeda - vern@haya.qc.ca
    Assistant System Administrator - Assistant List Administrator
    www.hay-net.net - www.haya.qc.ca - www.lists.ca
    Montreal, Quebec - Arrondissement de Pierrefonds-Senneville

  This message was mailed using Mutt 1.2.5i
(This is from my old .muttsig file - not yet updated)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/local/lib/gcc-lib/i686-pc-linux-gnu/2.95.3/specs
gcc version 2.95.3 20010315 (release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using ncurses 5.0]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/bin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /home/vern/.muttrc
set hostname="haya.qc.ca"
set folder = "~/mail"
set alias_file = ~/.mutt.aliases
source ~/.mutt.aliases
my_hdr X-Disclaimer: Opinions are strictly my own.
my_hdr Organization: Hay-Net Networks - http://www.hay-net.net
my_hdr Reply-to: vern@haya.qc.ca
my_hdr X-Keywords: 
set attribution    = "On %d, %n wrote:"
set forward_format = "Fwd: [%a: %s]"
set pager_format   = "%S [%C/%T] %n (%l) %s"               # Sven's favourite 
set quote_regexp   = "^ *[a-zA-Z]*[]>|}()%:=-][]>|}():=-]*"  # More Sven
set status_format  = "[%r] %h %f (%s) [%M/%m] [N=%n,*=%t,post=%p,new=%b]" 
set index_format   = "%4C %Z %{%b %d} %-15.15F (%4l) %s"
ignore *
unignore from date subject to cc reply-to:
unignore organization organisation x-mailer: x-newsreader: x-mailing-list:
unignore posted-to: x-mailfolder:
unset forward_decode
  set help
  set hostname =            "haya.qc.ca"
  set ignore_list_reply_to
  set include =             "yes"
  set mbox =                "=read-msgs"
  set mime_forward =        "ask-no"
  set move =                "no"
  set pager_index_lines =   6
  set pager_stop =          "no"
  set postponed =           "=postponed-msgs"
  set record =              "=sent-mail"
  set signature =           "~/.muttsig"
--- End /home/vern/.muttrc


--- Begin /usr/local/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /usr/local/etc/Muttrc


Received: (at submit) by bugs.guug.de; 21 Sep 2002 23:38:48 +0000
From vern@hay.haya.qc.ca Sun Sep 22 01:38:48 2002
Received: from aurora.cronomagic.com
	([209.71.203.17] helo=aurora.hay-net.net ident=root)
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 17stpn-0002Gx-00
	for <submit@bugs.guug.de>; Sun, 22 Sep 2002 01:38:47 +0200
Received: from aurora.hay-net.net (smmsp@localhost [127.0.0.1])
	by aurora.hay-net.net (8.12.3/8.12.3) with ESMTP id g8LNeVKI027050
	for <submit@bugs.guug.de>; Sat, 21 Sep 2002 23:40:31 GMT
Received: (from uucp@localhost)
	by aurora.hay-net.net (8.12.3/8.12.3/Submit) with UUCP id g8LNe7Qw027048
	for submit@bugs.guug.de; Sat, 21 Sep 2002 23:40:07 GMT
Received: (from vern@localhost)
	by hay.haya.qc.ca (8.10.2/8.10.2) id g8LNdHk04446;
	Sat, 21 Sep 2002 19:39:17 -0400
Date: Sat, 21 Sep 2002 19:39:17 -0400
From: Vernon Erle Ikeda <vern@hay.haya.qc.ca>
Message-Id: <200209212339.g8LNdHk04446@hay.haya.qc.ca>
Subject: mutt-1.4i: Segmentation fault (core dump) while opening folders in ~/mail
To: submit@bugs.guug.de
X-Virus-Scanned: by amavisd-milter (http://amavis.org/)

Package: mutt
Version: 1.4i
Severity: important

-- Please type your report below this line

  As before but with core dump from Segmentation fault.

   Vernon


    Vernon Erle Ikeda - vern@haya.qc.ca
    Assistant System Administrator - Assistant List Administrator
    www.hay-net.net - www.haya.qc.ca - www.lists.ca
    Montreal, Quebec - Arrondissement de Pierrefonds-Senneville


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/local/lib/gcc-lib/i686-pc-linux-gnu/2.95.3/specs
gcc version 2.95.3 20010315 (release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using ncurses 5.0]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/bin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /home/vern/.muttrc
set hostname="haya.qc.ca"
set folder = "~/mail"
set alias_file = ~/.mutt.aliases
source ~/.mutt.aliases
my_hdr X-Disclaimer: Opinions are strictly my own.
my_hdr Organization: Hay-Net Networks - http://www.hay-net.net
my_hdr Reply-to: vern@haya.qc.ca
my_hdr X-Keywords: 
set attribution    = "On %d, %n wrote:"
set forward_format = "Fwd: [%a: %s]"
set pager_format   = "%S [%C/%T] %n (%l) %s"               # Sven's favourite 
set quote_regexp   = "^ *[a-zA-Z]*[]>|}()%:=-][]>|}():=-]*"  # More Sven
set status_format  = "[%r] %h %f (%s) [%M/%m] [N=%n,*=%t,post=%p,new=%b]" 
set index_format   = "%4C %Z %{%b %d} %-15.15F (%4l) %s"
ignore *
unignore from date subject to cc reply-to:
unignore organization organisation x-mailer: x-newsreader: x-mailing-list:
unignore posted-to: x-mailfolder:
unset forward_decode
  set help
  set hostname =            "haya.qc.ca"
  set ignore_list_reply_to
  set include =             "yes"
  set mbox =                "=read-msgs"
  set mime_forward =        "ask-no"
  set move =                "no"
  set pager_index_lines =   6
  set pager_stop =          "no"
  set postponed =           "=postponed-msgs"
  set record =              "=sent-mail"
  set signature =           "~/.muttsig"
--- End /home/vern/.muttrc


--- Begin /usr/local/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /usr/local/etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 20:15:55 UTC brendan
* Added comment:
{{{
Obsolete, not enough info to reproduce.
}}}

* resolution changed to fixed
* status changed to closed
