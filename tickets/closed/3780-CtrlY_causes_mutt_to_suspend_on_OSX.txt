Ticket:  3780
Status:  closed
Summary: Ctrl-Y causes mutt to suspend on OSX

Reporter: balderdash
Owner:    mutt-dev

Opened:       2015-09-10 02:08:47 UTC
Last Updated: 2015-09-13 04:40:36 UTC

Priority:  trivial
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
On OSX 10.10.5, mutt from tip: start mutt, then hit Ctrl-y.  Observe that mutt is suspended and one is back at the bash prompt:

{{{
1]+  Stopped                 mutt
}}}

Nothing in the manual says that Ctrl-y should or would do this.  No other commandline software that I use is suspended like this upon hitting Ctrl-y.  Besides being annoying and surprising, it means one can't bind Ctrl-y to anything.

It happens in Terminal.app and in iTerm, and in neither case have I altered the terminal's default key settings to make Ctrl-y do something special.

This behavior has been around for a long time, but I finally got sick of it.

--------------------------------------------------------------------------------
2015-09-10 15:57:46 UTC dgc
* Added comment:
This is not a mutt issue, it's a terminal property (DSUSP) that is applicable only on BSD-derived systems such as OSX.  GNU glibc manual documents it thus:

  Macro: int VDSUSP
  This is the subscript for the DSUSP character in the special control character array. termios.c_cc[VDSUSP] holds the character itself.

  The DSUSP (suspend) character is recognized only if the implementation supports job control (see Job Control). It sends a SIGTSTP signal, like the SUSP character, but not right away—only when the program tries to read it as input. Not all systems with job control support DSUSP; only BSD-compatible systems (including GNU/Hurd systems).

If you run `stty -a` you'll see ctrl-Y bound to DSUSP. DSUSP sends SIGTSTP just like SUSP (ctrl-Z) does, but ''only when the program reads the ctrl-Y as input -- not before''. If you run `cat` at your terminal prompt and press ctrl-Y and enter, you'll see the same thing. (You have to press enter because cat line-buffers input. Mutt does not.)

To "fix" this, use stty to rebind DSUSP.

--------------------------------------------------------------------------------
2015-09-10 15:58:45 UTC dgc
* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2015-09-10 18:16:52 UTC balderdash
* Added comment:
Vim doesn't do this.  Nano doesn't do this.  Less doesn't do this.  Why must mutt interpret Ctrl-Y as DSUSP?  Mutt won't even let me bind it to a macro.

--------------------------------------------------------------------------------
2015-09-10 21:48:52 UTC dgc
* Added comment:
My point is mutt doesn't do this either. Your OS does. Try what I said: use stty to rebind DSUSP. If it works, I'm right. If not, I'm wrong.

{{{
    stty dsusp undef
}}}

--------------------------------------------------------------------------------
2015-09-11 02:33:04 UTC balderdash
* Added comment:
> My point is mutt doesn't do this either. Your OS does.

My OS is *involved*, but it doesn't follow that mutt shouldn't be fixed.  My OS, after all, is not the sole contributor to this behavior.

When used on this very same OS, other command line tools like vim, nano, and less somehow manage to not register ctrl-Y as an instruction to suspend.  Therefore a program doesn't *have* to pay attention to what ctrl-Y sends.  Mutt should be the same, and the fact that it isn't is a defect given the widely-used class of OSes that have this nonstandard property.

> If it works, I'm right. If not, I'm wrong.

It works, but the users shouldn't have to set some arcane tweak in their .profile to avoid this nonsense.  A tweak which may have unwanted side effects, for all I know.  Projects are always making adjustments around the peculiarites of various OSes and distros.  Mutt does this too (rightly so), and it should do so here.

--------------------------------------------------------------------------------
2015-09-11 04:36:47 UTC dgc
* Added comment:
In the end my mind isn't changed, but if the other devs see it your way I won't argue with a patch. I'll go -0 to your suggestion.

Now to explain my reasoning: I disagree with your general assessment. The OS '''is''' the sole factor here. Mutt isn't doing anything to cause this behavior. Your complaint is effectively that it should be doing something to prevent it. But there's nothing unusual in this situation when looking at software portability. As with most other OS features an application can work around DSUSP, but it's still the OS doing it.

A program '''does''' have to pay attention to what ctrl-Y sends, if ctrl-Y is wired to your terminal driver. It has no choice. Any application that wants to disregard a signal from the terminal must specifically disable those terminal events when they run, "unwiring" the keystroke. I'm not really a fan of manipulating termios to disable signal handling in general, no matter what vim, nano, etc do. To me this is precisely analogous to programs that explicitly disable ctrl-Z, and it's maddening. By disabling DSUSP in the terminal driver, these programs prevent users from having an option that your OS tried to provide them. The change you propose takes choice and capabilities away.

You have a workaround. It's easy to make termios changes using stty. Many people already do this. I've done it for manipulating other terminal attributes for 20+ years. It's why stty exists, and why profiles exist. By arguing that mutt should behave like a list of other programs, you're essentially saying you don't like this feature of your OS and you would prefer that the OS were different. Well, you have tools for altering your OS's behavior. It doesn't mean that your application software should disable OS features for everyone unless there's a real usability issue.

--------------------------------------------------------------------------------
2015-09-11 11:52:44 UTC vinc17
* Added comment:
I don't see any reason to disable DSUSP in Mutt (except if it breaks everything). I suppose that some users may want to use it, just like one may want to use SUSP (Ctrl-Z). Moreover, the old setting could not be restored if Mutt crashes or is killed (though the shell may provide a way to restore the terminal settings).

If you never want to be able to use some terminal feature, then disable it from your shell. Otherwise use a wrapper to disable/change what you want. Things you may want to do for Mutt:

{{{
mesg n
stty dsusp undef 2> /dev/null
stty flush undef
stty pass8
stty -ixon
}}}

--------------------------------------------------------------------------------
2015-09-11 20:16:47 UTC balderdash
* Added comment:
> It has no choice.

But it does have a choice, as you yourself admitted when you argued thus (in paraphrase): we should not alter mutt so that it starts making that choice, because it's bad when software makes that choice.

So the issue is whether mutt should be patched so that it ignores DSUSP, not whether it is possible to make such a patch.

Here is a list of at least some of the reasons in favor of making such a change:

1. It is very easy, on a US keyboard, to accidentally hit Ctrl-Y when one means to hit something else nearby (e.g., Ctrl-T for untag-message).  This means it's *very* easy to accidentally suspend mutt.  This is a bad thing.  In contrast, it's very difficult to accidentally hit Ctrl-Z, owing to the awkward location of "Z" on a US keyboard (plus the user *expects* Ctrl-Z cause suspension, since that's what it does everywhere else).

2. It is desirable to be able to map Ctrl-Y to a function or macro, which is currently impossible.

And here is why the reasons that have been given *against* such a change aren't compelling:

A. "By disabling DSUSP in the terminal driver, these programs prevent users from having an option that your OS tried to provide them. The change you propose takes choice and capabilities away."  Huh?  The point of this bug report is that the user WANTS that capability taken away, that it's a bad capability to have.  Taking the capability away from mutt doesn't mean it's gone wrt other programs.

B. (paraphrased) "You should just use stty locally".  Unless it's really difficult to change mutt in the relevant way, or would require some kind of architectural overhaul, or would break things for another big clump of users, it's incumbent on mutt to workaround the silly foibles of various OSes.  From "the user can easily work around the problem", it obviously doesn't follow that "mutt should not work around the problem".   And the mutt devs can't honestly believe that it follows.  If they did, we wouldn't have a configure script full of OS-specific workarounds that users could easily do themselves.  Some things a user shouldn't have to do things themselves even though they could.

C. "Moreover, the old setting could not be restored if Mutt crashes or is killed".  I really doubt this is true of all ways of getting Mutt to ignore Ctrl-Y.  Vim doesn't suspend on Ctrl-Y, but if I kill Vim and then immediately type 'mutt' and then Ctrl-Y mutt will suspend (I just tried it).  Perhaps this means that Vim is using some other mechanism to ignore DSUSP than the one mentioned, but then the question is: why couldn't mutt do that?

D. One the one hand I'm being told "you can make a simple one-line fix in your .profile", and on the other hand I'm being told that it's super -duper important that I have the freedom to send DSUSP to other programs.  But the suggested fix blocks me from sending DSUSP to other programs.  To actually exercise this precious freedom while concurrently self-fixing mutt, I would have to start writing wrappers that would invoke stty before and after program invocations, which means we're not dealing with such a trivial and simple fix after all.

E. "I suppose that some users may want to use it, just like one may want to use SUSP (Ctrl-Z)".  So everyone using BSD-ish systems, apart from a couple of people who might want to send DSUSP to mutt, should deal with accidental suspensions?  "No", someone will say, "the many should tweak their .profiles so that the few can send DSUSP".  But (see D) this fix is not as simple as you propose, unless you simultaneously admit that the ability to send DSUSP isn't really that important.

None of these are remotely compelling.  What would be compelling?  If it is difficult to patch mutt in the relevant way, that would be compelling.  If there were a horde of DSUSP fans, that would be compelling, but only if the horde is huge and only if the docs gain a section about how OSX and BSD users who don't want unexpected suspensions should put "stty dsusp [X]" in their .profiles, where [X] is some key not easy to hit by mistake.  I doubt such a horde exists.

--------------------------------------------------------------------------------
2015-09-11 20:17:18 UTC balderdash
* Added comment:
The ideal solution would be an option settable in muttrc which is set by default and which when set would cause DSUSP to be ignored.  I'm not a programmer and know nothing of mutt internals, so I have no idea whether that's even possible.

--------------------------------------------------------------------------------
2015-09-11 22:51:31 UTC cameron
* Added comment:
{{{
On 11Sep2015 20:17, Mutt <fleas@mutt.org> wrote:
[...]

No, the ideal solution has already been suggested: _you_ want ^Y not to 
suspend, so issue "stty dsusp undef" in your .bashrc or equivalent. It really 
is nothing to do wiuth mutt.

Also, if you do this you will then be free to bind ^Y to whatever you want.
- Cameron Simpson <cs@zip.com.au>
}}}

* resolution changed to 
* status changed to reopened

--------------------------------------------------------------------------------
2015-09-11 23:01:18 UTC brendan
* Added comment:
Consider: you would like to use a dvorak keyboard layout in mutt, but qwerty in vi because your finger memory needs it. Would it be convenient for you to have a mutt option $interpret_qwerty_as_dvorak? Yes. But it's cleaner to remap your keyboard to dvorak outside of mutt. It's not just the bloat in mutt, it's clearly a layer violation.

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2015-09-11 23:13:41 UTC balderdash
* Added comment:
> No, the ideal solution has already been suggested: _you_ want ^Y not to 
suspend, so issue "stty dsusp undef" in your .bashrc or equivalent.

I dare you to find one person with a US layout keyboard or similar who *wants* Ctrl-Y to suspend mutt, and wants it so badly that he will not be content with Ctrl-Z.

A solution that requires users to do something they shouldn't have to do is not ideal.  If Vim disappeared when Ctrl-Y is hit, and wouldn't even let you remap that key elsewhere in vimrc, there would be (rightly) a riot.

> It really is nothing to do wiuth mutt.

It's utterly baffling why people keep saying this.  Are people really confusing "it is not entirely mutt's fault" with "it is entirely not mutt's fault"?  It is at least partly mutt's fault, which is conclusively proven by the existence of numerous other tools that don't suspend upon Ctrl-Y during normal operation, including:

vim
nano
less
netstat
alpine
emacs
sudo

--------------------------------------------------------------------------------
2015-09-11 23:18:23 UTC balderdash
* Added comment:
> Consider: you would like to use a dvorak keyboard layout in mutt, but qwerty in vi because your finger memory needs it. Would it be convenient for you to have a mutt option $interpret_qwerty_as_dvorak? Yes. But it's cleaner to remap your keyboard to dvorak outside of mutt.

This is simply absurd.  That would be a one-off, for mere convenience, and the inconvenience isn't due to a mutt defect or an OS foible.  Nobody wants to accidentally suspend mutt, and the fact that mutt gets accidentally suspended by responding in that way to a prominent and central key is a mutt defect.

--------------------------------------------------------------------------------
2015-09-11 23:52:42 UTC barsnick
* Added comment:
{{{
On Fri, Sep 11, 2015 at 23:13:42 -0000, Mutt wrote:

This discussion should then essentially be about a design decision:
mutt is obeying SIGTSTP, some other applications apparently decide not
to do so.

SIGTSTP being on Ctrl+Y in MacOSX's terminal is Mac's design decision,
if that particular key is inconvenient to you, bark up a different
tree.
}}}

* resolution changed to 
* status changed to reopened

--------------------------------------------------------------------------------
2015-09-12 00:36:15 UTC vinc17
* Added comment:
If everyone agrees that Ctrl-Y is a bad feature, then instead of introducing non-standard code in every application, the correct solution would be to drop DSUSP at the OS level. So, I suggest to report a bug against OSX. Thus every application would benefit from this.

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2015-09-12 00:51:49 UTC vinc17
* Added comment:
FYI, DSUSP is not disabled in python: http://bugs.python.org/issue16768

And it is not disabled by {{{cat}}} either: http://stackoverflow.com/questions/29024007/disable-dsusp-in-python

So, Mutt is not an exception.

--------------------------------------------------------------------------------
2015-09-12 15:21:50 UTC balderdash
* Added comment:
I never said mutt is the *only* program like this.  Just that it doesn't have to be like this, since other software manages to avoid it without horrible side-effects.  I also never said that Ctrl-Y is bad in every program.  Just that it's bad in mutt (and *would* be bad in Vim for totally parallel reasons).

I am mystified at the general response, which is effectively parodied thus:

"So your popular class of OSes has made the decision to place important library L in a nonstandard directory N instead of standard location S and you want us to adjust the configure script so it can find L on your OS, like a whole host of other software does?  We won't, because the fact that you can't build mutt against L is 100% your OS's fault, and the solution is obviously for you to just symlink or copy L into S, which is easy as pie.' 

The fact that your OS put L in N *deliberately* has magically granted that decision the following special status: no software should work around this decision even if the effects of L being in N are bad for a large class of users of that software on that OS, and neither-good-nor bad for everyone else.

Indeed, the other software that manages to find N/L shouldn't be doing that.  If they were designed well, they would *also* make everyone on your OS except a hypothetical two peoople create this symlink (it's easy!).

If you don't like it, then you can just deal with a mutt that isn't built against L even though that demonstrably sucks, or you can file a bug with your OS because there's a realistic chance they'll listen.

What's that you say?  Some things a user shouldn't have to do themselves even though it would be easy for them to do so, and this sure looks like one of those cases?  But this is not one of those cases, because it's easy for the user to do it themselves.  We therefore close this issue."

--------------------------------------------------------------------------------
2015-09-12 21:24:12 UTC wyardley
* Added comment:
Honestly, the fact that you're still arguing when everyone else in the ticket has pointed out that there's a simple (and correct) workaround - that is, don't bind dsusp to `^`Y within your shell. I don't know if you're just embarrassed that you didn't realize the reason for this behavior or what, but I think your continued remarks (and continuing to re-open this ticket) make it clear that you are not planning on listening to what anyone has to say on this issue.

Just because Mutt is more correct in accepting signals than other software does not mean that the behavior is incorrect. Mutt is not mainstream software; it's software mostly used by people who do care about things like having their terminal behave as they expect.

Interestingly enough, I see this behavior in OS X, but primarily use Mutt on a FreeBSD system. On FreeBSD, while stty -a still shows "dsusp = `^`Y", the behavior is not the same. Installed via ports, but doesn't seem like any of the patches change this behavior.

Have to say, I've been using Mutt on a number of systems for many years, and have never found myself accidentally hitting `^`Y, and I've never seen a single other complaint about this behavior on the mutt-users mailing list.

--------------------------------------------------------------------------------
2015-09-12 21:24:33 UTC wyardley
* priority changed to trivial

--------------------------------------------------------------------------------
2015-09-13 04:40:36 UTC dgc
* Added comment:
Balderdash, as tempting as it is to go point for point with you, I don't think that serves any purpose. Your paraphrasing of the argument is a straw man, not what we're actually telling you. So far nobody but you has disagreed with our stance. This is very simply an OS issue. Other applications (but not most, by far) choose to sidestep the OS behavior. Mutt does not, and we feel there are good reasons for that. I think we've been pretty clear that this actually is harmful, not just a trivial bug that we refuse to fix. I've already spent more time researching and arguing than it would take to patch, commit, release, and announce an update.

I was curious about those applications that you hold as models though. I wanted to be confident that there's no reasonable way of doing what you asked — confident that I was not overlooking some smarter approach than what I knew about already. After all those vim guys are pretty smart. So I took a harder look at what vim does to reclaim !^Y. It turns out it's not really aiming for !^Y, that's just a side effect. What vim actually does is to disable ''all'' terminal-generated signals by the C (termios) equivalent of "stty -isig". Vim blocks !^C, !^Z, all of them, so that on keyboard read it actually receives those control characters (\003, \031, \032) instead of job control signals. Then vim explicitly maps !^C and !^Z in its keyboard bindings to interrupt and suspend, respectively, and when those keys are struck it sends itself an interrupt or suspend signal.

Well, what if I've remapped INTR and SUSP in my terminal? (It doesn't matter why I might do that; the point is UNIX allows it and an application shouldn't subvert that.) What about vim? ''Vim totally subverts that.'' Try this experiment: `stty susp ^P` to set !^P to suspend in your terminal. It works. Run any normal program; !^P will suspend it. That's how UNIX is supposed to work. You can rebind terminal signal generation.

Now run vim. Press !^P — nothing happens. Press !^Z — it suspends. Vim's approach means that it doesn't follow what the user or system has configured. I argue that's completely broken and vim has absolutely done the wrong thing.

Your argument is that nobody wants !^Y anyway, so I imagine you would also argue that nobody does (or, at least, should) rebind INTR and SUSP. But this is not mutt's decision to make. The OS provides these configurable capabilities for a reason. Because it provides them, there can be people who use them. If someone uses them, and they run mutt and find that it prevents what their OS allows, ''that is a mutt bug.'' As Brendan said, that's a layering violation.

Emacs? Same result as vim. Broken. Pine? I don't know, I use mutt. Netstat? I can't imagine why netstat is doing ''anything'' with raw keyboard bindings anyway, so I don't really care. Less? Less does what it should, if it's going to disable !^Y. Less is okayish. Sudo? Sudo is not the same class of program. It's like ssh, it allocates a pty and owns that terminal. You get the new terminal's behavior, not the old one's. Not analogous at all to mutt. (But try "sudo cat", i.e. run sudo without allocating a pty. Now !^Y suspends.) So you have one case of something else that does what you're proposing.

Google around for people asking why !^Y suspends their software. The answer is always "stty dsusp undef" or something like it. In every case I found, the OP is glad for an answer and happily accepts the solution. You're the only person I've ever heard unhappy with using what the OS gives you to control your operating environment, and asking for the application to circumvent it for you to save you one line in a shell config file. That is, as you say, not remotely compelling.

Mutt should not be making these decisions. Neither should vim, emacs, or netstat.

If you want a wrapper, here's one. But you don't. You don't want DSUSP at all, so just put the stty in your .profile and call it a day.

{{{
#!/bin/sh
# Disable DSUSP processing while running mutt

# Capture old termio params
old=$(stty -g)

# Disable DSUSP
stty dsusp undef

# Set a trap to restore termios
trap "rc=$?; stty $old; exit $rc" 0 1 2 3 15

# Run mutt
mutt "$@"
}}}
