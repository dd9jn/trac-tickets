Ticket:  3626
Status:  closed
Summary: Commit 98e031cd81d4 breaks compilation on OSX 10.7.5

Reporter: balderdash
Owner:    mutt-dev

Opened:       2013-01-20 22:27:11 UTC
Last Updated: 2013-01-22 03:59:43 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Commit 98e031cd81d4 breaks compilation on OSX 10.7.5.

It worked immediately prior to this patch, but now I get:

checking whether this iconv is good enough... yes
checking whether iconv is non-transcribing... no
checking for bind_textdomain_codeset... no
./configure: line 11837: syntax error near unexpected token `gnuidn,'
./configure: line 11837: `      PKG_CHECK_MODULES(gnuidn, libidn,'

Some part of the preparation process failed.
Please refer to doc/devel-notes.txt for details.

I don't have pkg-config installed, and I hope it hasn't just become a prereq for mutt.

--------------------------------------------------------------------------------
2013-01-21 01:32:52 UTC balderdash
* Added comment:
The commit also breaks configuring on OSX 10.8.2 with the exact same error as above.

There's a new wrinkle.  Just for kicks I installed pkg-config and started fresh and tried to configure.  So *with* pkg-config installed, on OSX 10.8.2, I still get nonsense:
{{{
checking whether iconv is non-transcribing... no
checking for bind_textdomain_codeset... no
checking for pkg-config... /usr/local/bin/pkg-config
checking pkg-config is at least version 0.9.0... yes
checking for gnuidn... no
configure: error: Package requirements (libidn) were not met:
No package 'libidn' found

Consider adjusting the PKG_CONFIG_PATH environment variable if you
installed software in a non-standard prefix.

Alternatively, you may set the environment variables gnuidn_CFLAGS
and gnuidn_LIBS to avoid the need to call pkg-config.
See the pkg-config man page for more details.

Some part of the preparation process failed.
Please refer to doc/devel-notes.txt for details.
}}}

So to sum up, the commit breaks configuring on OSX for those with and those without pkg-config.

--------------------------------------------------------------------------------
2013-01-21 14:25:35 UTC me
* Added comment:
{{{
which version of pkg-config did you try?

according to the pkg-config man page, if you give a third argument 
to PKG_CHECK_MODULES() it should override the default behavior of 
terminating the script, but that obviously did not happen here 
(pkgconfig 0.23 on CentOS 5.9).
}}}

--------------------------------------------------------------------------------
2013-01-21 15:05:16 UTC prlw1
* Added comment:
Why is it necessary to infect configure.ac with PKG_CHECK_MODULES at all? There seems to be a sane check using AC_CHECK_HEADERS and AC_SEARCH_LIBS in configure.ac already. Are those checks failing in some way?

--------------------------------------------------------------------------------
2013-01-21 15:16:36 UTC balderdash
* Added comment:
I tried what as far as I know (and I know little, since I grabbed it solely for the purposes of this test) is the latest version of pkg-config: 0.27.1.

I shall report soon on whether this commit broke configuring on OSX 10.5.8 and 10.4.11 too.

--------------------------------------------------------------------------------
2013-01-21 16:46:28 UTC balderdash
* Added comment:
Exact same errors on 10.5.8 and 10.4.11, both with and without pkg-config.

On all OSX versions, searching the filessystem for the string 'idn' and weeding out obviously irrelevant hits pulls up only some stuff with '.pyc' suffixes and some other stuff not anywhere close to the /usr directory.

--------------------------------------------------------------------------------
2013-01-22 02:02:20 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
(In [acc8b2afdf29]) remove PKG_CHECK_MODULE() check and always use AC_CHECK_HEADERS()

only check for <idn/idna.h> when <idna.h> isn't found

closes #3626

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2013-01-22 03:59:43 UTC balderdash
* Added comment:
The fix has restored configure-ability on OSX 10.7.5.  I haven't yet tested the others but presume it will work there too.

Many thanks!
