Ticket:  3547
Status:  closed
Summary: mutt is not robust enough when reading certificates file

Reporter: hhorak
Owner:    mutt-dev

Opened:       2011-11-03 15:38:29 UTC
Last Updated: 2012-07-09 00:27:00 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
This bug was originally reported at Bug 751124 - Crash when opening IMAP folder

Description of problem:
I have set

  set folder=imaps://matej@luther.ceplovi.cz/INBOX

and when starting mutt, I get this crash:
{{{
Program received signal SIGSEGV, Segmentation fault.
__strstr_sse2 (haystack_start=0x1 <Address 0x1 out of bounds>, 
    needle_start=0x4a8248 "-----BEGIN") at ../string/strstr.c:63
63   while (*haystack && *needle)
Missing separate debuginfos, use: debuginfo-install
bzip2-libs-1.0.5-7.el6_0.x86_64 keyutils-libs-1.4-3.el6.x86_64
libgcrypt-1.4.5-9.el6.x86_64 libgpg-error-1.7-4.el6.x86_64
libtasn1-2.3-3.el6.x86_64 nss-softokn-freebl-3.12.9-10.el6.x86_64
zlib-1.2.3-27.el6.x86_64
(gdb) thread apply all backtrace

Thread 1 (Thread 0x7ffff7fd67c0 (LWP 24792)):
#0  __strstr_sse2 (haystack_start=0x1 <Address 0x1 out of bounds>, 
    needle_start=0x4a8248 "-----BEGIN") at ../string/strstr.c:63
#1  0x000000000048798b in tls_compare_certificates (certdata=0xb9b8f0,
certstat=0, 
    hostname=<value optimized out>, chainidx=0, certerr=0x7fffffffca18,
savedcert=0x7fffffffca14)
    at mutt_ssl_gnutls.c:438
#2  tls_check_preauth (certdata=0xb9b8f0, certstat=0, hostname=<value optimized
out>, 
    chainidx=0, certerr=0x7fffffffca18, savedcert=0x7fffffffca14) at
mutt_ssl_gnutls.c:590
#3  0x0000000000489131 in tls_check_certificate (conn=0xb7d950) at
mutt_ssl_gnutls.c:1005
#4  tls_negotiate (conn=0xb7d950) at mutt_ssl_gnutls.c:346
#5  0x000000000048950c in tls_socket_open (conn=0xb7d950) at
mutt_ssl_gnutls.c:162
#6  0x0000000000490a27 in imap_open_connection (idata=0xb7def0) at imap.c:407
#7  0x0000000000490d68 in imap_conn_find (account=0x7fffffffd860, flags=<value
optimized out>)
    at imap.c:371
#8  0x00000000004924af in imap_open_mailbox (ctx=0xb7c650) at imap.c:574
#9  0x0000000000443be5 in mx_open_mailbox (path=<value optimized out>, flags=0,
pctx=0x0)
    at mx.c:661
#10 0x0000000000439aa6 in main (argc=1, argv=<value optimized out>) at
main.c:1017
(gdb) 
}}}
Version-Release number of selected component (if applicable):
mutt-1.5.20-2.20091214hg736b6a.el6_1.1.x86_64

How reproducible:
100% (three out of three attempts, also with attempt to access POP3 account)

Steps to Reproduce:
 1. try to access IMAP or POP3 account

Actual results:
crash

Expected results:
opened mailbox

--------------------------------------------------------------------------------
2011-11-03 15:39:36 UTC hhorak
* Added attachment mutt-1.5.21-certscomp.patch
* Added comment:
proposed patch to parse certificates file securely

--------------------------------------------------------------------------------
2011-11-03 15:41:08 UTC hhorak
* Added comment:
How to reproduce:
I'm able to reproduce the failure using a little bit damaged certificate
file (added a zero byte to the beginning).

--------------------------------------------------------------------------------
2012-07-08 07:01:46 UTC brendan
* Updated description:
This bug was originally reported at Bug 751124 - Crash when opening IMAP folder

Description of problem:
I have set

  set folder=imaps://matej@luther.ceplovi.cz/INBOX

and when starting mutt, I get this crash:
{{{
Program received signal SIGSEGV, Segmentation fault.
__strstr_sse2 (haystack_start=0x1 <Address 0x1 out of bounds>, 
    needle_start=0x4a8248 "-----BEGIN") at ../string/strstr.c:63
63   while (*haystack && *needle)
Missing separate debuginfos, use: debuginfo-install
bzip2-libs-1.0.5-7.el6_0.x86_64 keyutils-libs-1.4-3.el6.x86_64
libgcrypt-1.4.5-9.el6.x86_64 libgpg-error-1.7-4.el6.x86_64
libtasn1-2.3-3.el6.x86_64 nss-softokn-freebl-3.12.9-10.el6.x86_64
zlib-1.2.3-27.el6.x86_64
(gdb) thread apply all backtrace

Thread 1 (Thread 0x7ffff7fd67c0 (LWP 24792)):
#0  __strstr_sse2 (haystack_start=0x1 <Address 0x1 out of bounds>, 
    needle_start=0x4a8248 "-----BEGIN") at ../string/strstr.c:63
#1  0x000000000048798b in tls_compare_certificates (certdata=0xb9b8f0,
certstat=0, 
    hostname=<value optimized out>, chainidx=0, certerr=0x7fffffffca18,
savedcert=0x7fffffffca14)
    at mutt_ssl_gnutls.c:438
#2  tls_check_preauth (certdata=0xb9b8f0, certstat=0, hostname=<value optimized
out>, 
    chainidx=0, certerr=0x7fffffffca18, savedcert=0x7fffffffca14) at
mutt_ssl_gnutls.c:590
#3  0x0000000000489131 in tls_check_certificate (conn=0xb7d950) at
mutt_ssl_gnutls.c:1005
#4  tls_negotiate (conn=0xb7d950) at mutt_ssl_gnutls.c:346
#5  0x000000000048950c in tls_socket_open (conn=0xb7d950) at
mutt_ssl_gnutls.c:162
#6  0x0000000000490a27 in imap_open_connection (idata=0xb7def0) at imap.c:407
#7  0x0000000000490d68 in imap_conn_find (account=0x7fffffffd860, flags=<value
optimized out>)
    at imap.c:371
#8  0x00000000004924af in imap_open_mailbox (ctx=0xb7c650) at imap.c:574
#9  0x0000000000443be5 in mx_open_mailbox (path=<value optimized out>, flags=0,
pctx=0x0)
    at mx.c:661
#10 0x0000000000439aa6 in main (argc=1, argv=<value optimized out>) at
main.c:1017
(gdb) 
}}}
Version-Release number of selected component (if applicable):
mutt-1.5.20-2.20091214hg736b6a.el6_1.1.x86_64

How reproducible:
100% (three out of three attempts, also with attempt to access POP3 account)

Steps to Reproduce:
 1. try to access IMAP or POP3 account

Actual results:
crash

Expected results:
opened mailbox

--------------------------------------------------------------------------------
2012-07-09 00:27:00 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [243710db60e0]) gnutls:tls_compare_certificates: check strstr for failure (closes #3547)

A malformed certificate file could cause strstr to return an unhandled NULL.
Thanks to hhorak for the proposed patch. This one is similar but avoids using
memmem for the first time (I am not sure about its portability).

* resolution changed to fixed
* status changed to closed
