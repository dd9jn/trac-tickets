Ticket:  3271
Status:  closed
Summary: mutt 1.5.20 regression: not updating time fields on mbox

Reporter: antonio@dyne.org
Owner:    pdmef

Opened:       2009-06-17 18:41:12 UTC
Last Updated: 2010-09-13 02:54:21 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/533439

{{{
When mutt 1.5.20-1 writes changes to an mbox file, it
changes the file modify time to be the same as the
access time.  This includes just changing the
message-is-new / message-is-old-but-unread flags.

xbiff reports new mail
user runs mutt
xbiff reports new mail a second time


Mutt (including version 1.5.20-1) call utime to set
the modification time back to the previous value while
exiting, but in 1.5.20-1 it appears to be modified
after the call to utime.  Running strace without
tracing children doesn't show a culprit in the main
process, and running it with children traced makes
mutt believe that the mbox is read-only (and means
that flags don't change and the bug isn't reproduced).
}}}

and the following report from another user:

{{{
I believe the issue happens with or without xbiff as I have noticed
mutt says a file has new mail, so I visit it, read the new mails and
exit mutt. If I launch mutt again, it says the file has new mail
again, even if all mails have been read.

It is even capable of "confusing itself" in the same session when
multiple files get mail and I visit one (using change-folder function,
which suggests unread folders and in order), it will be listed as new
mail later when I have gone to a second folder, instead of suggesting
the third folder.

I manually downgraded to 1.5.19-4, which behaves correctly. I use
relatime mount option, just in case it matters.
}}}

--------------------------------------------------------------------------------
2009-06-17 20:55:14 UTC pdmef
* Added comment:
Hmm, I can't reproduce that on OS X and Debian. Can you confirm that [b080ae086a62] is the problem? How does xbiff detect changes? Just by comparing mtime vs. atime or does it also cache them for later comparison?

* Updated description:
Forwarding from http://bugs.debian.org/533439

{{{
When mutt 1.5.20-1 writes changes to an mbox file, it
changes the file modify time to be the same as the
access time.  This includes just changing the
message-is-new / message-is-old-but-unread flags.

xbiff reports new mail
user runs mutt
xbiff reports new mail a second time


Mutt (including version 1.5.20-1) call utime to set
the modification time back to the previous value while
exiting, but in 1.5.20-1 it appears to be modified
after the call to utime.  Running strace without
tracing children doesn't show a culprit in the main
process, and running it with children traced makes
mutt believe that the mbox is read-only (and means
that flags don't change and the bug isn't reproduced).
}}}

and the following report from another user:

{{{
I believe the issue happens with or without xbiff as I have noticed
mutt says a file has new mail, so I visit it, read the new mails and
exit mutt. If I launch mutt again, it says the file has new mail
again, even if all mails have been read.

It is even capable of "confusing itself" in the same session when
multiple files get mail and I visit one (using change-folder function,
which suggests unread folders and in order), it will be listed as new
mail later when I have gone to a second folder, instead of suggesting
the third folder.

I manually downgraded to 1.5.19-4, which behaves correctly. I use
relatime mount option, just in case it matters.
}}}
* owner changed to pdmef
* status changed to assigned

--------------------------------------------------------------------------------
2009-06-17 20:55:30 UTC pdmef
* milestone changed to 1.6
* status changed to accepted

--------------------------------------------------------------------------------
2009-06-17 21:02:33 UTC antonio@dyne.org
* Added comment:
Checking with the reporter, can you please add 533439@bugs.debian.org to cc?

--------------------------------------------------------------------------------
2009-06-17 22:05:45 UTC pdmef
* Added comment:
I attached a patch that is more careful about updating access/mod time. Can you test that?

--------------------------------------------------------------------------------
2009-06-17 23:51:59 UTC antonio@dyne.org
* Added comment:
I've provided the reporter with a version of mutt which incorporates your patch, I will let you know how the testing will go

--------------------------------------------------------------------------------
2009-06-18 22:00:07 UTC dylex
* Added comment:
I have the same problem with zsh's MAILCHECK on 1.5.20.  The logic zsh uses is: {{{ st.st_size && st.st_atime <= st.st_mtime && st.st_mtime > lastmailcheck }}}.  Applying the patch above fixed the problem for me.

--------------------------------------------------------------------------------
2009-06-19 12:22:28 UTC pdmef
* Added comment:
Replying to [comment:6 dylex]:
> I have the same problem with zsh's MAILCHECK on 1.5.20.  The logic zsh uses is: {{{ st.st_size && st.st_atime <= st.st_mtime && st.st_mtime > lastmailcheck }}}.  Applying the patch above fixed the problem for me.

Is there any chance you can try this: open an mbox folder that has no new mail, change a mail to being new, sync the folder and exit. Does mutt then report new mail? And zsh?

--------------------------------------------------------------------------------
2009-06-19 13:05:16 UTC antonio@dyne.org
* Added comment:
From the reporter:

{{{
Hi Antonio and Rocco,

Thanks, but I'm still seeing the bug with 1.5.20-1+fix533439.
Setting the access time to be equal to the modification time
seems to be an edge case that works for Bash but not XBiff.

xbiff's algorithm ("reset" means the user clicked on xbiff with
the mouse):
 Now check for changes.  If reset is set then we want to pretent that
 there is no mail.  If the mailbox is empty then we want to turn off
 the flag.  Otherwise if the mailbox has changed size then we want to
 put the flag up, unless the mailbox has been read since the last 
 write.

 The cases are:
    o  forced reset by user                        DOWN
    o  no mailbox or empty (zero-sized) mailbox    DOWN
    o  if read after most recent write 		   DOWN
    o  same size as last time                      no change
    o  bigger than last time                       UP
    o  smaller than last time but non-zero         UP
 

Mutt's mbox(5) manpage matches Mutt 1.5.20's system:
 If the modification-time (usually determined via stat(2)) of a
 nonempty mbox file is greater than the access-time the file has
 new mail.

I'm not sure what to do with this bug now.




Some test results:

xbiff only checks for updates every 30 seconds, unless it's
forced to redraw by being obscured and then exposed (which I'm
doing at every stage in testing this).

Similarly I've set bash's MAILCHECK delay to 1 second.

I'm running "frm" in the middle (package mailutils, lists the
headers of all mails), but omitting that step doesn't change the
results of the other steps.

During the test below, there is other mail in the mailbox, but
none of it has the "old" or "new" flags set.

New mail received
xbiff: new mail
bash: you have new mail (or sometimes just "you have mail")

   File: `/var/mail/steve'
   Size: 731600    	Blocks: 1440       IO Block: 4096   regular file
 Device: 807h/2055d	Inode: 570084      Links: 1
 Access: (0660/-rw-rw----)  Uid: ( 1010/   steve)   Gid: ( 1010/   steve)
 Access: 2009-06-19 10:48:28.000000000 +0100
 Modify: 2009-06-19 10:50:35.000000000 +0100
 Change: 2009-06-19 10:50:35.000000000 +0100

Run "frm"
xbiff: clear
bash: (no prompt)

   File: `/var/mail/steve'
   Size: 731600    	Blocks: 1440       IO Block: 4096   regular file
 Device: 807h/2055d	Inode: 570084      Links: 1
 Access: (0660/-rw-rw----)  Uid: ( 1010/   steve)   Gid: ( 1010/   steve)
 Access: 2009-06-19 10:51:25.000000000 +0100
 Modify: 2009-06-19 10:50:35.000000000 +0100
 Change: 2009-06-19 10:50:35.000000000 +0100

Run mutt, read the mail, leave it in the mailbox and exit
xbiff: new mail
bash: (no prompt)

   File: `/var/mail/steve'
   Size: 731638    	Blocks: 1440       IO Block: 4096   regular file
 Device: 807h/2055d	Inode: 570084      Links: 1
 Access: (0660/-rw-rw----)  Uid: ( 1010/   steve)   Gid: ( 1010/   steve)
 Access: 2009-06-19 10:50:35.000000000 +0100
 Modify: 2009-06-19 10:50:35.000000000 +0100
 Change: 2009-06-19 10:52:02.000000000 +0100

Run mutt a second time and exit immediately
xbiff: clear
bash: (no prompt)

   File: `/var/mail/steve'
   Size: 731638    	Blocks: 1440       IO Block: 4096   regular file
 Device: 807h/2055d	Inode: 570084      Links: 1
 Access: (0660/-rw-rw----)  Uid: ( 1010/   steve)   Gid: ( 1010/   steve)
 Access: 2009-06-19 10:52:40.000000000 +0100
 Modify: 2009-06-19 10:50:35.000000000 +0100
 Change: 2009-06-19 10:52:40.000000000 +0100

Cheers,
Steve
}}}

--------------------------------------------------------------------------------
2009-06-19 15:34:53 UTC pdmef
* Added attachment fix-3271.diff

--------------------------------------------------------------------------------
2009-06-19 15:38:56 UTC pdmef
* Added comment:
I've updated the patch to really not mangle atime by resetting it back to where it was before the changing the file. This is what the old working code did. The atime is modified if and only if the mailbox has at least one new mail and atime is newer than mtime. The last version errorneously also set atime=mtime if no new message was found.

--------------------------------------------------------------------------------
2009-06-19 17:29:04 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [bd59be56c6b0]) Don't mangle atime/mtime for mbox folders without new mail upon sync. Closes #1362, #3271.

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2009-06-20 01:02:45 UTC roger_
* cc changed to wenrui@gmail.com
* Added comment:
Replying to [comment:9 pdmef]:

The patch's working for me on dpkg-source extracted source.

--------------------------------------------------------------------------------
2009-06-24 05:24:15 UTC dhduvall
* cc changed to wenrui@gmail.com, duvall@comfychair.org
* Added comment:
It used to be that if you left a mailbox, mutt would no longer consider it in the list of mailboxes with new mail in them until another new message arrived.  Thus I could leave new messages in a mailbox for quite some time without mutt trying to take me back there.

This fix (the combination of bd59be56c6b0 and 9ae13dedb5ed) makes the situation worse for me, in the sense that it's even further from the behavior I'm used to.  My test is to be in a mailbox with some new messages in it, force a write (by Newing and then reading another message), changing to another mailbox, and seeing whether mutt reports the mailbox I came from has new mail.

Both before and after these changesets, mutt tells me I have new mail, but prior to this, if I *don't* force the write, I don't get the message.  Now I get the message regardless of whether the mailbox had been written.

I've traced the problem back to cset b080ae086a62, which introduced reset_atime() in the first place, and was the first attempt to fix bug 1362.  Is there any support for the behavior prior to this cset, or is this something I'm going to have to maintain in a private patch?

--------------------------------------------------------------------------------
2009-06-24 13:48:46 UTC Rocco Rutte
* Added comment:
{{{
Hi,

* Mutt wrote:

I'm afraid you have to maintain your own patch reverting this if you
can't find other ways to deal with it.

The point is that it's not a new feature because in that case the mutt
way would have been to add stuff but the make old behaviour the new
default, i.e. you wouldn't have noticed it at all. For example, many
users are unaware of the fact that $check_mbox_size exists as result of
turning the compile-time option into a runtime one, because mutt (by
default) continues to use atime vs. mtime to detect new mail.

This is a bug fix and you, as well as some others, have got used to a bug.

As has been said on mutt-dev discussing the change, this "new" behaviour
is default for maildir and mh forever.

Rocco
}}}

--------------------------------------------------------------------------------
2009-06-24 13:58:39 UTC Derek Martin
* Added comment:
{{{
On Wed, Jun 24, 2009 at 05:24:16AM -0000, Mutt wrote:

If you want something like this behavior, the "correct" way to get it
is to either set mark_old, or to flag messages that you don't want to
respond to now (default key 'f'), and make sure they're not marked as
new.
}}}

--------------------------------------------------------------------------------
2009-06-24 15:02:42 UTC dhduvall
* Added comment:
Yeah, "correct" is often in the eyes (or fingers) of the beholder.  The method I've been using over the years is that new messages mean I haven't dealt with them, but I intend to, old messages mean I haven't dealt with them and I'm not going to, and flagged messages are ones I've marked as interesting and might want to go back to quickly (generally because I'm not going to remember the appropriate search terms).  So keeping new messages around is common for me, but constantly being told there are new messages just masks when a real new message comes in.  That I can't do this for maildir folders has always been really annoying, too (though at the moment, it does appear to "work").

I can see if check_mbox_size works for me, but if not, then I'll just maintain the patch; I've got a couple of others, too, and with mercurial and mq, it's pretty easy to manage.

I'd also be happy to have another flag to mean what I want, but I don't think there's any support for that, either (though if there'd be interest, I could work on a patch in my spare time).

Thanks,
Danek

--------------------------------------------------------------------------------
2009-06-29 12:59:39 UTC antonio@dyne.org
* Added comment:
two reports from two different Debian users say that the combination of both patches does not fix the problem:

---
Now it is even worse, mutt always report that my main mailbox has new
mail:

md@bongo:~$ stat -c "%X %Y %Z" ~/Mailbox
1246192154 1246192154 1246192154
md@bongo:~$ mutt
La mailbox non è stata modificata.
md@bongo:~$ stat -c "%X %Y %Z" ~/Mailbox
1246192153 1246192154 1246192168
md@bongo:~$

/dev/mapper/home on /home type ext3 (rw,nodiratime,errors=continue,user_xattr,data=ordered)

Linux bongo.bofh.it 2.6.29-1-686 #1 SMP Fri Apr 17 14:35:16 UTC 2009 i686 GNU/Linux

-- 
ciao,
Marco

* resolution changed to 
* status changed to new

--------------------------------------------------------------------------------
2009-06-29 19:44:41 UTC pdmef
* Added comment:
We're planning to rework the new mail detection/reporting area, see NewMailHandling.

--------------------------------------------------------------------------------
2009-07-01 14:29:11 UTC pdmef
* status changed to accepted

--------------------------------------------------------------------------------
2009-07-01 14:29:25 UTC pdmef
* status changed to started

--------------------------------------------------------------------------------
2010-02-16 00:15:17 UTC bunk
* cc changed to wenrui@gmail.com, duvall@comfychair.org, bunk@stusta.de

--------------------------------------------------------------------------------
2010-07-29 20:06:19 UTC ademar
* cc changed to wenrui@gmail.com, duvall@comfychair.org, bunk@stusta.de, ademar.reis@gmail.com
* Added comment:
Hi there. Any news on this or NewMailHandling at all?

--------------------------------------------------------------------------------
2010-09-13 02:54:21 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [a51df78218e8]) add $mail_check_recent boolean to control whether Mutt will notify about all new mail, or just new mail received since the last visit to a mailbox

closes #3271

partly addresses #3310

* resolution changed to fixed
* status changed to closed
