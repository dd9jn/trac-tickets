Ticket:  1016
Status:  closed
Summary: imap_home_namespace documentation

Reporter: Marco d'Itri <md@Linux.IT>
Owner:    brendan

Opened:       2002-02-01 09:00:49 UTC
Last Updated: 2007-04-05 19:49:04 UTC

Priority:  minor
Component: IMAP
Keywords:  IMAP

--------------------------------------------------------------------------------
Description:
I think a documentation clarification would be appropriate, but I don't
understand enough IMAP to do it myself.


--------------------------------------------------------------------------------
2002-02-01 09:00:49 UTC Marco d'Itri <md@Linux.IT>
* Added comment:
{{{
Package: mutt
Version: 1.3.27-1
Severity: important

I was using "imap_home_namespace=mail" for awhile to get to the mail
folders in my account.  That stopped working a few updates ago, so I
tried using "set folder={kyler@lairds.com/ssl/mail}".  That worked
for awhile, but now when I look at folders, I'm shown my root
directory and then have to manually change to my mail/ directory.

Given the various problems with SSL handling, I suspect that the
handling of "ssl/" in the folder path is to blame.  (I have not
tested without SSL because that's all my firewall allows.)

--kyler

-- System Information
Debian Release: 3.0
Architecture: i386
Kernel: Linux muzzle 2.4.17 #3 Fri Dec 21 19:23:43 EST 2001 i686
Locale: LANG=C, LC_CTYPE=C

Versions of packages mutt depends on:
ii  libc6                    2.2.5-1         GNU C Library: Shared libraries an
ii  libncurses5              5.2.20020112a-3 Shared libraries for terminal hand
ii  libsasl7                 1.5.27-2        Authentication abstraction library
ii  masqmail [mail-transport 0.1.16-1        A mailer for hosts without permane
}}}

--------------------------------------------------------------------------------
2002-02-01 09:00:50 UTC Marco d'Itri <md@Linux.IT>
* Added comment:
{{{
On Tue, Jan 29, 2002 at 09:42:50AM -0500, Kyler Laird wrote:
> I was using "imap_home_namespace=mail" for awhile to get to the mail
> folders in my account.  That stopped working a few updates ago, so I
> tried using "set folder={kyler@lairds.com/ssl/mail}".  That worked
> for awhile, but now when I look at folders, I'm shown my root
> directory and then have to manually change to my mail/ directory.

This looks like a configuration problem.
I think that the correct setting here would be:
set folder={kyler@lairds.com/ssl}mail
The bit between the braces is essentially the host specification.

However, I believe that this PINE-style IMAP mailbox specification is
now deprecated, and the preferred format is the URL-style.
Using this, I would set your folder as:
set folder=imaps://kyler@lairds.com/mail

Can you confirm that this works, so that Marco (or you) can close the
bug report?

Regards,

Andrew
-- 
Andrew McDonald
E-mail: andrew@mcdonald.org.uk
http://www.mcdonald.org.uk/andrew/
}}}

--------------------------------------------------------------------------------
2002-02-01 09:00:51 UTC Marco d'Itri <md@Linux.IT>
* Added comment:
{{{
On Tue, Jan 29, 2002 at 07:11:39PM +0000, Andrew McDonald wrote:

> > I was using "imap_home_namespace=mail" for awhile to get to the mail
> > folders in my account.  That stopped working a few updates ago, so I
> > tried using "set folder={kyler@lairds.com/ssl/mail}".  That worked
> > for awhile, but now when I look at folders, I'm shown my root
> > directory and then have to manually change to my mail/ directory.
> 
> This looks like a configuration problem.
> I think that the correct setting here would be:
> set folder={kyler@lairds.com/ssl}mail
> The bit between the braces is essentially the host specification.

Ah...yes, that's correct.  It does work that way.

> However, I believe that this PINE-style IMAP mailbox specification is
> now deprecated, and the preferred format is the URL-style.
> Using this, I would set your folder as:
> set folder=imaps://kyler@lairds.com/mail

That works also.
 
> Can you confirm that this works, so that Marco (or you) can close the
> bug report?

I am able to use this, but it would be nice to have
imap_home_namespace removed from the documentation
if it is not going to work any more.

Thank you for the help.

--kyler
}}}

--------------------------------------------------------------------------------
2002-02-01 09:00:52 UTC Marco d'Itri <md@Linux.IT>
* Added comment:
{{{
On Tue, Jan 29, 2002 at 02:49:48PM -0500, kyler@lairds.com wrote:
> On Tue, Jan 29, 2002 at 07:11:39PM +0000, Andrew McDonald wrote:
[configuration problem with "set folder" and IMAP solved]
>
> > Can you confirm that this works, so that Marco (or you) can close the
> > bug report?
> 
> I am able to use this, but it would be nice to have
> imap_home_namespace removed from the documentation
> if it is not going to work any more.

Hmm. I've never used this myself. All I can sugget is try setting it
to:
set imap_home_namespace=imaps://kyler@lairds.com/mail
and trying again.

If this doesn't work I think it probably is a bug, and should be fixed.
However, not using it myself I'm not entirely sure. :-)

(If it is a bug it is a general IMAP related problem - I can't see
anything particularly SSL related about it.)


Andrew
-- 
Andrew McDonald
E-mail: andrew@mcdonald.org.uk
http://www.mcdonald.org.uk/andrew/
}}}

--------------------------------------------------------------------------------
2002-02-01 09:00:53 UTC Marco d'Itri <md@Linux.IT>
* Added comment:
{{{
On Jan 29, Andrew McDonald <andrew@mcdonald.org.uk> wrote:

 >Hmm. I've never used this myself. All I can sugget is try setting it
 >to:
 >set imap_home_namespace=imaps://kyler@lairds.com/mail
 >and trying again.
Can you confirm this works for you?

-- 
ciao,
Marco
}}}

--------------------------------------------------------------------------------
2002-02-01 09:00:54 UTC Marco d'Itri <md@Linux.IT>
* Added comment:
{{{
On Thu, Jan 31, 2002 at 03:44:02PM +0100, Marco d'Itri wrote:

>  >set imap_home_namespace=imaps://kyler@lairds.com/mail
>  >and trying again.
> Can you confirm this works for you?

It doesn't seem to hurt anything, but using
	set folder=imaps://kyler@lairds.com/mail
is sufficient.

The example you give seems contrary to, or at least not
obvious from the documentation.
	imap_home_namespace
	Type: string
	Default: "" 

	You normally want to see your personal folders alongside
	your INBOX in the IMAP browser.  If you see something
	else, you may set this variable to the IMAP path to your
	folders.

>From this, I would expect to give a namespace just like I
had in earlier versions of mutt and in other IMAP clients
like Netscape.  That is, I expect to give a "path" relative
to the IMAP server already in use.  "mail" or "#mh", for
example.

Thank you.

--kyler
}}}

--------------------------------------------------------------------------------
2005-09-06 17:33:50 UTC brendan
* Added comment:
{{{
Refiled as doc-bug.
}}}

--------------------------------------------------------------------------------
2007-02-08 13:13:41 UTC rado
* Added comment:
{{{
Move to IMAP category
}}}

--------------------------------------------------------------------------------
2007-03-31 23:10:23 UTC brendan
* Added comment:
NAMESPACE should be fixed up before 1.6.

* Updated description:
I think a documentation clarification would be appropriate, but I don't
understand enough IMAP to do it myself.
* milestone changed to 1.6
* owner changed to brendan

--------------------------------------------------------------------------------
2007-04-05 19:49:04 UTC brendan
* Added comment:
The option in its current form is nearly useless. I've removed it.

* resolution changed to fixed
* status changed to closed
