Ticket:  2603
Status:  closed
Summary: bogus CPPFLAGS Makefile.am

Reporter: jel@cs.uni-magdeburg.de
Owner:    mutt-dev

Opened:       2006-12-10 04:31:58 UTC
Last Updated: 2007-03-04 06:12:25 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Makefile.am: CPPFLAGS=@CPPFLAGS@ -I$(includedir)

One should NOT include blindly the destination include directory, since the includes required for compilation might not reside in that directory.

E.g. if on has installed ncurses on Solaris in /usr/local and thus has CPPFLAGS/CFLAGS set to -I/usr/local/include, compilation breaks, because the preprocessor uses the termcap includes from /usr/include (curses.h), which is different wrt. the ncuses curses.h ...
E.g.: /usr/include/curses.h has
#define vidattr         vid32attr
#define wattroff        w32attroff
#define wattron         w32attron
#define wattrset        w32attrset
#define acs_map         acs32map
#define box             box32

but ncurses doesn't provide any *32* functions ...
>How-To-Repeat:
>Fix:
gsed -i -e 's,-I$(includedir),,' Makefile.*
}}}

--------------------------------------------------------------------------------
2006-12-11 17:57:06 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2006-12-10 05:31:59 +0100, jel@cs.uni-magdeburg.de wrote:
> Makefile.am: CPPFLAGS=3D@CPPFLAGS@ -I$(includedir)
>=20
> One should NOT include blindly the destination include directory,
> since the includes required for compilation might not reside in that
> directory.

I completely agree with you. The user may have chosen a correct
include path order with $C_INCLUDE_PATH, and adding the destination
include directory to CPPFLAGS will destroy this order.

--=20
Vincent Lef=E8vre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / Arenaire project (LIP, ENS-Lyon)
}}}

--------------------------------------------------------------------------------
2007-03-05 00:12:25 UTC brendan
* Added comment:
{{{
On 2006-12-10 05:31:59 +0100, jel@cs.uni-magdeburg.de wrote:
I think you're right, this is too magical. I've removed this
in tip.
}}}

* resolution changed to fixed
* status changed to closed
