Ticket:  3047
Status:  closed
Summary: Couldn't lock /sent - Could not send the message

Reporter: Xoron
Owner:    mutt-dev

Opened:       2008-04-15 12:53:25 UTC
Last Updated: 2009-01-04 20:45:24 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When sending command line messages as root, error occurs:

----

''Couldn't lock /sent

''Could not send the message.


----



- Checked that /sent exists

- Checked permissions on /sent, set to 777 as a test (made no difference)

- Does work when su'd to another users, but does not work as root

- Environment: Solaris 10 SPARC


--------------------------------------------------------------------------------
2008-05-13 05:21:22 UTC brendan
* Added comment:
What are the permissions on / ? mutt_dotlock is setuid to a non-root user, and it must be able to create a file in the directory of the mailbox, I believe. I'd have to check why mutt_dotlock is getting invoked in this case, but I bet making the perms on / agree with mutt_dotlock would help. Better would be to set record to a directory other than /, and fix permissions there.

* priority changed to minor

--------------------------------------------------------------------------------
2009-01-04 20:45:24 UTC brendan
* Added comment:
closing, no response.

* resolution changed to worksforme
* status changed to closed
