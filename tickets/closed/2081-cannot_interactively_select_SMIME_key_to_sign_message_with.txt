Ticket:  2081
Status:  closed
Summary: cannot interactively select S/MIME key to sign message with

Reporter: riccardo.murri@ictp.it
Owner:    mutt-dev

Opened:       2005-09-22 03:04:18 UTC
Last Updated: 2005-10-05 20:21:32 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Hello,

if I try to select the S/MIME key to sign a mail with from
the selection menu, mutt truncates the key name 1 char too
early ("4650ec3b." instead of "4650ec3b.0"); so, openssl fails and
the only way to select a S/MIME key is by setting the
'smime_sign_as' variable.
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-09-22 03:18:06 UTC owner@bugs.debian.org (Debian Bug Tracking System)
* Added comment:
{{{
Thank you for the additional information you have supplied regarding
this problem report.  It has been forwarded to the package maintainer(s)
and to other interested parties to accompany the original report.

Your message has been sent to the package maintainer(s):
 Adeodato Simó <asp16@alu.ua.es>

If you wish to continue to submit further information on your problem,
please send it to 318470@bugs.debian.org, as before.

Please do not reply to the address at the top of this message,
unless you wish to report a problem with the Bug-tracking system.

Debian bug tracking system administrator
(administrator, Debian Bugs database)
}}}

--------------------------------------------------------------------------------
2005-10-05 23:37:53 UTC Jeff Ito <jeffi@rcn.com>
* Added comment:
{{{
I ran into the same issue,
Debugging the code, the string (filename) is returned correctly from
smime_ask_for_key and then is truncated with a (presumably) misplaced
null, the following fixed it for me.

--- smime.c.orig        2005-10-04 12:39:36.000000000 -0400
+++ smime.c     2005-10-04 12:39:56.000000000 -0400
@@ -1989,7 +1989,7 @@

     if ((p = smime_ask_for_key (_("Sign as: "), NULL, 0)))
     {
-      p[mutt_strlen (p)-1] = '\0';
+      p[mutt_strlen (p)] = '\0';
       mutt_str_replace (&SmimeDefaultKey, p);

       msg->security |= SIGN;




Jeff
}}}

--------------------------------------------------------------------------------
2005-10-06 14:21:32 UTC brendan
* Added comment:
{{{
Thank you for the additional information you have supplied regarding
 I ran into the same issue,
Fixed in CVS. Thanks to Jeff Ito for debugging the problem.
}}}

* resolution changed to fixed
* status changed to closed
