Ticket:  3252
Status:  closed
Summary: mutt: Should use transliterations for charset conversions

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-06-13 09:06:05 UTC
Last Updated: 2009-06-17 11:12:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/480757

{{{
Hello,

When for instance I receive a utf-8 mail with some unicode-only
characters (like ' ' in this mail), and read it with mutt in a latin1
terminal, these characters show up as triple question marks. mutt should
instead use iconv("latin1//translit", "UTF-8") to convert into latin1
with transliterations, which will render ' ' as a ' ' for instance,
'€' as 'EUR', etc.
}}}

--------------------------------------------------------------------------------
2009-06-13 10:37:45 UTC Thomas Dickey
* Added comment:
{{{
On Sat, 13 Jun 2009, Mutt wrote:


...keeping in mind that translit is an extension (not standard).
}}}

--------------------------------------------------------------------------------
2009-06-13 19:57:11 UTC brendan
* Added comment:
I think it's a lot easier for the user to do this than mutt. It's a simple config (or environment) tweak for the user, but mutt would have to figure out whether translit was supported. Furthermore, enabling translit can cause information loss that's a lot subtler than ???, so not all users would want it to be forced on them. I'm closing this as wontfix.

* resolution changed to wontfix
* status changed to closed

--------------------------------------------------------------------------------
2009-06-17 11:12:00 UTC vinc17
* cc changed to 480757@bugs.debian.org, vincent@vinc17.org
* Added comment:
Perhaps, but a user who does that can then be affected by bug #3121.
