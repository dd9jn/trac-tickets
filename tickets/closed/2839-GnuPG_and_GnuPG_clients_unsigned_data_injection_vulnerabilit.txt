Ticket:  2839
Status:  closed
Summary: GnuPG and GnuPG clients unsigned data injection vulnerability

Reporter: Christoph Berg <cb@df7cb.de>
Owner:    mutt-dev

Opened:       2007-03-08 22:15:02 UTC
Last Updated: 2013-01-17 17:17:21 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
 Forwarding #413688 here as well...
 
 The attached mbox is available at http://bugs.debian.org/413688.
 
 ----- Forwarded message from J=F6 Fahlke <jorrit@jorrit.de> -----
 
 Date: Tue, 6 Mar 2007 17:01:33 +0100
 From: J=F6 Fahlke <jorrit@jorrit.de>
 Reply-To: J=F6 Fahlke <jorrit@jorrit.de>, 413688@bugs.debian.org
 To: Debian Bug Tracking System <submit@bugs.debian.org>
 Subject: Bug#413688: mutt: GnuPG and GnuPG clients unsigned data injectio=
 n
 	vulnerability
 
 Package: mutt
 Version: 1.5.13-1.1
 Severity: normal
 Tags: security
 
 [ Stealing the summary from GnuPGs announcement ]
 
 Gerardo Richarte from Core Security Technologies identified a problem
 when using GnuPG in streaming mode.
 
 The problem is actually a variant of a well known problem in the way
 signed material is presented in a MUA.  It is possible to insert
 additional text before or after a signed (or signed and encrypted)
 OpenPGP message and make the user believe that this additional text is
 also covered by the signature.  The Core Security advisory describes
 several variants of the attack; they all boil down to the fact that it
 might not be possible to identify which part of a message is actually
 signed if gpg is not used correctly.
 
 Core Securities advisory:
 http://www.coresecurity.com/?action=3Ditem&id=3D1687
 
 Announcement on the GnuPG mailinglist:
 http://lists.gnupg.org/pipermail/gnupg-announce/2007q1/000251.html
 
 I was able to verify that the second way of attack variant 2 decribed
 by Core Security does indeed work with mutt from testing.  A testcase
 is attached.
 
 MfG,
 J=F6.
 
 ----- End forwarded message -----
 
 Christoph
 --=20
 cb@df7cb.de | http://www.df7cb.de/
 
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2007-03-09 16:17:56 UTC cb
* Added comment:
{{{
fixing category...
}}}

--------------------------------------------------------------------------------
2007-04-03 00:21:53 UTC brendan
* Added comment:
Must be properly assessed before 1.6.

* component changed to crypto
* Updated description:
{{{
 Forwarding #413688 here as well...
 
 The attached mbox is available at http://bugs.debian.org/413688.
 
 ----- Forwarded message from J=F6 Fahlke <jorrit@jorrit.de> -----
 
 Date: Tue, 6 Mar 2007 17:01:33 +0100
 From: J=F6 Fahlke <jorrit@jorrit.de>
 Reply-To: J=F6 Fahlke <jorrit@jorrit.de>, 413688@bugs.debian.org
 To: Debian Bug Tracking System <submit@bugs.debian.org>
 Subject: Bug#413688: mutt: GnuPG and GnuPG clients unsigned data injectio=
 n
 	vulnerability
 
 Package: mutt
 Version: 1.5.13-1.1
 Severity: normal
 Tags: security
 
 [ Stealing the summary from GnuPGs announcement ]
 
 Gerardo Richarte from Core Security Technologies identified a problem
 when using GnuPG in streaming mode.
 
 The problem is actually a variant of a well known problem in the way
 signed material is presented in a MUA.  It is possible to insert
 additional text before or after a signed (or signed and encrypted)
 OpenPGP message and make the user believe that this additional text is
 also covered by the signature.  The Core Security advisory describes
 several variants of the attack; they all boil down to the fact that it
 might not be possible to identify which part of a message is actually
 signed if gpg is not used correctly.
 
 Core Securities advisory:
 http://www.coresecurity.com/?action=3Ditem&id=3D1687
 
 Announcement on the GnuPG mailinglist:
 http://lists.gnupg.org/pipermail/gnupg-announce/2007q1/000251.html
 
 I was able to verify that the second way of attack variant 2 decribed
 by Core Security does indeed work with mutt from testing.  A testcase
 is attached.
 
 MfG,
 J=F6.
 
 ----- End forwarded message -----
 
 Christoph
 --=20
 cb@df7cb.de | http://www.df7cb.de/
 
>Fix:
Unknown
}}}
* milestone changed to 1.6

--------------------------------------------------------------------------------
2008-05-19 08:43:43 UTC paul
* Added attachment testcase.mbox
* Added comment:
mbox file from the Debian BTS (apparently shows the problem).

--------------------------------------------------------------------------------
2008-08-20 16:27:01 UTC pdmef
* Added comment:
If I'm reading the links the right, we need to parse the status output of gnupg (once we know that we're actually using gnupg, not sure what the best/easiest/most reliable way to find out is) and buffer each new plain text part in a new tempfile since the status  output tells us only afterwards whether the text was signed. After that we can compose a final tempfile to be displayed to the user with proper visual blocks indicating what is covered by gpg and what is not.

Since now the traditional pgp interface relies on commands and is more a pipe filter mechanism, this sounds quite difficult to implement cleanly since I think we'd need to apply the status parser on all gpg output we get.

Some more questions:

 * as the commands are highly configurable, so what if a user doesn't have --status-fd in his settings? Should we enforce it?
 * how do we determine we're actually using gpg? Apply a heuristic on the given pgp_*_command/pgp_good_sig setting? Apply a heuristic on the status output we're getting?

--------------------------------------------------------------------------------
2013-01-15 05:08:14 UTC me
* Added comment:
assuming the user is using the contrib/gpg.rc shipped with mutt, they will see the following output when viewing the mailbox testcase on this ticket:

{{{
From jorrit@jorrit.de Tue Mar 06 16:01:20 2007
Date: Tue, 6 Mar 2007 16:01:20 +0100
From: Jö Fahlke <jorrit@jorrit.de>
To: Jö <joe@localhost>
Subject: test

[-- PGP output follows (current time: Mon 14 Jan 2013 09:04:54 PM PST) --]
gpg: WARNING: multiple plaintexts seen
gpg: handle plaintext failed: unexpected data
gpg: Signature made Tue 06 Mar 2007 07:36:45 AM PST using DSA key ID 4EEFD4EB
gpg: BAD signature from "Jorrit Fahlke <jorrit@jorrit.de>"
[-- End of PGP output --]

[-- BEGIN PGP MESSAGE --]

This text is inserted by the attacker

[-- END PGP MESSAGE --]
}}}

* Updated description:
{{{
 Forwarding #413688 here as well...
 
 The attached mbox is available at http://bugs.debian.org/413688.
 
 ----- Forwarded message from J=F6 Fahlke <jorrit@jorrit.de> -----
 
 Date: Tue, 6 Mar 2007 17:01:33 +0100
 From: J=F6 Fahlke <jorrit@jorrit.de>
 Reply-To: J=F6 Fahlke <jorrit@jorrit.de>, 413688@bugs.debian.org
 To: Debian Bug Tracking System <submit@bugs.debian.org>
 Subject: Bug#413688: mutt: GnuPG and GnuPG clients unsigned data injectio=
 n
 	vulnerability
 
 Package: mutt
 Version: 1.5.13-1.1
 Severity: normal
 Tags: security
 
 [ Stealing the summary from GnuPGs announcement ]
 
 Gerardo Richarte from Core Security Technologies identified a problem
 when using GnuPG in streaming mode.
 
 The problem is actually a variant of a well known problem in the way
 signed material is presented in a MUA.  It is possible to insert
 additional text before or after a signed (or signed and encrypted)
 OpenPGP message and make the user believe that this additional text is
 also covered by the signature.  The Core Security advisory describes
 several variants of the attack; they all boil down to the fact that it
 might not be possible to identify which part of a message is actually
 signed if gpg is not used correctly.
 
 Core Securities advisory:
 http://www.coresecurity.com/?action=3Ditem&id=3D1687
 
 Announcement on the GnuPG mailinglist:
 http://lists.gnupg.org/pipermail/gnupg-announce/2007q1/000251.html
 
 I was able to verify that the second way of attack variant 2 decribed
 by Core Security does indeed work with mutt from testing.  A testcase
 is attached.
 
 MfG,
 J=F6.
 
 ----- End forwarded message -----
 
 Christoph
 --=20
 cb@df7cb.de | http://www.df7cb.de/
 
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2013-01-17 17:17:21 UTC me
* resolution changed to fixed
* status changed to closed
