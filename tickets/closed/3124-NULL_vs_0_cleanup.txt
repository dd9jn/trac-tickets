Ticket:  3124
Status:  closed
Summary: NULL vs. 0 cleanup

Reporter: ahf
Owner:    mutt-dev

Opened:       2008-10-09 20:44:13 UTC
Last Updated: 2009-01-04 20:23:48 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Fairly boring NULL vs. 0 cleanup when working with pointers.

--------------------------------------------------------------------------------
2009-01-04 20:23:48 UTC brendan
* Added comment:
duplicate of #3125

* resolution changed to duplicate
* status changed to closed
