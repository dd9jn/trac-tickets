Ticket:  528
Status:  closed
Summary: mutt: it doesn't say where is the error in the .muttrc

Reporter: "Marco d'Itri" <md@Linux.IT>
Owner:    mutt-dev

Opened:       2001-03-30 13:26:32 UTC
Last Updated: 2006-03-25 15:05:11 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2.5-4
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#92114.
Please Cc all your replies to 92114@bugs.debian.org.]

From: Szerb Tamas <toma@mlf.linux.rulez.org>
Subject: mutt: it doesn't say where is the error in the .muttrc
Date: Thu, 29 Mar 2001 19:19:19 +0200


invalid precending regular expression

said the mutt but where? i have no idea. it should be nice if it reports
where the problem occured.  this `bug'' is in 1.3.15-2 too.

-- System Information
Debian Release: 2.2
Kernel Version: Linux mlf 2.0.36 #1 Tue Feb 2 12:46:29 CET 1999 i586 unknown

Versions of the packages mutt depends on:
ii  libc6          2.1.3-15       GNU C Library: Shared libraries and Timezone
ii  libncurses5    5.0-6.0potato1 Shared libraries for terminal handling
ii  sendmail       8.9.3-23       A powerful mail transport agent.
	^^^ (Provides virtual package mail-transport-agent)


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2006-03-26 09:05:11 UTC paul
* Added comment:
{{{
Closed upstream. mutt now gives the line of an error; giving
the actual character would need changes to the regex
compiler, and to be frank (and having looked at it!) I don't
think it's worth the effort.
}}}

* resolution changed to fixed
* status changed to closed
