Ticket:  3888
Status:  closed
Summary: mutt 1.5.23 and 1.7.1 segfault on valid-seeming HTML message

Reporter: Lorens
Owner:    mutt-dev

Opened:       2016-10-20 22:45:00 UTC
Last Updated: 2016-11-13 23:22:06 UTC

Priority:  critical
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
I run 1.5.23 from Ubuntu 15.10. I get a regular message (MIME, one part only and that HTML, definitely non-spam but possibly ill-generated since it's sent from a website). Hitting [enter] on the message list systematically segfaults for every message from this sender. However, hitting [v] and then [enter] correctly opens a new tab in my browser like I want it to.

My binary was stripped and optimized, so instead of upgrading Ubuntu (it's planned :) ) I downloaded and compiled 1.7.1 with debugging symbols. I still have the segfault.

{{{
Program terminated with signal SIGSEGV, Segmentation fault.
[...]
(gdb) bt
#0  0x000000000045d6dc in resolve_types (
    buf=0x1a94490 "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"> <html> <head> <meta name=\"generator\" content=\"HTML Tidy for Windows (vers 14 February 2006), see www.w3.org\">"..., 
    raw=0x1a74780 "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\"> <html> <head> <meta name=\"generator\" content=\"HTML Tidy for Windows (vers 14 February 2006), see www.w3.org\">"..., lineInfo=0x1a73090, n=11, last=12, QuoteList=0x7fff38406d98, q_level=0x7fff38406d20, 
    force_redraw=0x7fff38406d24, q_classify=2) at pager.c:883
#1  0x000000000045ed0c in display_line (f=0x1a70b40, last_pos=0x7fff38406da8, lineInfo=0x7fff38406d90, n=11, last=0x7fff38406d1c, max=0x7fff38406d18, flags=66, 
    QuoteList=0x7fff38406d98, q_level=0x7fff38406d20, force_redraw=0x7fff38406d24, SearchRE=0x7fff38406e10, pager_window=0x1a73bf0) at pager.c:1347
#2  0x00000000004607c2 in mutt_pager (banner=0x0, fname=0x7fff38407690 "/home/lorens/.tmp/TMPDIR//mutt-rush-1000-14827-1801976544388463232", flags=66, 
    extra=0x7fff38407660) at pager.c:1811
#3  0x0000000000414c52 in mutt_display_message (cur=0x1a63b00) at commands.c:214
#4  0x00000000004254f1 in mutt_index_menu () at curs_main.c:1313
#5  0x000000000044ab0e in main (argc=1, argv=0x7fff38408e28) at main.c:1228
}}}

After mucking about in gdb it seems that in trying to colorize my HTML message of some 130kB, the line

     872		      if (++(lineInfo[n].chunks) > 1)

makes "short chunks" roll over to -32768. This is not caught, variable i follows, next time through the loop i is used to index into an array, and boom.

At this point I've not tried to analyze either the regex parsing or the HTML in detail, so I can't be sure if the HTML is totally wrecked or not (the w3 validator seems to think it is, but Chrome displays it with no apparent problems).

I suppose it is possible to configure mutt to launch my browser immediately instead of having it try to colorize the HTML (problem disappears if I comment out "color body default default ." from my .muttrc), but I do hate segfaults triggered by parsing incoming mail, so I'm grading this critical.

--------------------------------------------------------------------------------
2016-10-20 22:50:58 UTC Lorens
* Added comment:
I can't attach the mbox that I spent some time anonymizing, because the site says it's spam, apparently because there are too many links in it. It's an mbox containing one mail containing one HTML page, but yes, there's a load of links in that page. 

--------------------------------------------------------------------------------
2016-10-20 23:34:05 UTC kevin8t8
* Added comment:
Thank you for the excellent bug report and debugging!  I'll commit a fix to the stable branch this weekend.

--------------------------------------------------------------------------------
2016-10-21 23:15:21 UTC kevin8t8
* Added comment:
Looking more at this, I'd really like to see all your 'color body' lines.

The lineInfo[n] is recording the matches on a *single line* on the screen.  Although of course I need to fix the segfault, I think your color regexps may need some improvement.

For instance, the one you mentioned:
{{{
color body default default .
}}}
doesn't make any sense.  This is coloring every character as "default", character by character.

This is much better accomplished with simply a
{{{
color normal default default
}}}

But that alone doesn't explain overrunning a short int.  Perhaps you have some '.*' regexps?

Would you mind also emailing me the html attachment directly, just so I can see it?

Thank you!

--------------------------------------------------------------------------------
2016-10-22 01:22:05 UTC kevin8t8
* Added comment:
It looks like fill_buffer() will seek and read an entire line in.  Then resolve_types() will generate colorizing chunks for the entire line.  The amount rendered into a single display line is determined later.

So if the html were made up of just a few really long lines, then it's possible for the "color body default default ." to overrun the short int, since it will generate a synax chunk for every single character.

I'd still like a sample html file to test with, but this explanation makes sense.

--------------------------------------------------------------------------------
2016-10-22 01:40:14 UTC kevin8t8
* Added comment:
Attaching an untested patch that should abort the colorization if we reach SHRT_MAX.

--------------------------------------------------------------------------------
2016-10-22 01:40:46 UTC kevin8t8
* Added attachment ticket-3888.patch

--------------------------------------------------------------------------------
2016-10-23 21:51:08 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"c6704c7f8e234fa3c82e1f2c8e674a5a0aa3f967" 6829:c6704c7f8e23]:
{{{
#!CommitTicketReference repository="" revision="c6704c7f8e234fa3c82e1f2c8e674a5a0aa3f967"
Fix pager segfault when lineInfo.chunks overflows. (closes #3888)

The reporter had an html attachment with extremely long lines,
combined with a color pattern of "color body default default ."
This overflowed the lineInfo.chunks, causing a segfault.

Abort the body color patterns if this happens.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2016-11-13 23:22:06 UTC Lorens
* Added comment:
Hi,

Sorry for the delay. I confirm:

- Yes, the attachments that cause this are basically a single HTML line of 130 to 150 kB (being machine-generated, presumably nobody ever felt any need to insert line breaks, or else some post-optimizer is removing them all)

- Yes! applying your patch to 1.7.1 fixes the problem, awesome. No side effect that I can see, I just get HTML displayed as text which was what I expected. 

- the "color body default default ." has been in my .muttrc since . . . I don't know. I started using mutt in 1995 or 1996 ("Hey Lorens come look there's this new program that's way better than ELM!"), but I probably didn't worry about colors immediately. I think I took a heavily customized file that produced colors I liked and pared it down, but since it was some twenty years ago I'll freely admit that I don't remember. Since I also have "color normal default default", I've commented the line.

Thank you! Happily looking forward to 20 more years of using mutt!
