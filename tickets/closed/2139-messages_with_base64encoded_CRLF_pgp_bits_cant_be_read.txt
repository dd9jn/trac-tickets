Ticket:  2139
Status:  closed
Summary: messages with base64-encoded CRLF pgp bits can't be read

Reporter: asp16@alu.ua.es
Owner:    mutt-dev

Opened:       2005-11-21 20:41:36 UTC
Last Updated: 2009-06-12 16:37:44 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
On Debian's Bug#340116 (http://bugs.debian.org/340116), Daniel Stone
reports that messages that are partially PGP-encrypted can no longer be
read by mutt, which fails with "Could not copy message".

From what I understand, this is fixed with Brendan's following commit
(since I can't reproduce with current CVS):

    2005-10-10 18:26:31  Brendan Cully  <brendan@kublai.com>  (brendan)

            * pgp.c: Make PGP decode failure non-fatal when displaying
            messages (as opposed to decode-saving them). I think it would be
            nicer to include the original text when decryption fails
            though...

However, I myself was still experiencing the problem for some messages.
After a bit of investigation, I found out that Mutt was failing for
base64-encoded messages. For example, this one, created by Mutt itself:

    http://people.debian.org/~adeodato/tmp/2005-11-21/base64.mbox

On further investigation, I saw that it only failed if the result of
base64-decoding the text contained CRLF. If one takes the above message,
and recodes the base64 chunk to not contain CRLF, it can be opened
without problems:

    http://people.debian.org/~adeodato/tmp/2005-11-21/base64-nocrlf.mbox

Also, and with respect Daniel's original report, note that even if mutt
properly opens the message, it outputs somehow confusing messages.
First:

    Could not decrypt PGP message

And then, when displaying:

    PGP message successfully decrypted.
    
Sample message:

    http://people.debian.org/~adeodato/tmp/2005-11-21/encrypted.mbox
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-11-21 21:03:25 UTC owner@bugs.debian.org (Debian Bug Tracking System)
* Added comment:
{{{
Thank you for the additional information you have supplied regarding
this problem report.  It has been forwarded to the package maintainer(s)
and to other interested parties to accompany the original report.

Your message has been sent to the package maintainer(s):
 Adeodato Simó <dato@the-barrel.org>

If you wish to continue to submit further information on your problem,
please send it to 340116@bugs.debian.org, as before.

Please do not reply to the address at the top of this message,
unless you wish to report a problem with the Bug-tracking system.

Debian bug tracking system administrator
(administrator, Debian Bugs database)
}}}

--------------------------------------------------------------------------------
2005-11-23 06:13:12 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2005-11-21 21:41:36 +0100, asp16@alu.ua.es wrote:

> However, I myself was still experiencing the problem for some
> messages. After a bit of investigation, I found out that Mutt was
> failing for base64-encoded messages. For example, this one,
> created by Mutt itself:

>     http://people.debian.org/~adeodato/tmp/2005-11-21/base64.mbox

> On further investigation, I saw that it only failed if the result
> of base64-decoding the text contained CRLF. If one takes the
> above message, and recodes the base64 chunk to not contain CRLF,
> it can be opened without problems:

The problem seems to be that mutt's MIME handler doesn't apply
text-mode rules to CRLFs in application/pgp messages, even though it
should.  (Another reason why application/pgp is a bad thing in the
first place.)

The following patch should fix this:

--- handler.c	21 Oct 2005 04:35:37 -0000	3.23
+++ handler.c	22 Nov 2005 12:09:55 -0000
@@ -1740,7 +1740,7 @@
 
 void mutt_decode_attachment (BODY *b, STATE *s)
 {
-  int istext = mutt_is_text_part (b);
+  int istext = mutt_is_text_part (b) || ((WithCrypto & APPLICATION_PGP) && mutt_is_application_pgp (b));
   iconv_t cd = (iconv_t)(-1);
 
   if (istext && s->flags & M_CHARCONV)



One might be tempted to fix this by changing mutt_is_text_part in
the first place (where application/pgp is currently explicitly
flagged as a non-text part).  The reason for not doing this is that
mutt_is_text_part is elsewhere used to control all kinds of
character set related logic that does not apply (or only poorly
applies) to application/pgp.  (Yet another reason why
application/pgp is a bad thing in the first place.  Did I mention
that already?)

Regards,
-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2005-11-23 06:21:22 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2005-11-22 13:13:12 +0100, Thomas Roessler wrote:

> The problem seems to be that mutt's MIME handler doesn't apply
> text-mode rules to CRLFs in application/pgp messages, even though it
> should.  (Another reason why application/pgp is a bad thing in the
> first place.)

And there I went into my own trap (which is, indeed, still another
reason why application/pgp and other non-MIME-related PGP
integrations are bad).

Here's the correct patch:

diff -u -r3.23 handler.c
--- handler.c	21 Oct 2005 04:35:37 -0000	3.23
+++ handler.c	22 Nov 2005 12:20:46 -0000
@@ -1754,16 +1754,16 @@
   switch (b->encoding)
   {
     case ENCQUOTEDPRINTABLE:
-      mutt_decode_quoted (s, b->length, istext, cd);
+      mutt_decode_quoted (s, b->length, istext || ((WithCrypto & APPLICATION_PGP) && mutt_is_application_pgp (b)), cd);
       break;
     case ENCBASE64:
-      mutt_decode_base64 (s, b->length, istext, cd);
+      mutt_decode_base64 (s, b->length, istext || ((WithCrypto & APPLICATION_PGP) && mutt_is_application_pgp (b)), cd);
       break;
     case ENCUUENCODED:
-      mutt_decode_uuencoded (s, b->length, istext, cd);
+      mutt_decode_uuencoded (s, b->length, istext || ((WithCrypto & APPLICATION_PGP) && mutt_is_application_pgp (b)), cd);
       break;
     default:
-      mutt_decode_xbit (s, b->length, istext, cd);
+      mutt_decode_xbit (s, b->length, istext || ((WithCrypto & APPLICATION_PGP) && mutt_is_application_pgp (b)), cd);
       break;
   }
 



-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2005-11-23 06:32:00 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2005-11-21 21:41:36 +0100, asp16@alu.ua.es wrote:

> Also, and with respect Daniel's original report, note that even
> if mutt properly opens the message, it outputs somehow confusing
> messages. First:

>     Could not decrypt PGP message
> 
> And then, when displaying:
> 
>     PGP message successfully decrypted.

*sigh*

This patch complicates the spaghetti code in pgp.c a tiny bit more,
but fixes this particular problem:

diff -u -r3.60 pgp.c
--- pgp.c	21 Oct 2005 04:35:37 -0000	3.60
+++ pgp.c	22 Nov 2005 12:30:15 -0000
@@ -243,6 +243,7 @@
 
 int pgp_application_pgp_handler (BODY *m, STATE *s)
 {
+  int could_not_decrypt = 0;
   int needpass = -1, pgp_keyblock = 0;
   int clearsign = 0, rv, rc;
   int c = 1; /* silence GCC warning */
@@ -398,15 +399,16 @@
 	}
         if (!clearsign && (!pgpout || c == EOF))
 	{
+	  could_not_decrypt = 1;
+	  pgp_void_passphrase ();
+	}
+	
+	if (could_not_decrypt && !(s->flags & M_DISPLAY))
+	{
           mutt_error _("Could not decrypt PGP message");
 	  mutt_sleep (1);
-          pgp_void_passphrase ();
-
-	  if (!(s->flags & M_DISPLAY))
-	  {
-	    rc = -1;
-	    goto out;
-	  }
+	  rc = -1;
+	  goto out;
         }
       }
       
@@ -450,7 +452,10 @@
 	if (needpass)
         {
 	  state_attach_puts (_("[-- END PGP MESSAGE --]\n"), s);
-          mutt_message _("PGP message successfully decrypted.");
+	  if (could_not_decrypt)
+	    mutt_error _("Could not decrypt PGP message");
+	  else
+	    mutt_message _("PGP message successfully decrypted.");
         }
 	else if (pgp_keyblock)
 	  state_attach_puts (_("[-- END PGP PUBLIC KEY BLOCK --]\n"), s);


-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2006-03-03 19:12:12 UTC Adeodato Simó <dato@net.com.org.es>
* Added comment:
{{{
* Thomas Roessler [Tue, 22 Nov 2005 13:25:01 +0100]:

Hi again,

>  And there I went into my own trap (which is, indeed, still another
>  reason why application/pgp and other non-MIME-related PGP
>  integrations are bad).

>  Here's the correct patch:

>  diff -u -r3.23 handler.c
>  --- handler.c	21 Oct 2005 04:35:37 -0000	3.23
>  +++ handler.c	22 Nov 2005 12:20:46 -0000
>  @@ -1754,16 +1754,16 @@
>     switch (b->encoding)
>     {
>       case ENCQUOTEDPRINTABLE:
>  -      mutt_decode_quoted (s, b->length, istext, cd);
>  +      mutt_decode_quoted (s, b->length, istext || ((WithCrypto & APPLICATION_PGP) && mutt_is_application_pgp (b)), cd);

  This indeed (which made CVS) fixed the problem back in the day. Glad
  that the bug was not closed, though, since I've received another bug
  report showing this problem (bugs.debian.org/344704).

  I've nailed it down to the combination of those base64 messages _with_
  a multipart/alternative. A sample message that fails to open with
  latest Mutt CVS can be found here:

    http://people.debian.org/~adeodato/tmp/2006-03-03/base64+multipart.mbox
    
  (It's the same message as [1], but wrapped in multipart/alternative.)

    [1] http://people.debian.org/~adeodato/tmp/2005-11-21/base64.mbox

  This one time, though, non-crlf base64 does not help.

  Thanks in advance,

-- 
Adeodato Simó                                     dato at net.com.org.es
Debian Developer                                  adeodato at debian.org
 
                              Listening to: Pedro Guerra - Siete puertas
}}}

--------------------------------------------------------------------------------
2007-01-30 12:06:08 UTC Christoph Berg <cb@df7cb.de>
* Added comment:
{{{
forwarded 369857 http://bugs.mutt.org/2139
thanks

Hi,

I think http://bugs.debian.org/369857 is another instance of mutt PR
2139. The offending mbox is attached, mutt fails to open the contained
mail with "could not copy message". Changing the Content-Type not to
be application/pgp fixes that.

Christoph
-- 
cb@df7cb.de | http://www.df7cb.de/

--4ZLFUWh1odzi/v6L
Content-Type: application/octet-stream
Content-Disposition: attachment; filename="forsaker.mbox.gz"
Content-Transfer-Encoding: base64

H4sICLMzvkUCA2ZvcnNha2VyLm1ib3gA1VlrU+M4Fv08/hUappaG6bbjVxInJOkOENI0BAIB
Gnpqakq2FVvgWMaSE0JN7W/fK+cdoB87bM1sqhscS7r33KOroytxkLIB4iQWgqmGUbRNU6+o
hqM78M2uGE7ZdCx1gFOPaQHNuKD1YIBppHls8CElIktjrgUpyxKujXHImGxAF2GGPmUxQgYy
9apdrFoVeNBLyjnxCB0Sv4r60m/CEm1uTvnJHaMBjekgS33moREVIeqedi201SfCC2U/taRZ
mr2t/NRnKarFLCQp+xAxD0ch46KBtjiNg4iofsqS7R2J4x3SjRyL9L9A81aH72hrr9W72Fau
1XZufIHOce2iVSI+tiu60y8Ry/Z80/Jc3/DNvm8YFdvu635ZV/ZJBCNS4qsXrIqWefqwCGxh
FyI0dM2uaJahGcYkxF7noouoj6hheLxctEvluH+7o6DpJ49hKQTDqNpm1SgiVS/LELr7EMG6
B6uoWbZmOKseBoYzSA3LKlmmnYzXfehPndhrTuR0q10swiqqvXLONNZzIzYcrLlZFBGhcc9f
yq6tl5t+K5W0iq2ZVlmzy79vzwOUmfWwSLVVWkRZ59QxSobpJONAkwRo8p+cIs02NcP6caYm
oai97kEVJZhztDV3XkU+g+cYsf6rLzyfcBrEWBCOlplAmKOEpAMqBPGlU5+k28p+DuOIjNWe
wCLjVRQw5it7bDAAWFU0b+fvUY8QFAqRVAsFHAvKEzxYuC1MArqDnstGcyRZSqoI11OOVR5i
Ywfd1/2Y7yCvHrP+CB54PaIDvIP8em5vGhJY3QFBqHdcSze8u+aF2zqKkrvP3dFVEN1Gumu/
bff8qzbF5l4Uhpe95oNz0bxqHheO9uLAInH/pvjYLT+cffTElZXdkebIvWmJ+NPtzVn5Yc++
vhBfCt7e54fHYss4+xKclo6Dzpcjf9Q/iC/MSnp4GPKYiNHOelrKDDMNR4NfRed3mViQjc8n
Y55kJycHne7Oaso4VduomhakDHy+4sAq5g5E+S/Zv1Zv5BD1hIwGGJYvSCNJxRhmO6daJTKt
1nsdApx5Vg5kWkKPXp44gJPBdN6RdEnirtVmkuAU8iYa51IY0Th7ADEmQUxSij+sTe5S1Fv3
0gqqmEUT0XjI7iBFJwoA/LP0DnR8NTgT4is+T14W38VsFKOtBYlG2ZQ6IBXArMCKSZ5j8axz
1v1v3HxsHZ+iLFBZJlTDMmwNVhDsP9Lw9gQEaH3FXAYhsP0SCilIz6Ewn6AAQ897XVU2bHJm
VEqOYWZBf65h+d75jflp7DwrdWa5aulf3XnKDuiNVlnFkVVg3ylbpm6EWfZ1MQUPZuUFDzn3
70HRTNvWKrpm6NZ7tIV+cwwQbUdzLJA751uq35qBMnQ+MO1SxTHDbF31TfDh/CjQDuEcB0Q9
3Ift0bbLB5bhHADOil7Ui4u10lAuOUnVZpCLLJiWi8qlKSDSipqumWjr2jAK0o9eNPRt5fuW
Ux+qkNgjfOK72doFniyocowV36g2sWtWjBIUCYZdhmgDDHoOJoXAI+JqVMy6lXTQABMqBq3d
LMJ+NCm1Jmq/3McxYaPUTaOiZTH7w8XxI6YfBI2lKeUwhsIqmcrCd465VlsxzVGrVyTllMVV
pMv9TNekoJ2mNKCwuQAh6mG3ilYWGrST9kTaOhza4z74NaqGWdWrunKQyh24GfspwecERwTV
nspZYy6HoJZ9GpGF5jl3JWWmg9+alM5hp7XAbwD0DjiQoI8pF3I8F98yAjsliwX2nnRUQYAA
87rP9YI09/ddnhQJKhf+dXHQ1sVB6aawKH2Zb7Dus+huMvYy5pnLvZS68LomfQtWXYedLTo9
ER1lH0qX6rNlu1xu5UnZrvQy95Z4QOA5dP5tzcHv6CMbgV+0S9MUK4vU+1b4p5Gv7gHXsCzV
i3ECpgV5EIVQDCKYhBCnnIj6Ye9UdZwiVGrKvG+KYw6rD1LWYz44qKL7jEGhpSYphalzI6Ks
2sVJElEP0pfFhSRIdmRyDbCoS3876EGF2YamuizllFlpdn46N3JM4kCW4EXdsID4WK55w6wo
Sk1ibdRc5o8biqKo8rPbah+eoG67i3qH7ZPWPuq0er1mu5U31nbPG8pHzMFY72PTyL/mPy6g
BsScoT2ASqKIoVEKEVXzts0AQDahHkEGzInxDgWUgTzXW3uwdUGpGsQsn7V3qCOrV9TOq1cU
YiRnHcrdhZncFExyLisUxkFVPKQYythotU+SQmIAjncIx15I0IAEEWWapi0QAVIWABQUvaE+
wYuGRVSHEepkQCEZE0TeIA+nqz5Rd+4GSiT2BirlQZISlFAC/UhuI6Gex3KsnKCYxUikbEgR
hbOsjLKfsZRqQI9IqayiKfLegCufIhcMy2F5ALkpAJpSHs4AQPmEfQqkb4kUwxLmWA6bQZsx
sD0JuTWkguIBRIvzRJcZ/TQE8PQGQU5CkNICg4QkAqKG1ZDFE4qGLBIYhYBrmAkmX4NX7mVc
ihaR4oOijC6syZBhDBli1MdwosiNZDzrpxkFqiQEFwcLesAAzwZ4yDRIhwg0AxLg58WUqGie
hQARAIiQkLV2FfVE1u+jMctA0MbAJFAmQOO4PM1gF8ogNEvmf6POGJgMpABBWCnp1639jenJ
ZabhmpwZHHFI8VBjabDReK5DXVnqUivgxpKDzIUFjIIkQHDo+RFXBS5DmTTXFdkBc+95/y+M
mo6ZA5rQ9HSpNy8uz5dW+XwbasdZt42GsPlqFtpqn1wWjqUwbue95me/S3mRMu2bl00d9kgj
SDb1mWDJdK/WBuzRJ8MJpXXl5dZV8HTf2d89O2j1H9tdXH58e3QVXOttdt4MhknzU6X8WZgH
Jhld7pKB/vbq/vbS7cdHt7v39mjPb13qX3Ij1rnfap83qXVIjqNS5/4tOdzl101AmTfD7/7N
aXlBVutk/yWqFKVu6vN/Su1nyMA/f/31T5dA0YFIkCCps5DmMWwksgGyD0YhVHPThvwvH306
RFyMIyJ5GlEfBLuo68nDTr6rqCB3QVyFMiYEhYJTPlhWXQZrY1A1ZCfQGJZWf6nodUUWkDsb
jbwYrQnR6HVPT3qn5yDlx4cnR71aAd5JjwVwmT/kWw5yg9wGuP+F6LBbw1oGXYBCz4OZlaAM
ayN/lWDfn77SN1AOFZ4BbF1JHhrLVKzQgp58aiJFwzwwGC9Y0lBWW/11Qsziv2aBzYJ7mlzr
dxyFAPv8vYAeA755B796rC9GcPR8C7pVV0B33Uxun5sjY61xqcmUTThi8Vs+7bA5suBdh/iw
MUeTtyAA8r29bAZzzjyKJ1aKyy23ZESidLw5Kq24hfKDjWFNAbJND1pKmxx+Gra5qcH+Do/X
Xvb45foyDO+b2I4jbpzdZHuwfmYm0DLyfOEUQJrEjDb45jeemYz/LeVL1NUVSdJf5XrKtPIq
XE+YritPuN77XHHaZ+cH1h/p5/ji4bJk3nK/CVxLeGgGZEry30vxMjt1ZcLPq5BcV+xXI1l5
LqGdkts5uxdRku3vDg8e03PvrJmMNhpTKD9EM7xLX6L+7xcc4K6uLNj7y9MzWUqvND115Xm9
uQmPLl3+ePTx/rZ7HZpG073ojZb1ZsnlP2IlLEdcVyYx/3+shErrM9GvrEBcjc/w3v1p4IXD
aLRE9dTwP4tmiKuuzCJ7BaZhzl4tqTfryrNJfXUyDB4Ts6g/lvVds3VwZX26uV/ZROcm/prs
LLfAs6y01kskZb1uJLH/ctWofHd9+WxluVRMekQeipZKx7xwRMu1Z54ltTBdjHdZ6pN0pfR8
qViNSF9MLQB7N82Pp6c/o/b56WW3t1yJTuvQHG0ti2b9axFFYpxIp/w+gylpbMYuT3auKKdC
Hu3Syd8j0MZXchfOFLPsla8Ka1c6G421F3KuN5A8pIcEyatNiWrit1aAIkp8C92FPBHP76sm
F9AipHyC9V3+pzQ40aP8byh1BcnLjYWLRSA/fhdWV97zyTUXjF+6WHsS49eM5LkOeGAvmAb9
7ZBv5FRkcHpnfZTfgcIk5wYRhD2FND2ooxqUyOuT5TNvWWZo3GcFyMsBL2w0pvYu5Fdpv0fS
IfXySkCbTUitIJNmkj5/d6rPcrkg0Xzvgobe8gKult8cwov/AKoY0WliIQAA

--4ZLFUWh1odzi/v6L--
}}}

--------------------------------------------------------------------------------
2009-06-12 16:37:44 UTC pdmef
* Added comment:
All supplied mboxes work without problems with latest tip. The content of forsaker.mbox is actually inline PGP wrapped in HTML labeled as application/pgp. This is so horribly broken that we can't do anything about I'm afraid.

* Updated description:
{{{
On Debian's Bug#340116 (http://bugs.debian.org/340116), Daniel Stone
reports that messages that are partially PGP-encrypted can no longer be
read by mutt, which fails with "Could not copy message".

From what I understand, this is fixed with Brendan's following commit
(since I can't reproduce with current CVS):

    2005-10-10 18:26:31  Brendan Cully  <brendan@kublai.com>  (brendan)

            * pgp.c: Make PGP decode failure non-fatal when displaying
            messages (as opposed to decode-saving them). I think it would be
            nicer to include the original text when decryption fails
            though...

However, I myself was still experiencing the problem for some messages.
After a bit of investigation, I found out that Mutt was failing for
base64-encoded messages. For example, this one, created by Mutt itself:

    http://people.debian.org/~adeodato/tmp/2005-11-21/base64.mbox

On further investigation, I saw that it only failed if the result of
base64-decoding the text contained CRLF. If one takes the above message,
and recodes the base64 chunk to not contain CRLF, it can be opened
without problems:

    http://people.debian.org/~adeodato/tmp/2005-11-21/base64-nocrlf.mbox

Also, and with respect Daniel's original report, note that even if mutt
properly opens the message, it outputs somehow confusing messages.
First:

    Could not decrypt PGP message

And then, when displaying:

    PGP message successfully decrypted.
    
Sample message:

    http://people.debian.org/~adeodato/tmp/2005-11-21/encrypted.mbox
>How-To-Repeat:
>Fix:
Unknown
}}}
* resolution changed to fixed
* status changed to closed
