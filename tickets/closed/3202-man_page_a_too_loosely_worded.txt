Ticket:  3202
Status:  closed
Summary: man page -a too loosely worded

Reporter: jidanni
Owner:    mutt-dev

Opened:       2009-03-11 13:06:10 UTC
Last Updated: 2009-03-15 15:47:41 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I have a big problem with the man page's
{{{
       -a file [...]
              Attach a file to your message using MIME.  To attach multiple files, separating filenames  and  recipient
              addresses with "--" is mandatory, e.g. mutt -a img.jpg *.png -- addr1 addr2.
}}}
Kindly change it to:
{{{
       -a file [...]
              Attach a file to your message using MIME. Must be at the
              end of the options, e.g.,
              mutt [-s subject] -a file1 [file2...] -- addr1 [addr2...]
}}}
My tests showed that even attaching one file needs this format.

Dare to do any different and well, -s 'bob@example.com is a boob' will
get to bob, etc. risks.


Proof:
{{{
echo OK, here\'s an attached patch.|mutt -a patch -s 'RE: patch for MessagesZh_{hant,tw}.php' jidanni
Can't stat jidanni: No such file or directory
jidanni: unable to attach file.

echo OK, here\'s an attached patch.|mutt -a patch -- -s 'RE: patch for MessagesZh_{hant,tw}.php' jidanni
  -s@jidanni.org
    Unrouteable address
  patchforMessagesZh_{hant@jidanni.org
    Unrouteable address
  tw}.php@jidanni.org
    Unrouteable address
To: -s@jidanni.org, RE:  patchforMessagesZh_{hant@jidanni.org,
	tw}.php@jidanni.org, jidanni@jidanni.org
}}}
(By the way, we quick users just look at the man page for quick
answers, no other docs.)


--------------------------------------------------------------------------------
2009-03-11 13:06:35 UTC jidanni
* cc changed to jidanni@jidanni.org

--------------------------------------------------------------------------------
2009-03-15 15:47:41 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [2b286d58647b]) mutt.1: Mention that -a should be last in option list. Closes #3202

* resolution changed to fixed
* status changed to closed
