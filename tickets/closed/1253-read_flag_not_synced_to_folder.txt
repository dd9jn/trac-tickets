Ticket:  1253
Status:  closed
Summary: "read" flag not synced to folder

Reporter: Oswald Buddenhagen <ossi@kde.org>
Owner:    mutt-dev

Opened:       2002-06-21 17:41:06 UTC
Last Updated: 2005-10-26 18:47:54 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.0-1
Severity: normal

-- Please type your report below this line

when i read my mail (and even reply to it ... hmm, actually, possibly 
_only_ when i reply - not sure now) and then change the folder, the read 
status is sometimes(*) not synced. [(*) i didn't find a pattern yet.] that 
is, when i return to the inbox, the messages i just replied to are marked 
new. this happens only with the inbox, iirc, but i would not bet on it.


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011002 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.3.28.cvs.indexsegfault
patch-1.3.27.bse.xtitles.1
patch-1.3.26.appoct.3
patch-1.3.15.sw.pgp-outlook.1
patch-1.3.27.admcd.gnutls.19
Md.use_editor
Md.paths_mutt.man
Md.muttbug_no_list
Md.use_etc_mailname
Md.muttbug_warning
Md.gpg_status_fd
patch-1.4.0.cd.edit_threads.9.2
patch-1.3.24.rr.compressed.1
patch-1.3.23.1.ametzler.pgp_good_sign

--- Begin /home/ossi/.muttrc
set spoolfile=~/maildir/inbox
set folder=~/maildir
set maildir_trash=yes
set mbox_type=Maildir
source "genmutthooks|"
source ~/.muttrc.all
--- End /home/ossi/.muttrc

--- Begin /home/ossi/.muttrc.all
ignore *
unignore from: reply-to: to: cc: subject: date: organization: organisation:

set record=!	#+sent-mail
set mbox=+saved-mail
set postponed=+postponed
mailboxes ! +debian +kde +kde-cvs +kde-cvs-null +kde-user +kdm-cvs +kernel +spam

set alternates='<weird regex removed>'
set from='Oswald Buddenhagen <ossi@kde.org>'
set use_from=yes

fcc-hook ~l +sent-mail

hdr_order date from reply-to to cc subject

set autoedit=yes
set auto_tag=yes
set certificate_file=~/.certificates
set confirmappend=no
set delete=yes
set edit_headers=yes
set editor=~/bin/vimmail
set include=yes
set mark_old=no
set menu_scroll=yes
set mime_forward=yes
set mime_forward_decode=yes
set move=no
set pager_stop=yes
set pgp_create_traditional=yes
set prompt_after=no
set reverse_alias=yes
set reverse_name=yes
set reverse_realname=no
set save_empty=no
set save_address=yes
set thorough_search=yes
set wait_key=no

bind pager <up> previous-line
bind pager <down> next-line
bind pager p previous-entry
bind pager n next-entry
bind pager <left> exit
bind pager <right> view-attachments
bind pager q quit

macro index <left> c?
bind index <right> display-message
bind index x sync-mailbox
bind index <up> previous-entry
bind index <down> next-entry
bind index ' ' next-page

macro generic q "<exit>q"
macro generic <left> <exit>
bind generic <right> select-entry

auto_view text/x-c text/x-c++

# Clearsign the message. May work, may not.
macro compose S "Fgpg --sign --armor --textmode --clearsign\ny^T^text/plain; format=text; x-action=sign\n" 'clearsign the message'

charset-hook US-ASCII	ISO-8859-1
charset-hook x-unknown	ISO-8859-1
set charset="ISO-8859-1"

# colors
color hdrdefault	cyan default
color quoted		green default
color signature		cyan default
color attachment	brightyellow default
color indicator		brightblack cyan
color status		brightgreen blue
color tree	brightred default
color markers	brightred default
color tilde	brightblue default
color header	brightgreen default ^From:
color header	brightgreen default ^Reply-To:
color header	brightcyan default ^To:
color header	brightcyan default ^Cc:
color header	brightwhite default ^Subject:
color body	brightred default [\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+
color body	cyan default (http|ftp)://[\-\.\,/%~_:?\#a-zA-Z0-9]+

set alias_file=~/.muttalias
source ~/.muttalias
--- End /home/ossi/.muttrc.all

--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
ignore date delivered-to precedence errors-to in-reply-to user-agent
ignore x-loop x-sender x-mailer x-msmail-priority x-mimeole x-priority
ignore x-accept-language x-authentication-warning
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro index   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro pager   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
open-hook \\.gz$ "gzip -cd %f > %t"
close-hook \\.gz$ "gzip -c %t > %f"
append-hook \\.gz$ "gzip -c %t >> %f"
color normal	white black
color attachment brightyellow black
color hdrdefault cyan black
color indicator black cyan
color markers	brightred black
color quoted	green black
color signature cyan black
color status	brightgreen blue
color tilde	blue black
color tree	red black
charset-hook windows-1250 CP1250
charset-hook windows-1251 CP1251
charset-hook windows-1252 CP1252
charset-hook windows-1253 CP1253
charset-hook windows-1254 CP1254
charset-hook windows-1255 CP1255
charset-hook windows-1256 CP1256
charset-hook windows-1257 CP1257
charset-hook windows-1258 CP1258
set ispell=ispell
set pgp_decode_command="/usr/bin/gpg   --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet  --batch  --output - %f"
set pgp_verify_command="/usr/bin/gpg   --status-fd=2 --no-verbose --quiet  --batch  --output - --verify %s %f"
set pgp_decrypt_command="/usr/bin/gpg   --status-fd=2 --passphrase-fd 0 --no-verbose --quiet  --batch  --output - %f"
set pgp_sign_command="/usr/bin/gpg    --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="/usr/bin/gpg   --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg    --batch  --quiet  --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg  --passphrase-fd 0  --batch --quiet  --no-verbose  --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="/usr/bin/gpg  --no-verbose --import -v %f"
set pgp_export_command="/usr/bin/gpg   --no-verbose --export --armor %r"
set pgp_verify_key_command="/usr/bin/gpg   --verbose --batch  --fingerprint --check-sigs %r"
set pgp_list_pubring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-keys %r" 
set pgp_list_secring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-secret-keys %r" 
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"
--- End /etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-07-19 18:54:37 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
{{{
Oswald Buddenhagen wrote:
> when i read my mail (and even reply to it ... hmm, actually, possibly 
> _only_ when i reply - not sure now) and then change the folder, the read 
> status is sometimes(*) not synced. [(*) i didn't find a pattern yet.] that 
> is, when i return to the inbox, the messages i just replied to are marked 
> new. this happens only with the inbox, iirc, but i would not bet on it.

Which mailbox type were you using?  Does this happen repeatedly if you
keep switching to another mailbox?
}}}

--------------------------------------------------------------------------------
2002-07-20 12:57:26 UTC Oswald Buddenhagen <ossi@kde.org>
* Added comment:
{{{
On Fri, Jul 19, 2002 at 10:54:37AM -0700, Michael Elkins wrote:
> Which mailbox type were you using?
>
maildir - i thought it was obvious from the config file. ;)

> Does this happen repeatedly if you keep switching to another mailbox?
>
it doesn't, iirc. no warranty.

greetings

-- 
Hi! I'm a .signature virus! Copy me into your ~/.signature, please!
--
Chaos, panic, and disorder - my work here is done.
}}}

--------------------------------------------------------------------------------
2005-08-21 10:34:04 UTC rado
* Added comment:
{{{
old, does this still apply with current versions?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-21 10:39:58 UTC Oswald Buddenhagen <ossi@kde.org>
* Added comment:
{{{
not that i would have noticed.
}}}

--------------------------------------------------------------------------------
2005-08-21 10:53:36 UTC rado
* Added comment:
{{{
not that i would have noticed.
Seems gone with current.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2005-10-27 12:47:54 UTC ab
* Added comment:
{{{
deduppe unform
}}}
