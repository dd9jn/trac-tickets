Ticket:  3316
Status:  closed
Summary: mutt hangs when some IMAP FETCH responses were ignored

Reporter: mlichvar
Owner:    brendan

Opened:       2009-08-18 15:01:57 UTC
Last Updated: 2011-06-26 04:45:46 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
I got a report for that mutt hangs when fetching headers:

https://bugzilla.redhat.com/show_bug.cgi?id=516210

It seems to be caused by server returning for some messages this:

{{{
[2009-08-15 11:02:13] 4< * OK Message 1992 no longer exists
[2009-08-15 11:02:13] 4< * 1992 FETCH (FLAGS (\Seen Old) UID 4076 INTERNALDATE "24-Sep-2008 10:57:10 +0200" RFC822.SIZE 5862 BODY[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)] "")
}}}

mutt prints to debug "msg_fetch_header: ignoring fetch response with no body" and it will hang in imap_read_headers() as msgno won't reach msgend to end the for cycle.

Making the cycle break after the last OK is received makes mutt segfault, there are holes in ctx->hdrs.

--------------------------------------------------------------------------------
2010-01-10 19:57:42 UTC raorn
* cc changed to raorn

--------------------------------------------------------------------------------
2011-03-29 16:02:24 UTC hhorak
* cc changed to raorn, hhorak@redhat.com
* Added comment:
While investigating #3288, I've seen the same behaviour as reporter, but I cannot reproduce it any more. I think it can be caused by a similar reason as #3288 and could probably have the same solution.

--------------------------------------------------------------------------------
2011-06-26 04:45:46 UTC brendan
* cc changed to raorn, hhorak@redhat.com
* Added comment:
Resolving as fixed along with #3288.

* resolution changed to fixed
* status changed to closed
