Ticket:  3681
Status:  closed
Summary: Cannot use IPv6 literal address in IMAP/POP3 URL

Reporter: evgeni
Owner:    kevin8t8

Opened:       2014-03-10 08:39:41 UTC
Last Updated: 2016-10-15 21:52:05 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
From Debian: https://bugs.debian.org/644992

====

For tests with newly installed servers I usually access IMAP and/or
POP3 Servers directly in the URL like this:

mutt -fimap://user:pass@127.0.0.1 
mutt -fpop://user:pass@127.0.0.1

This works correctly as expected.  Now I tried the same with an IPv6
address like

mutt -fimap://user:pass@'[::1]'
mutt -fpop://user:pass@'[::1]'

but with this I run into error messages and mutt terminates with the
following messages:

imap://user:pass@[::1] is an invalid IMAP path
pop://user:pass@[::1] is an invalid POP path

I also tried the same without brackets, but this also fails (as
expected, because isn't defined whether the last colon is part of the
IP or the separator of the port number).

Maybe there is some other syntax expected, but I didn't find a hint in
the manual...

====

Attaching a (pretty ugly, IMHO) patch, that solves this issue for me.

--------------------------------------------------------------------------------
2014-03-10 08:40:17 UTC evgeni
* Added attachment mutt-ipv6-literal.patch

--------------------------------------------------------------------------------
2016-09-04 20:13:16 UTC antonio@dyne.org
* Added comment:
This seems simple enough, would you mind looking into it? thanks!

--------------------------------------------------------------------------------
2016-09-04 20:13:39 UTC antonio@dyne.org
* cc changed to antonio@dyne.org

--------------------------------------------------------------------------------
2016-10-15 03:28:30 UTC kevin8t8
* owner changed to kevin8t8
* status changed to assigned

--------------------------------------------------------------------------------
2016-10-15 03:45:04 UTC kevin8t8
* Added comment:
Thanks for the ping, Antonio.

One problem with the original patch is that the mutt_socket.c changes corrupt the conn->account.host ifndef HAVE_LIBIDN.

I'm attaching a modified version of the patch that simplifies the url.c changes and adds a strdup in mutt_socket.c.  I'll test it some more this weekend before pushing it.

--------------------------------------------------------------------------------
2016-10-15 03:45:29 UTC kevin8t8
* Added attachment ticket-3681.patch

--------------------------------------------------------------------------------
2016-10-15 21:16:13 UTC kevin8t8
* Added comment:
After thinking about it some more, I don't think it's correct to leave the "[]" delimiters in the ciss.host field.

The reason this was done is because mutt converts it back and forth between a URL string, so instead the delimiters were removed inside raw_socket_open.  However there are other places in the code that use the host field, and they shouldn't all have the responsibility of stripping the "[]" off.

I'm attaching v2 patch that removes the delimiter inside ciss_parse_userhost() but puts them back on inside url_ciss_tostring() if the host name contains a ':'.  This version of the patch therefor doesn't need to touch raw_socket_open().



--------------------------------------------------------------------------------
2016-10-15 21:16:35 UTC kevin8t8
* Added attachment ticket-3681-v2.patch

--------------------------------------------------------------------------------
2016-10-15 21:52:05 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"2a6bfdb9f869d8f07b453f6697c8de0246e24755" 6817:2a6bfdb9f869]:
{{{
#!CommitTicketReference repository="" revision="2a6bfdb9f869d8f07b453f6697c8de0246e24755"
Allow IPv6 literal addresses in URLs. (closes #3681)

RFCs 2732 and 3986 specify a literal IPv6 address be surrounded by
"[]".

This patch removes the "[]" delimiters when parsing the URL, but adds
them back in url_ciss_tostring() if the host name contains a ':'.

Thanks to Evgeni Golov for the original patch.
}}}

* resolution changed to fixed
* status changed to closed
