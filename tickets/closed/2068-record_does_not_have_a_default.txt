Ticket:  2068
Status:  closed
Summary: record does not have a default

Reporter: michael.tatge@web.de
Owner:    mutt-dev

Opened:       2005-09-14 20:16:29 UTC
Last Updated: 2006-04-03 20:30:42 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
$record does not have a default. This can result in unwanted mail loss.
Since $mbox for instance _does_ have a default. $record should have one too.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-16 15:46:19 UTC ab
* Added comment:
{{{
Agreed. Thanks for the suggestion Michael. Tag patch.
}}}

--------------------------------------------------------------------------------
2006-04-04 14:30:42 UTC brendan
* Added comment:
{{{
Equivalent patch (I prefer ~/sent to ~/Sent Messages, and I
think it's more like the mbox default) applied, thanks.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:02 UTC 
* Added attachment patch-1.5.10.ab.default_record.1.gz
* Added comment:
patch-1.5.10.ab.default_record.1.gz
