Ticket:  3025
Status:  closed
Summary: configure check for md5 fails

Reporter: muttbug111
Owner:    mutt-dev

Opened:       2008-02-01 21:13:34 UTC
Last Updated: 2008-05-18 00:36:17 UTC

Priority:  major
Component: mutt
Keywords:  md5 md5sum configure

--------------------------------------------------------------------------------
Description:
The header caching option contains a check for a program md5, in preference to programs md5sum and openssl:

    AC_CHECK_PROGS([MD5], [md5 md5sum openssl], [none])

I have never heard of a program called md5 on Linux (or anywhere else), but I have had one of my own for over a decade. The check then finds md5 in preference to md5sum, creating an hcversion.h file with the line

    #define HCACHEVER 0xUsage: m

which causes building to fail.

Could you perhaps consider checking for md5sum before md5, or at least making sure with further tests that when looking for an exotic program, it's actually the one you're looking for?

This is in mutt 1.5.16 (which openSUSE 10.3 compiles with --enable-hcache).

Thanks!

--------------------------------------------------------------------------------
2008-02-01 21:33:29 UTC Kyle Wheeler
* Added attachment part0001.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Friday, February  1 at 09:13 PM, quoth Mutt:
> I have never heard of a program called md5 on Linux (or anywhere 
> else), but I have had one of my own for over a decade.

/sbin/md5 is part of MacOS X, FreeBSD, OpenBSD, and NetBSD. Probably a 
few others too. It’s hardly “exotic”. What “anywhere else” did you 
have in mind? Cygwin?

> Could you perhaps consider checking for md5sum before md5, or at 
> least making sure with further tests that when looking for an exotic 
> program, it's actually the one you're looking for?

I think it’s probably a wise idea to ensure that the program in 
question has the expected interface. +1

As for re-ordering… that’s a bandaid that would help *you*, but nobody 
else (it’s theoretically possible that a few BSD users have their own 
md5sum programs).

~Kyle
}}}

--------------------------------------------------------------------------------
2008-02-01 23:47:50 UTC muttbug111
* Added comment:
Hi Kyle,

> As for re-ordering… that’s a bandaid that would help *you*, but nobody 
> else (it’s theoretically possible that a few BSD users have their own 
> md5sum programs).

I don't use BSD (other than on firewalls behind a BUI), but md5sum is dominant in Unix (because it's a GNU tool), so the name shouldn't be re-used for something else. The test (order) is inappropriate for Linux, and probably any system which has GNU basics (aka coreutils) installed.

--------------------------------------------------------------------------------
2008-02-02 00:23:09 UTC Thomas Dickey
* Added comment:
{{{
On Fri, 1 Feb 2008, Mutt wrote:

> #3025: configure check for md5 fails
>
> Comment (by muttbug111):
>
> Hi Kyle,
>
> > As for re-ordering… that’s a bandaid that would help *you*, but nobody
> > else (it’s theoretically possible that a few BSD users have their own
> > md5sum programs).
>
> I don't use BSD (other than on firewalls behind a BUI), but md5sum is
> dominant in Unix (because it's a GNU tool), so the name shouldn't be re-
> used for something else. The test (order) is inappropriate for Linux, and
> probably any system which has GNU basics (aka coreutils) installed.

There's probably an md5sum in one of the GNU collections, but the original
program wasn't written for the GNU project.  It appears to have been 
released as public domain.

For example -

http://lists.debian.org/debian-dpkg/1999/09/msg00008.html

(there's a poor-quality wikipedia page which I won't discuss ;-)
}}}

--------------------------------------------------------------------------------
2008-02-02 01:41:16 UTC muttbug111
* Added comment:
I'm in no mood to comment on the dumb version of md5sum which Debian ships which doesn't even manage to eat its own output. They ship a functional GNU one too, though perhaps not as default. Point is, the same name of program gives sort of the functionality one expects.

On Linux it's called md5sum, not md5. Dito with GNU. That IMHO justifies to check for md5sum before md5. Alternatively, can the check be fixed if it finds md5 not to be an md5sum?

--------------------------------------------------------------------------------
2008-02-20 11:14:17 UTC pdmef
* Added comment:
Well, it's only used to generate a checksum to match header cache files against which is computed once at compile time... Given the fact that mutt ships with a complete md5 implementation, it shouldn't be too difficult to solve this by building our own md5 checksum tool with a handful lines of C.

For me this sounds far easier than making sure that what we think is the md5 checksum tool really does what it's supposed to (e.g. verify the md5 sum of a well known string or something like that).

--------------------------------------------------------------------------------
2008-05-18 00:36:17 UTC brendan
* Added comment:
(In [d4a903e7077b]) Build local md5 tool for hcachever.sh. Closes #3025.

* resolution changed to fixed
* status changed to closed
