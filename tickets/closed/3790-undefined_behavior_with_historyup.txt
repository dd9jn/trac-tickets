Ticket:  3790
Status:  closed
Summary: undefined behavior with <history-up>

Reporter: vinc17
Owner:    mutt-dev

Opened:       2015-11-08 21:50:30 UTC
Last Updated: 2015-11-19 00:17:32 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When I configure Mutt r6522 with:
{{{
./configure CFLAGS="-g -O2 -fsanitize=undefined -fno-sanitize-recover"
}}}
on my Debian/unstable amd64 machine, I get the following error with <history-up>, e.g. by typing "/" followed by the UP arrow key:
{{{
enter.c:170:3: runtime error: null pointer passed as argument 1, which is declared to never be null
}}}

--------------------------------------------------------------------------------
2015-11-09 00:46:27 UTC kevin8t8
* Added comment:
Just guessing after a quick glance that savelen is 0, so savebuf is NULL in the call to memcpy.

Some quick searches show that (for C99 and C11) technically NULL is not allowed as a parameter, even when length is 0.

Has this corner case been discussed on mutt-dev before?  It's sad the spec says so, but I guess we could create a mutt_memcpy() that explicitly checks for len == 0 and noops.

--------------------------------------------------------------------------------
2015-11-09 08:46:47 UTC vinc17
* Added comment:
Replying to [comment:1 kevin8t8]:
> Has this corner case been discussed on mutt-dev before?

I don't know for mutt-dev. But bug report #3775 mentions a patch that does: "''Guard memcpy with if (savebuf) in enter.c to avoid undefined behavior when copying zero bytes from NULL.''"

--------------------------------------------------------------------------------
2015-11-10 02:35:14 UTC kevin8t8
* Added comment:
That might be good news.  If just this part of the code is generating the len=0 case, perhaps we only need to fix it here.

If other cases come up, we can revisit whether we want to wrap memcpy.

--------------------------------------------------------------------------------
2015-11-17 21:19:25 UTC Safari
* Added comment:

enter.c replace_part() so search function finds this ticket.


--------------------------------------------------------------------------------
2015-11-19 00:17:32 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [444f1f013f1b775fae255dbd33faec3ed41a5e74]:
{{{
#!CommitTicketReference repository="" revision="444f1f013f1b775fae255dbd33faec3ed41a5e74"
Fix memcpy buf=NULL/len=0 issue in replace_part(). (closes #3790)

Calling memcpy with src or dest=NULL is technically illegal, even if
len=0.  Recent compilers seem to now be generating warnings/errors with
this.

replace_part() is currently the only place we are getting bug reports,
so for now just fix the problem in this one place.
}}}

* resolution changed to fixed
* status changed to closed
