Ticket:  3600
Status:  closed
Summary: postpone inserts space when line starts with space

Reporter: me
Owner:    me

Opened:       2012-11-30 05:06:59 UTC
Last Updated: 2012-12-01 22:34:31 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
A message consisting of the single line
 test
(with one space at the start of the line) will become
  test
when postponed.  This can be verified by checking the postponed mail
folder with a text editor.  This can be repeated: each postpone adds
a space to the start of any lines that already begin with spaces.

Which part of the code should I start looking at?  I see this in
1.5.21 and the current version in the repository, but not in 1.5.13.
(I've checked some obvious parts of the code but after ending up
in rfc2047.c realised I cannot easily determine what is likely to
be relevant.  A pointer to potentially relevant code would allow me
to binary search the commit log for the changeset that introduced
the problem.)
}}}

--------------------------------------------------------------------------------
2012-11-30 05:07:28 UTC me
* Added comment:
Confirmed this behavior with hg tip.

* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2012-11-30 05:22:25 UTC me
* Added comment:
This seems to happen both with mbox and maildir formats, so it has to be something within mutt_write_mime_body(), called from mutt_write_fcc()

--------------------------------------------------------------------------------
2012-11-30 05:47:23 UTC me
* Added comment:
The space stuffing happens prior to the mutt_write_mime_body() call.

--------------------------------------------------------------------------------
2012-11-30 05:55:55 UTC me
* Added comment:
Tracked this down to sendlib.c:1411.  What is happening is that after editing the message, mutt is performing RFC3676 space stuffing when the user is using format=flowed text.  Normally this is only supposed to happen one time, but when recalling a postponed message, the space stuffing is not undone, which is the real problem.

--------------------------------------------------------------------------------
2012-11-30 20:29:32 UTC danf
* Added comment:
I'm able to reproduce this. Section 6.4.3. of the docs specifically state "Furthermore, Mutt only applies space-stuffing once after the initial edit is finished."  So, since a postponed message has already had this space-stuffing performed, subsequent "initial" edits on messages pulled from the postponed folder should not perform space stuffing again.

--------------------------------------------------------------------------------
2012-11-30 20:29:50 UTC danf
* cc changed to Andras, Salamon, <andras@dns.net>, danf

--------------------------------------------------------------------------------
2012-12-01 22:34:31 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [109ebf6d4e53]) don't perform rfc3676 space stuffing when recalling a postponed message where it has already been done.

closes #3600

* resolution changed to fixed
* status changed to closed
