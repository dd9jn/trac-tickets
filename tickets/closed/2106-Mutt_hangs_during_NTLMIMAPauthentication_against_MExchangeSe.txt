Ticket:  2106
Status:  closed
Summary: Mutt hangs during NTLM-IMAP-authentication against M$-Exchange-Server

Reporter: hadan@mathematik.hu-berlin.de
Owner:    mutt-dev

Opened:       2005-10-07 15:24:26 UTC
Last Updated: 2006-01-08 06:57:25 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Using ltrace, the problem could traced down to the function imap_cmd_step in imap/command.c called from imap_auth_sasl in imap/auth_sasl.c: After sending

a0003 AUTHENTICATE NTLM\r\n

to the IMAP server, the server replies with a "+\r\n". But mutt expects "+ " and goes on reading on the socket. But the server expects the client to send some authentication information resulting in both client and server waiting
for the other to talk.

fetchmail - as an example - successfully authenticates  against the same server: After receiving "+\r\n" it sends a Base64 encoded message which basically contains username and realm (i.e. Windows Domain). After answering the following challange of the server ("+ <Base64encodedMessage>\r\n"), fetchmail goes on talking IMAP protocoll.

From this observation one might conclude that it could suffice if mutt was able to deal with empty continuation responses of the IMAP server. The Example for the AUTHENTICATE command in RFC 3501 suggests that an empty continuation request of the server is a legal behavior.
>How-To-Repeat:
Simply authenticate against an M$-Exchange-Server using NTLM.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-10-09 22:47:14 UTC brendan
* Added comment:
{{{
As you'd expect, this is just MS quietly violating the RFC:
...
continue-req    = "+" SP (resp-text / base64) CRLF
...
(ie the space is NOT optional)

But it is probably harmless to assume continuation as soon as + is spotted at the front of the line. Try the attached patch.
}}}

--------------------------------------------------------------------------------
2005-10-09 22:47:46 UTC brendan
* Added comment:
{{{
awaiting feedback on patch
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2006-01-09 00:57:25 UTC brendan
* Added comment:
{{{
Bug should be fixed (feedback never received)
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:20 UTC 
* Added attachment exchangecont.diff
* Added comment:
exchangecont.diff
