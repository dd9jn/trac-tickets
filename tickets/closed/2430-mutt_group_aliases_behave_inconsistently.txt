Ticket:  2430
Status:  closed
Summary: mutt group aliases behave inconsistently

Reporter: kyle-mutt-dev@memoryhole.net
Owner:    mutt-dev

Opened:       2006-08-20 18:45:50 UTC
Last Updated: 2009-03-07 15:49:14 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
If you create a group alias, like so: alias friends All My Friends <a@b.com>, <b@c.com>, <c@d.com> - then when composing a message to that group, the "to" list shows up like this: All My Friends <a@b.com>, b@c.com, c@d.com - note that the wockas (<>) have been removed from the subsequent addresses. For most MTA's, this is merely a cosmetic issue, but still, it's kinda weird.
>How-To-Repeat:
Create alias as described, then attempt to send mail to it.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-08-21 09:18:35 UTC Kyle Wheeler <kyle-mutt-dev@memoryhole.net>
* Added comment:
{{{
On Monday, August 21 at 03:22 AM, quoth Michael Tatge:
>> If you create a group alias, like so:
>> alias friends All My Friends <a@b.com>, <b@c.com>, <c@d.com>
>> then when composing a message to that group, the "to" list shows up
>> like this:
>> All My Friends <a@b.com>, b@c.com, c@d.com - note that the wockas (<>)
>> have been removed from the subsequent addresses.
>> it's kinda weird
>
>So it removes the unneeded <>. Why is that weird?

Because it's unexpected.

>Note that <> are only needed when you have a real name part.
>so a@b.com is fine
>some guy <a@b.com> <> needed. Consequently mutt removes the unnecessary
>brackets.

It's not *wrong*, I'll grant you. It's just unexpected. Perhaps it 
should merely be documented...

--- muttrc.man.head.old 2006-08-20 22:07:38.000000000 -0400
+++ muttrc.man.head     2006-08-20 22:17:49.000000000 -0400
@@ -73,3 +73,6 @@
 .IP
-\fBalias\fP defines an alias \fIkey\fP for the given addresses.
+\fBalias\fP defines an alias \fIkey\fP for the given addresses. Each
+\fIaddress\fP will be resolved into either an email address (user@example.com)
+or a named email address (User Name <user@example.com>). The address may be specified in either format, or in the format \(lquser@example.com (User
+Name)\(rq.
 \fBunalias\fP removes the alias corresponding to the given \fIkey\fP or

>> For most MTA's, this is merely a cosmetic issue
>
>For every mta it ought to be cosmetic. Otherwise it's a hughe bug.
>rfc822

Fair enough.

~Kyle
-- 
No, I don't know that Atheists should be considered as citizens, nor 
should they be considered patriots. This is one nation under God.
                                 -- George H. W. Bush, August 27, 1987

--RpqchZ26BWispMcB
Content-Type: application/pgp-signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Comment: Thank you for using encryption!

iD8DBQFE6Rf7BkIOoMqOI14RAmYmAJ4yZLO/Z/w6R9Idw3O7YGbhHyOLqgCfdAcv
kr353qb8mKy/mLIzehe0HwQ=9GtH
-----END PGP SIGNATURE-----

--RpqchZ26BWispMcB--
}}}

--------------------------------------------------------------------------------
2006-08-21 18:35:09 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Kyle,

 On Sunday, August 20, 2006 at 19:45:50 +0200, Kyle Wheeler wrote:

>| alias friends All My Friends <a@b.com>, <b@c.com>, <c@d.com>
> then when composing a message to that group, the "to" list shows up
> like this: All My Friends <a@b.com>, b@c.com, c@d.com

    Mutt canonicalizes every individual address to either "em@il" if
there is no associated realname, or "Real Name <em@il>" otherwise. Both
RFC compliant.


Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Folding@home project at <URL:http://folding.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2006-08-21 20:22:59 UTC Michael Tatge <Michael.Tatge@web.de>
* Added comment:
{{{
* On Sun, Aug 20, 2006 kyle-mutt-dev@memoryhole.net muttered:
> >Number:         2430
> >Category:       mutt
> >Synopsis:       mutt group aliases behave inconsistently
> If you create a group alias, like so:
> alias friends All My Friends <a@b.com>, <b@c.com>, <c@d.com>
> then when composing a message to that group, the "to" list shows up
> like this:
> All My Friends <a@b.com>, b@c.com, c@d.com - note that the wockas (<>)
> have been removed from the subsequent addresses.
> it's kinda weird

So it removes the unneeded <>. Why is that weird?
Note that <> are only needed when you have a real name part.
so a@b.com is fine
some guy <a@b.com> <> needed. Consequently mutt removes the unnecessary
brackets.
Lets, brake the alias down:

alias friends All My Friends <a@b.com>, <b@c.com>, <c@d.com>
cmd   alias   real name to a@b.com    , b@c.com  , c@d.com

Maybe =BBAll My Friends=AB should be the group name?
alias friends All My Friends: joe black <a@b.com>, some guy <b@c.com>, c@=
d.com;

> For most MTA's, this is merely a cosmetic issue

For every mta it ought to be cosmetic. Otherwise it's a hughe bug.
rfc822

Michael
--=20
The only "intuitive" interface is the nipple.  After that, it's all learn=
ed.
		-- Bruce Ediger, bediger@teal.csn.org, on X interfaces

PGP-Key-ID: 0xDC1A44DD
Jabber:     init[0]@amessage.de
}}}

--------------------------------------------------------------------------------
2006-08-22 06:46:12 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Monday, August 21, 2006 at 4:25:02 +0200, Kyle Wheeler wrote:

> unexpected. Perhaps it should merely be documented...

    This canonicalization is not specific to aliases: Mutt does it
everywhere, including in the sent "From:" field, or on display of
"Return-Path:" in pager. And vanishing useless <> is only one aspect
between others.

    Now, it's the third time this normalizing rewrite of addresses is
reported as a bug (see wontfix flea/1702 and thread "[BUG] headers
addresses in pager wysinwit" on mutt-dev). So documenting this gory
detail might not be so bad an idea.


Bye!	Alain.
-- 
When you post a new message, beginning a new topic, use the "mail" or
"post" or "new message" functions.
When you reply or followup, use the "reply" or "followup" functions.
Do not do the one for the other, this breaks or hijacks threads.
}}}

--------------------------------------------------------------------------------
2006-08-22 15:30:15 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2006-08-21 15:55:02 +0200, Alain Bench wrote:

>      Now, it's the third time this normalizing rewrite of addresses is
>  reported as a bug (see wontfix flea/1702 and thread "[BUG] headers
>  addresses in pager wysinwit" on mutt-dev). So documenting this gory
>  detail might not be so bad an idea.

I kind of wonder whether the "view full header" mode (right now, it
only toggles header weeding) should be turned into a "view raw
header" mode that turns off things like wrapping, and rewriting of
addresses to display IDNs properly.

-- 
Thomas Roessler   <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2006-08-22 15:53:28 UTC Michael Tatge <Michael.Tatge@web.de>
* Added comment:
{{{
* On Mon, Aug 21, 2006 Thomas Roessler (roessler@does-not-exist.org) muttered:
>  On 2006-08-21 15:55:02 +0200, Alain Bench wrote:
>  
>  >      Now, it's the third time this normalizing rewrite of addresses is
>  >  reported as a bug
>  I kind of wonder whether the "view full header" mode (right now, it
>  only toggles header weeding) should be turned into a "view raw
>  header" mode that turns off things like wrapping, and rewriting of
>  addresses to display IDNs properly.

I usually just pipe the message to less if I need the feature. But it
doesn't sound like a bad idea to have raw mode in mutt.

Michael
-- 
"...Unix, MS-DOS, and Windows NT (also known as the Good, the Bad, and
the Ugly)."
(By Matt Welsh)

PGP-Key-ID: 0xDC1A44DD
Jabber:     init[0]@amessage.de
}}}

--------------------------------------------------------------------------------
2006-08-23 08:00:37 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hi Thomas,

 On Monday, August 21, 2006 at 22:35:02 +0200, Thomas Roessler wrote:

> the "view full header" mode (right now, it only toggles header
> weeding) should be turned into a "view raw header" mode that turns off
> things like wrapping, and rewriting of addresses to display IDNs
> properly.

    Hum... A no-wrap no-idn-decode no-rewrite mode would be interesting.
In fact even a full-raw mode would be interesting too ($pipe_decode=no
pipe-to-less does work, but outside of Mutt's pager, without its
coloring, <functions>, and mini-index).

    But I question the mariage with $weed. After all users might want
weeded no-rewrite, and/or full-headers normal rewrite. Rewrite and IDN
are technically linked: What about extending $use_idn action?

    OTOS if we push the reasoning, we'll end with 42 tons of
$display_weed, $display_header_sort, $display_header_wrap,
$display_decode_idn, $display_decode_rfc2047=ask-yes,
$display_nearly_raw_but_transcode_charset... Optionally linked with
<display-toggle-weed> or not. That might be pushing too far.

    What about:

 - $weed unchanged (weeding and sorting at once).
 - $use_idn decodes IDN and rewrites addresses (with an explanation
about why those 2 are linked together, and visible consequences on <>,
"", (comments), addy (realname), syntax errors, and so on).
 - A future $pager_decode boolean (=yes by default) permits full raw
display, similarly to $pipe_decode, $print_decode, $thorough_search, and
friends. Dealing with charsets needs some thinking.
 - All this pager display-only. <edit-headers> and sent addresses are
always canonicalized.

    MHO: $pager_decode is low priority (pipe-to-less exists). But I'm
all for a no-rewrite mode: I somewhat dislike when Mutt shows me
silently dumbed-down things, What You See Is Not What Is There. Given
the IDN frequency, I'd probably set $use_idn=no permanently in muttrc.


Bye!	Alain.
-- 
When you post a new message, beginning a new topic, use the "mail" or
"post" or "new message" functions.
When you reply or followup, use the "reply" or "followup" functions.
Do not do the one for the other, this breaks or hijacks threads.
}}}

--------------------------------------------------------------------------------
2009-02-20 18:37:05 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [3f57a654639d]) Document address normalization. Closes #2430.

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2009-02-21 02:05:01 UTC vinc17
* Added comment:
"This section documents various feature that fit nowhere else."

This should be feature*s*.

--------------------------------------------------------------------------------
2009-03-07 15:49:14 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [49d3d03d41c2]) Fix typo, see #2430.
