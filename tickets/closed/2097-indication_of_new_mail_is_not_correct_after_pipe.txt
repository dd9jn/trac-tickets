Ticket:  2097
Status:  closed
Summary: indication of new mail is not correct after pipe

Reporter: kyle-mutt-dev@memoryhole.net
Owner:    mutt-dev

Opened:       2005-09-30 22:12:29 UTC
Last Updated: 2005-10-04 10:45:01 UTC

Priority:  trivial
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
When imap_peek is set to "no" (so that messages are marked as read whenever they are fetched from the server) and a message is piped to a program as part of a macro (for example, ":set pipe_decode\n|urlview.sh\n:unset pipe_decode\n") from the index, the indicator that says whether the message is still new or not doesn't get refreshed (to indicate that the message is no longer new) until either the screen gets refreshed (by pressing control-L for example) or until the cursor moves off of the message. It's probably more correct to update it immediately after the piped program returns.
>How-To-Repeat:
As described above.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-10-02 09:23:14 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Kyle, and again thank you for your reports,

 On Friday, September 30, 2005 at 23:12:29 +0200, Kyle Wheeler wrote:

> [in] the index, the indicator that says whether the message is still
> new or not doesn't get refreshed (to indicate that the message is no
> longer new) until either the screen gets refreshed (by pressing
> control-L for example) or until the cursor moves off of the message.

    Which new indicator? The "N" letter? So it's probably more or less
intended behaviour, though undocumented: See discussion in mutt/1004
""New" flag does not disappear right away in pager with
pager_index_lines > 0" if it applies. In short: Counting and matching
(coloring) new is immediate, flag N delayed as a reminder of previous
state. Note this bug was AFAICS never closed, waiting for the feature to
be documented. But it disappeared from Gnats: You'll have to search it
in archives.


    The final proposed note for 2.3.1.1. Status Flags chapter was:

| Note:  Upon opening a new (or old) message in the pager, the
| message becomes "read" immediately.  It loses its new (or old)
| status and is no longer counted or matched as such.  However, the
| N (or O) flag character remains in the index or pager status line
| while the message contents are being displayed.  This provides a
| reminder of the previous status of the message.


Bye!	Alain.
-- 
Microsoft Outlook Express users concerned about readability: For much
better viewing quotes in your messages, check the little freeware
program OE-QuoteFix by Dominik Jain on <URL:http://flash.to/oblivion/>.
It'll change your life. :-) Now exists also for Outlook.
}}}

--------------------------------------------------------------------------------
2005-10-02 22:30:07 UTC Kyle Wheeler <kyle-mutt-dev@memoryhole.net>
* Added comment:
{{{
On Saturday, October  1 at 08:45 PM, quoth Alain Bench:
> Hello Kyle, and again thank you for your reports,

:) 'allo Alain.

> > [in] the index, the indicator that says whether the message is still
> > new or not doesn't get refreshed (to indicate that the message is no
> > longer new) until either the screen gets refreshed (by pressing
> > control-L for example) or until the cursor moves off of the message.
> 
>     Which new indicator? The "N" letter? So it's probably more or less
> intended behaviour, though undocumented: See discussion in mutt/1004
> ""New" flag does not disappear right away in pager with
> pager_index_lines > 0" if it applies. In short: Counting and matching
> (coloring) new is immediate, flag N delayed as a reminder of previous
> state. Note this bug was AFAICS never closed, waiting for the feature to
> be documented. But it disappeared from Gnats: You'll have to search it
> in archives.

Ahh, indeed -- in the pager, when pager_index_lines, I expect this. My 
situation is when you stay in the index and do not view the message with 
the pager, but pipe the message to something. Because you've accessed it 
via IMAP (and because imap_peek=no), piping the message somewhere while 
staying in the index causes to message to become read.

Maybe this is also "intended" behavior... but I don't see a purpose to 
it.

Part of what makes me think that this is not intended behavior is that 
if I pipe the message by hand, i.e. type in "|urlview.sh" myself, then 
mutt marks the message as read right away. But if I pipe the message 
somewhere from within a macro, the message does not get marked as read.

~Kyle
-- 
I created a cron job to remind me of the Alamo.
                                                       -- Arun Rodriguez

--Clx92ZfkiYIKRjnr
Content-Type: application/pgp-signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Comment: Thank you for using encryption!

iD8DBQFDQBkfBkIOoMqOI14RAlWXAKC5MPULOseXX8o0hMsPWkomDFzYWgCfTOIA
YtfybkPuF/MEB/tvjRQi2Uk=EYst
-----END PGP SIGNATURE-----

--Clx92ZfkiYIKRjnr--
}}}

--------------------------------------------------------------------------------
2005-10-04 01:12:04 UTC Kyle Wheeler <kyle-mutt-dev@memoryhole.net>
* Added comment:
{{{
On Monday, October  3 at 09:15 PM, quoth Alain Bench:
> Uploaded patch-1.5.11.ab.flea_imap_2097.1 fixing matching, counting, 
> and N flag to change immediatly upon piping or printing with or 
> without tagging. Are there other index operations needing fix?

Aha! It works exactly as I expect it to now. Thanks!

I haven't noticed any other odd behavior...

> > if I pipe the message by hand, i.e. type in "|urlview.sh" myself, 
> > then mutt marks the message as read right away. But if I pipe the 
> > message somewhere from within a macro, the message does not get 
> > marked as read.
> 
>     I don't experience the direct/macro piping discrepency you state.
> Both cases mostly keep N flag until move or redraw. But... this hides
> some oddity: The messages you explicitly mark new, sync, then pipe are
> not marked read at all. They stay new, flagged N, and colored ~N, even
> with patch. Could this be IMAP server behaviour?

Ahh, it could be. On some servers, I imagine, it's possible that 
fetching a message doesn't automatically mark it as read (thus why 
imap_peek behavior was originally created as an option). For what it's 
worth, I'm using BincIMAP.

~Kyle
-- 
People who would give up their Freedom for security deserve neither.
                                                    -- Benjamin Franklin

--mP3DRpeJDSE+ciuQ
Content-Type: application/pgp-signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Comment: Thank you for using encryption!

iD8DBQFDQZCTBkIOoMqOI14RAthCAJ0SWcGJY5lGd9BFjT4zdNcVTWcwUgCg5jbX
rxBnDL7c3IvUiA8At57cSEQ=z1l1
-----END PGP SIGNATURE-----

--mP3DRpeJDSE+ciuQ--
}}}

--------------------------------------------------------------------------------
2005-10-04 12:18:22 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Sunday, October 2, 2005 at 20:35:01 +0200, Kyle Wheeler wrote:

> "intended" behavior... but I don't see a purpose to it.

    Well, the same: Keep the N as a reminder of previous state, until
next move or redraw. Granted that seems probably much less usefull than
in pager. Zero really?

    Now index would be more consistent with pager mini-index if only
color matching new or old and counts were updated immediatly... That
doesn't seem to be the case.

    Uploaded patch-1.5.11.ab.flea_imap_2097.1 fixing matching, counting,
and N flag to change immediatly upon piping or printing with or without
tagging. Are there other index operations needing fix?


> if I pipe the message by hand, i.e. type in "|urlview.sh" myself, then
> mutt marks the message as read right away. But if I pipe the message
> somewhere from within a macro, the message does not get marked as
> read.

    I don't experience the direct/macro piping discrepency you state.
Both cases mostly keep N flag until move or redraw. But... this hides
some oddity: The messages you explicitly mark new, sync, then pipe are
not marked read at all. They stay new, flagged N, and colored ~N, even
with patch. Could this be IMAP server behaviour?


Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Folding@home project at <URL:http://folding.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2005-10-04 14:12:51 UTC ab
* Added comment:
{{{
Hello Kyle, and again thank you for your reports,
 
 upload patch-1.5.11.ab.flea_imap_2097.1 for testing
}}}

--------------------------------------------------------------------------------
2005-10-04 18:02:37 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Monday, October 3, 2005 at 22:15:02 +0200, Kyle Wheeler wrote:

> On some servers, I imagine, it's possible that fetching a message
> doesn't automatically mark it as read (thus why imap_peek behavior was
> originally created as an option). For what it's worth, I'm using
> BincIMAP.

    I tried 2 servers: First an old uw-imapd 2001a that marks read from
origin N state, but as said not anymore after reading and <toggle-new>.
And another one, unknown, that doesn't mark read at all.


Bye!	Alain.
-- 
When you want to reply to a mailing list, please avoid doing so from a
digest. This often builds incorrect references and breaks threads.
}}}

--------------------------------------------------------------------------------
2005-10-04 23:30:28 UTC brendan
* Added comment:
{{{
On Sunday, October 2, 2005 at 20:35:01 +0200, Kyle Wheeler wrote:
 
Applied, thanks.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:20 UTC 
* Added attachment patch-1.5.11.ab.flea_imap_2097.1.gz
* Added comment:
patch-1.5.11.ab.flea_imap_2097.1.gz
