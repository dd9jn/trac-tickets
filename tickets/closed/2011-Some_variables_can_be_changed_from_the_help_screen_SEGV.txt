Ticket:  2011
Status:  closed
Summary: Some variables can be changed from the help screen. (SEGV!)

Reporter: ttakah@lapis.plala.or.jp
Owner:    mutt-dev

Opened:       2005-07-29 16:57:12 UTC
Last Updated: 2005-09-21 04:49:30 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Some dangerous variables can be changed from within the help.
This causes SEGV.


A segv occurs if you press 'q' after
     mutt -RnF /dev/null -e 'push "?:set sort=reverse-threads\n"'
Because the value is changed even if the pager
complains "Not available in this menu."
>How-To-Repeat:
mutt -RnF /dev/null -e 'push "?:set sort=reverse-threads\n"'
>Fix:
grep CHECK_PAGER in
http://www.momonga-linux.org/~tamo/patch-1.5.9.tamo.comval.7

-> divided; attached

}}}

--------------------------------------------------------------------------------
2005-09-20 08:52:04 UTC tamo
* Added comment:
{{{
My patch is attached.
}}}

--------------------------------------------------------------------------------
2005-09-21 22:49:30 UTC brendan
* Added comment:
{{{
Thanks for extracting the patch. I've applied it. Doesn't seem like the cleanest 
way of doing things, but of course that's not your fault. Thanks for the fix...
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:42:58 UTC 
* Added attachment patch-1.5.11.tamo.checkpager.1
* Added comment:
patch-1.5.11.tamo.checkpager.1
