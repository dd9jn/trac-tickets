Ticket:  3852
Status:  closed
Summary: Problem with Compiler: section of "mutt -v" output for version 1.6.1 on Solaris

Reporter: richburridge
Owner:    brendan

Opened:       2016-07-08 18:15:14 UTC
Last Updated: 2016-07-09 02:17:12 UTC

Priority:  minor
Component: IMAP
Keywords:  solaris

--------------------------------------------------------------------------------
Description:
I'm in the process of integrating mutt version 1.6.1 into Solaris 12.
We build it with the Studio compiler. The Studio C compiler doesn't
recognize the "-v" and "--version" command line options, so the
"Compiler:" section of the output for:

  $ mutt -v

looks like:

...
Compiler:
usage: cc [ options ] files.  Use 'cc -flags' for details
cc: Warning: Option --version passed to ld, if ld is invoked, ignored otherwise
usage: cc [ options ] files.  Use 'cc -flags' for details
cc: Sun C 5.13 SunOS_i386 2014/10/20 
...

The following patch fixes this nicely and is hopefully compatible
with version output from the GNU C compiler too:

--- mutt-1.6.1/Makefile.in.orig 2016-07-08 06:23:52.142383932 -0700
+++ mutt-1.6.1/Makefile.in      2016-07-08 11:02:13.004791220 -0700
@@ -1306,9 +1306,9 @@
 
 conststrings.c: txt2c config.status
        ( \
-               $(CC) -v || \
-               $(CC) --version || \
-               $(CC) -V || \
+               ($(CC) -v >/dev/null 2>&1 && $(CC) -v) || \
+               ($(CC) --version >/dev/null 2>&1 && $(CC) --version) || \
+               ($(CC) -V >/dev/null 2>&1 && $(CC) -V) || \
                echo "unknown compiler"; \
        ) 2>&1 | ${srcdir}/txt2c.sh cc_version >conststrings_c
        echo "$(CFLAGS)" | ${srcdir}/txt2c.sh cc_cflags >>conststrings_c

You will also need to apply it to the Makefile.am file too.

Thanks.

--------------------------------------------------------------------------------
2016-07-08 18:18:27 UTC richburridge
* Added attachment fix-version-message.patch
* Added comment:
The real patch as it got garbled with a pain text cut & paste.

--------------------------------------------------------------------------------
2016-07-08 20:19:21 UTC barsnick
* Added comment:
{{{
On Fri, Jul 08, 2016 at 18:15:15 -0000, Mutt wrote:

Does the script really need to capture stderr?

(But I agree - the output of the failing commands in the '||' shell
chain should be omitted. As your patch does.)

Moritz
}}}

--------------------------------------------------------------------------------
2016-07-08 22:15:33 UTC richburridge
* Added comment:
> Does the script really need to capture stderr?

Yes it does; without it I see the usage messages again.

--------------------------------------------------------------------------------
2016-07-09 02:17:12 UTC rich.burridge@oracle.com
* Added comment:
In [changeset:"89ae904a6b301783e19fe0fd9b07ec544c9e4d18" 6721:89ae904a6b30]:
{{{
#!CommitTicketReference repository="" revision="89ae904a6b301783e19fe0fd9b07ec544c9e4d18"
Fix conststrings compiler version string generation. (closes #3852)

The Makefile.am tries compiler flags -v, --version, and -V but
neglected to filter error messages if these flags aren't recognized.
}}}

* resolution changed to fixed
* status changed to closed
