Ticket:  3228
Status:  closed
Summary: allow ssl certificates with insecure algorithm if they are in the cache

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-05-18 20:49:48 UTC
Last Updated: 2009-05-18 20:50:27 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/

---
{{{
Source file: mutt_ssl_gnutls.c
Function: tls_check_certificate

What we see if that GNUtls classify the certificate as signed with an insecure algorithm (i.e. setting the GNUTLS_CERT_INSECURE_ALGORITHM bit in certstat), that bit is not handled, even if the cert is in the cache, that means that the cert will always be invalid and the user will be asked to accept it again even if it's in the cache.

From what I see in tls_check_certificate from line 572 to line 595 you unset all certstat bits if the certificate is in the cache (i.e. tls_compare_certificates() returned 1), unfortunately you are not handling the GNUTLS_CERT_INSECURE_ALGORITHM bit causing the side effect described above.

The attached patch fix the problem
}}}

Cheers
Antonio

--------------------------------------------------------------------------------
2009-05-18 20:50:27 UTC antonio@dyne.org
* Added comment:
dup 3229

* resolution changed to duplicate
* status changed to closed
