Ticket:  3840
Status:  closed
Summary: Ctrl-C in Mutt at start-up yields incomplete display

Reporter: vinc17
Owner:    mutt-dev

Opened:       2016-05-04 09:27:53 UTC
Last Updated: 2017-05-18 08:06:10 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
Just after I started Mutt, I typed Ctrl-C, then decided not to quit. The display was incomplete. See screenshot of a part of the terminal window.

--------------------------------------------------------------------------------
2016-05-04 09:28:10 UTC vinc17
* Added attachment mutt.png
* Added comment:
screenshot

--------------------------------------------------------------------------------
2016-05-04 09:29:27 UTC vinc17
* Added comment:
Note: this is Mutt based on r6619, with some patches (not related to the display).

--------------------------------------------------------------------------------
2016-05-04 09:31:05 UTC vinc17
* Added comment:
And this is reproducible (FYI, I have 2290 messages in my mailbox).

--------------------------------------------------------------------------------
2017-05-16 19:40:50 UTC kevin8t8
* Added comment:
Hi Vincent,

Is this still occurring with tip?

SigInt is set by the signal handler, but it isn't polled until mutt_getch().  I believe that always occurs after the display is finished drawing though.  So I'm confused how it could be interrupting rendering the display.

If it is still occurring, would you try paring down your muttrc to see if something in there influences the behavior?  Maybe I am missing something.

Thank you.

-Kevin

--------------------------------------------------------------------------------
2017-05-18 08:06:10 UTC vinc17
* Added comment:
Closing since I can't reproduce the problem, even with an old version (r6562). In both cases, the Ctrl-C is not taken into account at all before the index is drawn. So, I don't see how this could happen (unless there was a temporary bug in one of the software components on this machine that affected signal handling).

* resolution changed to worksforme
* status changed to closed
