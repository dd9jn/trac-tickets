Ticket:  1305
Status:  closed
Summary: IMAP doesn't honor unset mark_old when browsing folders

Reporter: Richard Henderson <rth@dot.sfbay.redhat.com>
Owner:    mutt-dev

Opened:       2005-07-24 16:02:32 UTC
Last Updated: 2007-02-07 19:53:31 UTC

Priority:  minor
Component: IMAP
Keywords:  IMAP, patch

--------------------------------------------------------------------------------
Description:
{{{
From rth@dot.sfbay.redhat.com Wed Aug 07 00:27:01 2002
Received: from mx2.redhat.com ([205.180.83.106])
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 17cCn6-0004cQ-00
	for <submit@bugs.guug.de>; Wed, 07 Aug 2002 00:27:01 +0200
Received: from int-mx2.corp.redhat.com (int-mx2.corp.redhat.com [172.16.27.26])
	by mx2.redhat.com (8.11.6/8.11.6) with ESMTP id g76MDms20742
	for <submit@bugs.guug.de>; Tue, 6 Aug 2002 18:13:48 -0400
Received: from potter.sfbay.redhat.com (potter.sfbay.redhat.com [172.16.27.15])
	by int-mx2.corp.redhat.com (8.11.6/8.11.6) with ESMTP id g76MSau15617
	for <submit@bugs.guug.de>; Tue, 6 Aug 2002 18:28:37 -0400
Received: from dot.sfbay.redhat.com (dot.sfbay.redhat.com [172.16.24.7])
	by potter.sfbay.redhat.com (8.11.6/8.11.6) with ESMTP id g76MSae21632
	for <submit@bugs.guug.de>; Tue, 6 Aug 2002 15:28:36 -0700
Received: (from rth@localhost)
	by dot.sfbay.redhat.com (8.11.6/8.11.6) id g76MSd003681;
	Tue, 6 Aug 2002 15:28:39 -0700
Date: Tue, 6 Aug 2002 15:28:39 -0700
From: Richard Henderson <rth@dot.sfbay.redhat.com>
Message-Id: <200208062228.g76MSd003681@dot.sfbay.redhat.com>
Subject: mutt-1.4i: IMAP doesn't honor unset mark_old when browsing folders
To: submit@bugs.guug.de

Package: mutt
Version: 1.4i
Severity: normal

-- Please type your report below this line

Since the IMAP server won't distinguish New and Old in any way
that is useful to me, I have "unset mark_old" in my muttrc.
This works fine inside the folder, but the folder browser does
not check this flag when counting messages.

The first hunk in the following patch fixes this.  The second
hunk is something that appears to want similar treatment, 
discovered only by inspection; I don't know where this gets
called from.

Thanks.


r~


--- imap/imap.c.markold	Sun Apr  7 14:19:14 2002
+++ imap/imap.c	Tue Aug  6 14:34:06 2002
@@ -1140,7 +1140,6 @@
 
 /* returns count of recent messages if new = 1, else count of total messages.
  * (useful for at least postponed function)
- * Question of taste: use RECENT or UNSEEN for new?
  *   0+   number of messages in mailbox
  *  -1    error while polling mailboxes
  */
@@ -1191,7 +1190,7 @@
 	   mutt_bit_isset(idata->capabilities,STATUS))
   {				
     snprintf (buf, sizeof (buf), "STATUS %s (%s)", mbox,
-      new ? "RECENT" : "MESSAGES");
+      new ? (option(OPTMARKOLD) ? "RECENT" : "UNSEEN") : "MESSAGES");
   }
   else
     /* Server does not support STATUS, and this is not the current mailbox.
--- mx.c.orig	Tue Aug  6 10:47:05 2002
+++ mx.c	Tue Aug  6 10:47:47 2002
@@ -1076,7 +1076,7 @@
       if (!ctx->hdrs[j]->read)
       { 
 	ctx->unread++;
-	if (!ctx->hdrs[j]->old)
+	if (!ctx->hdrs[j]->old || !option(OPTMARKOLD))
 	  ctx->new++;
       } 
 
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-12-31 18:45:23 UTC brendan
* Added comment:
{{{
Should be fixed in CVS.
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2006-03-25 15:48:36 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
Hi Richard,

This bug was set to 'feedback' at the start of the year, with a note that it
should be fixed in CVS. Have you had chance to try it?

Cheers,

-- 
Paul

Arthur: Are you aware your roommate is a hideous monster from
        another dimension with evil plans for world domination?
Thrakkorzog's roommate: Listen, a good roommate relationship is
                        based on a respect for privacy.

--x+RZeZVNR8VILNfK
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFEJIVUP9fOqdxRstoRAnKqAJ9bSSLyVqtZBWcqRgqOgc3kB66qJACfecPy
+ofo41D1HuietIa5lpTINz4=T8K3
-----END PGP SIGNATURE-----

--x+RZeZVNR8VILNfK--
}}}

--------------------------------------------------------------------------------
2006-03-27 18:10:27 UTC Richard Henderson <rth@redhat.com>
* Added comment:
{{{
On Fri, Mar 24, 2006 at 11:48:36PM +0000, Paul Walker wrote:
> This bug was set to 'feedback' at the start of the year, with a note that it
> should be fixed in CVS. Have you had chance to try it?

Nope.  Due to poor server-side filtering, I've moved to fetchmail
for the actual imaping.


r~
}}}

--------------------------------------------------------------------------------
2007-02-08 13:53:31 UTC rado
* Added comment:
{{{
On Fri, Mar 24, 2006 at 11:48:36PM +0000, Paul Walker wrote:
Supposedly fixed, OP switched to fetchmail for IMAP
=> won't reproduce/ test.
Let's assume brendan did a good job, re-open otherwise. ;)
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-02-08 13:53:32 UTC rado
* Added comment:
{{{
Move to IMAP category.
We trust in brendan this has been fixed.
}}}
