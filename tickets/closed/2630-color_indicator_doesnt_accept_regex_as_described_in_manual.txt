Ticket:  2630
Status:  closed
Summary: color indicator doesn't accept regex as described in manual

Reporter: mutt@lists.sweth.net
Owner:    mutt-dev

Opened:       2006-12-19 21:54:46 UTC
Last Updated: 2008-08-21 09:02:08 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
The mutt manual indicates that the "color" command for objects other than the index accepts an optional final argument of a regexp; "color indicator" rejects a regexp as the final argument, however.
>How-To-Repeat:
:color indicator blue default "mutt.org"
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2007-03-26 00:00:54 UTC Sweth Chandramouli <mutt@lists.sweth.net>
* Added comment:
{{{
On Sunday, 25 March 2007 at 11:44:40 EDT,
   Alain Bench (<veronatif@free.fr>) wrote:
>     Yes. In "Usage: color object foreground background [ regexp ]", the
> regexp is not really optional, but either prohibited or mandatory,
> depending on the object. And the list of objects states which ones want
> a regexp. The indicator object doesn't take a regexp (just like
> attachment, bold, error, hdrdefault, and some such...).
> 
>     This documentation seems rather clear to me, as it is. Would you (or
> anyone else) have a suggestion enhancing the clarity of this point?

Square brackets in command syntax definitions almost universally means
"optional argument", so if it's clear to you how it should be used, I
have to assume that that is because you already know how it should be
used--multiple others that I've asked about this (including others on the
mutt-users list) all agree that the usage summary given should mean that
a regexp can be optionally used with any object.

Also, until you explained it above, it was non-obvious to me that the
references to regexps in the object list was meant to indicate which
objects accepted regexps--those references seem more like clarifications
of exactly what the regexp is matched against if used for that
particular object.

The best way that I can see to clarify the docs would be to follow the
example of the index, which gets its own syntax definition, and give
separate syntaxes for regexp-compatible and non-regex-compatible
objects.  It might even make sense to generalize the index definition
into a pattern_object definition:

   color re_object foreground background regexp
   color non_re_object foreground background
   color pattern_object foreground background pattern
   uncolor pattern_object pattern [ pattern ... ]

   re_object can be one of:
      * body (match regexp in the body of messages)
      * header (match regexp in the message header)

   non_re_object can be one of:
      * attachment
      * error
      (etc ... )

   pattern_object can currently only be:
      * index (match pattern in the message index)

.  That would make it obvious to everyone that regexps are only for
certain objects.
 
>     Note that adding regexp support to indicator was previously
> discussed and rejected due to needless muttrc overcomplexity (see
> wish/1717 at <URL:http://bugs.mutt.org/1717>). The usual solution is to
> set color index at will, and use the default "mono indicator reverse".

AFAIK, mono commands only apply when using a terminal that doesn't
support color.  Do they also affect color displays as well?  Testing
just now doesn't seem to show any change if I set mono indicator
reverse.  In either case, though, ow would that give the desired effect
(in my case, to have the foreground color stay the same regardless of
whether the line is selected or not)?  (FWIW, I'm not lobbying for
addition of regexp support for indicator as a feature; it would be nice
for me, but it's not a deal-breaker.  My main concern was that the docs
don't describe the actual behaviour.)

-- Sweth.

-- 
Sweth Chandramouli - http://EthicalHomes.com/
Real Estate Brokerage through Keller Williams Realty, Vienna, VA
Mortgage Brokerage through HomeFirst Mortgage, Alexandria, VA
Solutions for buyers, sellers, and homeowners in DC, VA, and MD
Referrals to first-rate Realtors and Mortgage Brokers nationwide
}}}

--------------------------------------------------------------------------------
2007-03-26 07:27:35 UTC Derek Martin <code@pizzashack.org>
* Added comment:
{{{
On Sun, Mar 25, 2007 at 08:05:01PM +0200, Sweth Chandramouli wrote:
>  On Sunday, 25 March 2007 at 11:44:40 EDT,
>     Alain Bench (<veronatif@free.fr>) wrote:
>  >     Yes. In "Usage: color object foreground background [ regexp ]", the
>  > regexp is not really optional, but either prohibited or mandatory,
>  > depending on the object. And the list of objects states which ones want
>  > a regexp. The indicator object doesn't take a regexp (just like
>  > attachment, bold, error, hdrdefault, and some such...).
>  > 
>  >     This documentation seems rather clear to me, as it is. Would you (or
>  > anyone else) have a suggestion enhancing the clarity of this point?
>  
>  Square brackets in command syntax definitions almost universally means
>  "optional argument"

FWIW, I agree with this.  In the past I also found this confusing,
though I did get it after a second reading and actually trying it.

>     color re_object foreground background regexp
>     color non_re_object foreground background
>     color pattern_object foreground background pattern
>     uncolor pattern_object pattern [ pattern ... ]

Although a bit more verbose than I think people would generally
prefer, I agree here too.  Moreover, you will often see this style of
repeating syntax for multiple syntax forms in man pages, c.f.
useradd(8).

-- 
Derek D. Martin
code@pizzashack.org


--10jrOL3x2xqLmOsH
Content-Type: application/pgp-signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.1 (GNU/Linux)

iD8DBQFGBxN3HEnASN++rQIRAuvoAJ4hMDeEtzZ2gPo248kpjVKjSDNQEgCeNV7k
SAYFZhg5aUcM2eSdwqh3bC8=
=ME14
-----END PGP SIGNATURE-----

--10jrOL3x2xqLmOsH--
}}}

--------------------------------------------------------------------------------
2007-03-26 10:44:40 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Sweth, and thank you for reporting us this problem.

 On Tuesday, December 19, 2006 at 22:54:46 +0100, Sweth Chandramouli wrote:

> The mutt manual indicates that the "color" command for objects other
> than the index accepts an optional final argument of a regexp; "color
> indicator" rejects a regexp as the final argument, however.

    Yes. In "Usage: color object foreground background [ regexp ]", the
regexp is not really optional, but either prohibited or mandatory,
depending on the object. And the list of objects states which ones want
a regexp. The indicator object doesn't take a regexp (just like
attachment, bold, error, hdrdefault, and some such...).

    This documentation seems rather clear to me, as it is. Would you (or
anyone else) have a suggestion enhancing the clarity of this point?


> :color indicator blue default "mutt.org"

    Note that adding regexp support to indicator was previously
discussed and rejected due to needless muttrc overcomplexity (see
wish/1717 at <URL:http://bugs.mutt.org/1717>). The usual solution is to
set color index at will, and use the default "mono indicator reverse".


Bye!	Alain.
-- 
Everything about locales on Sven Mascheck's excellent site at new
location <URL:http://www.in-ulm.de/~mascheck/locale/>. The little tester
utility is at <URL:http://www.in-ulm.de/~mascheck/locale/checklocale.c>.
}}}

--------------------------------------------------------------------------------
2007-03-28 08:08:41 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Sunday, March 25, 2007 at 20:05:01 +0200, Sweth Chandramouli wrote:

| Usage: color object foreground background [ regexp ]
> Square brackets in command syntax definitions almost universally means
> "optional argument"

    Sure. But doesn't "prohibited or mandatory" fit into "optional"? No,
I must admit not really. It's a too innacurate simplification, leading
to confusion.


> give separate syntaxes for regexp-compatible and non-regex-compatible
> objects. [snip plan]

    Thank you. The regexp point indeed becomes crystal clear. But the
chapter becomes more verbose, with the added re/non-re keywords that
need to be understood, and the objects list becomes structured by this
point. I think it reduces overall understandability (any better word?)
of the chapter.

    This regexp point is important, but surely not *the* essential one
of the color chapter. I think the list of objects needs to be unique.
When the reader searches something, he first needs to find the object
easely, then only which params it takes.

    What about: Just before the objects list, insert a phrase like
"Some objects below require a regexp parameter, or require a Mutt search
pattern, as indicated. Object can be one of:". And in the single list,
include some clearer hints. It still lacks some good replacement for the
"Usage:" syntax line (ideas?).


> Testing just now doesn't seem to show any change if I set mono
> indicator reverse.

    The feature is riddled with bugs: Doesn't work with S-Lang, doesn't
work with some *curses setups, doesn't cooperate with some patches
(indexcolors), doesn't work when the wind blows, and so on. When it
accepts to work, it reverses the index color. Example: When over a red
on black line, the indicator becomes a red bar with black text.


> would that give the desired effect (in my case, to have the foreground
> color stay the same regardless of whether the line is selected or not)?

    No. But your desire would be covered by wish/1717 and its "keep"
proposal. I also would love a color scheme with smooth:

| color indicator keep brightdefault

    Thinking to it, "keep" is not enough. One may also want to pick the
underlying ink color for ones paper, and the reverse. One may also want
to pick some color for paper, and make the ink in contrast (black ink
when paper is anything bright, white (or brightwhite?) ink when paper is
anything dark). One may also want to mix attributes and colors at will
(just like with Slrn). One may also want better 16 color support,
especially for bright papers.


Bye!	Alain.
-- 
Followups to bug reports are public, and should go to bug-any@bugs.mutt.org,
and the reporter (unless he is a member of mutt-dev). Do not CC mutt-dev mailing
list, the BTS does it already. Do not send to mutt-dev only, your writings would
not be tracked. Do not remove the "mutt/nnnn:" tag from subject.
}}}

--------------------------------------------------------------------------------
2008-08-21 09:02:08 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [bc8b07e9e95b]) Improve color/mono command synopsis.
Closes #2630.

* resolution changed to fixed
* status changed to closed
