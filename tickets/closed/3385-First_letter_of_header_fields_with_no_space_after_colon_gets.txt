Ticket:  3385
Status:  closed
Summary: First letter of header fields with no space after colon gets suppressed

Reporter: bisson
Owner:    me

Opened:       2010-02-23 11:15:51 UTC
Last Updated: 2010-03-21 16:53:06 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Header fields such as "Subject:Message for you" (i.e. no space after colon) do not seem to violate RFC822; however, Mutt displays them as "Subject: essage for you" (replacing the first letter by a space).

Tested with mutt-1.5.20 (i686) over a maildir containing the file attached.

--------------------------------------------------------------------------------
2010-02-23 11:16:22 UTC bisson
* Added attachment 2,S
* Added comment:
test-email for the bug

--------------------------------------------------------------------------------
2010-02-23 11:22:31 UTC bisson
* cc changed to gaetan.bisson@loria.fr

--------------------------------------------------------------------------------
2010-03-15 00:02:12 UTC me
* Added attachment patch-1.5.20-header-field-trunc-fix.diff
* Added comment:
patch fixing the problem

--------------------------------------------------------------------------------
2010-03-15 00:03:30 UTC me
* Added comment:
The problem is the header field folding code that is invoked prior to displaying the message in the pager.  It assumed a space would always exist.  I've attached a fix for this bug.

* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2010-03-21 16:53:06 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [0a29e3f4f4b9]) Do not assume whitespace follows the colon in a header field.

Closes #3385.

* resolution changed to fixed
* status changed to closed
