Ticket:  3324
Status:  closed
Summary: mutt: smime_keys fails if tmpdir is set

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-09-03 00:06:10 UTC
Last Updated: 2015-05-15 17:50:34 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/542817

{{{
Hi!

	In my .muttrc I have set tmpdir with the following entry away
from /tmp (causes mail loss if suspend fails while a message is in the
pipe). This setting seems to confise smime_keys.

<=====
set tmpdir="$HOME/Mail/tmp"
====>

<=====
LANG="C" smime_keys add_p12 christoph.p12 

NOTE: This will ask you for two passphrases:
       1. The passphrase you used for exporting
       2. The passphrase you wish to secure your private key with.

Enter Import Password:
MAC verified OK
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
Couldn't open =tmp/smime/cert_tmp.0: No such file or directory at
/usr/bin/smime_keys line 518.
====>
}}}

--------------------------------------------------------------------------------
2015-05-15 17:50:34 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [babc30377614f627594c120af0da5d988d2455f5]:
{{{
#!CommitTicketReference repository="" revision="babc30377614f627594c120af0da5d988d2455f5"
Start cleaning up and fixing smime_keys.pl (closes #3324) (see #2456)

* Convert to using File::Temp (#3324).  This was also suggested at
  https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=775199

* Use File::Temp for add_p12 temp file. (#2456)

* Make the query_label() method a bit more robust with empty strings,
  ctrl-d, and leading spaces.

* Clean up openssl_do_verify() logic.  Mark cert as invalid
  rather that die'ing if an openssl verify command fails.

* General cleanup:
  - Clearly separate op handler, certificate management, and helper
    functions by section and using prefixes.
  - Create openssl helper functions to reduce copy/paste invocations
    and make the code clearer.
  - Make indentation consistent at 2 spaces.
  - Change handle_add_pem() to re-use handle_add_chain() once the
    correct files are identified.
  - Change openssl_parse_pem() to return a single array of data
    structures representing the parsed certs/keys.
}}}

* resolution changed to fixed
* status changed to closed
