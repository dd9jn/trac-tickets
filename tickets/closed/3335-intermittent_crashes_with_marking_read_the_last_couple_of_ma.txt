Ticket:  3335
Status:  closed
Summary: intermittent crashes with marking read the last couple of mails in an IMAP folder

Reporter: antonio@dyne.org
Owner:    me

Opened:       2009-09-12 12:27:02 UTC
Last Updated: 2010-09-11 14:25:11 UTC

Priority:  minor
Component: IMAP
Keywords:  patch crash

--------------------------------------------------------------------------------
Description:
This is the analysis of debian bug http://bugs.debian.org/543467

The user hit Ctrl+R to mark a thread as read but this did not mark some messages as read; then he hit TAB to go to the first of these messages and hit again Ctrl+R, this caused the segfault.


From the analysis of the corefile the problem happens in flags.c:mutt_thread_set_flags, when while(cur->parent), cur is defined as *hdr->thread which is, strangely, 0x0, that causes the segfault. Honestly I don't know why this happens but at least a Ctrl+L should fix the view.

This is the dump of the *hdr of the message which causes the segfault:
{{{
(gdb) print *hdr
$2 = {security = 0, mime = 0, flagged = 0, tagged = 0, appended = 0, purged = 0, deleted = 0, changed = 0, attach_del = 0, old = 0, read = 0, expired = 0, 
  superseded = 0, replied = 0, subject_changed = 0, threaded = 0, display_subject = 0, recip_valid = 1, active = 1, trash = 0, zhours = 3, zminutes = 0, 
  zoccident = 0, searched = 0, matched = 0, attach_valid = 0, collapsed = 0, limited = 0, num_hidden = 0, recipient = 0, pair = 256, 
  date_sent = 1251183950, received = 1251183965, offset = 0, lines = 0, index = 1345, msgno = 1345, virtual = 1345, score = 0, env = 0x2db8a70, 
  content = 0x2db8f30, path = 0x0, tree = 0x0, thread = 0x0, attach_total = 0, chain = 0x0, refno = 0, data = 0x2db1480, maildir_flags = 0x0}
}}}

as you can see tree and thread are both 0x0, even if the message is threaded in the index

To reproduce we need to have a message marked with these two fields as null, so:
{{{
1) gdb /usr/bin/mutt 
2) (gdb) set args -f $HOME/mailbox-with-thread
3) (gdb) break _mutt_set_flag
4) (gdb) run
5) select a message and hit N, this will bring you
to the gdb prompt, from there take a note of the address of h
6) (gdb) d 1
7) (gdb) c
8) mark all thread as New, then hit Ctrl+C
9) (gdb) set ((struct header *)theaddressofh)->thread=0x0
10) (gdb) set ((struct header *)theaddressofh)->tree=0x0
11) (gdb) continue
12) go the message previously tagged as new and hit ctrl+R, this will cause the segfault
}}}

The attached patch fixes the problem

--------------------------------------------------------------------------------
2009-09-12 12:29:03 UTC antonio@dyne.org
* Added attachment 543467-thread-segfault.patch

--------------------------------------------------------------------------------
2010-08-08 21:35:16 UTC me
* milestone changed to 1.6

--------------------------------------------------------------------------------
2010-08-08 21:35:31 UTC me
* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2010-08-11 00:15:19 UTC me
* Added attachment imap-null-thread-debug.diff
* Added comment:
debugging patch

--------------------------------------------------------------------------------
2010-08-11 00:16:32 UTC me
* Added comment:
I've attached a debugging patch which will cause Mutt to dump core as soon as the proximate cause of this bug is detected.  Hopefully that will give some clue as to why it is happening.

* status changed to started

--------------------------------------------------------------------------------
2010-08-12 02:15:01 UTC me
* Added comment:
I set up a gmail account and subscribed to several high volume mailing lists.  I open the "+[Gmail]/All Mail" with "set sort=threads sort_aux=last-date" and leave Mutt open.  So far I have not been able to get this crash.

Any hints as to settings/actions that are more likely to trigger this bug?

* keywords changed to patch crash

--------------------------------------------------------------------------------
2010-08-17 03:38:44 UTC me
* Added attachment imap-idle-newmail.diff.txt
* Added comment:
proposed fix

--------------------------------------------------------------------------------
2010-08-17 03:39:55 UTC me
* Added comment:
This bug occurs when the user has an IMAP mailbox open and $imap_keepalive < $timeout.

The patch attached to this bug report should resolve the issue.

* component changed to IMAP
* status changed to infoneeded

--------------------------------------------------------------------------------
2010-08-17 04:22:06 UTC me
* Added attachment imap-idle-newmail.diff.2
* Added comment:
updated patch

--------------------------------------------------------------------------------
2010-08-17 04:22:51 UTC me
* Added comment:
Please see the updated patch version #2

--------------------------------------------------------------------------------
2010-09-11 14:25:11 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [1916aa200ddc]) fix crash when new mail is received during IMAP IDLE

closes #3335

* resolution changed to fixed
* status changed to closed
