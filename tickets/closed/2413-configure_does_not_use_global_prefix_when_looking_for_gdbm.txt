Ticket:  2413
Status:  closed
Summary: configure does not use global prefix when looking for gdbm

Reporter: gunnar@ekolin.se
Owner:    mutt-dev

Opened:       2006-08-11 10:05:45 UTC
Last Updated: 2007-04-05 22:22:32 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Calling:

./prepare \
        --prefix /sw \
        --with-homespool \
        --enable-imap \
        --enable-hcache \
        --with-curses=/sw \
        --with-gdbm \
        --with-ssl 

Will not find a usable gdbm even though that one is installed in the /sw tree pointed to by --prefix. When looking at the configur.in script I also noted that it is not possible to specify an explicit location of gdbm using the form --with-gdbm=/sw. To be able to build mutt with gdbm i did the following "hack" in configure.in. Can someone that knows the semantics and syntax of configure.in please do better solution?

cvs diff
? doc/manual.xml
cvs diff: Diffing .
Index: configure.in
===================================================================
RCS file: /home/roessler/cvs/mutt/configure.in,v
retrieving revision 3.52
diff -r3.52 configure.in
793,794c793,794
<         CPPFLAGS="$OLDCPPFLAGS"
<         LIBS="$OLDLIBS -lgdbm";
---
>         CPPFLAGS="$OLDCPPFLAGS -I/sw/include"
>         LIBS="$OLDLIBS -L/sw/lib -lgdbm";
855,856c855,856
<         CPPFLAGS="$OLDCPPFLAGS"
<         LIBS="$OLDLIBS -lgdbm";
---
>         CPPFLAGS="$OLDCPPFLAGS -I/sw/include"
>         LIBS="$OLDLIBS -L/sw/lib -lgdbm";
cvs diff: Diffing contrib
cvs diff: Diffing contrib/iconv
cvs diff: Diffing doc
cvs diff: Diffing imap
cvs diff: Diffing intl
cvs diff: Diffing m4
cvs diff: Diffing po


Also note that the same problem applies to some of the other databases that one may select via prepre/configure
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-08-12 04:13:46 UTC Ralf Wildenhues <wildenhues@ins.uni-bonn.de>
* Added comment:
{{{
* gunnar@ekolin.se wrote on Fri, Aug 11, 2006 at 11:05:45AM CEST:
> 
> ./prepare \
>         --prefix /sw \
>         --with-homespool \
>         --enable-imap \
>         --enable-hcache \
>         --with-curses=/sw \
>         --with-gdbm \
>         --with-ssl 
> 
> Will not find a usable gdbm even though that one is installed in the
> /sw tree pointed to by --prefix. When looking at the configur.in
> script I also noted that it is not possible to specify an explicit
> location of gdbm using the form --with-gdbm=/sw.

Just add to the above command line
  CPPFLAGS=-I/sw/include LDFLAGS=-L/sw/lib

Cheers,
Ralf
}}}

--------------------------------------------------------------------------------
2007-04-05 22:22:32 UTC brendan
* Added comment:
--with-gdbm accepts a prefix as of [7cf4415ec23d]

* Updated description:
{{{
Calling:

./prepare \
        --prefix /sw \
        --with-homespool \
        --enable-imap \
        --enable-hcache \
        --with-curses=/sw \
        --with-gdbm \
        --with-ssl 

Will not find a usable gdbm even though that one is installed in the /sw tree pointed to by --prefix. When looking at the configur.in script I also noted that it is not possible to specify an explicit location of gdbm using the form --with-gdbm=/sw. To be able to build mutt with gdbm i did the following "hack" in configure.in. Can someone that knows the semantics and syntax of configure.in please do better solution?

cvs diff
? doc/manual.xml
cvs diff: Diffing .
Index: configure.in
===================================================================
RCS file: /home/roessler/cvs/mutt/configure.in,v
retrieving revision 3.52
diff -r3.52 configure.in
793,794c793,794
<         CPPFLAGS="$OLDCPPFLAGS"
<         LIBS="$OLDLIBS -lgdbm";
---
>         CPPFLAGS="$OLDCPPFLAGS -I/sw/include"
>         LIBS="$OLDLIBS -L/sw/lib -lgdbm";
855,856c855,856
<         CPPFLAGS="$OLDCPPFLAGS"
<         LIBS="$OLDLIBS -lgdbm";
---
>         CPPFLAGS="$OLDCPPFLAGS -I/sw/include"
>         LIBS="$OLDLIBS -L/sw/lib -lgdbm";
cvs diff: Diffing contrib
cvs diff: Diffing contrib/iconv
cvs diff: Diffing doc
cvs diff: Diffing imap
cvs diff: Diffing intl
cvs diff: Diffing m4
cvs diff: Diffing po


Also note that the same problem applies to some of the other databases that one may select via prepre/configure
>How-To-Repeat:
>Fix:
Unknown
}}}
* resolution changed to fixed
* status changed to closed
