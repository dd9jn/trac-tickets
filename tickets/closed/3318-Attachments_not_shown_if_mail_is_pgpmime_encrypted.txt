Ticket:  3318
Status:  closed
Summary: Attachments not shown if mail is pgp/mime encrypted

Reporter: tannhauser
Owner:    mutt-dev

Opened:       2009-08-25 08:21:55 UTC
Last Updated: 2013-11-19 22:45:21 UTC

Priority:  major
Component: mutt
Keywords:  pgp attachment

--------------------------------------------------------------------------------
Description:
Since using Version 1.5.20 on Arch Linux (1.5.18 was ok), i can't see attachments if the message is pgp/mime encrypted. i can use the mailcap file to show them inline. to complicate this, test mails send with mutt to myself worked without a problem. a typical output looks like this, here we have an rtf-file attached.

{{{
[-- Attachment #1 --]
[-- Type: multipart/encrypted, Encoding: 7bit, Size: 29K --]

[-- PGP output follows (current time: Tue 25 Aug 2009 12:00:49 AM CEST) --]
[-- End of PGP output --]

[-- The following data is PGP/MIME encrypted --]

[-- Attachment #1 --]
[-- Type: text/plain, Encoding: quoted-printable, Size: 0.1K --]

some text

[-- Attachment #2: text.rtf --]
[-- Type: application/rtf, Encoding: base64, Size: 75K --

[-- Autoview using unrtf --text '/tmp/text.rtf' --]

[the rtf-file shown inline]

}}}


If i press 'v', i.e. to save the attachment, i only get this, with 1 being the the whole encrypted message, including all attachments:



{{{
  I     1 <no description>                 [multipa/encrypted, 7bit, 29K]
  I     2 <no description>         [text/plain, quoted, iso-8859-1, 0.1K]
}}}

The gpg.rc is the one provided with the mutt package.

My .mailcap:

{{{
text/html;           elinks -dump %s; nametemplate=%s.html; copiousoutput
application/msword;  antiword %s; copiousoutput
text/rtf;            unrtf --text %s; copiousoutput
application/rtf;     unrtf --text %s; copiousoutput
image/*;             feh %s > /dev/null;
application/pdf;     evince %s;

}}}


mutt -v:

{{{
Mutt 1.5.20 (2009-06-14)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.30-ARCH (i686)
slang: 20104
hcache backend: GDBM version 1.8.3. 10/15/2002 (built May 10 2009 07:54:56)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
-HAVE_REGCOMP  +USE_GNU_REGEX  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  +CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
}}}









--------------------------------------------------------------------------------
2010-01-09 13:00:19 UTC longlivedeath
* Added comment:
Happens to me too with mutt 1.5.20 on Ubuntu 9.10. I can't see attachments sent by Thunderbird/Enigmail users.

--------------------------------------------------------------------------------
2010-01-09 13:03:51 UTC longlivedeath
* cc changed to the.dead.shall.rise@gmail.com

--------------------------------------------------------------------------------
2013-11-05 23:55:02 UTC kevin8t8
* Added comment:
I am testing this with Thunderbird 24.1.0 and mutt 1.5.22.  So far I can't duplicate this problem.

I have the "use PGP/MIME by default" option enabled in Thunderbird, and have sent a couple encrypted message using Thunderbird: one with an RTF attachment, and one with a PDF attachment.  In both cases, the attachment is displaying fine in mutt, but inline and in the "v" menu:
{{{
  I     1 <no description>                      [text/plain, quoted, iso-8859-1, 0.1K] 
  A     2 foo.rtf                                          [applica/rtf, base64, 0.1K] 
}}}


Can you still duplicate this problem?

--------------------------------------------------------------------------------
2013-11-19 22:45:21 UTC kevin8t8
* resolution changed to worksforme
* status changed to closed
