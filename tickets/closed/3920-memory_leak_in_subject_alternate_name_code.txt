Ticket:  3920
Status:  closed
Summary: memory leak in subject alternate name code.

Reporter: m-a
Owner:    mutt-dev

Opened:       2017-03-02 09:11:49 UTC
Last Updated: 2017-03-02 23:55:12 UTC

Priority:  major
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
When a certificate processed by mutt provides subject alternat[iv]e name extension, mutt forgets to free the name stack before letting the pointer go out of scope. The attached fix plugs that leak, and is orthogonal to (independent of) the code discussed in #3916 and commutes with it (can be applied in any order).

--------------------------------------------------------------------------------
2017-03-02 09:12:24 UTC m-a
* Added attachment ticket-3920-fix-memory-leak.patch

--------------------------------------------------------------------------------
2017-03-02 23:55:12 UTC Matthias Andree <matthias.andree@gmx.de>
* Added comment:
In [changeset:"5fc3c0729a07174d5c26d03a6351730342a4817e" 6956:5fc3c0729a07]:
{{{
#!CommitTicketReference repository="" revision="5fc3c0729a07174d5c26d03a6351730342a4817e"
SSL: Fix memory leak in subject alternative name code. (closes #3920)
}}}

* resolution changed to fixed
* status changed to closed
