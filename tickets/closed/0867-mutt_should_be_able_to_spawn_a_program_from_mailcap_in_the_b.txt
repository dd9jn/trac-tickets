Ticket:  867
Status:  closed
Summary: mutt should be able to spawn a program (from mailcap) in the background

Reporter: ma@dt.e-technik.uni-dortmund.de (Matthias Andree)
Owner:    mutt-dev

Opened:       2001-11-13 08:30:43 UTC
Last Updated: 2005-08-22 11:06:50 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.22.1i
Severity: wishlist

-- Please type your report below this line

mutt should be able to spawn a program from mailcap in the background,
to be collected with wait() when mutt exits (much like emacs does when
spawning Netscape). A current entry like

	application/msexcel;gnumeric %s &

will not work because the file is gone when gnumeric tries to open it.

	application/msexcel;gnumeric %s & sleep 5

is the obvious workaround, but depends on gnumeric opening the file
within 5 seconds, which may or may not work out, depending on I/O and
CPU load.

-- Mutt Version Information

Mutt 1.3.22.1i (2001-08-30)
Copyright (C) 1996-2001 Michael R. Elkins und andere.
Mutt übernimmt KEINERLEI GEWÄHRLEISTUNG. Starten Sie `mutt -vv', um
weitere Details darüber zu erfahren. Mutt ist freie Software. 
Sie können es unter bestimmten Bedingungen weitergeben; starten Sie
`mutt -vv' für weitere Details.

System: Linux 2.4.14 [using ncurses 5.0]
Einstellungen bei der Compilierung:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, verwenden Sie bitte das Programm flea(1).



>How-To-Repeat:
>Fix:
application/msexcel; mutt_bgrun gnumeric %s
}}}

--------------------------------------------------------------------------------
2005-08-02 12:10:17 UTC brendan
* Added comment:
{{{
Changed from sw-bug to change-request. This can be handled by an appropriate helper application, and should possibly be closed.
}}}

--------------------------------------------------------------------------------
2005-08-23 05:06:50 UTC ab
* Added comment:
{{{
Thanks for the suggestion, Matthias. This is doable 
with mutt_bgrun. Closing.
}}}

* resolution changed to fixed
* status changed to closed
