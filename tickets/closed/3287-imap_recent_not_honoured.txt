Ticket:  3287
Status:  closed
Summary: imap recent not honoured

Reporter: prlw1
Owner:    brendan

Opened:       2009-07-01 10:47:43 UTC
Last Updated: 2013-01-17 17:04:49 UTC

Priority:  major
Component: IMAP
Keywords:  recent status noop exists

--------------------------------------------------------------------------------
Description:
eg:


{{{
4> a0006 NOOP^M
4< * 223258 EXISTS
Handling EXISTS
cmd_handle_untagged: New mail in INBOX - 223258 messages total.
4< * 205 RECENT
4< a0006 OK Completed
IMAP queue drained
}}}

but mutt says New:4
which are the 4 messages which arrived while mutt was running i.e., not including the 205 which arrived since mutt had last viewed the inbox.

I am not using the header cache, and this is against a cyrus imap server. Using today's (1st July) mercurial source.


--------------------------------------------------------------------------------
2009-07-05 05:35:42 UTC brendan
* milestone changed to 1.6

--------------------------------------------------------------------------------
2012-05-25 15:56:17 UTC dog
* Added comment:
RECENT is not actually that useful since it only refers to the number of messages that have appeared during the current session (i.e. since the last SELECT or EXAMINE from that client). At best RECENT is an indicator that the mailbox contents have changed and that we need to reexamine the /Seen flags to determine which messages are actually new. The number it gives is merely an indicator of the extent to which the mailbox may have changed.

--------------------------------------------------------------------------------
2013-01-17 17:04:49 UTC me
* Added comment:
I believe this bug has been fixed by [a51df78218e8] in which the $mail_check_recent option controls whether or not we use UNSEEN or RECENT to report the number of new messages in the mailbox. 

* Updated description:
eg:


{{{
4> a0006 NOOP^M
4< * 223258 EXISTS
Handling EXISTS
cmd_handle_untagged: New mail in INBOX - 223258 messages total.
4< * 205 RECENT
4< a0006 OK Completed
IMAP queue drained
}}}

but mutt says New:4
which are the 4 messages which arrived while mutt was running i.e., not including the 205 which arrived since mutt had last viewed the inbox.

I am not using the header cache, and this is against a cyrus imap server. Using today's (1st July) mercurial source.
* resolution changed to fixed
* status changed to closed
