Ticket:  3411
Status:  closed
Summary: "Sending in background" with gmail

Reporter: error27
Owner:    me

Opened:       2010-05-17 20:48:13 UTC
Last Updated: 2010-05-25 16:19:11 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
This is similar to bug #2961 and bug #2890.

I am in visiting Zambia and have a _really_ flaky connection.  Normally my emails go through but once in a while it will sit for a while and then do the "Sending in background" thing.  If that happens my email gets saved to my sent-msgs file but it doesn't get sent.

I have this line in my .muttrc
set sendmail_wait=0
I understood from the other bug that it would make the problem go away but it hasn't.

I'm using mutt from Ubuntu 8.10, I have some additional patches installed that came as .debs.  If there is a patch for me to test, I could compile that.  Here is the output from mutt -v.

Mutt 1.5.18 (2008-05-17)
Copyright (C) 1996-2008 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.34-rc7 (i686)
ncurses: ncurses 5.6.20071124 (compiled with 5.6)
libidn: 1.8 (compiled with 1.8)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Jun 15 2006 21:19:27)
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_IMAP  +USE_SMTP  +USE_GSS  -USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.13.cd.ifdef.2
patch-1.5.13.cd.purge_message.3.4
patch-1.5.13.nt+ab.xtitles.4
patch-1.5.18.sidebar.20080611.txt
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.8.hr.sensible_browser_position.3




--------------------------------------------------------------------------------
2010-05-18 01:08:15 UTC me
* Added comment:
Do you have smtp_url set?  It is important to know if you are using $sendmail or $smtp_url in order to diagnose the problem.

Also, how are you sending mail: from the command line or using the send-menu?

* status changed to infoneeded_new

--------------------------------------------------------------------------------
2010-05-18 07:38:37 UTC error27
* Added comment:
I'm using: set smtp_url="smtp://error27@gmail.com@smtp.gmail.com:587/"

I send from the send-menu.


* status changed to new

--------------------------------------------------------------------------------
2010-05-18 15:41:11 UTC me
* Added attachment send-message-retval.diff
* Added comment:
proposed fix

--------------------------------------------------------------------------------
2010-05-18 15:42:19 UTC me
* Added comment:
Please try the attached patch.

The problem was that the smtp code could return a value < -1 on error, and the code in the send menu only considered -1 to be an error.  With this patch, Mutt should return to the send-menu if any error occurs.

* owner changed to me
* status changed to assigned

--------------------------------------------------------------------------------
2010-05-19 19:43:39 UTC error27
* Added comment:
Thanks so much for the patch.  I will give test it for a week or two and let you know how it goes.


--------------------------------------------------------------------------------
2010-05-24 16:50:50 UTC error27
* Added comment:
I think this patch fixed it.  I see "read errors" and I don't think I saw those before.  (As in I'm pretty sure there is a read error caused by the network, but before it probably did the sending in background thing).  I haven't seen the bug since I applied your patch.

Thanks for doing this.  It really helps me.



* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2010-05-25 16:19:11 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
(In [29e37994a536]) Consider any negative return value from send_message() to be an error and allow the user to resend.

Closes #3411.
