Ticket:  2100
Status:  closed
Summary: Attach more files at once with a wildcard

Reporter: swimmer@xs4all.nl
Owner:    mutt-dev

Opened:       2005-10-05 13:35:20 UTC
Last Updated: 2005-10-06 05:07:48 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
It would be nice if it was possible to attach more files at once with a wildcard. It costs a lot of time if you have to attach more than 20 files ;-)
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-10-06 23:07:48 UTC brendan
* Added comment:
{{{
Duplicate of #351.
}}}

* resolution changed to fixed
* status changed to closed
