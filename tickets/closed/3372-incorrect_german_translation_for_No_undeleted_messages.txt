Ticket:  3372
Status:  closed
Summary: incorrect german translation for "No undeleted messages."

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2010-01-31 16:23:12 UTC
Last Updated: 2010-03-01 01:23:40 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.mutt.org/553238
---
{{{
"No undeleted messages." should be translated as "Keine weiteren ungelöschten Nachrichten." instead of "Alle Nachrichten gelöscht.".
}}}

The attached patch fixes the problem (without "weiteren", I've asked two different German native speakers).

--------------------------------------------------------------------------------
2010-01-31 16:23:34 UTC antonio@dyne.org
* Added attachment 553238-german-intl.patch

--------------------------------------------------------------------------------
2010-03-01 01:23:40 UTC Antonio Radici <antonio@dyne.org>
* Added comment:
(In [25459cbb132a]) Fix German translation of "No undeleted messages." (closes #3372)

* resolution changed to fixed
* status changed to closed
