Ticket:  3705
Status:  closed
Summary: mutt_sasl.c: 2 * possible bad test ?

Reporter: dcb314
Owner:    brendan

Opened:       2014-09-07 15:12:28 UTC
Last Updated: 2014-09-07 19:00:32 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
mutt_sasl.c:199:25: warning: logical not is only applied to the left hand side of comparison [-Wlogical-not-parentheses]

    if (!iptostring((struct sockaddr *)&local, size, iplocalport,
              IP_PORT_BUFLEN) != SASL_OK)

Boolean 0 / 1 compared to SASL_OK. Looks like it might be a problem.
Removing the double negative might also help.

Same thing a few lines further down
mutt_sasl.c:210:25: warning: logical not is only applied to the left hand side of comparison [-Wlogical-not-parentheses]




--------------------------------------------------------------------------------
2014-09-07 19:00:32 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
In [1b583341d5ad677c8a1935eb4110eba27606878a]:
{{{
#!CommitTicketReference repository="" revision="1b583341d5ad677c8a1935eb4110eba27606878a"
mutt_sasl: fix double negative in iptostring result check (fixes #3705)
}}}

* resolution changed to fixed
* status changed to closed
