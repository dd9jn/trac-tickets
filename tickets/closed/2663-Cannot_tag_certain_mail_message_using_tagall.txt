Ticket:  2663
Status:  closed
Summary: Cannot tag certain mail message using tag-all

Reporter: alayne@twobikes.ottawa.on.ca
Owner:    mutt-dev

Opened:       2007-01-05 02:51:50 UTC
Last Updated: 2007-03-09 14:47:49 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
The Tag-all command (T a) does not work on certain messages in an mbox opened by mutt.
>How-To-Repeat:
unzip mutt-error.gz
mutt -f mutt-error
T
(get response Tag messages matching:)
a

No messages are tagged.
>Fix:
Can tag messages manually using "t" command, but this is a pain when dealing with many messages.

T. is the short tag-all, alias of the canonical T~A
}}}

--------------------------------------------------------------------------------
2007-03-10 08:47:49 UTC ab
* Added comment:
{{{
Yet another duplicate of closed mistaken bug 2662. Use T. 
to tag-all.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:17 UTC 
* Added attachment mutt-error.gz
* Added comment:
mutt-error.gz
