Ticket:  2570
Status:  closed
Summary: crash when other MUA accesses IMAP

Reporter: charlie@rubberduck.com
Owner:    brendan

Opened:       2006-11-26 17:27:25 UTC
Last Updated: 2007-04-01 21:01:40 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
while changing mailboxes, and Mail.app is also starting up, i can repeatedly crash mutt. here is the bt. there is nothing in the debug file.
{{{
Date/Time:      2006-11-26 17:21:11.079 +0000
OS Version:     10.4.8 (Build 8L2127)
Report Version: 4

Command: mutt
Path:    /opt/local/bin/mutt
Parent:  zsh [2303]

Version: ??? (???)

PID:    9773
Thread: 0

Exception:  EXC_BAD_ACCESS (0x0001)
Codes:      KERN_PROTECTION_FAILURE (0x0002) at 0x0000003c

Thread 0 Crashed:
0   mutt 	0x00042dff mx_update_context + 145 (mx.c:1643)
1   mutt 	0x00092795 imap_read_headers + 2272 (message.c:347)
2   mutt 	0x0008fdd8 imap_open_mailbox + 2517 (imap.c:737)
3   mutt 	0x00040ec6 mx_open_mailbox + 1050 (mx.c:701)
4   mutt 	0x0001bdd7 mutt_index_menu + 16541 (curs_main.c:1118)
5   mutt 	0x000368ec main + 4199 (main.c:970)
6   mutt 	0x00001bf2 _start + 216
7   mutt 	0x00001b19 start + 41

Thread 0 crashed with X86 Thread State (32-bit):
  eax: 0x002f8000    ebx: 0x00042d7c ecx: 0x00007650 edx: 0x011218c0
  edi: 0x00000000    esi: 0x00000000 ebp: 0xbfffc928 esp: 0xbfffc8f0
   ss: 0x0000001f    efl: 0x00010297 eip: 0x00042dff  cs: 0x00000017
   ds: 0x0000001f     es: 0x0000001f  fs: 0x00000000  gs: 0x00000037

Binary Images Description:
    0x1000 -    0xa6fff mutt 	/opt/local/bin/mutt
  0x227000 -   0x259fff libncursesw.5.dylib 	/opt/local/lib/libncursesw.5.dylib
  0x277000 -   0x2a9fff libssl.0.9.8.dylib 	/opt/local/lib/libssl.0.9.8.dylib
  0x2bc000 -   0x2c3fff libintl.3.dylib 	/opt/local/lib/libintl.3.dylib
  0x2db000 -   0x2ecfff libz.1.dylib 	/opt/local/lib/libz.1.dylib
  0x405000 -   0x4f4fff libcrypto.0.9.8.dylib 	/opt/local/lib/libcrypto.0.9.8.dylib
  0x55c000 -   0x56cfff libsasl2.2.dylib 	/opt/local/lib/libsasl2.2.dylib
  0x5a8000 -   0x67dfff libiconv.2.dylib 	/opt/local/lib/libiconv.2.dylib
  0x6be000 -   0x6eafff libidn.11.dylib 	/opt/local/lib/libidn.11.dylib
  0x6fe000 -   0x7b2fff libdb-4.3.dylib 	/opt/local/lib/libdb-4.3.dylib
  0x7e9000 -   0x7ebfff libanonymous.2.0.21.so 	/opt/local/lib/sasl2/libanonymous.2.0.21.so
 0x1008000 -  0x100afff libcrammd5.2.0.21.so 	/opt/local/lib/sasl2/libcrammd5.2.0.21.so
 0x101a000 -  0x1022fff libdigestmd5.2.0.21.so 	/opt/local/lib/sasl2/libdigestmd5.2.0.21.so
 0x103c000 -  0x1040fff libgssapiv2.2.0.21.so 	/opt/local/lib/sasl2/libgssapiv2.2.0.21.so
 0x1052000 -  0x1054fff liblogin.2.0.21.so 	/opt/local/lib/sasl2/liblogin.2.0.21.so
 0x1063000 -  0x1068fff libntlm.2.0.21.so 	/opt/local/lib/sasl2/libntlm.2.0.21.so
 0x107d000 -  0x1085fff libotp.2.0.21.so 	/opt/local/lib/sasl2/libotp.2.0.21.so
 0x109f000 -  0x10a1fff libplain.2.0.21.so 	/opt/local/lib/sasl2/libplain.2.0.21.so
 0x10af000 -  0x10b4fff libsasldb.2.0.21.so 	/opt/local/lib/sasl2/libsasldb.2.0.21.so
 0x10c6000 -  0x10cefff libsrp.2.0.21.so 	/opt/local/lib/sasl2/libsrp.2.0.21.so
0x8fe00000 - 0x8fe49fff dyld 46.9	/usr/lib/dyld
0x90000000 - 0x9016ffff libSystem.B.dylib 	/usr/lib/libSystem.B.dylib
0x901bf000 - 0x901c1fff libmathCommon.A.dylib 	/usr/lib/system/libmathCommon.A.dylib
0x90806000 - 0x908cefff com.apple.CoreFoundation 6.4.6 (368.27)	/System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation
0x9090c000 - 0x9090cfff com.apple.CoreServices 10.4 (???)	/System/Library/Frameworks/CoreServices.framework/Versions/A/CoreServices
0x9090e000 - 0x90a01fff libicucore.A.dylib 	/usr/lib/libicucore.A.dylib
0x90a51000 - 0x90ad0fff libobjc.A.dylib 	/usr/lib/libobjc.A.dylib
0x90af9000 - 0x90b5dfff libstdc++.6.dylib 	/usr/lib/libstdc++.6.dylib
0x90bcc000 - 0x90bd3fff libgcc_s.1.dylib 	/usr/lib/libgcc_s.1.dylib
0x90bd8000 - 0x90c4bfff com.apple.framework.IOKit 1.4.6 (???)	/System/Library/Frameworks/IOKit.framework/Versions/A/IOKit
0x90c60000 - 0x90c72fff libauto.dylib 	/usr/lib/libauto.dylib
0x90c78000 - 0x90f1efff com.apple.CoreServices.CarbonCore 682.15	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CarbonCore.framework/Versions/A/CarbonCore
0x90f61000 - 0x90fc9fff com.apple.CoreServices.OSServices 4.1	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/OSServices.framework/Versions/A/OSServices
0x91001000 - 0x9103ffff com.apple.CFNetwork 129.18	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CFNetwork.framework/Versions/A/CFNetwork
0x91052000 - 0x91062fff com.apple.WebServices 1.1.3 (1.1.0)	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/WebServicesCore.framework/Versions/A/WebServicesCore
0x9106d000 - 0x910ebfff com.apple.SearchKit 1.0.5	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SearchKit.framework/Versions/A/SearchKit
0x91120000 - 0x9113efff com.apple.Metadata 10.4.4 (121.36)	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/Metadata.framework/Versions/A/Metadata
0x9114a000 - 0x91158fff libz.1.dylib 	/usr/lib/libz.1.dylib
0x9115b000 - 0x912fafff com.apple.security 4.5.1 (29002)	/System/Library/Frameworks/Security.framework/Versions/A/Security
0x913f8000 - 0x91400fff com.apple.DiskArbitration 2.1.1	/System/Library/Frameworks/DiskArbitration.framework/Versions/A/DiskArbitration
0x91407000 - 0x9142dfff com.apple.SystemConfiguration 1.8.6	/System/Library/Frameworks/SystemConfiguration.framework/Versions/A/SystemConfiguration
0x94975000 - 0x94992fff libresolv.9.dylib 	/usr/lib/libresolv.9.dylib
0x94b3d000 - 0x94c15fff edu.mit.Kerberos 5.5.23	/System/Library/Frameworks/Kerberos.framework/Versions/A/Kerberos
0x95f41000 - 0x95f4afff libpam.1.dylib 	/usr/lib/libpam.1.dylib

Model: MacBookPro1,1, BootROM MBP11.0055.B08, 2 processors, Intel Core Duo, 2.16 GHz, 2 GB
Graphics: ATI Radeon X1600, ATY,RadeonX1600, PCIe, 256 MB
Memory Module: BANK 0/DIMM0, 1 GB, DDR2 SDRAM, 667 MHz
Memory Module: BANK 1/DIMM1, 1 GB, DDR2 SDRAM, 667 MHz
AirPort: spairport_wireless_card_type_airport_extreme (0x168C, 0x86), 0.1.27
Bluetooth: Version 1.7.9f12, 2 service, 1 devices, 1 incoming serial ports
Network Service: AirPort, AirPort, en1
Serial ATA Device: ST910021AS, 93.16 GB
Parallel ATA Device: MATSHITADVD-R   UJ-857
USB Device: Built-in iSight, Micron, Up to 480 Mb/sec, 500 mA
USB Device: Apple Internal Keyboard / Trackpad, Apple Computer, Up to 12 Mb/sec, 500 mA
USB Device: IR Receiver, Apple Computer, Inc., Up to 12 Mb/sec, 500 mA
USB Device: Bluetooth HCI, Up to 12 Mb/sec, 500 mA
FireWire Device: LS-9000 ED, Nikon, Up to 400 Mb/sec
FireWire Device: LaCie Hard Drive FireWire+, LaCie Group SA, Up to 400 Mb/sec
}}}

--------------------------------------------------------------------------------
2007-02-08 15:11:04 UTC rado
* Added comment:
{{{
Move to IMAP category
}}}

--------------------------------------------------------------------------------
2007-03-31 22:07:42 UTC brendan
* Added comment:
Can you still produce this? I can't.

* Updated description:
while changing mailboxes, and Mail.app is also starting up, i can repeatedly crash mutt. here is the bt. there is nothing in the debug file.
{{{
Date/Time:      2006-11-26 17:21:11.079 +0000
OS Version:     10.4.8 (Build 8L2127)
Report Version: 4

Command: mutt
Path:    /opt/local/bin/mutt
Parent:  zsh [2303]

Version: ??? (???)

PID:    9773
Thread: 0

Exception:  EXC_BAD_ACCESS (0x0001)
Codes:      KERN_PROTECTION_FAILURE (0x0002) at 0x0000003c

Thread 0 Crashed:
0   mutt 	0x00042dff mx_update_context + 145 (mx.c:1643)
1   mutt 	0x00092795 imap_read_headers + 2272 (message.c:347)
2   mutt 	0x0008fdd8 imap_open_mailbox + 2517 (imap.c:737)
3   mutt 	0x00040ec6 mx_open_mailbox + 1050 (mx.c:701)
4   mutt 	0x0001bdd7 mutt_index_menu + 16541 (curs_main.c:1118)
5   mutt 	0x000368ec main + 4199 (main.c:970)
6   mutt 	0x00001bf2 _start + 216
7   mutt 	0x00001b19 start + 41

Thread 0 crashed with X86 Thread State (32-bit):
  eax: 0x002f8000    ebx: 0x00042d7c ecx: 0x00007650 edx: 0x011218c0
  edi: 0x00000000    esi: 0x00000000 ebp: 0xbfffc928 esp: 0xbfffc8f0
   ss: 0x0000001f    efl: 0x00010297 eip: 0x00042dff  cs: 0x00000017
   ds: 0x0000001f     es: 0x0000001f  fs: 0x00000000  gs: 0x00000037

Binary Images Description:
    0x1000 -    0xa6fff mutt 	/opt/local/bin/mutt
  0x227000 -   0x259fff libncursesw.5.dylib 	/opt/local/lib/libncursesw.5.dylib
  0x277000 -   0x2a9fff libssl.0.9.8.dylib 	/opt/local/lib/libssl.0.9.8.dylib
  0x2bc000 -   0x2c3fff libintl.3.dylib 	/opt/local/lib/libintl.3.dylib
  0x2db000 -   0x2ecfff libz.1.dylib 	/opt/local/lib/libz.1.dylib
  0x405000 -   0x4f4fff libcrypto.0.9.8.dylib 	/opt/local/lib/libcrypto.0.9.8.dylib
  0x55c000 -   0x56cfff libsasl2.2.dylib 	/opt/local/lib/libsasl2.2.dylib
  0x5a8000 -   0x67dfff libiconv.2.dylib 	/opt/local/lib/libiconv.2.dylib
  0x6be000 -   0x6eafff libidn.11.dylib 	/opt/local/lib/libidn.11.dylib
  0x6fe000 -   0x7b2fff libdb-4.3.dylib 	/opt/local/lib/libdb-4.3.dylib
  0x7e9000 -   0x7ebfff libanonymous.2.0.21.so 	/opt/local/lib/sasl2/libanonymous.2.0.21.so
 0x1008000 -  0x100afff libcrammd5.2.0.21.so 	/opt/local/lib/sasl2/libcrammd5.2.0.21.so
 0x101a000 -  0x1022fff libdigestmd5.2.0.21.so 	/opt/local/lib/sasl2/libdigestmd5.2.0.21.so
 0x103c000 -  0x1040fff libgssapiv2.2.0.21.so 	/opt/local/lib/sasl2/libgssapiv2.2.0.21.so
 0x1052000 -  0x1054fff liblogin.2.0.21.so 	/opt/local/lib/sasl2/liblogin.2.0.21.so
 0x1063000 -  0x1068fff libntlm.2.0.21.so 	/opt/local/lib/sasl2/libntlm.2.0.21.so
 0x107d000 -  0x1085fff libotp.2.0.21.so 	/opt/local/lib/sasl2/libotp.2.0.21.so
 0x109f000 -  0x10a1fff libplain.2.0.21.so 	/opt/local/lib/sasl2/libplain.2.0.21.so
 0x10af000 -  0x10b4fff libsasldb.2.0.21.so 	/opt/local/lib/sasl2/libsasldb.2.0.21.so
 0x10c6000 -  0x10cefff libsrp.2.0.21.so 	/opt/local/lib/sasl2/libsrp.2.0.21.so
0x8fe00000 - 0x8fe49fff dyld 46.9	/usr/lib/dyld
0x90000000 - 0x9016ffff libSystem.B.dylib 	/usr/lib/libSystem.B.dylib
0x901bf000 - 0x901c1fff libmathCommon.A.dylib 	/usr/lib/system/libmathCommon.A.dylib
0x90806000 - 0x908cefff com.apple.CoreFoundation 6.4.6 (368.27)	/System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation
0x9090c000 - 0x9090cfff com.apple.CoreServices 10.4 (???)	/System/Library/Frameworks/CoreServices.framework/Versions/A/CoreServices
0x9090e000 - 0x90a01fff libicucore.A.dylib 	/usr/lib/libicucore.A.dylib
0x90a51000 - 0x90ad0fff libobjc.A.dylib 	/usr/lib/libobjc.A.dylib
0x90af9000 - 0x90b5dfff libstdc++.6.dylib 	/usr/lib/libstdc++.6.dylib
0x90bcc000 - 0x90bd3fff libgcc_s.1.dylib 	/usr/lib/libgcc_s.1.dylib
0x90bd8000 - 0x90c4bfff com.apple.framework.IOKit 1.4.6 (???)	/System/Library/Frameworks/IOKit.framework/Versions/A/IOKit
0x90c60000 - 0x90c72fff libauto.dylib 	/usr/lib/libauto.dylib
0x90c78000 - 0x90f1efff com.apple.CoreServices.CarbonCore 682.15	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CarbonCore.framework/Versions/A/CarbonCore
0x90f61000 - 0x90fc9fff com.apple.CoreServices.OSServices 4.1	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/OSServices.framework/Versions/A/OSServices
0x91001000 - 0x9103ffff com.apple.CFNetwork 129.18	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CFNetwork.framework/Versions/A/CFNetwork
0x91052000 - 0x91062fff com.apple.WebServices 1.1.3 (1.1.0)	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/WebServicesCore.framework/Versions/A/WebServicesCore
0x9106d000 - 0x910ebfff com.apple.SearchKit 1.0.5	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SearchKit.framework/Versions/A/SearchKit
0x91120000 - 0x9113efff com.apple.Metadata 10.4.4 (121.36)	/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/Metadata.framework/Versions/A/Metadata
0x9114a000 - 0x91158fff libz.1.dylib 	/usr/lib/libz.1.dylib
0x9115b000 - 0x912fafff com.apple.security 4.5.1 (29002)	/System/Library/Frameworks/Security.framework/Versions/A/Security
0x913f8000 - 0x91400fff com.apple.DiskArbitration 2.1.1	/System/Library/Frameworks/DiskArbitration.framework/Versions/A/DiskArbitration
0x91407000 - 0x9142dfff com.apple.SystemConfiguration 1.8.6	/System/Library/Frameworks/SystemConfiguration.framework/Versions/A/SystemConfiguration
0x94975000 - 0x94992fff libresolv.9.dylib 	/usr/lib/libresolv.9.dylib
0x94b3d000 - 0x94c15fff edu.mit.Kerberos 5.5.23	/System/Library/Frameworks/Kerberos.framework/Versions/A/Kerberos
0x95f41000 - 0x95f4afff libpam.1.dylib 	/usr/lib/libpam.1.dylib

Model: MacBookPro1,1, BootROM MBP11.0055.B08, 2 processors, Intel Core Duo, 2.16 GHz, 2 GB
Graphics: ATI Radeon X1600, ATY,RadeonX1600, PCIe, 256 MB
Memory Module: BANK 0/DIMM0, 1 GB, DDR2 SDRAM, 667 MHz
Memory Module: BANK 1/DIMM1, 1 GB, DDR2 SDRAM, 667 MHz
AirPort: spairport_wireless_card_type_airport_extreme (0x168C, 0x86), 0.1.27
Bluetooth: Version 1.7.9f12, 2 service, 1 devices, 1 incoming serial ports
Network Service: AirPort, AirPort, en1
Serial ATA Device: ST910021AS, 93.16 GB
Parallel ATA Device: MATSHITADVD-R   UJ-857
USB Device: Built-in iSight, Micron, Up to 480 Mb/sec, 500 mA
USB Device: Apple Internal Keyboard / Trackpad, Apple Computer, Up to 12 Mb/sec, 500 mA
USB Device: IR Receiver, Apple Computer, Inc., Up to 12 Mb/sec, 500 mA
USB Device: Bluetooth HCI, Up to 12 Mb/sec, 500 mA
FireWire Device: LS-9000 ED, Nikon, Up to 400 Mb/sec
FireWire Device: LaCie Hard Drive FireWire+, LaCie Group SA, Up to 400 Mb/sec
}}}
* priority changed to major

--------------------------------------------------------------------------------
2007-03-31 22:31:47 UTC brendan
* milestone changed to 1.6
* owner changed to brendan

--------------------------------------------------------------------------------
2007-04-01 21:01:40 UTC yeled
* Added comment:
cannot replicate this anymore.

* resolution changed to worksforme
* status changed to closed
