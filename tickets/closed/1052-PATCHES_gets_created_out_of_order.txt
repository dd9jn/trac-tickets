Ticket:  1052
Status:  closed
Summary: PATCHES gets created out of order

Reporter: David T-G <davidtg-muttusers@justpickone.org>
Owner:    mutt-dev

Opened:       2002-02-16 04:15:57 UTC
Last Updated: 2007-11-05 16:06:43 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.25i
Severity: wishlist

-- Please type your report below this line

Now that patchlist.c is back and PATCHES is used for the source, we have
a way to track what patches are applied; great.  Unfortunately, the
patches included in the file are in reverse order because everyone just
adds a line at the top -- but, worse yet, the first patch applied *is* at
the top!

To standardize, PATCHES should exist in the distribution and should
contain only a token string like "## END OF FILE ##" or some such so that
developers can insert their line above that line and have the patches
listed in the order applied.

What I'd *really* like is a patchlist.c output as shown below, but just
the patch names as most authors seem to do would be sufficient.


TIA & HAND

:-D T-G


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i686-pc-linux-gnu/2.95.1/specs
gcc version 2.95.1 19990816 (release)

- CFLAGS
-Wall -pedantic -Wall -pedantic -O6 -mpentiumpro -malign-loops=2 -malign-jumps=2 -malign-functions=2

-- Mutt Version Information

Mutt 1.3.25i (2002-01-01)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.5 (i686) [using ncurses 5.0]
Compile options:
-DOMAIN
+DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Mailbox"
PKGDATADIR="/home/davidtg/local/share/mutt"
SYSCONFDIR="/home/davidtg/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

  Patches I have applied (I find this non-standard list helpful)

  Feature patch: patch-1.3.25.rr.compressed.1 (dtg)
  Feature patch: patch-0.00.sec.patchlist.8.1.dtg (dtg)
  Feature patch: %_                   0.94.12 by O'Shaughnessy Evans
  Feature patch: reverse-reply        0.95.4 by Stefan `Sec` Zehl (+ hb)
  Feature patch: patch-1.1.1.hb.save_alias.1 (dtg)
  Feature patch: patch-1.3.23.bj.hash_destroy.1 (dtg)
  Feature patch: patch-1.3.23.bj.my_hdr_subject.1 (dtg)
  Feature patch: patch-1.3.23.bj.noquote_hdr_term.1 (dtg)
  Feature patch: patch-1.3.24.bj.status-time.1-dtg (dtg)
  Feature patch: patch-1.3.23.bj.current_shortcut.1 (dtg)
  Feature patch: patch-1.3.24.dgc.xlabel_ext.4 (dtg)
  Feature patch: patch-1.3.24.dgc.deepif.1 (dtg)
  Feature patch: patch-1.3.24.dgc.attach.2 (dtg)
  Feature patch: patch-1.3.24.dgc.markmsg.2 (dtg)
  Feature patch: patch-1.3.24.dgc.unbind.1 (dtg)
  Feature patch: patch-1.3.24.dgc.isalias.1 (dtg)
  Feature patch: patch-1.2.mha.resend-fcc.1 (dtg)
  Feature patch: patch-1.3.25.cd.edit_threads.9.1 (dtg)
  Feature patch: patch-1.3.24.cd.trash_folder.1 (dtg)
  Feature patch: patch-1.3.20000609.mg.hdrcolor.1 (dtg)
  Feature patch: patch-1.3.15.sw.pgp-outlook.1 (dtg)
  Feature patch: patch-1.3.23.2.nr.tag_prefix_cond (dtg)
  Feature patch: patch-1.3.23.ats.mark_old-1.3.23+cvs-1.diff (dtg)
  Feature patch: patch-1.3.25i.devl.narrow_tree.1 (dtg)
  Feature patch: patch-1.3.25.dw.pgp-hook (dtg)



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-06 22:47:58 UTC brendan
* Added comment:
{{{
Refiled as change-request.
}}}

--------------------------------------------------------------------------------
2007-11-05 15:38:43 UTC pdmef
* Added comment:
Properly keeping the order of patches is only interesting if you're hacking the source and use some sort of patch queue support, once mutt is installed, it doesn't provide additional info. As far as mutt -v is concerned, it only matters if a patch is applied or not. (for debugging, bug reports, etc.)

* resolution changed to wontfix
* status changed to closed

--------------------------------------------------------------------------------
2007-11-05 16:06:43 UTC David Champion
* Added comment:
{{{
>  Properly keeping the order of patches is only interesting if you're
>  hacking the source and use some sort of patch queue support, once mutt is
>  installed, it doesn't provide additional info. As far as mutt -v is
>  concerned, it only matters if a patch is applied or not. (for debugging,
>  bug reports, etc.)

Conversely, I'd say it's interesting mainly if you're using patched
builds (whether or not you personally are a source hacker) and you're
*not* using any kind of patch queue support.  PATCHES, if it listed
patches in the order they were applied, would be most useful for
reminding you how to build the next version.

I've used it that way steadily in the past, remembering that patch order
is almsot the reverse of what appears in PATCHES.  With Mercurial + mq
it's less of a problem, but the wishlist item is still justified for
people building from tarballs.
}}}
