Ticket:  2973
Status:  closed
Summary: configurable encrypt-to-self with gpgme

Reporter: LeSpocky
Owner:    mutt-dev

Opened:       2007-10-26 14:51:23 UTC
Last Updated: 2017-10-18 07:29:27 UTC

Priority:  minor
Component: crypto
Keywords:  encrypt self gpgme gpgsm

--------------------------------------------------------------------------------
Description:
According to a thread [http://marc.info/?l=mutt-users&m=119262851607365&w=2 (1)] on mutt-users mailing list I propose a config option for encrypt-to-self with gpgme. My opinion is this should be no global option of GnuPG but a per application thing as Thunderbird/Enigmail handles this too.

--------------------------------------------------------------------------------
2008-05-18 01:42:12 UTC brendan
* priority changed to minor

--------------------------------------------------------------------------------
2013-10-21 21:30:54 UTC tdussa
* Added comment:
+1 for this.  In fact, IMHO this should also be implemented for GPGSM.  This is a standard functionality for MUAs, and it is quite surprising that it is not present yet in mutt.

--------------------------------------------------------------------------------
2013-10-21 21:31:07 UTC tdussa
* cc changed to tobias-mutt@dussa.de
* keywords changed to encrypt self gpgme gpgsm

--------------------------------------------------------------------------------
2017-10-18 07:29:27 UTC kevin8t8
* Added comment:
This was implemented in changeset:ca95f3e38355

* resolution changed to fixed
* status changed to closed
