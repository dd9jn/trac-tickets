Ticket:  3451
Status:  closed
Summary: Mutt crashed after a Ctrl-Z when running a macro with a pipe

Reporter: vinc17
Owner:    mutt-dev

Opened:       2010-09-15 12:07:49 UTC
Last Updated: 2013-01-17 12:06:46 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I have the following macro:
{{{
macro index a ": set pipe_split=yes\CM|$HOME/scripts/addlabels " "add labels"
}}}
I tagged several messages, then typed 'a', some arguments, and [Return]. While the macro hadn't finished running yet, I typed Ctrl-Z, then fg, and Mutt crashed:
{{{
Host Name:      prunille
Date/Time:      2010-09-07 21:38:37.935 +0200
OS Version:     10.4.11 (Build 8S165)
Report Version: 4

Command: mutt
Path:    /Users/vinc17/bin/mutt
Parent:  mutt [22463]

Version: ??? (???)

PID:    22511
Thread: 0

Exception:  EXC_BAD_ACCESS (0x0001)
Codes:      KERN_PROTECTION_FAILURE (0x0002) at 0x00000058

Thread 0 Crashed:
0   <<00000000>> 	0xffff82cc __pthread_getspecific + 12 (cpu_capabilities.h:181)
1   libSystem.B.dylib   	0x90025078 sprintf$LDBL128 + 216
2   libncursesw.5.dylib 	0x0025fdc8 tparm + 2184
3   libncursesw.5.dylib 	0x00245b08 _nc_mvcur_resume + 104
4   libncursesw.5.dylib 	0x00254248 doupdate + 168
5   libncursesw.5.dylib 	0x002494b0 wrefresh + 144
6   mutt                	0x0006aa60 sighandler + 208 (signal.c:81)
7   libSystem.B.dylib   	0x90130f54 _sigtramp + 104
8   libSystem.B.dylib   	0x90130eec _sigtramp + 0
9   libSystem.B.dylib   	0x90031ff8 fork + 88

Thread 0 crashed with PPC Thread State 64:
  srr0: 0x00000000ffff82cc srr1: 0x100000000000d030                        vrsave: 0x0000000000000000
    cr: 0x2222827c          xer: 0x0000000000000000   lr: 0x0000000090025078  ctr: 0x0000000090008940
    r0: 0x0000000090025078   r1: 0x00000000bfffb910   r2: 0x00000000a00022d8   r3: 0x0000000000000000
    r4: 0x0000000000000048   r5: 0x0000000000000058   r6: 0x0000000000000000   r7: 0x0000000000000000
    r8: 0x0000000000000000   r9: 0x00000000bfffba8c  r10: 0x0000000000000064  r11: 0x00000000a000611c
   r12: 0x0000000090008940  r13: 0x0000000000000000  r14: 0x0000000000000000  r15: 0x00000000000ba3d8
   r16: 0x0000000000000000  r17: 0x0000000000000000  r18: 0x0000000000000001  r19: 0x0000000000000000
   r20: 0x0000000000000000  r21: 0x0000000000000000  r22: 0x0000000001104fa0  r23: 0x0000000000000010
   r24: 0x0000000001800a3e  r25: 0x000000000027f550  r26: 0x000000000027f550  r27: 0x0000000001104fa0
   r28: 0x00000000bfffb94c  r29: 0x0000000001104fd2  r30: 0x0000000000000000  r31: 0x0000000090024fa8

Binary Images Description:
    0x1000 -    0xb5fff mutt 	/Users/vinc17/bin/mutt
  0x23a000 -   0x279fff libncursesw.5.dylib 	/opt/local/lib/libncursesw.5.dylib
  0x28d000 -   0x2d3fff libssl.1.0.0.dylib 	/opt/local/lib/libssl.1.0.0.dylib
  0x2ec000 -   0x2f4fff libintl.8.dylib 	/opt/local/lib/libintl.8.dylib
  0x405000 -   0x537fff libcrypto.1.0.0.dylib 	/opt/local/lib/libcrypto.1.0.0.dylib
  0x5ad000 -   0x5befff libz.1.dylib 	/opt/local/lib/libz.1.dylib
  0x5c2000 -   0x6bbfff libiconv.2.dylib 	/opt/local/lib/libiconv.2.dylib
  0x6c7000 -   0x6f4fff libidn.11.dylib 	/opt/local/lib/libidn.11.dylib
0x8fe00000 - 0x8fe52fff dyld 46.16	/usr/lib/dyld
0x90000000 - 0x901bcfff libSystem.B.dylib 	/usr/lib/libSystem.B.dylib
0x90214000 - 0x90219fff libmathCommon.A.dylib 	/usr/lib/system/libmathCommon.A.dylib
0x907bb000 - 0x90895fff com.apple.CoreFoundation 6.4.11 (368.35)	/System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation
0x908e0000 - 0x909e2fff libicucore.A.dylib 	/usr/lib/libicucore.A.dylib
0x90a3c000 - 0x90ac0fff libobjc.A.dylib 	/usr/lib/libobjc.A.dylib
0x90b70000 - 0x90b82fff libauto.dylib 	/usr/lib/libauto.dylib
0x91433000 - 0x9143efff libgcc_s.1.dylib 	/usr/lib/libgcc_s.1.dylib
}}}
Mutt's version:
{{{
Mutt 1.5.20-6134-vl-r38670 (2010-08-25)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Darwin 8.11.0 (Power Macintosh)
ncurses: ncurses 5.7.20081102 (compiled with 5.7)
libiconv: 1.13
libidn: 1.19 (compiled with 1.19)
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_SMTP
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  +HAVE_GETADDRINFO
-HAVE_REGCOMP  +USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE
ISPELL="/opt/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH=""
PKGDATADIR="/Users/vinc17/share/mutt"
SYSCONFDIR="/Users/vinc17/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.20hg.pdmef.progress.vl.2
patch-1.5.20hg.tamovl.patterns.1
patch-1.5.15hg.cd.trash_folder.vl.1
patch-1.5.20hg.jlh-vl.parentchildmatch.2
}}}

Note: I originally posted that as a comment of bug #2446, but the reporter closed the bug (it was against an old version). I don't know whether the cause is the same, but they look very similar.

--------------------------------------------------------------------------------
2010-09-15 13:22:16 UTC me
* milestone changed to 1.6

--------------------------------------------------------------------------------
2013-01-17 02:06:21 UTC me
* Added comment:
Are you still having this issue?  It appears to have crashed inside of ncurses itself rather than mutt, so I'm wondering if it hasn't been fixed in the meantime.


* Updated description:
I have the following macro:
{{{
macro index a ": set pipe_split=yes\CM|$HOME/scripts/addlabels " "add labels"
}}}
I tagged several messages, then typed 'a', some arguments, and [Return]. While the macro hadn't finished running yet, I typed Ctrl-Z, then fg, and Mutt crashed:
{{{
Host Name:      prunille
Date/Time:      2010-09-07 21:38:37.935 +0200
OS Version:     10.4.11 (Build 8S165)
Report Version: 4

Command: mutt
Path:    /Users/vinc17/bin/mutt
Parent:  mutt [22463]

Version: ??? (???)

PID:    22511
Thread: 0

Exception:  EXC_BAD_ACCESS (0x0001)
Codes:      KERN_PROTECTION_FAILURE (0x0002) at 0x00000058

Thread 0 Crashed:
0   <<00000000>> 	0xffff82cc __pthread_getspecific + 12 (cpu_capabilities.h:181)
1   libSystem.B.dylib   	0x90025078 sprintf$LDBL128 + 216
2   libncursesw.5.dylib 	0x0025fdc8 tparm + 2184
3   libncursesw.5.dylib 	0x00245b08 _nc_mvcur_resume + 104
4   libncursesw.5.dylib 	0x00254248 doupdate + 168
5   libncursesw.5.dylib 	0x002494b0 wrefresh + 144
6   mutt                	0x0006aa60 sighandler + 208 (signal.c:81)
7   libSystem.B.dylib   	0x90130f54 _sigtramp + 104
8   libSystem.B.dylib   	0x90130eec _sigtramp + 0
9   libSystem.B.dylib   	0x90031ff8 fork + 88

Thread 0 crashed with PPC Thread State 64:
  srr0: 0x00000000ffff82cc srr1: 0x100000000000d030                        vrsave: 0x0000000000000000
    cr: 0x2222827c          xer: 0x0000000000000000   lr: 0x0000000090025078  ctr: 0x0000000090008940
    r0: 0x0000000090025078   r1: 0x00000000bfffb910   r2: 0x00000000a00022d8   r3: 0x0000000000000000
    r4: 0x0000000000000048   r5: 0x0000000000000058   r6: 0x0000000000000000   r7: 0x0000000000000000
    r8: 0x0000000000000000   r9: 0x00000000bfffba8c  r10: 0x0000000000000064  r11: 0x00000000a000611c
   r12: 0x0000000090008940  r13: 0x0000000000000000  r14: 0x0000000000000000  r15: 0x00000000000ba3d8
   r16: 0x0000000000000000  r17: 0x0000000000000000  r18: 0x0000000000000001  r19: 0x0000000000000000
   r20: 0x0000000000000000  r21: 0x0000000000000000  r22: 0x0000000001104fa0  r23: 0x0000000000000010
   r24: 0x0000000001800a3e  r25: 0x000000000027f550  r26: 0x000000000027f550  r27: 0x0000000001104fa0
   r28: 0x00000000bfffb94c  r29: 0x0000000001104fd2  r30: 0x0000000000000000  r31: 0x0000000090024fa8

Binary Images Description:
    0x1000 -    0xb5fff mutt 	/Users/vinc17/bin/mutt
  0x23a000 -   0x279fff libncursesw.5.dylib 	/opt/local/lib/libncursesw.5.dylib
  0x28d000 -   0x2d3fff libssl.1.0.0.dylib 	/opt/local/lib/libssl.1.0.0.dylib
  0x2ec000 -   0x2f4fff libintl.8.dylib 	/opt/local/lib/libintl.8.dylib
  0x405000 -   0x537fff libcrypto.1.0.0.dylib 	/opt/local/lib/libcrypto.1.0.0.dylib
  0x5ad000 -   0x5befff libz.1.dylib 	/opt/local/lib/libz.1.dylib
  0x5c2000 -   0x6bbfff libiconv.2.dylib 	/opt/local/lib/libiconv.2.dylib
  0x6c7000 -   0x6f4fff libidn.11.dylib 	/opt/local/lib/libidn.11.dylib
0x8fe00000 - 0x8fe52fff dyld 46.16	/usr/lib/dyld
0x90000000 - 0x901bcfff libSystem.B.dylib 	/usr/lib/libSystem.B.dylib
0x90214000 - 0x90219fff libmathCommon.A.dylib 	/usr/lib/system/libmathCommon.A.dylib
0x907bb000 - 0x90895fff com.apple.CoreFoundation 6.4.11 (368.35)	/System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation
0x908e0000 - 0x909e2fff libicucore.A.dylib 	/usr/lib/libicucore.A.dylib
0x90a3c000 - 0x90ac0fff libobjc.A.dylib 	/usr/lib/libobjc.A.dylib
0x90b70000 - 0x90b82fff libauto.dylib 	/usr/lib/libauto.dylib
0x91433000 - 0x9143efff libgcc_s.1.dylib 	/usr/lib/libgcc_s.1.dylib
}}}
Mutt's version:
{{{
Mutt 1.5.20-6134-vl-r38670 (2010-08-25)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Darwin 8.11.0 (Power Macintosh)
ncurses: ncurses 5.7.20081102 (compiled with 5.7)
libiconv: 1.13
libidn: 1.19 (compiled with 1.19)
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_SMTP
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  +HAVE_GETADDRINFO
-HAVE_REGCOMP  +USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE
ISPELL="/opt/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH=""
PKGDATADIR="/Users/vinc17/share/mutt"
SYSCONFDIR="/Users/vinc17/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.20hg.pdmef.progress.vl.2
patch-1.5.20hg.tamovl.patterns.1
patch-1.5.15hg.cd.trash_folder.vl.1
patch-1.5.20hg.jlh-vl.parentchildmatch.2
}}}

Note: I originally posted that as a comment of bug #2446, but the reporter closed the bug (it was against an old version). I don't know whether the cause is the same, but they look very similar.
* status changed to infoneeded_new

--------------------------------------------------------------------------------
2013-01-17 12:06:21 UTC vinc17
* Added comment:
I've just tried a dozen of times on a GNU/Linux machine and couldn't reproduce the problem on it.

When I reported the problem, it was on an old Mac OS X machine (which is no longer working, so that I can no longer try on it). Anyway there doesn't seem to be a bug in Mutt.

* status changed to new

--------------------------------------------------------------------------------
2013-01-17 12:06:46 UTC vinc17
* Added comment:
Thus closing.

* resolution changed to worksforme
* status changed to closed
