Ticket:  3264
Status:  closed
Summary: Erroneous "invalid IMAP path" stops mutt cold

Reporter: rdump
Owner:    pdmef

Opened:       2009-06-15 13:34:00 UTC
Last Updated: 2009-06-20 10:48:24 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I upgraded to mutt 1.5.20 via fink <http://www.finkproject.org/> on Mac OS X 10.5.7.

I've a new error message showing up in mutt after my upgrade to 1.5.20:

    imaps://localhost.example.edu:40993/INBOX is an invalid IMAP path

mutt 1.5.20 will as a result of this no longer proceed to opening my mail.

In my .muttrc, the relevant settings are:

    set folder="imaps://localhost.example.edu:40993/"
    set spoolfile="imaps://localhost.example.edu:40993/INBOX"

Port 40993 on localhost is port forwarded via ssh to my mail server running dovecot.

A similar .muttrc for another server running courier and lacking the port specification (":40993") does work in 1.5.20.

According to <http://mutt.sourceforge.net/imap/> that syntax is OK.  Those settings worked fine for years in previous versions of mutt.  The only change was upgrading to mutt 1.5.20.  I don't know whether the bug was introduced in the mutt base distribution or in a finkproject.org patch.

I'm backing off to mutt 1.5.16 for the nonce, as I'll be offline for a while very soon, and must have working email before I flee.  I can assist further when I return sometime next week, if necessary.



--------------------------------------------------------------------------------
2009-06-15 15:08:27 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [d6f88fbf8387]) Don't parse URL port as signed short, but to int and cast to unsigned short. Closes #3264.

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2009-06-19 19:42:51 UTC sthen
* cc changed to sthen

--------------------------------------------------------------------------------
2009-06-19 19:48:56 UTC dhill
* Added comment:
The new patch breaks my mutt when sending an email.

folder-hook work set smtp_url=smtp://davidh:mypassword@mail.server.net:587

Removing ':587' from the line fixes it.


Uploading message... 0K/0.3K (0%)
Program received signal SIGSEGV, Segmentation fault.
url_pct_decode (s=0x24b <Address 0x24b out of bounds>) at url.c:56
56      url.c: No such file or directory.
        in url.c
(gdb) bt
#0  url_pct_decode (s=0x24b <Address 0x24b out of bounds>) at url.c:56
#1  0x1c063327 in ciss_parse_userhost (ciss=0xcfbcb120, src=0x8567d407 "davidh") at url.c:154
#2  0x1c0633c0 in url_parse_ciss (ciss=0xcfbcb120, src=0x8567d400 "smtp://davidh") at url.c:169
#3  0x1c0717d4 in smtp_fill_account (account=0xcfbcb560) at smtp.c:336
#4  0x1c071566 in mutt_smtp_send (from=0x24b, to=0x7cbff080, cc=0x0, bcc=0x0, 
    msgfile=0xcfbcb710 "/tmp/mutt-wind-1000-16619-4", eightbit=0) at smtp.c:262
#5  0x1c053118 in send_message (msg=0x7cddc700) at send.c:1030
#6  0x1c053b46 in ci_send_message (flags=0, msg=0x7cddc700, tempfile=0x0, ctx=0x877a6180, cur=0x0)
    at send.c:1685
#7  0x1c019c60 in mutt_index_menu () at curs_main.c:1943
#8  0x1c030bc2 in main (argc=1, argv=0xcfbcd024) at main.c:1020

* resolution changed to 
* status changed to new

--------------------------------------------------------------------------------
2009-06-19 20:25:28 UTC pdmef
* Added comment:
Replying to [comment:3 dhill]:
> The new patch breaks my mutt when sending an email.
>
> folder-hook work set smtp_url=smtp://davidh:mypassword@mail.server.net:587
>
> Removing ':587' from the line fixes it.

I guess you have proper quotes for the folder-hook? Does adding a slash help? The parser had a bug when no trailing slash for the path was present, but that is fixed and your example works fine in my unit tests. For the current parser I get:

{{{
url=[smtp://davidh:mypassword@mail.server.net:587]
rc=0 scheme=5 user=[davidh] pass=[mypassword] host=[mail.server.net] port=587 path=[]
}}}

which is correct. What version do you use?

* milestone changed to 1.6
* owner changed to pdmef
* status changed to accepted

--------------------------------------------------------------------------------
2009-06-19 21:21:20 UTC dhill
* Added comment:
I was using revision 5899.  5901 works fine.  Thanks.


* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2009-06-20 10:48:24 UTC sthen
* cc changed to 
