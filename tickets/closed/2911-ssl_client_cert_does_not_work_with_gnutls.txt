Ticket:  2911
Status:  closed
Summary: ssl_client_cert does not work with gnutls

Reporter: LeSpocky
Owner:    mutt-dev

Opened:       2007-06-14 22:21:25 UTC
Last Updated: 2010-10-25 13:11:17 UTC

Priority:  minor
Component: crypto
Keywords:  smtp tls gnutls openssl

--------------------------------------------------------------------------------
Description:
I compiled mutt with GnuTLS since it's GPL. Parameter ''ssl_client_cert'' is not recognized there (it is if I compile against OpenSSL). This makes it impossible to use SMTP over TLS because the client cert is required there. The mailserver responses with
{{{
2007-06-15 00:12:44 TLS error on connection from localhost 
(poldy.lespocky.dyndns.org) [127.0.0.1] (SSL_accept): 
error:140890C7:SSL routines:SSL3_GET_CLIENT_CERTIFICATE:peer 
did not return a certificate
}}}
if I don't set a client cert.

--------------------------------------------------------------------------------
2007-07-13 16:47:26 UTC brendan
* Added comment:
I had a look at adding gnutls support for client certificates, but I didn't see anything in the API providing passphrase callbacks to decrypt the certificate key.

--------------------------------------------------------------------------------
2008-05-18 01:40:00 UTC brendan
* component changed to crypto
* milestone changed to 2.0
* priority changed to minor

--------------------------------------------------------------------------------
2008-06-29 01:44:56 UTC brendan
* Added comment:
(In [fe615fd5e0de]) Basic support for $ssl_client_cert when compiled with gnutls.
The key must not be encrypted. Closes #2911.

* resolution changed to fixed
* status changed to closed
