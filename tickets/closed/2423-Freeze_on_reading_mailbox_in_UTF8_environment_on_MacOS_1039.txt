Ticket:  2423
Status:  closed
Summary: Freeze on reading mailbox in UTF-8 environment on MacOS 10.3.9

Reporter: blacktrash@gmx.net
Owner:    mutt-dev

Opened:       2006-08-16 09:18:29 UTC
Last Updated: 2006-08-19 00:46:15 UTC

Priority:  minor
Component: mutt
Keywords:  #2424

--------------------------------------------------------------------------------
Description:
{{{
Freeze when trying to read attached mailbox in a LANG=en_US.UTF-8
environment.
>How-To-Repeat:
On MacOS 10.3.9:
$ LANG=en_US.UTF-8 mutt -F /dev/null -f TESTmbox
>Fix:
set assumed_charset=cp1252
}}}

--------------------------------------------------------------------------------
2006-08-19 18:46:15 UTC ab
* Added comment:
{{{
dupe of #2424
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:14 UTC 
* Added attachment TESTmbox
* Added comment:
TESTmbox
