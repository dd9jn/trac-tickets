Ticket:  2749
Status:  closed
Summary: %l in status_format (total size of folder) fails for IMAP servers

Reporter: regrado@web.de
Owner:    brendan

Opened:       2007-02-10 16:36:57 UTC
Last Updated: 2007-04-02 06:13:35 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
The total size of folder %l always shows 0KB for every IMAP-server,
while it works OK for local mbox folders.
Untested with maildir, no idea what format IMAP server uses.

--------------------------------------------------------------------------------
2007-02-28 11:21:50 UTC rado
* Added comment:
{{{
Added interested user to notify-list
}}}

--------------------------------------------------------------------------------
2007-04-02 03:28:34 UTC brendan
* Updated description:
The total size of folder %l always shows 0KB for every IMAP-server,
while it works OK for local mbox folders.
Untested with maildir, no idea what format IMAP server uses.
* milestone changed to 1.6
* owner changed to brendan

--------------------------------------------------------------------------------
2007-04-02 06:13:35 UTC brendan
* Added comment:
Fixed in [4f598543d7a5]

* resolution changed to fixed
* status changed to closed
