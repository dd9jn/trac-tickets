Ticket:  3922
Status:  closed
Summary: Patch to let filters pass on environment variables

Reporter: rsmmr
Owner:    kevin8t8

Opened:       2017-03-06 05:41:23 UTC
Last Updated: 2017-03-20 17:20:49 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Filters spawned by mutt_create_filter_fd() aren't passing on environment variables that were set through "setenv". This attached patch fixes that.

--------------------------------------------------------------------------------
2017-03-06 05:42:03 UTC rsmmr
* Added attachment mutt-1.8.0.rs.filter-env.1

--------------------------------------------------------------------------------
2017-03-06 19:00:20 UTC kevin8t8
* Added comment:
Thanks for the patch.  It's a bit more complicated than this, because the filters set the COLUMNS environment variable just above.

--------------------------------------------------------------------------------
2017-03-17 00:16:52 UTC kevin8t8
* owner changed to kevin8t8
* status changed to assigned

--------------------------------------------------------------------------------
2017-03-18 21:50:41 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"37209157e33c6a96d3895f68c42ca726f9b6ae58" 6971:37209157e33c]:
{{{
#!CommitTicketReference repository="" revision="37209157e33c6a96d3895f68c42ca726f9b6ae58"
Pass envlist to filter children too.  (closes #3922)

The new setenv patch neglected to pass the envlist for filters too.

Unfortunately, the filter code was already set up to pass COLUMNS to
children, so it needed to be changed to add this to the envlist
instead.

Factor out mutt_envlist_set() from the parse_setenv() function, which
the filter code can then use to set COLUMNS after forking.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2017-03-18 21:50:42 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"2b9c40f13e13aa717074e4bee4dcbae3998fe0f8" 6972:2b9c40f13e13]:
{{{
#!CommitTicketReference repository="" revision="2b9c40f13e13aa717074e4bee4dcbae3998fe0f8"
Fix mutt_envlist_set() for the case that envlist is null. (see #3922)
}}}

--------------------------------------------------------------------------------
2017-03-20 17:20:49 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"7cefa378ab7e144216fdfef4e993ffa766f39196" 6974:7cefa378ab7e]:
{{{
#!CommitTicketReference repository="" revision="7cefa378ab7e144216fdfef4e993ffa766f39196"
Fix setenv overwriting to not truncate the envlist. (see #3922)

The refactor in 2b9c40f13e13 exposed a bug I hadn't noticed.  The
match loop performed a FREE() on the slot.  Then, below, it was
checking if (*envp) to see whether it was overwriting or creating a
new slot.  However, FREE() nulls out *envp.  This would end up
truncating the envlist just after the set slot!

Move the free down, using a mutt_str_replace(), when overwriting the
slot.
}}}
