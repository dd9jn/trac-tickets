Ticket:  3843
Status:  closed
Summary: hangs changing to Deleted Items folder with Office365

Reporter: william
Owner:    brendan

Opened:       2016-05-18 04:30:06 UTC
Last Updated: 2017-10-18 10:47:18 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
I've been having some hangs with Mutt (with header caching enabled, linked against Tokyo Cabinet). In the past, I've usually been able to resolve it. It's typically when I go into the "Deleted Items" folder of my work email (Office365). Sometimes just removing the cache helps, but today, I tried removing everything in my header cache folder outright, and still see the problem. I have not yet tried updating to 1.6.1, but will try that when I get a chance.

In mutt itself, I see a hang like this:
{{{
Fetching message headers... 5810/5816 (99%)
}}}
It does not seem to time out for a very, very long time, and ^G or ^C don't seem to abort; I typically have to suspend mutt and send it a SIGTERM and then resume to kill.

With -d3, I see this in the debug output:

{{{
[2016-05-17 21:18:53] Mailbox is unchanged.
[2016-05-17 21:18:54] Reading imaps://outlook.office365.com/Deleted Items...
[2016-05-17 21:18:54] Selecting Deleted Items...
[2016-05-17 21:18:54] 6> a0012 CLOSE
a0013 STATUS "Drafts" (MESSAGES)
a0014 SELECT "Deleted Items"
[2016-05-17 21:18:54] 6< a0012 OK CLOSE completed.
[2016-05-17 21:18:54] 6< * STATUS Drafts (MESSAGES 0) 
[2016-05-17 21:18:54] Drafts (UIDVALIDITY: 0, UIDNEXT: 0) 0 messages, 0 recent, 0 unseen
[2016-05-17 21:18:54] Running default STATUS handler
[2016-05-17 21:18:54] 6< a0013 OK STATUS completed.
[2016-05-17 21:18:54] 6< * 5816 EXISTS
[2016-05-17 21:18:54] Handling EXISTS
[2016-05-17 21:18:54] cmd_handle_untagged: New mail in Deleted Items - 5816 messages total.
[2016-05-17 21:18:54] 6< * 0 RECENT
}}}
(then follows it reading all of my 5800 deleted messages)
{{{
[2016-05-17 21:19:12] 6< * 5816 FETCH (UID 9791 FLAGS () INTERNALDATE "17-May-2016 19:11:46 -0700" RFC822.SIZE 33044 BODY[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)] {316}
[2016-05-17 21:19:12] Handling FETCH
[2016-05-17 21:19:12] FETCH response ignored for this message
[2016-05-17 21:19:12] imap_read_literal: reading 316 bytes
[2016-05-17 21:19:12] 6< )
[2016-05-17 21:19:12] parse_parameters: `charset="UTF-8"'
[2016-05-17 21:19:12] parse_parameter: `charset' = `UTF-8'
[2016-05-17 21:19:12] 6< a0015 OK FETCH completed.
[2016-05-17 21:19:12] IMAP queue drained
}}}

If I run dtruss, here's the part where it's opening the db file:
{{{
getattrlist("/Users/wby/hcache/imaps:me@example.com@outlook.office365.com/Deleted Items.hcache\0", 0x7FFF8DC101B4, 0x7FFF5CEE6E30)		 = 0 0
open("/Users/wby/hcache/imaps:me@example.com@outlook.office365.com/Deleted Items.hcache\0", 0x202, 0x1A4)		 = 8 0
fcntl(0x8, 0x9, 0x7FFF5CEE54A0)		 = 0 0
fstat64(0x8, 0x7FFF5CEE5510, 0x7FFF5CEE54A0)		 = 0 0
lseek(0x8, 0x0, 0x0)		 = 0 0
read(0x8, "ToKyO CaBiNeT\n1.0:911\n\0", 0x100)		 = 256 0
mmap(0x0, 0x200B4, 0x3, 0x1, 0x8, 0x0)		 = 0x103062000 0
pread(0x8, "\0", 0x104C, 0x200B4)		 = 4172 0
pwrite(0x8, "\0", 0x2, 0x200B4)		 = 2 0
pread(0x8, "\310\300\0", 0x100, 0x21100)		 = 256 0
write_nocancel(0x3, "[2016-05-17 21:18:55] \0", 0x16)		 = 22 0
write_nocancel(0x3, "Fetching message headers... 0/5816 (0%)\n\0", 0x28)
}}}

the last part (where it hangs), shows:
{{{
write_nocancel(0x3, "imap_read_literal: reading 313 bytes\n\0", 0x25)		 = 37 0
write_nocancel(0x3, "[2016-05-17 21:19:12] ject: [GitHub] A new public key was added to\n xxxxxx\nContenB\001\0", 0x16)		 = 22 0
write_nocancel(0x3, "6< )\n\0", 0x5)		 = 5 0
write_nocancel(0x7, "Date: Tue, 17 May 2016 19:03:23 -0700\nFrom: GitHub <noreply@github.com>\nMessage-ID: <xxx@github-api18-cp1-prd.iad.github.net.mail>\nSubject: [GitHub] A new public key was added to xxxxxx\nContent-Type: text/pla", 0x132)		 = 306 0
lseek(0x7, 0x0, 0x1)		 = 306 0
write_nocancel(0x7, "\n\n\0", 0x2)		 = 2 0
lseek(0x7, 0x0, 0x0)		 = 0 0
}}}

mutt -v output:
{{{
Mutt 1.6.0 (2016-04-01)
Copyright (C) 1996-2016 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Darwin 14.5.0 (x86_64)
ncurses: ncurses 5.7.20081102 (compiled with 5.7)
libiconv: 1.11
hcache backend: tokyocabinet 1.4.48

Compiler:
Configured with: --prefix=/Applications/Xcode.app/Contents/Developer/usr --with-gxx-include-dir=/usr/include/c++/4.2.1
Apple LLVM version 7.0.2 (clang-700.1.81)
Target: x86_64-apple-darwin14.5.0
Thread model: posix

Configure options: '--enable-hcache' '--enable-imap' '--with-ssl' '--enable-smtp' '--with-sasl=/usr/local' '--with-included-gettext' '--enable-debug'

Compilation CFLAGS: -Wall -pedantic -Wno-long-long -g -O2

Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
-USE_POP  +USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.
}}}

--------------------------------------------------------------------------------
2016-05-18 04:31:29 UTC william
* Added comment:
ps - Aside from the hanging, it would be nice to (safely) be able to interrupt operations like this if it's at all feasible, rather than having to kill mutt entirely.

--------------------------------------------------------------------------------
2017-10-18 10:43:25 UTC kevin8t8
* Added comment:
This sounds very similar to ticket #3942, fixed in the 1.9.0 release.  Would you please try against 1.9.1 or tip?

--------------------------------------------------------------------------------
2017-10-18 10:47:18 UTC kevin8t8
* Added comment:
Oh wait, you are the same Will as in ticket #3942!  :-)

Okay, I'm closing this ticket up.

* resolution changed to duplicate
* status changed to closed
