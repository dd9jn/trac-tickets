Ticket:  3853
Status:  closed
Summary: mutt segfault in _mutt_make_string with fcc-save-hook set

Reporter: wfiveash
Owner:    mutt-dev

Opened:       2016-07-11 22:21:45 UTC
Last Updated: 2016-07-12 01:41:29 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:

With the following in my ~/.muttrc
fcc-save-hook "will\.fiveash@oracle\.com"      =muttdebugging

Running this:
$ ./mutt -s "mutt testing" -c will.fiveash@oracle.com my-gmail < /etc/motd

causes mutt to segfault (no mapping at the fault address).  Here is the stack:

(dbx:_mutt_make_string) where
=>[1] _mutt_make_string(dest = 0xffff80ffbfffd9fd "", destlen = 255U, s = 0x9d5610 "/export/home/wfiveash/mail/muttdebugging", ctx = (nil), hdr = 0x6bab60, flags = <unknown enum member 0>), line 757 in "hdrline.c"
  [2] mutt_addr_hook(path = 0xffff80ffbfffd9fd "", pathlen = 255U, type = 8, ctx = (nil), hdr = 0x6bab60), line 396 in "hook.c"
  [3] mutt_select_fcc(path = 0xffff80ffbfffd9fd "", pathlen = 255U, hdr = 0x6bab60), line 438 in "hook.c"
  [4] ci_send_message(flags = 32, msg = 0x6bab60, tempfile = (nil), ctx = (nil), cur = (nil)), line 1587 in "send.c"
  [5] main(argc = 2, argv = 0xffff80ffbfffe6d8), line 1102 in "main.c"

Note that _mutt_make_string() is calling:
mutt_FormatString (dest, destlen, 0, MuttIndexWindow->cols, s, hdr_format_str, (unsigned long) &hfi, flags);
                                     ^^^^^^^^^^^^^^^^^^^^^

but in the debugger:
(dbx:_mutt_make_string) print MuttIndexWindow
MuttIndexWindow = (nil)

Thus the MuttIndexWindow->cols pointer dereference causes the segfault.

This is on Solaris 12 with mutt -v output:

Mutt 1.6.2+10 (89ae904a6b30) (2016-07-01)
Copyright (C) 1996-2016 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: SunOS 5.12 (i86pc)
slang: 20204
libidn: 1.32 (compiled with 1.32)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Jun 24 2016 17:26:58)

Compiler:
cc: Sun C 5.13 SunOS_i386 2014/10/20

Configure options: '--with-idn' '--with-wc-funcs' '--with-slang' '--prefix=/export/home/wfiveash/app_support' '--bindir=/export/home/wfiveash/bin/i386' '--sbindir=/export/home/wfiveash/bin/i386' '--enable-imap' '--enable-mailtool' '--enable-smtp' '--with-ssl=/usr/lib' '--with-sasl=/usr' '--enable-hcache' 'CC=/opt/solarisstudio12.4/bin/cc' 'CFLAGS=-g -m64 -I/usr/include/idn' 'LDFLAGS=-m64'

Compilation CFLAGS: -g -m64 -I/usr/include/idn

Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
-USE_POP  +USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  +SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  -USE_SIDEBAR  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/export/home/wfiveash/app_support/share/mutt"
SYSCONFDIR="/export/home/wfiveash/app_support/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

--------------------------------------------------------------------------------
2016-07-11 23:03:09 UTC kevin8t8
* Added comment:
Thanks for the bug report Will.  It looks there are some paths that touch the new mutt_window_t even when curses isn't initialized.  I could play whack-a-mole, but I think I will instead change it so that those are created and freed whether curses is used or not.

--------------------------------------------------------------------------------
2016-07-11 23:14:47 UTC will.fiveash@oracle.com
* Added comment:
{{{
On Mon, Jul 11, 2016 at 11:03:09PM -0000, Mutt wrote:

When testing your fix make sure the muttrc fcc-save-hook directive works as
it should.
}}}

--------------------------------------------------------------------------------
2016-07-12 01:41:29 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"9378d21fc7fe338cfa8a9674c411c7e3fb54c8b3" 6722:9378d21fc7fe]:
{{{
#!CommitTicketReference repository="" revision="9378d21fc7fe338cfa8a9674c411c7e3fb54c8b3"
Initialize mutt windows even in batch mode. (closes #3853)

mutt_select_fcc() calls mutt_addr_hook() -> mutt_make_string() which
refers to MuttIndexWindow->cols when calling mutt_FormatString().  In
batch mode, MuttIndexWindow hasn't been initialized, leading to a
segfault.

This might be the only overlap, but it seems wiser to just initialize
the mutt windows in case there are other references (now or in the
future) when processing format strings in batch mode.
}}}

* resolution changed to fixed
* status changed to closed
