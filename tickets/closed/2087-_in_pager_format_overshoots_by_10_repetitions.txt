Ticket:  2087
Status:  closed
Summary: "%>" in pager_format overshoots by 10 repetitions

Reporter: ken@digitas.harvard.edu
Owner:    me

Opened:       2005-09-25 21:53:29 UTC
Last Updated: 2007-04-15 22:44:39 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi, this is Debian Bug`#259145`. The original report was:

Hello,

A construction like "%>-" in pager_format should make the rest of the
pager status line flush right against " -- (63%) ". Instead, "%>-" in
pager_format fails to take the width of " -- (63%) " (that is, 10)
into account.  Hence the last 10 characters of the pager status line
gets cut off.  A current workaround is to append ten characters to my
pager_format setting.

* * *

I can reproduce this with e.g.:

mutt -nF /dev/null -e 'set pager_format="%>- %s"' -e 'push <ENTER>' -f any-mailbox

--------------------------------------------------------------------------------
2005-09-27 06:12:22 UTC tamo
* Added comment:
{{{
Thank you for the additional information you have supplied regarding
 Uploading a patch to add M_FORMAT_PAGER.
}}}

--------------------------------------------------------------------------------
2005-09-27 20:05:49 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
> >Number:         2087
> >Notify-List:    259145@bugs.debian.org
> >Category:       mutt
> >Synopsis:       "%>" in pager_format overshoots by 10 repetitions

> A construction like "%>-" in pager_format should make the rest of the
> pager status line flush right against " -- (63%) ". Instead, "%>-" in
> pager_format fails to take the width of " -- (63%) " (that is, 10)
> into account.  Hence the last 10 characters of the pager status line
> gets cut off.  A current workaround is to append ten characters to my
> pager_format setting.

mutt_FormatString (in muttlib.c) uses COLS directly
(i.e. not as an argument).

We can either
(1) write the precise behaviour of "%>X" in the manual,
(2) create another %-expando to be translated to "63%", or
(3) add a flag or a number argument to _mutt_make_string
(in hdrline.c) and mutt_FormatString (in muttlib.c).

#1 is the easiest.

#2 is the most clean in design viewpoint, IMO. The less
hardcoded strings, the better. I don't want the percentage
to be shown on some small screens. But this will be an
incompatibility.

#3 is ugly. But I'm going to upload a patch of this fix.

-- 
tamo
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:24 UTC 
* Added attachment patch-1.5.11.tamo.bug2087-3.1
* Added comment:
patch-1.5.11.tamo.bug2087-3.1

--------------------------------------------------------------------------------
2007-04-02 03:10:38 UTC brendan
* component changed to display
* Updated description:
Hi, this is Debian Bug`#259145`. The original report was:

Hello,

A construction like "%>-" in pager_format should make the rest of the
pager status line flush right against " -- (63%) ". Instead, "%>-" in
pager_format fails to take the width of " -- (63%) " (that is, 10)
into account.  Hence the last 10 characters of the pager status line
gets cut off.  A current workaround is to append ten characters to my
pager_format setting.

* * *

I can reproduce this with e.g.:

mutt -nF /dev/null -e 'set pager_format="%>- %s"' -e 'push <ENTER>' -f any-mailbox
* milestone changed to 1.6
* owner changed to brendan
* status changed to assigned

--------------------------------------------------------------------------------
2007-04-11 20:59:38 UTC brendan
* owner changed to mutt-dev
* status changed to new

--------------------------------------------------------------------------------
2007-04-15 21:57:26 UTC me
* owner changed to me

--------------------------------------------------------------------------------
2007-04-15 22:44:39 UTC me
* Added comment:
(In [cb9bef17b3a8]) Removed hardcoded pager progress indicator and add %P format code to $pager_status which contains the same information.

Append "%> -- (%P)" to $pager_status to emulate old behavior.

Closes #2087.

* resolution changed to fixed
* status changed to closed
