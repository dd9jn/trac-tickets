Ticket:  1143
Status:  closed
Summary: Mailbox is read-only

Reporter: root <root@deepgreen.demon.com.mx>
Owner:    mutt-dev

Opened:       2002-04-02 11:14:42 UTC
Last Updated: 2005-08-02 01:19:24 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.28i
Severity: normal

-- Please type your report below this line

Current directory and files permissions:


-rwxr-xr-x    1 root     root      1332912 Apr  2 00:40 /usr/local/bin/mutt
-rwxr-sr-x    1 root     mail        41761 May 29  2001 /usr/local/bin/mutt_dotlock
-rwxr-xr-x    1 root     root           26 Apr  2 00:40 /usr/local/bin/muttbug


drwxrwxr-x    2 root     mail         4096 Apr  2 00:27 /var/spool/mail


-rw-rw----    1 demon    mail       413259 Apr  2 00:10 /var/spool/mail/demon



MMMM.. now i see.. mutt_dotlock has a old date, maybe from my old mutt binaries..

ok.. anyway there its no mutt_dotlock binary on my build directory.. i found why..

make mutt_dotlock
cp ./dotlock.c mutt_dotlock.c
gcc -DPKGDATADIR=\"/usr/local/share/mutt\" -DSYSCONFDIR=\"/usr/local/etc\"      -DBINDIR=\"/usr/local/bin\" -DMUTTLOCALEDIR=\"/usr/local/share/locale\"        -DHAVE_CONFIG_H=1 -I. -I.  -Iintl  -I/usr/include/ncurses -I./intl -I/usr/local/include  -Wall -pedantic -g -O2 -c mutt_dotlock.c
gcc  -Wall -pedantic -g -O2  -o mutt_dotlock  mutt_dotlock.o  
/usr/lib/crt1.o: In function `_start':
/usr/lib/crt1.o(.text+0x18): undefined reference to `main'
mutt_dotlock.o: In function `dotlock_lock':
/hall1/share/trash/mutt-1.3.28/mutt_dotlock.c:595: undefined reference to `Hostname'
collect2: ld returned 1 exit status
make: *** [mutt_dotlock] Error 1

So reading the code its supposed that the mutt_dotlock its inside mutt itself..
because i dont define DL_STANDALONE but.. it dont seems to work.. even erasing
the mutt_dotlock binary on /usr/local/bin/





-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/local/lib/gcc-lib/i686-pc-linux-gnu/3.0/specs
Configured with: ./configure  : (reconfigured) ./configure 
Thread model: single
gcc version 3.0

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.28i (2002-03-13)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.12 (i686) [using ncurses 5.1]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /usr/local/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /usr/local/etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 19:19:24 UTC brendan
* Added comment:
{{{
Incorrect installation.
}}}

* resolution changed to fixed
* status changed to closed
