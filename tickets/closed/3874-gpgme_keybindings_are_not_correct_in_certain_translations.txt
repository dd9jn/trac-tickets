Ticket:  3874
Status:  closed
Summary: gpgme keybindings are not correct in certain translations

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2016-09-13 20:54:01 UTC
Last Updated: 2016-09-22 01:13:27 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
http://bugs.debian.org/837673

As soon as we switched to gpgme by default in Debian, we noticed that some translations have the translation for the string

msgid "S/MIME (s)ign, sign (a)s, (p)gp, (c)lear, or (o)ppenc mode off? "

marked as 'fuzzy', but the keybindings are not marked as 'fuzzy', therefore the user is shown a English message but the keybindings associated to this message are not in English, and it makes things very confusing.

I'll attach a patch as soon as the issue is opened.


--------------------------------------------------------------------------------
2016-09-13 20:54:23 UTC antonio@dyne.org
* summary changed to gpgme keybindings are not correct in certain translations

--------------------------------------------------------------------------------
2016-09-13 20:55:20 UTC antonio@dyne.org
* Added comment:
This patch addresses Italian, German and French

--------------------------------------------------------------------------------
2016-09-13 20:55:59 UTC antonio@dyne.org
* Added attachment 837673-fix-gpgme-sign-bindings.patch

--------------------------------------------------------------------------------
2016-09-14 15:24:54 UTC vinc17
* Added comment:
Replying to [comment:2 antonio@…]:
> This patch addresses Italian, German and French

You meant Spanish, not French. The French translations are already up-to-date.

--------------------------------------------------------------------------------
2016-09-22 01:13:27 UTC Antonio Radici <antonio@dyne.org>
* Added comment:
In [changeset:"ee0fe5834195d7bdb3fc6e3d0bf38ad004ba38a5" 6796:ee0fe5834195]:
{{{
#!CommitTicketReference repository="" revision="ee0fe5834195d7bdb3fc6e3d0bf38ad004ba38a5"
Mark some gpgme pgp menu keybinding translations as fuzzy. (closes #3874)

Some translations for crypt-gpgme.c are marked as fuzzy but the keybindings
attached to these translations are not, this creates confusions for the users
who see the english message but have the keybindings for a message in their own
language available.

As long as the translations are fuzzy, the keybindings should stay fuzzy.
}}}

* resolution changed to fixed
* status changed to closed
