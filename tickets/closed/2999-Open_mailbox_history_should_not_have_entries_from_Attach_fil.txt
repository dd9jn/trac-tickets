Ticket:  2999
Status:  closed
Summary: "Open mailbox" history should not have entries from "Attach file" history

Reporter: emallove
Owner:    brendan

Opened:       2007-12-07 20:41:23 UTC
Last Updated: 2007-12-11 12:13:45 UTC

Priority:  minor
Component: IMAP
Keywords:  "Open mailbox" "Attach file"

--------------------------------------------------------------------------------
Description:
If one creates a mail message with a file attachement (e.g., with attached file named "foo"), then when they try to connect to an IMAP mailbox the history at the `Open mailbox:` prompt will contain "foo". This is wrong because "foo" is not an IMAP mailbox, it is a file. This seems like a violation of the [http://en.wikipedia.org/wiki/Principle_of_least_astonishment Law of Least Astonishment]. It would be nice if the "Attach file" history and the "Open mailbox" history were separate.

The long explanation for why this is a problem is that I run mutt from within GNU Screen (e.g., ten Screen windows, each containing an instance of mutt), and the below command can be used to reopen the previously open mailbox in every window if the IMAP server disconnects. The below command blindly goes back one entry in the "Open mailbox" history and presses "Enter":

{{{
:at "#" stuff "c\020\015"
}}}

--------------------------------------------------------------------------------
2007-12-11 12:13:45 UTC pdmef
* Added comment:
(In [9709d244c64b]) Maintain different history lists for files and mailboxes. Closes #2999.

* resolution changed to fixed
* status changed to closed
