Ticket:  3223
Status:  closed
Summary: spam command examples should mention "imap_headers"

Reporter: matthijs
Owner:    mutt-dev

Opened:       2009-04-27 12:44:42 UTC
Last Updated: 2009-04-28 09:17:58 UTC

Priority:  minor
Component: doc
Keywords:  

--------------------------------------------------------------------------------
Description:
I've recently found the spam handling features in mutt, which are very helpful and well documented. However, when using IMAP for mail storage, the spam command will probably not work right away, since the spam headers are not downloaded by default and should be enabled manually by setting the imap_headers option.

Though it would not strictly be necessary to document this with spam command, the examples there suggest that they should just work. Without any error message or indication what is wrong (error in regexp, error in displaying of the marker, etc.), I've spent quite some time debugging. Also see #2208, which is an "invalid" ticket, resulting from this lack of docs.

I would propose to add the following paragraph to the [http://www.mutt.org/doc/devel/manual.html#spam docs for "spam detection"]:

{{{
Note:

The spam command can only refer to headers that are available in the 
index. For IMAP mailboxes, these are only a few standard headers.
See the '$imap_headers' configuration value for more details.
}}}



--------------------------------------------------------------------------------
2009-04-28 09:17:58 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [c60ce9a3bae7]) For spam detection, mention $imap_headers. Closes #3223.

* resolution changed to fixed
* status changed to closed
