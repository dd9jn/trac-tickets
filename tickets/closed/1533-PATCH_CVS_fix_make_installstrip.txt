Ticket:  1533
Status:  closed
Summary: [PATCH] CVS: fix "make install-strip"

Reporter: matthias.andree@gmx.de
Owner:    mutt-dev

Opened:       2003-04-12 05:32:40 UTC
Last Updated: 2005-07-26 08:03:40 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: mutt-1.4i-216
Severity: important

-- Please type your report below this line

make install-strip of the CVS version on SuSE 8.1 breaks at smime_keys,
because it's listed as a bin_PROGRAM in Makefile.am, so automake tries
to strip it at install time (which must fail because it's no binary).

The fix is to list smime_keys in bin_SCRIPTS instead and add an
EXTRA_SCRIPTS parameter (because it's an autoconf expansion).

Please apply this patch, against CVS checked out around 2003-04-11
11:15 GMT:

Index: Makefile.am
===================================================================
RCS file: /home/roessler/cvs/mutt/Makefile.am,v
retrieving revision 3.19
diff -u -r3.19 Makefile.am
--- Makefile.am	19 Mar 2003 22:43:06 -0000	3.19
+++ Makefile.am	11 Apr 2003 10:26:16 -0000
@@ -3,6 +3,7 @@
 
 AUTOMAKE_OPTIONS = foreign
 EXTRA_PROGRAMS = mutt_dotlock pgpring makedoc
+EXTRA_SCRIPTS = smime_keys
 
 if BUILD_IMAP
 IMAP_SUBDIR = imap
@@ -11,11 +12,11 @@
 
 SUBDIRS = m4 po intl doc contrib $(IMAP_SUBDIR)
 
-bin_SCRIPTS = muttbug flea
+bin_SCRIPTS = muttbug flea @SMIMEAUX_TARGET@
 
 BUILT_SOURCES = keymap_defs.h patchlist.c
 
-bin_PROGRAMS = mutt @DOTLOCK_TARGET@ @PGPAUX_TARGET@ @SMIMEAUX_TARGET@
+bin_PROGRAMS = mutt @DOTLOCK_TARGET@ @PGPAUX_TARGET@
 mutt_SOURCES = $(BUILT_SOURCES) \
 	addrbook.c alias.c attach.c base64.c browser.c buffy.c color.c \
         crypt.c cryptglue.c \

-- System Information
System Version: Linux merlin 2.4.19-4GB #1 Wed Nov 27 00:56:53 UTC 2002 i686 unknown
RPM Packager: http://www.suse.de/feedback
SuSE Release: SuSE Linux 8.1 (i386)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/3.2/specs
Configured with: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --libdir=/usr/lib --enable-languages=c,c++,f77,objc,java,ada --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i486-suse-linux
Thread model: posix
gcc version 3.2

- CFLAGS
-Wall -pedantic -pipe -g -march=pentium -mcpu=i686 -Os -Wall -W

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins und andere.
Mutt übernimmt KEINERLEI GEWÄHRLEISTUNG. Starten Sie `mutt -vv', um
weitere Details darüber zu erfahren. Mutt ist freie Software. 
Sie können es unter bestimmten Bedingungen weitergeben; starten Sie
`mutt -vv' für weitere Details.

System: Linux 2.4.19-4GB (i686) [using ncurses 5.2]
Einstellungen bei der Compilierung:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Maildir/"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, verwenden Sie bitte das Programm flea(1).



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-04-12 05:32:40 UTC matthias.andree@gmx.de
* Added comment:
{{{
Package: mutt
Version: mutt-1.4i-216
Severity: important

-- Please type your report below this line

make install-strip of the CVS version on SuSE 8.1 breaks at smime_keys,
because it's listed as a bin_PROGRAM in Makefile.am, so automake tries
to strip it at install time (which must fail because it's no binary).

The fix is to list smime_keys in bin_SCRIPTS instead and add an
EXTRA_SCRIPTS parameter (because it's an autoconf expansion).

Please apply this patch, against CVS checked out around 2003-04-11
11:15 GMT:

Index: Makefile.am
===================================================================
RCS file: /home/roessler/cvs/mutt/Makefile.am,v
retrieving revision 3.19
diff -u -r3.19 Makefile.am
--- Makefile.am	19 Mar 2003 22:43:06 -0000	3.19
+++ Makefile.am	11 Apr 2003 10:26:16 -0000
@@ -3,6 +3,7 @@

AUTOMAKE_OPTIONS = foreign
EXTRA_PROGRAMS = mutt_dotlock pgpring makedoc
+EXTRA_SCRIPTS = smime_keys

if BUILD_IMAP
IMAP_SUBDIR = imap
@@ -11,11 +12,11 @@

SUBDIRS = m4 po intl doc contrib $(IMAP_SUBDIR)

-bin_SCRIPTS = muttbug flea
+bin_SCRIPTS = muttbug flea @SMIMEAUX_TARGET@

BUILT_SOURCES = keymap_defs.h patchlist.c

-bin_PROGRAMS = mutt @DOTLOCK_TARGET@ @PGPAUX_TARGET@ @SMIMEAUX_TARGET@
+bin_PROGRAMS = mutt @DOTLOCK_TARGET@ @PGPAUX_TARGET@
mutt_SOURCES = $(BUILT_SOURCES) \
	addrbook.c alias.c attach.c base64.c browser.c buffy.c color.c \
        crypt.c cryptglue.c \

-- System Information
System Version: Linux merlin 2.4.19-4GB #1 Wed Nov 27 00:56:53 UTC 2002 i686 unknown
RPM Packager: http://www.suse.de/feedback
SuSE Release: SuSE Linux 8.1 (i386)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/3.2/specs
Configured with: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --libdir=/usr/lib --enable-languages=c,c++,f77,objc,java,ada --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i486-suse-linux
Thread model: posix
gcc version 3.2

- CFLAGS
-Wall -pedantic -pipe -g -march=pentium -mcpu=i686 -Os -Wall -W

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins und andere.
Mutt übernimmt KEINERLEI GEWÄHRLEISTUNG. Starten Sie `mutt -vv', um
weitere Details darüber zu erfahren. Mutt ist freie Software. 
Sie können es unter bestimmten Bedingungen weitergeben; starten Sie
`mutt -vv' für weitere Details.

System: Linux 2.4.19-4GB (i686) [using ncurses 5.2]
Einstellungen bei der Compilierung:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Maildir/"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, verwenden Sie bitte das Programm flea(1).
}}}

--------------------------------------------------------------------------------
2005-07-27 02:03:40 UTC brendan
* Added comment:
{{{
Fixed in CVS, thanks.
}}}

* resolution changed to fixed
* status changed to closed
