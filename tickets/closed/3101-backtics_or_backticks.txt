Ticket:  3101
Status:  closed
Summary: backtics or backticks?

Reporter: jhawk
Owner:    mutt-dev

Opened:       2008-08-11 10:16:34 UTC
Last Updated: 2008-08-11 14:42:36 UTC

Priority:  trivial
Component: mutt
Keywords:  backtic backtics

--------------------------------------------------------------------------------
Description:
Trying to search for documentation on mutt's backtick expansion is a bit confusing because both code and documentation spells the word both "backtick" and "backtic." This becomes a problem when searching for plurals ("backticks" and "backtics"). Could someone please pick one and stick to it?

"backtics":
.
{{{
./doc/manual.xml.head:1
./doc/manual.html:1
./alias.c:2
./init.c:1
}}}


"backticks":
{{{
./doc/configuration.html:1
./ChangeLog.old:1

}}}

I suggest standardizing on "backticks" (with the k).

--------------------------------------------------------------------------------
2008-08-11 14:42:36 UTC pdmef
* Added comment:
(In [508ffc26dad2]) Prefer "backtick" over "backtic" on comments and docs as it's more common, closes #3101.

* resolution changed to fixed
* status changed to closed
