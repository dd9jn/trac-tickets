Ticket:  719
Status:  closed
Summary: display is not restored when returning from sendmail error screen

Reporter: ma@dt.e-technik.uni-dortmund.de (Matthias Andree)
Owner:    mutt-dev

Opened:       2001-08-02 06:13:51 UTC
Last Updated: 2005-08-09 11:40:27 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.19i
Severity: important

-- Please type your report below this line

When my sendmail command throws output, this is displayed in the mutt internal
pager. However, if I press i to return to the index, the display of the index
is not restored, I need to press ^L.

-- Mutt Version Information

Mutt 1.3.19i (2001-06-07)
Copyright (C) 1996-2001 Michael R. Elkins und andere.
Mutt übernimmt KEINERLEI GEWÄHRLEISTUNG. Starten Sie `mutt -vv', um
weitere Details darüber zu erfahren. Mutt ist freie Software. 
Sie können es unter bestimmten Bedingungen weitergeben; starten Sie
`mutt -vv' für weitere Details.

System: Linux 2.4.7-ma1 [using ncurses 5.0]
Einstellungen bei der Compilierung:
-DOMAIN
+DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  
-HAVE_REGCOMP  +USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Maildir/"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, verwenden Sie bitte das Programm flea(1).



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 11:01:47 UTC brendan
* Added comment:
{{{
I can't reproduce this with a current version. Can you?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-10 03:40:31 UTC Matthias Andree <matthias.andree@gmx.de>
* Added comment:
{{{
I cannot reproduce this with the current CVS version any more.

-- 
Matthias Andree
}}}

--------------------------------------------------------------------------------
2005-08-10 05:40:27 UTC cb
* Added comment:
{{{
I cannot reproduce this with the current CVS version any more.
No longer reproducible.
Thanks for your feedback.
}}}

* resolution changed to fixed
* status changed to closed
