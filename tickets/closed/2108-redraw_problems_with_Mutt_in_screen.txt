Ticket:  2108
Status:  closed
Summary: redraw problems with Mutt in "screen"

Reporter: vincent@vinc17.org
Owner:    mutt-dev

Opened:       2005-10-10 11:03:15 UTC
Last Updated: 2007-02-28 11:00:37 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
I use Mutt in the "screen" utility, and when I executed screen in a different terminal, Mutt's index was incorrectly redrawn:

[...]
    337 r ! Oct 10 xxx xxxx        (  34)                   mq>
->  338   ! Oct 10 To xxxxxxxxxxx@ (  19)                     mq>
---Mutt: =Maildir [Msg:339 New:76 Inc:15 12M]---(threads)-----------------(99%)-






  
  
->
  
    339 N T Oct 10 xxxx xxxxxxxxxx (  16) Re: [xxxxxxxxxx] C10
                                                                           end

I can't reproduce this problem.

Also, each time I resize the terminal, the screen is redrawn twice (always reproducible).
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2007-02-28 15:09:26 UTC Christoph Berg <cb@df7cb.de>
* Added comment:
{{{
Hi Vincent,

Re: To Mutt Developers 2005-10-10 <E1EOuV1-0004ns-JX@trithemius.gnupg.org>
> I use Mutt in the "screen" utility, and when I executed screen in a different terminal, Mutt's index was incorrectly redrawn:
> 
> [...]
>     337 r ! Oct 10 xxx xxxx        (  34)                   mq>
> ->  338   ! Oct 10 To xxxxxxxxxxx@ (  19)                     mq>
> ---Mutt: =Maildir [Msg:339 New:76 Inc:15 12M]---(threads)-----------------(99%)-
> 
>   
>     339 N T Oct 10 xxxx xxxxxxxxxx (  16) Re: [xxxxxxxxxx] C10
>                                                                            end
> 
> I can't reproduce this problem.

Did you really mean "can't" here? Does the problem still occur?

> Also, each time I resize the terminal, the screen is redrawn twice (always reproducible).

Do you still experience that with recent mutt versions? (Seems ok
here, though the terminal could just be too fast.)

Christoph
-- 
cb@df7cb.de | http://www.df7cb.de/
}}}

--------------------------------------------------------------------------------
2007-02-28 20:20:05 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2007-02-27 22:15:02 +0100, Christoph Berg wrote:
>  > I use Mutt in the "screen" utility, and when I executed screen in a =
different terminal, Mutt's index was incorrectly redrawn:
>  >=20
>  > [...]
>  >     337 r ! Oct 10 xxx xxxx        (  34)                   mq>
>  > ->  338   ! Oct 10 To xxxxxxxxxxx@ (  19)                     mq>
>  > ---Mutt: =3DMaildir [Msg:339 New:76 Inc:15 12M]---(threads)---------=
--------(99%)-
>  >=20
>  >  =20
>  >     339 N T Oct 10 xxxx xxxxxxxxxx (  16) Re: [xxxxxxxxxx] C10
>  >                                                                     =
       end
>  >=20
>  > I can't reproduce this problem.
> =20
>  Did you really mean "can't" here? Does the problem still occur?

It no longer occurs.

>  > Also, each time I resize the terminal, the screen is redrawn twice (=
always reproducible).
> =20
>  Do you still experience that with recent mutt versions?

I haven't notice that for a while. I've just done tests and everything
is fine.

> (Seems ok here, though the terminal could just be too fast.)

I've tried with a double SSH connection. :)

--=20
Vincent Lef=E8vre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / Arenaire project (LIP, ENS-Lyon)
}}}

--------------------------------------------------------------------------------
2007-03-01 05:00:37 UTC cb
* Added comment:
{{{
Hi Vincent,
 On 2007-02-27 22:15:02 +0100, Christoph Berg wrote:
no longer reproducible
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-03-01 05:00:38 UTC cb
* Added comment:
{{{
Hi,

thanks for the answer, I'm closing the report for now.

Christoph
}}}
