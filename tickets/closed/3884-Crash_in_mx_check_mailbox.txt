Ticket:  3884
Status:  closed
Summary: Crash in mx_check_mailbox()

Reporter: ahuffman
Owner:    mutt-dev

Opened:       2016-10-06 06:46:27 UTC
Last Updated: 2016-10-10 18:19:18 UTC

Priority:  major
Component: maildir/mh
Keywords:  

--------------------------------------------------------------------------------
Description:

This appears to be triggered by a problem with the connection to the IMAP server.
On the previous version, I would see fairly regular connection drops, and I would have manually to reconnect. With 1.7.0, Mutt crashes.

Mutt is left open on the mailbox index, inside a tmux session.

{{{
(gdb) bt full
#0  0x00005555555aaeac in mx_check_mailbox (ctx=0x5555559a0a30, index_hint=0x7fffffffcd44) at mx.c:1302
No locals.
#1  0x0000555555581bc6 in mutt_index_menu () at curs_main.c:557
        check = <optimized out>
        buf = "-*-Mutt: =INBOX [Msgs:122071 New:2654 Old:95402 Flag:532 Post:6 2136M]---(date/date)", '-' <repeats 123 times>...
        helpstr = "q:Quit  d:Del  u:Undel  s:Save  m:Mail  r:Reply  g:Group  ?:Help\000\000\000\000\000\000\000\000P\327\372\367\377\177\000\000\062\000\000\000\000\000\000\000\314\035", '\000' <repeats 14 times>, "\b\000\000\000\000\000\000\000\b\000\000\000\000\000\000\000h\021]UUU\000\000\313\035\000\000\000\000\000\000(;\310\352\377\177\000\000\313\035\000\000\000\000\000\000N\324\003\366\377\177\000\000V\324\377\377\377\177\000\000\000\000\000\000\000\000\000\000\b\000\000\000\000\000\000\000 \325\377\377\377\177\000\000\001\000\000\000\000\000\000\000"...
        op = -1
        done = 0
        i = 0
        j = <optimized out>
        tag = 0
        newcount = 0
        oldcount = <optimized out>
        rc = <optimized out>
        menu = 0x55555f93a380
        cp = <optimized out>
        index_hint = 0
        do_buffy_notify = 1
        close = <optimized out>
        attach_msg = <optimized out>
#2  0x0000555555569814 in main (argc=1, argv=<optimized out>) at main.c:1228
        folder = "imaps://<username>@<server>/", '\000' <repeats 229 times>
        subject = 0x0
        includeFile = 0x0
        draftFile = 0x0
        newMagic = 0x0
        msg = 0x0
        attach = 0x0
        commands = 0x0
        queries = 0x0
        alias_queries = 0x0
        sendflags = 0
        flags = 0
        version = 0
        i = <optimized out>
        explicit_folder = 0
        dump_variables = 0
        edit_infile = 0
        double_dash = <optimized out>
        nargc = <optimized out>
}}}

mutt -v
Mutt 1.7.0 (2016-08-17)
Copyright (C) 1996-2016 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 4.7.4-200.fc24.x86_64 (x86_64)
ncurses: ncurses 6.0.20160709 (compiled with 6.0)
libidn: 1.33 (compiled with 1.33)
hcache backend: tokyocabinet 1.4.48

Compiler:
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/libexec/gcc/x86_64-redhat-linux/6.1.1/lto-wrapper
Target: x86_64-redhat-linux
Configured with: ../configure --enable-bootstrap --enable-languages=c,c++,objc,obj-c++,fortran,ada,go,lto --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-shared --enable-threads=posix --enable-checking=release --enable-multilib --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-linker-hash-style=gnu --enable-plugin --enable-initfini-array --disable-libgcj --with-isl --enable-libmpx --enable-gnu-indirect-function --with-tune=generic --with-arch_32=i686 --build=x86_64-redhat-linux
Thread model: posix
gcc version 6.1.1 20160621 (Red Hat 6.1.1-3) (GCC)

Configure options: '--build=x86_64-redhat-linux-gnu' '--host=x86_64-redhat-linux-gnu' '--program-prefix=' '--disable-dependency-tracking' '--prefix=/usr' '--exec-prefix=/usr' '--bindir=/usr/bin' '--sbindir=/usr/sbin' '--sysconfdir=/etc' '--datadir=/usr/share' '--includedir=/usr/include' '--libdir=/usr/lib64' '--libexecdir=/usr/libexec' '--localstatedir=/var' '--sharedstatedir=/var/lib' '--mandir=/usr/share/man' '--infodir=/usr/share/info' 'SENDMAIL=/usr/sbin/sendmail' 'ISPELL=/usr/bin/hunspell' '--enable-debug' '--enable-pop' '--enable-imap' '--enable-smtp' '--enable-hcache' '--without-gdbm' '--without-qdbm' '--with-gnutls' '--with-sasl' '--with-gss' '--enable-gpgme' '--enable-sidebar' '--with-docdir=/usr/share/doc/mutt' 'build_alias=x86_64-redhat-linux-gnu' 'host_alias=x86_64-redhat-linux-gnu' 'CFLAGS=-O2 -g -pipe -Wall -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -m64 -mtune=generic' 'LDFLAGS=-Wl,-z,relro -specs=/usr/lib/rpm/redhat/redhat-hardened-ld'

Compilation CFLAGS: -Wall -pedantic -Wno-long-long -O2 -g -pipe -Wall -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -m64 -mtune=generic

Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  -USE_DOTLOCK  +DL_STANDALONE  +USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +USE_SMTP
-USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +USE_GSS  +HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  +CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  +USE_SIDEBAR
ISPELL="/usr/bin/hunspell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.



--------------------------------------------------------------------------------
2016-10-06 12:34:35 UTC driegel
* Added comment:
I think this has already been reported and fixed by this commit: https://dev.mutt.org/hg/mutt/rev/9f6e08ba6ff3 . Could you confirm that it fixes your crash?

--------------------------------------------------------------------------------
2016-10-06 16:54:52 UTC kevin8t8
* Added comment:
Thank you for the bug report.  I suspect this may have been fixed as driegel mentions above.

I am overdue to release 1.7.1 with this fix, and will work on getting that release out this coming weekend.

--------------------------------------------------------------------------------
2016-10-08 21:30:41 UTC kevin8t8
* Added comment:
I've just release 1.7.1.  Would you mind testing with that version, just to make sure this is indeed the same problem fixed in changeset:9f6e08ba6ff3?

Thank you!

--------------------------------------------------------------------------------
2016-10-09 18:00:47 UTC ahuffman
* Added comment:
Apologies for not being able to test earlier.

I'm now running 1.7.1 and will let you know what happens.

--------------------------------------------------------------------------------
2016-10-10 06:52:52 UTC ahuffman
* Added comment:
I can confirm that with 1.7.1, when the connection to the IMAP server is closed, Mutt no longer crashes as 1.7.0 did.

Thanks for the very prompt fix.

--------------------------------------------------------------------------------
2016-10-10 18:19:18 UTC kevin8t8
* Added comment:
I'm glad to hear that fixed the issue.  I'll close this ticket out now.

* resolution changed to fixed
* status changed to closed
