Ticket:  3075
Status:  closed
Summary: Segfault when viewing attachement

Reporter: raorn
Owner:    mutt-dev

Opened:       2008-06-11 09:33:09 UTC
Last Updated: 2008-06-12 05:46:00 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Mutt may catch a segfault when viewing an attachment with certain mailcap entry.  Patch attached.

--------------------------------------------------------------------------------
2008-06-11 09:33:46 UTC raorn
* Added attachment mutt-alt-check-descriptors.patch
* Added comment:
mutt-alt-check-descriptors.patch

--------------------------------------------------------------------------------
2008-06-11 09:34:54 UTC raorn
* version changed to 1.5.18

--------------------------------------------------------------------------------
2008-06-11 09:45:44 UTC raorn
* cc changed to kas@altlinux.org

--------------------------------------------------------------------------------
2008-06-12 05:46:00 UTC Alexey I. Froloff <raorn@altlinux.org>
* Added comment:
(In [1e8ca708a52f]) Do not attempt to close invalid descriptors. Closes #3075

* resolution changed to fixed
* status changed to closed
