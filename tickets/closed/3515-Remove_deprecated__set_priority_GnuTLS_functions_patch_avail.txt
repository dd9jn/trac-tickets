Ticket:  3515
Status:  closed
Summary: Remove deprecated _set_priority GnuTLS functions (patch available)

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2011-05-03 18:18:20 UTC
Last Updated: 2015-05-03 15:50:30 UTC

Priority:  minor
Component: crypto
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Reporting from http://bugs.debian.org/624058

{{{
This package is using some functions which are marked deprecated in
newer versions of GnuTLS (>=2.12.x). (They are not yet removed, though.)
These functions will be removed in future releases. GnuTLS 2.12.x is
already available in experimental.

Excerpt from buildlog:
====================================================
../mutt_ssl_gnutls.c:313:3: warning: 'gnutls_protocol_set_priority' is deprecated (declared at /usr/include/gnutls/compat.h:344)
../mutt_ssl_gnutls.c:313:3: warning: 'gnutls_protocol_set_priority' is deprecated (declared at /usr/include/gnutls/compat.h:344)
====================================================

The gnutls_*_set_priority family of functions has been marked deprecated
in 2.12.x. These functions have been superceded by 
gnutls_priority_set_direct(). The replacement function was added in gnutls
stable release 2.2.0 (released 2007-12-14).
}}}

The attached patch fixes the problem

--------------------------------------------------------------------------------
2011-05-03 18:18:34 UTC antonio@dyne.org
* Added attachment 624058-gnutls-deprecated-set-priority.patch

--------------------------------------------------------------------------------
2012-07-09 00:31:52 UTC brendan
* Added comment:
Thanks for the patch, but I think it needs some autoconf support to only call the new function when it is available, otherwise to fall back to the old one.

* Updated description:
Reporting from http://bugs.debian.org/624058

{{{
This package is using some functions which are marked deprecated in
newer versions of GnuTLS (>=2.12.x). (They are not yet removed, though.)
These functions will be removed in future releases. GnuTLS 2.12.x is
already available in experimental.

Excerpt from buildlog:
====================================================
../mutt_ssl_gnutls.c:313:3: warning: 'gnutls_protocol_set_priority' is deprecated (declared at /usr/include/gnutls/compat.h:344)
../mutt_ssl_gnutls.c:313:3: warning: 'gnutls_protocol_set_priority' is deprecated (declared at /usr/include/gnutls/compat.h:344)
====================================================

The gnutls_*_set_priority family of functions has been marked deprecated
in 2.12.x. These functions have been superceded by 
gnutls_priority_set_direct(). The replacement function was added in gnutls
stable release 2.2.0 (released 2007-12-14).
}}}

The attached patch fixes the problem

--------------------------------------------------------------------------------
2014-08-23 02:08:16 UTC kevin8t8
* Added comment:
Attached is a revised patch.  It uses autoconf to check if the new function is available, and if not reverts to the old behavior.

The TLS code has more options since the previous patch was written.  So the revised patch uses a slightly different technique to generate the priority string.

Also, a note to antonio: the previous patch was mistakenly dropped from the Debian package during the 1.5.22-1 release.  If this revised patch looks good, you may want to add it to the Debian package in place of the old one.

--------------------------------------------------------------------------------
2014-08-23 02:09:21 UTC kevin8t8
* Added attachment ticket-3515.patch
* Added comment:
Revised patch using autoconf

--------------------------------------------------------------------------------
2014-08-26 05:09:04 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [48f5e2a518095333ec06debb2307fa35f6f192e5]:
{{{
#!CommitTicketReference repository="" revision="48f5e2a518095333ec06debb2307fa35f6f192e5"
Fix deprecated gnutls_protocol_set_priority.  (closes #3515)

This patch is based off of the patch at
http://dev.mutt.org/trac/attachment/ticket/3515/624058-gnutls-deprecated-set-priority.patch

The mutt source has changed since then, with more TLS options being
added.  This patch therefore uses a slightly different strategy for generating the
priority string.

The patch also adds autoconf support, as requested by brendan at
http://dev.mutt.org/trac/ticket/3515#comment:1
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2015-05-03 13:18:28 UTC ametzler
* Added comment:
Current HG is broken, it uses gnutls_protocol_set_priority() and therefore fails to build with GnuTLS 3.4.

* resolution changed to 
* status changed to reopened

--------------------------------------------------------------------------------
2015-05-03 14:53:30 UTC kevin8t8
* Added comment:
The usage of gnutls_protocol_set_priority() is contained in an #else/#endif block, which should only be compiled if the function gnutls_priority_set_direct() is not found by configure.

Can you re-run configure and confirm what this section of the output looks like for you:
{{{
checking for gnutls_check_version in -lgnutls... yes
checking whether GNUTLS_VERIFY_DISABLE_TIME_CHECKS is declared... yes
checking for gnutls_priority_set_direct... yes
checking for gnutls_certificate_credentials_t... yes
checking for gnutls_certificate_status_t... yes
checking for gnutls_datum_t... yes
checking for gnutls_digest_algorithm_t... yes
checking for gnutls_session_t... yes
checking for gnutls_transport_ptr_t... yes
checking for gnutls_x509_crt_t... yes
}}}

--------------------------------------------------------------------------------
2015-05-03 15:50:30 UTC ametzler
* Added comment:
kevin8t8 wrote:
''The usage of gnutls_protocol_set_priority() is contained in an #else/#endif block, which should only be compiled if the function gnutls_priority_set_direct() is not found by configure.'' 

You are absolutely right. I am very sorry for the noise.

cu Andreas

* resolution changed to fixed
* status changed to closed
