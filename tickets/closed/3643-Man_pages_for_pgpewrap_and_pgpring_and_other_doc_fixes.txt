Ticket:  3643
Status:  closed
Summary: Man pages for pgpewrap and pgpring and other doc fixes

Reporter: hhorak
Owner:    mutt-dev

Opened:       2013-05-20 15:15:47 UTC
Last Updated: 2013-10-07 17:19:34 UTC

Priority:  trivial
Component: doc
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi, this is only unimportant patch, that adds some missing options in doc and adds man pages for binaries pgpewrap and pgpring.

--------------------------------------------------------------------------------
2013-05-20 15:16:19 UTC hhorak
* Added attachment mutt-1.5.21-manhelp.patch
* Added comment:
patch that adds missing options in doc and adds two man pages

--------------------------------------------------------------------------------
2013-10-07 17:19:34 UTC hhorak@redhat.com
* Added comment:
In [76f8df0ac80396559aa80e7f145be6f235a6db51]:
{{{
#!CommitTicketReference repository="" revision="76f8df0ac80396559aa80e7f145be6f235a6db51"
adds some missing options in doc and adds man pages for binaries pgpewrap and pgpring.

closes #3643
}}}

* resolution changed to fixed
* status changed to closed
