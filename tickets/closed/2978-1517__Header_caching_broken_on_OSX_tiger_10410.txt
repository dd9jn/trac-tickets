Ticket:  2978
Status:  closed
Summary: 1.5.17 - Header caching broken on OSX tiger 10.4.10

Reporter: gufo
Owner:    mutt-dev

Opened:       2007-11-03 18:06:25 UTC
Last Updated: 2008-05-21 07:28:48 UTC

Priority:  minor
Component: header cache
Keywords:  header cache, db4, OS X

--------------------------------------------------------------------------------
Description:
Mutt is compiled with  +USE_HCACHE [[BR]]
on my muttrc (the same I use with mutt-1.5.16, where heade cache works correctly) is set: [[BR]]
... [[BR]]
# Cache maildir headers [[BR]]
set header_cache = "~/.mutt/headers"[[BR]]
# On my systems mutt is the only program which plays with my [[BR]]
# emails, so there's no reason to spend a stat system call for [[BR]]
# each mail [[BR]]
unset maildir_header_cache_verify [[BR]]

The problem arise if ~/.mutt/headers is either a file or a folder. 

--------------------------------------------------------------------------------
2007-11-03 20:00:04 UTC pdmef
* Added comment:
It works fine here. Can you please attach the output of 'mutt -v'? How did you install it (self, find, macports)?

--------------------------------------------------------------------------------
2007-11-04 11:01:25 UTC gufo
* Added comment:
Replying to [comment:1 pdmef]:
> It works fine here. Can you please attach the output of 'mutt -v'? How did you install it (self, find, macports)?

I installed it using macports. [[BR]]
port installed mutt-devel [[BR]]
The following ports are currently installed: [[BR]]
mutt-devel @1.5.17_2+db4+headercache+imap+pop (active) [[BR]]

The output of mutt -v is attached. I know macports doesn't have 1.5.17, I just changed the version in the portfile and the checksums.
 

--------------------------------------------------------------------------------
2007-11-04 11:01:57 UTC gufo
* Added attachment mutt-v
* Added comment:
mutt -v

--------------------------------------------------------------------------------
2007-11-05 11:08:56 UTC gufo
* Added comment:
I tried some different configuration and tracing what happened using debug. [[BR]]
Header caching started working when I enabled gdbm instead of the db4 support. [[BR]]
Now mutt -v is like the attached one, the only difference between my last try (not the one posted in this ticket before) is using gdbm instead of db4. [[BR]]

--------------------------------------------------------------------------------
2007-11-05 11:10:26 UTC gufo
* Added attachment qdbm-compile-error.txt
* Added comment:
gdbm support

--------------------------------------------------------------------------------
2007-11-05 17:01:36 UTC pdmef
* Added comment:
So gdbm works, qdbm works here (on a site note you could report that error to macports). That means db4 on OS X is broken/has trouble. I see if I have time to look into it.

* keywords changed to header cache, db4, OS X

--------------------------------------------------------------------------------
2008-05-21 07:28:48 UTC pdmef
* Added comment:
I can build a functional mutt with bdb from macports just fine. Other reports say it's working without macports, too.

* resolution changed to worksforme
* status changed to closed
