Ticket:  2840
Status:  closed
Summary: "%>XY" in conditional clause (eg. "%?V?%>XY?") doesn't show Y

Reporter: schertz123456@web.de
Owner:    me

Opened:       2007-03-09 13:37:26 UTC
Last Updated: 2007-04-16 01:36:41 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
"%>XY" in conditional clause (eg. "%?V?%>XY?") doesn't show Y

this error occurs in the status_format

--------------------------------------------------------------------------------
2007-03-27 23:07:29 UTC brendan
* Updated description:
"%>XY" in conditional clause (eg. "%?V?%>XY?") doesn't show Y

this error occurs in the status_format

--------------------------------------------------------------------------------
2007-04-12 05:40:30 UTC Michael Elkins
* Added attachment patch-1.5.14-me.format-col-recurse.1.patch
* Added comment:
Added by email2trac

* Added comment:
{{{
On Fri, Mar 09, 2007 at 02:37:26PM +0100, schertz123456@web.de wrote:
> "%>XY" in conditional clause (eg. "%?V?%>XY?") doesn't show Y
> 
> this error occurs in the status_format

Attached is a patch which fixes the problem.  It is fairly invasive, so
I would like some other folks to test it out to make sure that nothing
was broke as a side effect.

me
}}}

--------------------------------------------------------------------------------
2007-04-12 05:42:00 UTC me
* owner changed to me

--------------------------------------------------------------------------------
2007-04-15 21:56:09 UTC me
* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-16 01:36:41 UTC brendan
* Added comment:
Sorry for not testing this patch before it was applied. GPGME support no longer compiles -- would you mind fixing it?

Thanks.
