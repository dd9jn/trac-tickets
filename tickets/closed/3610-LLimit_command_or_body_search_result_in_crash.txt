Ticket:  3610
Status:  closed
Summary: LLimit command or body search result in crash.

Reporter: ralf
Owner:    mutt-dev

Opened:       2012-12-19 22:35:09 UTC
Last Updated: 2012-12-21 00:00:53 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Receipe to reproduce:

1) copy attached file to file <mbox>
2) open the file with mutt -f <mbox>
3) search the folder ('/' command) or apply a limit ('l' command) using the pattern "~b asdf" (without the quotes of course) for the pattern.  You need to search the body of the email but it doesn't matter if searching for asdf or something else.

This will mutt die with SIGSEGV; my Linux system also logs "mutt![2175]: segfault at 1 ip 000000000046373d sp 00007fffb0130570 error 4 in mutt[400000+be000]" in syslog.

Test enviroment: Fedora 14, kernel-2.6.35.14-106.fc14.x86_64.  However I've seen this bug for a very long time on distributions both older and newer than this.  But now it was getting *really* annoying.

--------------------------------------------------------------------------------
2012-12-19 22:39:44 UTC ralf
* Added attachment mbox-crashing-mutt.mbox
* Added comment:
mbox crashing mutt when body is searched or when limit command based on body search is applied

--------------------------------------------------------------------------------
2012-12-19 22:48:44 UTC ralf
* Added comment:
In extracting a minimal test case I found that the length of the headers of the attachment of the mail in the provided test mbox matters.  Removing any of the four headers:

{{{
Return-Path: <aaaaaa@bbb.c.ddd>
To: e@fff.gg.hhhh.ii
Subject: jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj
Date: Fri, 10 Jun 2012 12:12:12 -0000
}}}

will make the SIGSEGV go away.  The length of the Email addresses or its atoms are sensitive just like the length of the Subject string.  This suggests the issue may be a buffer overflow and may not be reproducible on all systems.

--------------------------------------------------------------------------------
2012-12-19 23:48:04 UTC me
* Added comment:
I am not able to reproduce this problem with mutt tip.  I ran `valgrind --leak-check=yes` to run the memcheck tool and verified there are no illegal read or write errors.

are you running mutt compiled from our mercurial repository?


* status changed to infoneeded_new

--------------------------------------------------------------------------------
2012-12-20 10:17:46 UTC ralf
* Added comment:
Replying to [comment:2 me]:
> I am not able to reproduce this problem with mutt tip.  I ran `valgrind --leak-check=yes` to run the memcheck tool and verified there are no illegal read or write errors.
> 
> are you running mutt compiled from our mercurial repository?
> 

I was observing this with the packages from the Fedora repository, in this case mutt-1.5.21-6.fc14.x86_64.

Sorry for giving you the kernel version which obviously is fairly pointless.  A Freudian bugreport from a kernel hacker, I guess ;-)

* status changed to new

--------------------------------------------------------------------------------
2012-12-20 15:49:40 UTC ralf
* Added comment:
I did some further testing.  On Fedora 16 mutt-1.5.21-7.fc16.x86_64 is also broken for me.  But Fedora 17 mutt-1.5.21-11.fc17.x86_64 is broken but mutt-1.5.21-12.fc17.x86_64 appears to be working fine.  The rpm packages changelog between the two releases is:

* Thu May 10 2012 Honza Horak <hhorak@redhat.com> - 5:1.5.21-12
- Fix segmentation fault while syncing mailbox
  (rhbz#691719)
- Fix unhandled strchr output
  (rhbz#833044)

These are the two bugzilla links:

https://bugzilla.redhat.com/show_bug.cgi?id=691719

https://bugzilla.redhat.com/show_bug.cgi?id=833044

691719 affects the IMAP code but my issue doesn't involve IMAP.  This suggests that 833044 might be what I was facing or it's an unrelated issue in another system component.

Anyway, this now means I have a solution though a painful one.

--------------------------------------------------------------------------------
2012-12-21 00:00:53 UTC me
* Added comment:
closing this ticket.  please reopen if you think this issue is not resolved in the mutt repo.

thanks.

* resolution changed to worksforme
* status changed to closed
