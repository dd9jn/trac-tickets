Ticket:  1715
Status:  closed
Summary: Saving an IMAP message sometimes fails to delete it

Reporter: Gary Mills <mills@cc.UManitoba.CA>
Owner:    mutt-dev

Opened:       2003-11-22 21:38:09 UTC
Last Updated: 2005-10-04 16:15:01 UTC

Priority:  minor
Component: mutt
Keywords:  IMAP

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.1
Severity: normal

-- Please type your report below this line

When I read an IMAP message and answer `no' to this prompt:

     Move read messages to /home/uadmin/mills/Mail/mbox? ([no]/yes):

the message remains in my IMAP INBOX.  This is correct.

When I read it again, and answer `yes' to this prompt:

     Move read messages to /home/uadmin/mills/Mail/mbox? ([no]/yes): y

it is saved to mbox, but is not deleted from my IMAP INBOX.
This is not correct.  I have to read it again and explicitly
delete it to make it go away.

However, if I answer `yes' to the prompt:

	 Move read messages to /home/uadmin/mills/Mail/mbox? ([no]/yes): y

the first time, the message is saved to mbox, and is also deleted
from my IMAP INBOX.  This is correct.

I have transcripts of the IMAP dialogue on the Cyrus IMAP server.
When I read the message and leave it in my INBOX, the session ends
like this:

a0007 OK Completed
<1069519696<a0008 UID STORE 93375 FLAGS.SILENT (\Seen)
>1069519696>a0008 OK Completed
<1069519696<a0009 CLOSE
>1069519696>a0009 OK Completed
<1069519696<a0010 LOGOUT
>1069519696>* BYE LOGOUT received
a0010 OK Completed

When I re-read it, and save it to mbox, it ends like this:

a0007 OK Completed
<1069519741<a0008 CLOSE
>1069519741>a0008 OK Completed
<1069519741<a0009 LOGOUT
>1069519741>* BYE LOGOUT received
a0009 OK Completed

However, when I read and save it in one session, it ends like this:

a0007 OK Completed
<1069520135<a0008 UID STORE 93377 +FLAGS.SILENT (\Deleted)
>1069520135>a0008 OK Completed
<1069520135<a0009 CLOSE
>1069520135>a0009 OK Completed
<1069520135<a0010 LOGOUT
>1069520135>* BYE LOGOUT received
a0010 OK Completed

-- Build environment information

env CC=cc \
./configure \
	    --enable-pop \
	    --enable-imap

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: SunOS 5.8 (sun4u)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  +ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/lib/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-05 13:16:03 UTC brendan
* Added comment:
{{{
Does this still happen in 1.5.10?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-10-04 19:35:23 UTC Gary Mills <mills@cc.umanitoba.ca>
* Added comment:
{{{
On Sun, Sep 04, 2005 at 08:16:03PM +0200, Brendan Cully wrote:
> Synopsis: Saving an IMAP message sometimes fails to delete it
> 
> State-Changed-From-To: open->feedback
> State-Changed-By: brendan
> State-Changed-When: Sun, 04 Sep 2005 20:16:03 +0200
> State-Changed-Why:
> Does this still happen in 1.5.10?
> 
> **** Comment added by brendan on Sun, 04 Sep 2005 20:16:03 +0200 ****

I've only recently compiled 1.5.10i on Solaris for initial testing.

I'm pleased to report that this problem no longer occurs with
the 1.5.10i version of mutt.  Thanks for your efforts.

-- 
-Gary Mills-    -Unix Support-    -U of M Academic Computing and Networking-
}}}

--------------------------------------------------------------------------------
2005-10-04 23:36:34 UTC brendan
* Added comment:
{{{
Unreproducible; no feedback.
}}}

* resolution changed to fixed
* status changed to closed
