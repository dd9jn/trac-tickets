Ticket:  3442
Status:  closed
Summary: memory leak in postpone.c (mutt_num_postponed)

Reporter: vinc17
Owner:    mutt-dev

Opened:       2010-08-09 13:32:35 UTC
Last Updated: 2010-08-09 17:27:15 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
In postpone.c, function mutt_num_postponed, the old {{{OldPostponed}}} is never freed. Patch attached.

--------------------------------------------------------------------------------
2010-08-09 13:33:04 UTC vinc17
* Added attachment mutt.patch
* Added comment:
patch adding a FREE

--------------------------------------------------------------------------------
2010-08-09 17:27:15 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
(In [e86ee9991dc3]) In postpone.c, function mutt_num_postponed, the old {{{OldPostponed}}} is never
freed.

closes #3442

* resolution changed to fixed
* status changed to closed
