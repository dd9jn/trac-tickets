Ticket:  2913
Status:  closed
Summary: no locale for gpgme context set

Reporter: LeSpocky
Owner:    mutt-dev

Opened:       2007-06-17 23:10:14 UTC
Last Updated: 2008-07-02 01:30:08 UTC

Priority:  minor
Component: crypto
Keywords:  gpgme locale

--------------------------------------------------------------------------------
Description:
If mutt crypto backend is GPGME passphrase input is handled by external tools. AFAIK gpg-agent is invoked which calls pinentry. On this way a locale setting has to be handed over. As long as mutt is concerned, the locale setting of the console mutt was started at has to be handed over to GPGME. Documentation of GPGME says:

 File: gpgme.info,  Node: Locale,  Prev: Progress Meter Callback,  Up: Context 
 Attributes

 7.3.9 Locale
 ------------

 A locale setting can be associated with a context.  This locale is
 passed to the crypto engine, and used for applications like the PIN
 entry, which is displayed to the user when entering a passphrase is
 required.

   The default locale is used to initialize the locale setting of all
   contexts created afterwards.

 -- Function: gpgme_error_t gpgme_set_locale (gpgme_ctx_t CTX, int CATEGORY, const char *VALUE)
     The function `gpgme_set_locale' sets the locale of the context
     CTX, or the default locale if CTX is a null pointer.

     The locale settings that should be changed are specified by
     CATEGORY.  Supported categories are `LC_CTYPE', `LC_MESSAGES', and
     `LC_ALL', which is a wildcard you can use if you want to change
     all the categories at once.

     The value to be used for the locale setting is VALUE, which will
     be copied to GPGME's internal data structures.  VALUE can be a
     null pointer, which disables setting the locale, and will make PIN
     entry and other applications use their default setting, which is
     usually not what you want.

     Note that the settings are only used if the application runs on a
     text terminal, and that the settings should fit the configuration
     of the output terminal.  Normally, it is sufficient to initialize
     the default value at startup.

     The function returns an error if not enough memory is available.

I assume it would be the easiest to pass the locale directly after each (of the 4) invocation of ''gpgme_new'' in file ''crypt-gpgme.c''.

--------------------------------------------------------------------------------
2008-07-02 01:30:08 UTC brendan
* Added comment:
(In [f9af0501d2dd]) Set GPGME locale on first attempt to create context.
Closes #2913.

* resolution changed to fixed
* status changed to closed
