Ticket:  2039
Status:  closed
Summary: doc: hide_top_* mismatch?

Reporter: ttakah@lapis.plala.or.jp
Owner:    mutt-dev

Opened:       2005-08-16 07:49:48 UTC
Last Updated: 2005-09-04 21:10:40 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
init.h reads;

>   { "hide_top_limited",	DT_BOOL, R_TREE|R_INDEX, OPTHIDETOPLIMITED, 0 },
>   /*
>   ** .pp
>   ** When set, mutt will not show the presence of messages that are hidden
>   ** by limiting, at the top of threads in the thread tree.  Note that when
>   ** $$hide_missing is set, this option will have no effect.
              ^^^^^^^
>   */
>   { "hide_top_missing",	DT_BOOL, R_TREE|R_INDEX, OPTHIDETOPMISSING, 1 },
>   /*
>   ** .pp
>   ** When set, mutt will not show the presence of missing messages at the
>   ** top of threads in the thread tree.  Note that when $$hide_limited is
                                                                 ^^^^^^^
>   ** set, this option will have no effect.
>   */

I suppose $hide_missing has nothing to do with $hide_top_limited.
Doc-bug?

ref: http://marc.theaimsgroup.com/?l=mutt-dev&m=101787551622100&w=2

>How-To-Repeat:
>Fix:
fix init.h

}}}

--------------------------------------------------------------------------------
2005-09-05 15:10:40 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
