Ticket:  2127
Status:  closed
Summary: mutt

Reporter: clock@twibright.com
Owner:    mutt-dev

Opened:       2005-11-01 14:06:50 UTC
Last Updated: 2005-11-01 15:09:14 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
When I try to reply to the e-mail in original mailbox, mutt segfaults with this backtrace:
(gdb) bt full
#0  0x080ae830 in ?? ()
No symbol table info available.

When I save the e-mail to isolate the problem and run mutt on the saved e-mail, it doesn't crash anymore. I won't send you the original mailbox as there is confidential information. 
>How-To-Repeat:
No way
>Fix:
Read the source code of mutt and check it's written correctly
}}}

--------------------------------------------------------------------------------
2005-11-02 09:09:14 UTC cb
* Added comment:
{{{
This PR is nothing but insulting. Closed. HAND.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:06 UTC 
* Added attachment segfault
* Added comment:
segfault
