Ticket:  3155
Status:  closed
Summary: tunnel drops, mutt won't quit

Reporter: agriffis
Owner:    brendan

Opened:       2009-01-23 13:58:21 UTC
Last Updated: 2009-06-29 01:36:24 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
This is a regression from 1.5.18 where mutt would always notice that the tunnel had dropped and close the mailbox.

I'm seeing in 1.5.19 that my tunnel can drop, i.e. pre-authenticated imapd running over ssh, and mutt gets into a state where it allows navigation through the index, and will allow me to view messages that are contained in the message cache.  `<quit>` doesn't work (completely ignored!) and neither does `<change-folder>^` (same behavior).

The way to make mutt notice that the tunnel has dropped is to attempt to view a message that's not in the message-cache.  At that point it suddenly reports "Mailbox closed" and then I'm able to `<change-folder>^` to re-open the connection.

I'll try to get a debug trace of this.

--------------------------------------------------------------------------------
2009-01-23 14:00:18 UTC agriffis
* Added comment:
In both those superscript instances in the description, it was supposed to be:
{{{
<change-folder>^
}}}
i.e. change to the current folder

--------------------------------------------------------------------------------
2009-01-26 14:30:59 UTC brendan
* component changed to IMAP
* milestone changed to 1.6
* owner changed to brendan

--------------------------------------------------------------------------------
2009-06-07 17:27:16 UTC brendan
* Updated description:
This is a regression from 1.5.18 where mutt would always notice that the tunnel had dropped and close the mailbox.

I'm seeing in 1.5.19 that my tunnel can drop, i.e. pre-authenticated imapd running over ssh, and mutt gets into a state where it allows navigation through the index, and will allow me to view messages that are contained in the message cache.  `<quit>` doesn't work (completely ignored!) and neither does `<change-folder>^` (same behavior).

The way to make mutt notice that the tunnel has dropped is to attempt to view a message that's not in the message-cache.  At that point it suddenly reports "Mailbox closed" and then I'm able to `<change-folder>^` to re-open the connection.

I'll try to get a debug trace of this.
* status changed to accepted

--------------------------------------------------------------------------------
2009-06-10 01:50:35 UTC brendan
* Added comment:
I couldn't reproduce this with tip, running a preauth ssh tunnel to a dovecot server.

* status changed to infoneeded

--------------------------------------------------------------------------------
2009-06-18 17:28:50 UTC agriffis
* Added comment:
I've been able to reproduce this but I'm not sure how yet. If I can figure out a sequence I'll update this bug, otherwise I'll close it in a couple days.

--------------------------------------------------------------------------------
2009-06-18 19:24:07 UTC agriffis
* Added comment:
I'm able to reproduce this with the following sequence:

 1. Put in .ssh/config to enable multiplexed ssh connections:

{{{
Host *
ControlPath ~/.ssh/%r@%h:%p-mux
}}}

 2. Start a master connection to your server:

{{{
$ ssh -fNM server
}}}

 3. Start mutt which should use the master connection for the tunnel. (Make sure the hostname in your muttrc is exactly the same as what you used to start the master connection in step 2)

 4. In another term, kill the master connection. This can happen, for example, if the network connection bounces, but we can do it manually with:

{{{
$ ssh -O exit server
}}}

 5. I'm not sure how long you have to wait, but I find that if I use mutt immediately, it recognizes the connection has gone and behaves accordingly.  If I wait for some period of time, it will still announce the connection is gone "Tunnel to mail.hp.com returned error 255 ()" but then it gets into this strange state.

 6. I'll attach a debug log.  You can see at the end of the log lots of "Draining IMAP command pipeline".  This is what happens when I try to quit, among other actions.

* status changed to assigned

--------------------------------------------------------------------------------
2009-06-18 19:55:32 UTC agriffis
* Added attachment .muttdebug0
* Added comment:
-d3 trace (sanitized and annotated)

--------------------------------------------------------------------------------
2009-06-29 00:29:08 UTC brendan
* Added comment:
I've reproduced your environment and a similar muttdebug, but mutt still quits when I press 'q', and lets me change folder to ^. I can confirm that it doesn't close the mailbox as a result of the imap_keepalive NOOP failing (possibly this is bad), and that you can still read cached messages (which is reasonable, since doing so doesn't require issuing any network commands).

* Updated description:
This is a regression from 1.5.18 where mutt would always notice that the tunnel had dropped and close the mailbox.

I'm seeing in 1.5.19 that my tunnel can drop, i.e. pre-authenticated imapd running over ssh, and mutt gets into a state where it allows navigation through the index, and will allow me to view messages that are contained in the message cache.  `<quit>` doesn't work (completely ignored!) and neither does `<change-folder>^` (same behavior).

The way to make mutt notice that the tunnel has dropped is to attempt to view a message that's not in the message-cache.  At that point it suddenly reports "Mailbox closed" and then I'm able to `<change-folder>^` to re-open the connection.

I'll try to get a debug trace of this.

--------------------------------------------------------------------------------
2009-06-29 01:36:24 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [fe30f394cbe6]) Not being able to start an IMAP command is a fatal error.
Closes #3155.

* resolution changed to fixed
* status changed to closed
