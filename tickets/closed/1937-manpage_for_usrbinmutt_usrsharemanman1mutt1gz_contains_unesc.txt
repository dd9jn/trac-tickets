Ticket:  1937
Status:  closed
Summary: manpage for /usr/bin/mutt (/usr/share/man/man1/mutt.1.gz) contains unescaped hyphens

Reporter: Marco d'Itri <md@linux.it>
Owner:    pdmef

Opened:       2004-11-04 17:58:18 UTC
Last Updated: 2009-06-15 03:53:30 UTC

Priority:  minor
Component: doc
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6-20040722+1
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#279591.
Please Cc all your replies to 279591@bugs.debian.org .]

From: Joseph Nahmias <joe@nahmias.net>
Subject: manpage for /usr/bin/mutt (/usr/share/man/man1/mutt.1.gz) contains unescaped hyphens
Date: Wed,  3 Nov 2004 23:32:08 +0000 (UTC)

The manpage '/usr/share/man/man1/mutt.1.gz' contains unescaped hyphens
which causes groff to render them as hyphens (U+2010) rather than
regular ASCII dashes (0x2D).

This causes problems in a number of cases: for example when someone
wants to cut-and-paste from a manpage -- since programs do not interpret
the unicode hyphen as a dash.  Also, most pagers differentiate between
the two characters, making searching for text that includes a dash
difficult.

Please change all instances of the '-' character in the manpage to '\-'
to fix this.  The following command should highlight most of the
problematic instances:

    zgrep -Hn [^\\]- /usr/share/man/man1/mutt.1.gz | egrep -v '[0-9]+:\.\\"'

Thanks,
Joe Nahmias


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-11-04 17:58:18 UTC Marco d'Itri <md@linux.it>
* Added comment:
{{{
Package: mutt
Version: 1.5.6-20040722+1
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#279591.
Please Cc all your replies to 279591@bugs.debian.org .]

From: Joseph Nahmias <joe@nahmias.net>
Subject: manpage for /usr/bin/mutt (/usr/share/man/man1/mutt.1.gz) contains unescaped hyphens
Date: Wed,  3 Nov 2004 23:32:08 +0000 (UTC)

The manpage '/usr/share/man/man1/mutt.1.gz' contains unescaped hyphens
which causes groff to render them as hyphens (U+2010) rather than
regular ASCII dashes (0x2D).

This causes problems in a number of cases: for example when someone
wants to cut-and-paste from a manpage -- since programs do not interpret
the unicode hyphen as a dash.  Also, most pagers differentiate between
the two characters, making searching for text that includes a dash
difficult.

Please change all instances of the '-' character in the manpage to '\-'
to fix this.  The following command should highlight most of the
problematic instances:

   zgrep -Hn [^\\]- /usr/share/man/man1/mutt.1.gz | egrep -v '[0-9]+:\.\\"'

Thanks,
Joe Nahmias
}}}

--------------------------------------------------------------------------------
2004-12-20 15:07:09 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
On Sun, Dec 19, 2004 at 01:45:59PM +0900, TAKAHASHI Tamotsu wrote:
> #!/bin/sh
> 
> cat <<EOF> fixhyphen.sed
> s/-/\\-/g
> # revert title strings
> s/\\\\-/-/g
> # revert comment strings
> /^\.\\\"/s/\\-/-/g
> EOF
> 
> for i in \
>   doc/dotlock.man \
>   doc/mbox.man \
>   doc/mutt.man \
>   doc/muttbug.man \
>   doc/muttrc.man.head \
>   doc/muttrc.man.tail
> do
>   sed -f fixhyphen.sed $i

sed -f fixhyphen.sed $i > $i.new
mv $i.new $i

> done

-- 
tamo
}}}

--------------------------------------------------------------------------------
2005-08-14 19:49:25 UTC Debian Listmaster <listmaster@lists.debian.org>
* Added comment:
{{{
Unfortunately it's currently impossible to use the Debian mailing lists with
a e-mail address that matches procmail's check for mail coming from a daemon.

This means that a username may not have "mail", "admin", "root", "master",
"list" or similar strings in it (see procmailrc(5) for "FROM_DAEMON").

We apologize for the inconvenience and ask you to consider using some other
username.

Below please find all headers of your mail:
-- 
> From listmaster@murphy.debian.org  Sun Aug 14 09:49:25 2005
> Return-Path: <listmaster@murphy.debian.org>
> X-Original-To: listmaster@lists.debian.org
> Received: by murphy.debian.org (Postfix, from userid 38)
> 	id CA7142ECF5; Sun, 14 Aug 2005 09:49:25 -0500 (CDT)
> X-From_:debbugs@bugs.debian.org  Sun Aug 14 09:49:25 2005
> X-Original-To: debian-bugs-dist@lists.debian.org
> Received: from spohr.debian.org (spohr.debian.org [140.211.166.43])
> 	by murphy.debian.org (Postfix) with ESMTP id A569C2DE92;
> 	Sun, 14 Aug 2005 09:49:25 -0500 (CDT)
> Received: from debbugs by spohr.debian.org with local (Exim 3.36 1 (Debian))
> 	id 1E4JmY-0001t0-00; Sun, 14 Aug 2005 07:48:14 -0700
> X-Loop: owner@bugs.debian.org
> Subject: Bug#279591: mutt/1937: manpage for /usr/bin/mutt (/usr/share/man/man1/mutt.1.gz) contains unescaped hyphens
> Reply-To: bug-any@bugs.mutt.org, 279591@bugs.debian.org
> Resent-From: Christoph Berg <cb@df7cb.de>
> Original-Sender: Gnats Bug-Reporting System (admin) <gnats@trithemius.gnupg.org>
> Resent-To: debian-bugs-dist@lists.debian.org
> Resent-Cc: Marco d'Itri <md@linux.it>
> Resent-Date: Sun, 14 Aug 2005 14:48:14 UTC
> Resent-Message-ID: <handler.279591.B279591.112403053931000@bugs.debian.org>
> X-Debian-PR-Message: report 279591
> X-Debian-PR-Package: mutt
> X-Debian-PR-Keywords: upstream
> Received: via spool by 279591-submit@bugs.debian.org id=B279591.112403053931000
>           (code B ref 279591); Sun, 14 Aug 2005 14:48:14 UTC
> Received: (at 279591) by bugs.debian.org; 14 Aug 2005 14:42:19 +0000
> Received: from trithemius.gnupg.org [217.69.76.44] 
> 	by spohr.debian.org with esmtp (Exim 3.36 1 (Debian))
> 	id 1E4Jgp-00083r-00; Sun, 14 Aug 2005 07:42:19 -0700
> Received: from gnats by trithemius.gnupg.org with local (Exim 4.50 #1 (Debian))
> 	id 1E4JbX-0007lF-II; Sun, 14 Aug 2005 16:36:51 +0200
> From: Christoph Berg <cb@df7cb.de>
> To: Mutt Developers <mutt-dev@mutt.org>,
> 	Christoph Berg <cb@df7cb.de>, Marco d'Itri <md@linux.it>,
> 	279591@bugs.debian.org
> X-Mutt-PR: mutt/1937
}}}

--------------------------------------------------------------------------------
2005-08-15 09:36:51 UTC cb
* Added comment:
{{{
Testing the forwaring to nnn@bugs.d.o. (please ignore)
}}}

--------------------------------------------------------------------------------
2005-09-05 14:32:30 UTC brendan
* Added comment:
{{{
Unfortunately it's currently impossible to use the Debian mailing lists with
 Refiled as change-request.
}}}

--------------------------------------------------------------------------------
2009-06-06 22:30:33 UTC pdmef
* component changed to doc
* Updated description:
{{{
Package: mutt
Version: 1.5.6-20040722+1
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#279591.
Please Cc all your replies to 279591@bugs.debian.org .]

From: Joseph Nahmias <joe@nahmias.net>
Subject: manpage for /usr/bin/mutt (/usr/share/man/man1/mutt.1.gz) contains unescaped hyphens
Date: Wed,  3 Nov 2004 23:32:08 +0000 (UTC)

The manpage '/usr/share/man/man1/mutt.1.gz' contains unescaped hyphens
which causes groff to render them as hyphens (U+2010) rather than
regular ASCII dashes (0x2D).

This causes problems in a number of cases: for example when someone
wants to cut-and-paste from a manpage -- since programs do not interpret
the unicode hyphen as a dash.  Also, most pagers differentiate between
the two characters, making searching for text that includes a dash
difficult.

Please change all instances of the '-' character in the manpage to '\-'
to fix this.  The following command should highlight most of the
problematic instances:

    zgrep -Hn [^\\]- /usr/share/man/man1/mutt.1.gz | egrep -v '[0-9]+:\.\\"'

Thanks,
Joe Nahmias


>How-To-Repeat:
>Fix:
}}}
* owner changed to pdmef

--------------------------------------------------------------------------------
2009-06-06 22:48:30 UTC antonio@dyne.org
* Added comment:
Hi,
this bug was fixed on our side with the following patch:
http://git.debian.org/?p=pkg-mutt/mutt.git;a=blob;f=debian/patches/misc/hyphen-as-minus.patch;h=e8310ad5d691fc11ee97a251b8ca1b05398603c5;hb=HEAD

Cheers
Antonio

--------------------------------------------------------------------------------
2009-06-07 00:21:56 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [a786b0e8627c]) Fix hyphens in manpages. Closes #1937

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2009-06-14 22:52:23 UTC antonio@dyne.org
* Added comment:
still 4 errors, probably due to options added in the meantime, the attached patch fixes them.

* resolution changed to 
* status changed to new

--------------------------------------------------------------------------------
2009-06-14 22:54:32 UTC antonio@dyne.org
* Added attachment hyphen-as-minus.2.patch

--------------------------------------------------------------------------------
2009-06-14 22:54:39 UTC antonio@dyne.org
* Added attachment hyphen-as-minus.patch

--------------------------------------------------------------------------------
2009-06-15 03:53:30 UTC Antonio Radici <antonio@dyne.org>
* Added comment:
(In [fef17099376d]) Escape hyphens in man page. Closes #1937 again.

* resolution changed to fixed
* status changed to closed
