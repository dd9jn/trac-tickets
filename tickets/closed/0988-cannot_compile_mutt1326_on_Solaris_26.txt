Ticket:  988
Status:  closed
Summary: cannot compile mutt-1.3.26 on Solaris 2.6

Reporter: Ulli Horlacher <Ulli.Horlacher@RUS.Uni-Stuttgart.DE>
Owner:    mutt-dev

Opened:       2002-01-19 10:00:42 UTC
Last Updated: 2005-07-24 22:54:14 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.26i
Severity: normal

-- Please type your report below this line

mutt-1.3.26 make fails on Solaris 2.6 with:

bofh:/sw/src/mutt-1.3.26: make
(...)
make[2]: Entering directory `/opt/local/src/sw/mutt-1.3.26'
gcc  -Wall -pedantic -O2 -I/client/include -L/client/lib -L/client/lib  -L/client/lib -o mutt  patchlist.o addrbook.o alias.o attach.o base64.o browser.o buffy.o color.o commands.o complete.o compose.o copy.o curs_lib.o curs_main.o date.o edit.o enter.o flags.o init.o filter.o from.o getdomain.o handler.o hash.o hdrline.o headers.o help.o hook.o keymap.o main.o mbox.o menu.o mh.o mx.o pager.o parse.o pattern.o postpone.o query.o recvattach.o recvcmd.o rfc822.o rfc1524.o rfc2047.o rfc2231.o score.o send.o sendlib.o signal.o sort.o status.o system.o thread.o charset.o history.o lib.o muttlib.o editmsg.o utf8.o mbyte.o wcwidth.o url.o ascii.o pgp.o pgpinvoke.o pgpkey.o pgplib.o gnupgparse.o pgpmicalg.o pgppacket.o resize.o dotlock.o  regex.o  -lncurses         ./intl/libintl.a -liconv -liconv -L/client/lib
Undefined                       first referenced
 symbol                             in file
btowc                               regex.o
ld: fatal: Symbol referencing errors. No output written to mutt
collect2: ld returned 1 exit status



-- Build environment information

- gcc version information
gcc
Reading specs from /sw/sun4_56/gcc-2.95.2/lib/gcc-lib/sparc-sun-solaris2.6/2.95.2/specs
gcc version 2.95.2 19991024 (release)

- CFLAGS
-Wall -pedantic -O2 -I/client/include -L/client/lib

-- Mutt Version Information
mutt-1.3.26i
CC=gcc CFLAGS=-O2 ./configure --prefix=/sw/sun4_56/mutt-1.3.26 --with-regex --with-libiconv-prefix=/client --with-curses=/client

-- Operating System information
SunOS bofh.BelWue.DE 5.6 Generic_105181-28 sun4u sparc SUNW,Ultra-5_10



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-01-19 08:05:11 UTC Lars Hecking <lhecking@nmrc.ie>
* Added comment:
{{{
> mutt-1.3.26 make fails on Solaris 2.6 with:
[...] 
> Undefined                       first referenced
>  symbol                             in file
> btowc                               regex.o
[...]

See http://marc.theaimsgroup.com/?l=mutt-dev&m=101127054616804&w=2 .
}}}

--------------------------------------------------------------------------------
2005-07-25 16:54:14 UTC brendan
* Added comment:
{{{
Obsolete version.
}}}

* resolution changed to fixed
* status changed to closed
