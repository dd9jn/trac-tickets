Ticket:  2037
Status:  closed
Summary: Makefile problem with Sun's make(1)

Reporter: jeff@cjsa.com
Owner:    mutt-dev

Opened:       2005-08-13 05:45:08 UTC
Last Updated: 2005-08-13 14:58:08 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
The Makefile.in in the doc subdirectory contains the following entry:

    mutt.1: $(srcdir)/mutt.man
            $(EDIT) $< > $@

This works gine with gmake(1) but fails using Sun's make(1).  The "$<" resolvs to the empty string using make but is  replaced by "mutt.man" using gmake.
>How-To-Repeat:
To check, try this in a Makefile:

    testx:
            @echo :$<:$?:

% make testx
>Fix:
The solution is to use "$?" which properly resolves to "mutt.man" with both versions of make.
}}}

--------------------------------------------------------------------------------
2005-08-14 08:58:08 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
