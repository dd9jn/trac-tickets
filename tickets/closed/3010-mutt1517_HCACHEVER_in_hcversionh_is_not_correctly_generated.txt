Ticket:  3010
Status:  closed
Summary: mutt-1.5.17: HCACHEVER in hcversion.h is not correctly generated

Reporter: Olaf Föllinger <olaf.foellinger@ivu.de>
Owner:    mutt-dev

Opened:       2008-01-09 10:13:57 UTC
Last Updated: 2008-02-27 12:34:31 UTC

Priority:  major
Component: build
Keywords:  cygwin

--------------------------------------------------------------------------------
Description:


{{{
Package: mutt
Version: 1.5.17
Severity: important

-- Please type your report below this line
Hi,

the file hcversion.h is not correctly generated using cygwin/windows xp. It contains only

#define HCACHEVER 0x

after the generation.

Regards Olaf


-- System Information
System Version: CYGWIN_NT-5.1 FOE-NB 1.5.25(0.156/4/2) 2007-12-14 19:21 i686 Cygwin

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc/i686-pc-cygwin/3.4.4/specs
Configured with: /usr/build/package/orig/test.respin/gcc-3.4.4-3/configure --verbose --prefix=/usr --exec-prefix=/usr --sysconfdir=/etc --libdir=/usr/lib --libexecdir=/usr/lib --mandir=/usr/share/man --infodir=/usr/share/info --enable-languages=c,ada,c++,d,f77,pascal,java,objc --enable-nls --without-included-gettext --enable-version-specific-runtime-libs --without-x --enable-libgcj --disable-java-awt --with-system-zlib --enable-interpreter --disable-libgcj-debug --enable-threads=posix --enable-java-gc=boehm --disable-win32-registry --enable-sjlj-exceptions --enable-hash-synchronization --enable-libstdcxx-debug
Thread model: posix
gcc version 3.4.4 (cygming special, gdc 0.12, using dmd 0.125)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information
Package: mutt
Version: 1.5.17
Severity: important

-- Please type your report below this line
Hi,

the file hcversion.h is not correctly generated using cygwin/windows xp. It contains only

#define HCACHEVER 0x

in the last line after the generation (the comments have been omitted).

Regards Olaf


-- System Information
System Version: CYGWIN_NT-5.1 FOE-NB 1.5.25(0.156/4/2) 2007-12-14 19:21 i686 Cygwin

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc/i686-pc-cygwin/3.4.4/specs
Configured with: /usr/build/package/orig/test.respin/gcc-3.4.4-3/configure --verbose --prefix=/usr --exec-prefix=/usr --sysconfdir=/etc --libdir=/usr/lib --libexecdir=/usr/lib --mandir=/usr/share/man --infodir=/usr/share/info --enable-languages=c,ada,c++,d,f77,pascal,java,objc --enable-nls --without-included-gettext --enable-version-specific-runtime-libs --without-x --enable-libgcj --disable-java-awt --with-system-zlib --enable-interpreter --disable-libgcj-debug --enable-threads=posix --enable-java-gc=boehm --disable-win32-registry --enable-sjlj-exceptions --enable-hash-synchronization --enable-libstdcxx-debug
Thread model: posix
gcc version 3.4.4 (cygming special, gdc 0.12, using dmd 0.125)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.17 (2007-11-01)
Copyright (C) 1996-2007 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: CYGWIN_NT-5.1 1.5.25(0.156/4/2) (i686)
ncurses: ncurses 5.5.20061104 (compiled with 5.5)
libiconv: 1.11
libidn: 1.2 (compiled with 1.2)
hcache backend: Sleepycat Software: Berkeley DB 4.2.52: (December 17, 2007)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE
+USE_FCNTL  -USE_FLOCK   -USE_INODESORT
+USE_POP  +USE_IMAP  +USE_SMTP  -USE_GSS  -USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  +LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.


Gruß Olaf Föllinger
}}}

--------------------------------------------------------------------------------
2008-01-09 12:14:39 UTC pdmef
* Added comment:
Replying to [ticket:3010 Olaf Föllinger <olaf.foellinger@ivu.de>]:

> the file hcversion.h is not correctly generated using cygwin/windows xp. It contains only
> 
> #define HCACHEVER 0x
> 
> after the generation.
 
I suppose you don't have any of these commands on cygwin: md5, md5sum, openssl?

Since mutt ships with md5 code anyway, a sane fix could be to first build a local
md5sum tool and use that one in hcachever.sh.

Rocco

* component changed to build
* keywords changed to cygwin
* milestone changed to 1.6
* summary changed to mutt-1.5.17: HCACHEVER in hcversion.h is not correctly generated
* version changed to 1.5.17

--------------------------------------------------------------------------------
2008-01-09 12:33:08 UTC Olaf Föllinger
* Added comment:
{{{
Hi,

thanks for the fast reaction.

* Mutt <fleas@mutt.org> [2008-01-09 12:14:45]:

> #3010: mutt-1.5.17: HCACHEVER in hcversion.h is not correctly generated
> 
> Changes (by pdmef):
> 
>   * keywords:  => cygwin
>   * summary:  mutt-1.5.17: HCACHEVER in hcversion.h is not correctly
>               generated
>         in cygwin/windows xp => mutt-1.5.17: HCACHEVER in
>               hcversion.h is not correctly generated
>   * component:  mutt => build
>   * version:  => 1.5.17
>   * milestone:  => 1.6
> 
> Comment:
> 
>  Replying to [ticket:3010 Olaf Föllinger <olaf.foellinger@ivu.de>]:
> 
>  > the file hcversion.h is not correctly generated using cygwin/windows xp.
>  It contains only
>  >
>  > #define HCACHEVER 0x
>  >
>  > after the generation.
> 
>  I suppose you don't have any of these commands on cygwin: md5, md5sum,
>  openssl?

md5sum is available on cygwin so this is not the problem.

>  Since mutt ships with md5 code anyway, a sane fix could be to first build
>  a local
>  md5sum tool and use that one in hcachever.sh.

Maybe this helps but it shouldn't be necessary.


Gruß Olaf Föllinger

}}}

--------------------------------------------------------------------------------
2008-01-09 13:34:57 UTC pdmef
* Added comment:

> >  I suppose you don't have any of these commands on cygwin: md5, md5sum,
> >  openssl?
> 
> md5sum is available on cygwin so this is not the problem.

Here too. But then configure should find it and set $MD5 in hcachver.sh correctly.

The contents of that file is expected (I guess) to be like this when you don't build with
header caching as then the header hcversion.h isn't used. But once you build with
hcache this shouldn't happen.

This does happen when you try to build with --enable-hcache?

If yes, it would be nice if you could search config.log for 'md5' and verify that hcachever.sh
contains only 'MD5='.

Rocco

--------------------------------------------------------------------------------
2008-01-09 14:15:10 UTC Olaf Föllinger
* Added comment:
{{{
* Mutt <fleas@mutt.org> [2008-01-09 13:34:58]:

> #3010: mutt-1.5.17: HCACHEVER in hcversion.h is not correctly generated
> 
> Comment (by pdmef):
> 
>  > >  I suppose you don't have any of these commands on cygwin: md5,
>  md5sum,
>  > >  openssl?
>  >
>  > md5sum is available on cygwin so this is not the problem.
> 
>  Here too. But then configure should find it and set $MD5 in hcachver.sh
>  correctly.
> 
>  The contents of that file is expected (I guess) to be like this when you
>  don't build with
>  header caching as then the header hcversion.h isn't used. But once you
>  build with
>  hcache this shouldn't happen.
> 
>  This does happen when you try to build with --enable-hcache?

Yes. It did. Now it has vanished. The problem can be set to "non
reproducible". Sorry for bothering you.

Gruß Olaf Föllinger
}}}

--------------------------------------------------------------------------------
2008-02-27 12:34:31 UTC pdmef
* resolution changed to invalid
* status changed to closed
