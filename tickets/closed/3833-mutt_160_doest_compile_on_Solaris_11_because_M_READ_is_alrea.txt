Ticket:  3833
Status:  closed
Summary: mutt 1.6.0 does't compile on Solaris 11 because M_READ is already defined in /usr/include/sys/stream.h

Reporter: mutti
Owner:    mutt-dev

Opened:       2016-04-23 09:14:25 UTC
Last Updated: 2016-04-27 20:22:18 UTC

Priority:  minor
Component: mutt
Keywords:  solaris

--------------------------------------------------------------------------------
Description:
Hi,

mutt 1.6.0 doesn't compile on Solaris 11

{{{
/opt/solarisstudio12.4/bin/cc -DPKGDATADIR=\"/usr/share/mutt\" -DSYSCONFDIR=\"/etc/mutt\" -DBINDIR=\"/usr/bin\" -DMUTTLOCALEDIR=\"/usr/share/locale\" -DHAVE_CONFIG_H=1 -I. -I/tmp/mutt-1.6.0  -I. -I/tmp/mutt-1.6.0 -I/tmp/mutt-1.6.0/imap  -Iintl -I/tmp/mutt-1.6.0/intl  -m32 -xO4 -xchip=pentium -xregs=no%frameptr    -mt -I/usr/include/idn -c -o getdomain.o /tmp/mutt/mutt-1.6.0/getdomain.c
"/tmp/mutt-1.6.0/mutt.h", line 86: warning: macro redefined: M_CMD
"/tmp/mutt-1.6.0/mutt.h", line 180: syntax error before or at: 0x85
cc: acomp failed for /tmp/mutt-1.6.0/getdomain.c
}}}

because M_READ is already defined in `/usr/include/sys/stream.h`:

{{{
$ grep M_READ /usr/include/sys/stream.h
#define M_READ          0x85            /* generate read notification */
}}}

I guess the easiest fix would be to rename `M_READ` in `mutt.h`

--------------------------------------------------------------------------------
2016-04-23 10:55:27 UTC mutti
* Added comment:
Same with `M_CMD`:

{{{
$ grep M_CMD /usr/include/sys/stream.h 
#define M_CMD           0x93            /* out-of-band ioctl command */
}}}

--------------------------------------------------------------------------------
2016-04-23 14:28:07 UTC grobian
* Added comment:
FWIW, I used this sed to work around it without any problems:


{{{
 sed -i \
        -e 's/\<M_CMD\>/MT_CMD/g' \
        -e 's/\<M_READ\>/MT_READ/g' \
        *.[ch] imap/*.[ch]
}}}

--------------------------------------------------------------------------------
2016-04-23 17:11:35 UTC kevin8t8
* Added comment:
I believe OpenSolaris (and OpenIndiana, etc), are in the wrong here.  "internal" macros, and especially those indirectly included, should be prefixed with "_" or double-underscore unless they are reserved.  

https://www.gnu.org/software/libc/manual/html_node/Reserved-Names.html has a nice write-up about this.

It looks like a bug was opened at https://www.illumos.org/issues/6856 about this, and in my opinion it should be fixed properly in the OS.

Now, my ranting done, I realize that likely isn't going to happen with OpenSolaris.  :-)

Vincent, you mentioned renaming these flags would break patches.  Do you have an idea of how bad the breakage would be.  Do any contributors/devs have other suggestions about a _clean_ way to fix this?

--------------------------------------------------------------------------------
2016-04-25 22:33:49 UTC kevin8t8
* Added comment:
Attaching a work around patch.  It's a bit intrusive, but at least it's clean.  If there are no issues with it, I'll push and include in 1.6.1.

--------------------------------------------------------------------------------
2016-04-25 22:34:19 UTC kevin8t8
* Added attachment ticket-3833.patch

--------------------------------------------------------------------------------
2016-04-26 06:10:52 UTC marcusk
* _comment0 changed to 1461651073832094
* Added comment:
This is also an issue on Solaris 10. The patch works though.

--------------------------------------------------------------------------------
2016-04-27 20:22:18 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"23334e967dd7bbe283c894eb6f840524c72bd7df"]:
{{{
#!CommitTicketReference repository="" revision="23334e967dd7bbe283c894eb6f840524c72bd7df"
Create a wrapper sys_socket.h to work around Solaris namespace issues.  (closes #3833)

Solaris includes "sys/stream.h" inside their "sys/socket.h".  This
include file adds many non-reserved macros to Mutt's namespace, two of
which conflict with existing Mutt macros.

The simplest fix would be to rename those macros in Mutt, however this
will cause difficulty with out-of-tree patches.  This fix creates a
wrapper include file that preserves those existing macros and prevents
the Solaris values from entering Mutt's namespace.
}}}

* resolution changed to fixed
* status changed to closed
