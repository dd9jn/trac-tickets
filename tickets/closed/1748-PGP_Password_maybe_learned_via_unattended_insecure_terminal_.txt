Ticket:  1748
Status:  closed
Summary: PGP Password maybe learned via unattended insecure terminal by counting charaters until beep

Reporter: brettcar@segvio.org
Owner:    mutt-dev

Opened:       2004-01-05 12:15:31 UTC
Last Updated: 2006-03-27 00:24:53 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4+20031024
Severity: important

-- Please type your report below this line
There is a social engineering vulnerability in mutt's PGP password entry field. If a user types in their PGP password but does not hit return the following is possible:
1) Other user approaches mutt (say, via an insecure terminal or the user leaving).
2) Other user deletes character by character, counting.
3) There will be a console beep when no more characters are left to delete.

This seems benign but an intelligent person now knows the length of the user's passphrase. Seeing as this requires an insecure terminal, they can probably get their secret key too. This makes brute-forcing the password much, much easier.

Although this situation is unlikey, there is no reason for it to exist. I acknowledge that the console beep may be the terminal however and out of mutt's scope.

-- System Information
System Version: Linux spoont 2.4.22-1-k6 #5 Sat Oct 4 14:38:05 EST 2003 i586 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.2/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.2 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.22-1-k6 (i586) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.14)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.3.rr.compressed.1
patch-1.5.4.helmersson.incomplete_multibyte
patch-1.5.4.fw.maildir_inode_sort
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
patch-1.3.27.bse.xtitles.1
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.56d


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-01-05 12:15:31 UTC brettcar@segvio.org
* Added comment:
{{{
Package: mutt
Version: 1.5.4+20031024
Severity: important

-- Please type your report below this line
There is a social engineering vulnerability in mutt's PGP password entry field. If a user types in their PGP password but does not hit return the following is possible:
1) Other user approaches mutt (say, via an insecure terminal or the user leaving).
2) Other user deletes character by character, counting.
3) There will be a console beep when no more characters are left to delete.

This seems benign but an intelligent person now knows the length of the user's passphrase. Seeing as this requires an insecure terminal, they can probably get their secret key too. This makes brute-forcing the password much, much easier.

Although this situation is unlikey, there is no reason for it to exist. I acknowledge that the console beep may be the terminal however and out of mutt's scope.

-- System Information
System Version: Linux spoont 2.4.22-1-k6 #5 Sat Oct 4 14:38:05 EST 2003 i586 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.2/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.2 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.22-1-k6 (i586) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.14)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.3.rr.compressed.1
patch-1.5.4.helmersson.incomplete_multibyte
patch-1.5.4.fw.maildir_inode_sort
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
patch-1.3.27.bse.xtitles.1
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.56d
}}}

--------------------------------------------------------------------------------
2004-01-06 04:31:46 UTC Edmund GRIMLEY EVANS <edmundo@rano.org>
* Added comment:
{{{
brettcar@segvio.org <brettcar@segvio.org>:

> This seems benign but an intelligent person now knows the length of
> the user's passphrase. Seeing as this requires an insecure terminal,
> they can probably get their secret key too.

And, by attaching a debugger, they can get the actual passphrase, too,
but the only way I can think of fixing that is by calculating an
irreversible hash as the passphrase is typed, which would presumably
mean disallowing character-by-character delete altogether.
}}}

--------------------------------------------------------------------------------
2004-01-08 02:08:11 UTC David Shaw <dshaw@jabberwocky.com>
* Added comment:
{{{
On Mon, Jan 05, 2004 at 12:31:46PM +0000, Edmund GRIMLEY EVANS wrote:
> brettcar@segvio.org <brettcar@segvio.org>:
> 
> > This seems benign but an intelligent person now knows the length of
> > the user's passphrase. Seeing as this requires an insecure terminal,
> > they can probably get their secret key too.
> 
> And, by attaching a debugger, they can get the actual passphrase, too,
> but the only way I can think of fixing that is by calculating an
> irreversible hash as the passphrase is typed, which would presumably
> mean disallowing character-by-character delete altogether.

If it is irreversible, we'll have a minor problem in passing the
passphrase to GnuPG or PGP... ;)

David
}}}

--------------------------------------------------------------------------------
2004-01-08 03:45:10 UTC David Shaw <dshaw@jabberwocky.com>
* Added comment:
{{{
On Wed, Jan 07, 2004 at 09:22:22PM +0000, Edmund GRIMLEY EVANS wrote:
> David Shaw <dshaw@jabberwocky.com>:
> 
> > > And, by attaching a debugger, they can get the actual passphrase, too,
> > > but the only way I can think of fixing that is by calculating an
> > > irreversible hash as the passphrase is typed, which would presumably
> > > mean disallowing character-by-character delete altogether.
> > 
> > If it is irreversible, we'll have a minor problem in passing the
> > passphrase to GnuPG or PGP... ;)
> 
> I suppose you'd have to arrange for GnuPG or PGP to only ever see the
> hashed passphrase, so that it's what they expect to see.

Then we have the original problem.  If GnuPG accepts a hashed
passphrase from mutt, then a stolen hashed passphrase is as useful to
the thief as the regular passphrase would be.  It doesn't help against
an attacker with a debugger.

> But it wasn't really a serious proposal ...

I know.  I was kidding back.

David
}}}

--------------------------------------------------------------------------------
2004-01-08 13:22:22 UTC Edmund GRIMLEY EVANS <edmundo@rano.org>
* Added comment:
{{{
David Shaw <dshaw@jabberwocky.com>:

> > And, by attaching a debugger, they can get the actual passphrase, too,
> > but the only way I can think of fixing that is by calculating an
> > irreversible hash as the passphrase is typed, which would presumably
> > mean disallowing character-by-character delete altogether.
> 
> If it is irreversible, we'll have a minor problem in passing the
> passphrase to GnuPG or PGP... ;)

I suppose you'd have to arrange for GnuPG or PGP to only ever see the
hashed passphrase, so that it's what they expect to see. But it wasn't
really a serious proposal ...
}}}

--------------------------------------------------------------------------------
2005-08-26 11:28:00 UTC rado
* Added comment:
{{{
verbose subject
change-request,

need to balance between this security threat
and means for legal user to know when buffer is empty to start over.
}}}

--------------------------------------------------------------------------------
2006-03-27 20:24:53 UTC paul
* Added comment:
{{{
Inactive for some time. No particular evidence that this is
a real problem - if you walk off with your PGP passphrase
entered but without hitting enter, you have much greater
problems than mutt beeping when someone's deleted it!

(Also no particularly obvious way to fix.)

If anyone has any violent objections, feel free to re-open.
}}}

* resolution changed to fixed
* status changed to closed
