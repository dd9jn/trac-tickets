Ticket:  2402
Status:  closed
Summary: damaged regexps in folder-hooks

Reporter: Alain Bench <veronatif@free.fr>
Owner:    mutt-dev

Opened:       2006-08-03 06:31:38 UTC
Last Updated: 2015-07-29 16:13:46 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{

Hello ALL,

    In Mutt 1.5.10 and more, the regexp in folder-hooks can
be damaged in some circumstances, leading to false positive
matches. That seems to happen when the regexp both: Has
uppercase characters (is case-significant), and begins by an
'^' anchor.

    Example, in muttrc:

| folder-hook ^ABC$ yaddahyaddah

    ...and:

| $ cd /tmp
| $ touch nomatchABC
| $ mutt -f nomatchABC

    ...beeps and shouts:

| yaddahyaddah: unknown command

     The hook wrongly matched, as if the ^ anchor had been
lost. And Tamo's comval.9 feature confirms, the
<status-commands> function showing the hook as stored
internally:

| Status of commands:
|
| folder-hook
|   ABC$
|     yaddahyaddah

    ...no more anchor. Note this does not happen with Mutt
up to 1.5.9, nor with all-lowercase regexps.


Alain.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2006-08-03 17:34:57 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Thursday, 03 August 2006 at 17:45, Rocco Rutte wrote:
> The following reply was made to PR mutt/2402; it has been noted by GNATS.
> 
> From: Rocco Rutte <pdmef@gmx.net>
> To: bug-any@bugs.mutt.org
> Cc: 
> Subject: Re: mutt/2402: damaged regexps in folder-hooks
> Date: Thu, 3 Aug 2006 15:36:03 +0000
> 
>  Hi,
>  
>  * Alain Bench [06-08-03 17:15:01 +0200] wrote:
>  
>  >     <brainstorming> Is there really a problem outside of "^"? Other
>  > shortcuts are "! > < - !! ~ = + @". All of them expand only when in
>  > first position.
>  
>  Right.
>  
>  >     Otherwise: Why are they expanded if backslashed? They don't need to
>  > (outside of ! because of ! negation)? "^a" could be expanded to
>  > "/tmp/nomatchABCa", but "\^a" to "^a" regexp. Hum... One backslash is
>  > stripped too early, right?
>  
>  Yepp: already when reading/tokenizing the line.
>  
>  >     They are not expanded if double\\ed. One backslash reaches the
>  > regexp engine. This doesn't hurt normal chars, but of course hurts "^".
>  > Grrr...
>  
>  So you tried it, too? :)
>  
>  Another idea: we simply rename '^' to something else and announce that 
>  with big fat warnings. We can continue to support '^' except for when 
>  _mutt_expand_path() get's a regexp. That would add an extra 'case' label 
>  as well as a check for the rx parameter.

I think this is the only sensible thing to do. Let's use '.' instead
:)

I don't think '^' has been in use long enough to warrant
backward-compatibility hacks.
}}}

--------------------------------------------------------------------------------
2006-08-04 01:29:40 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
{{{
Hi,

* Alain Bench [06-08-03 07:31:38 +0200] wrote:

>    In Mutt 1.5.10 and more, the regexp in folder-hooks can
>be damaged in some circumstances, leading to false positive
>matches. That seems to happen when the regexp both: Has
>uppercase characters (is case-significant), and begins by an
>'^' anchor.

I found more cases to not work: case-sensivity doesn't matter (I put 
some debug lines in and always see errors for your samples).

The problem is that mailbox expansion is applied to folder-hooks. I'd 
call this a feature and thus your report is both valid and invalid at 
the same time... :(

Please see: mutt_parse_hook() in hook.c and _mutt_expand_path() in 
muttlib.c. The core problem is that '^' is a shortcut for the current 
folder and thus is subject to expansion and is not treated as a regex 
special character. Basically we need a way to distinct between regex and 
"folder" special characters.

Any ideas?

   bye, Rocco
-- 
:wq!
}}}

--------------------------------------------------------------------------------
2006-08-04 01:52:50 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
{{{
Hi,

* Thomas Roessler [06-08-03 12:45:02 +0200] wrote:

> Eek.  The obvious solution would be another level of \
> escaping, but I don't like that thought at all. I guess I'd
> rather throw out the folder expansion and just stick to regular
> expressions.

I though of that, too, but then I have in my config:

   folder-hook ! '...'
   folder-hook ~/Mail/archive '...'
   folder-hook =IN.mutt-dev '...'

and more.

   bye, Rocco
-- 
:wq!
}}}

--------------------------------------------------------------------------------
2006-08-04 04:16:59 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
{{{
Hi,

* Michael Tatge [06-08-03 15:05:01 +0200] wrote:

> This is an old bug iirc. See #1911

Nope. When parsing:

   folder-hook ^foobar$ ...

mutt correctly extracts ^foobar$ as text. Afterwards, it tries to expand 
the path. As '^' is for the current folder, it's inserted. (During 
startup there's no 'current folder').

Afterwards the string used as regex just is: 'foobar$'.

   bye, Rocco
-- 
:wq!
}}}

--------------------------------------------------------------------------------
2006-08-04 05:40:16 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
Eek.  The obvious solution would be another level of \
escaping, but I don't like that thought at all. I guess I'd
rather throw out the folder expansion and just stick to regular
expressions.

-- 
Thomas Roessler			      <roessler@does-not-exist.org>






On 2006-08-03 12:35:02 +0200, Rocco Rutte wrote:
> From: Rocco Rutte <pdmef@gmx.net>
> To: Mutt Developers <mutt-dev@mutt.org>
> Date: Thu, 03 Aug 2006 12:35:02 +0200
> Subject: Re: mutt/2402: damaged regexps in folder-hooks
> Reply-To: bug-any@bugs.mutt.org
> X-Spam-Level: 
> 
> The following reply was made to PR mutt/2402; it has been noted by GNATS.
> 
> From: Rocco Rutte <pdmef@gmx.net>
> To: bug-any@bugs.mutt.org
> Cc: 
> Subject: Re: mutt/2402: damaged regexps in folder-hooks
> Date: Thu, 3 Aug 2006 10:29:40 +0000
> 
>  Hi,
>  
>  * Alain Bench [06-08-03 07:31:38 +0200] wrote:
>  
>  >    In Mutt 1.5.10 and more, the regexp in folder-hooks can
>  >be damaged in some circumstances, leading to false positive
>  >matches. That seems to happen when the regexp both: Has
>  >uppercase characters (is case-significant), and begins by an
>  >'^' anchor.
>  
>  I found more cases to not work: case-sensivity doesn't matter (I put 
>  some debug lines in and always see errors for your samples).
>  
>  The problem is that mailbox expansion is applied to folder-hooks. I'd 
>  call this a feature and thus your report is both valid and invalid at 
>  the same time... :(
>  
>  Please see: mutt_parse_hook() in hook.c and _mutt_expand_path() in 
>  muttlib.c. The core problem is that '^' is a shortcut for the current 
>  folder and thus is subject to expansion and is not treated as a regex 
>  special character. Basically we need a way to distinct between regex and 
>  "folder" special characters.
>  
>  Any ideas?
>  
>     bye, Rocco
>  -- 
>  :wq!
>  
> 
>
}}}

--------------------------------------------------------------------------------
2006-08-04 06:05:26 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2006-08-03 12:55:01 +0200, Rocco Rutte wrote:

>  I though of that, too, but then I have in my config:
>  
>     folder-hook ! '...'
>     folder-hook ~/Mail/archive '...'
>     folder-hook =IN.mutt-dev '...'
>  

Would we lose much by having hooks *either* do regular
expression matching *or* mailbox shortcuts?

One way to multiplex between the two modes would be to check
the first character for being special (with the obvious
exception of ^), and possibly add folder-hook-rx and
folder-hook-path keywords to override.

-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2006-08-04 06:36:03 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
{{{
Hi,

* Alain Bench [06-08-03 17:15:01 +0200] wrote:

>     <brainstorming> Is there really a problem outside of "^"? Other
> shortcuts are "! > < - !! ~ = + @". All of them expand only when in
> first position.

Right.

>     Otherwise: Why are they expanded if backslashed? They don't need to
> (outside of ! because of ! negation)? "^a" could be expanded to
> "/tmp/nomatchABCa", but "\^a" to "^a" regexp. Hum... One backslash is
> stripped too early, right?

Yepp: already when reading/tokenizing the line.

>     They are not expanded if double\\ed. One backslash reaches the
> regexp engine. This doesn't hurt normal chars, but of course hurts "^".
> Grrr...

So you tried it, too? :)

Another idea: we simply rename '^' to something else and announce that 
with big fat warnings. We can continue to support '^' except for when 
_mutt_expand_path() get's a regexp. That would add an extra 'case' label 
as well as a check for the rx parameter.

Almost no setup would break (that isn't broken already) and almost 
nobody would have to migrate to a new syntax.

Before we do something like this, we still would have to make sure to 
locate all places where a regex-enabled path is expanded.

Oh, hook.c is (at it seems) the only where a regex-enabled folder is 
used, so I'd give this idea a "go"... :)

More opinions?

   bye, Rocco
-- 
:wq!
}}}

--------------------------------------------------------------------------------
2006-08-04 07:59:56 UTC Michael Tatge <Michael.Tatge@web.de>
* Added comment:
{{{
* On Thu, Aug 03, 2006 Alain Bench (veronatif@free.fr) muttered:
> >Number:         2402
> >Notify-List:    
> >Category:       mutt
> >Synopsis:       damaged regexps in folder-hooks

This is an old bug iirc. See #1911

Michael
}}}

--------------------------------------------------------------------------------
2006-08-04 10:08:07 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hi Rocco and Thomas,

 On Thursday, August 3, 2006 at 12:35:02 +0200, Rocco Rutte wrote:

> The problem is that mailbox expansion is applied to folder-hooks. I'd
> call this a feature

    Good catch! The ^ current folder expands to nothing at startup
muttrc parsing time. The same regexp entered interactively
":folder-hook ^ABC$ yaddahyaddah" while in nomatchABC folder expands to
"nomatchABCABC$". And this explains why this fleature exists only since
1.5.10, when current folder patch was commited.


> case-sensivity doesn't matter

    Right. I was confused about forced caseful matching even when the
regexp is only lowcase. "^abc$" didn't wrongly match "nomatchABC", but
not because of the not eaten anchor.


> Basically we need a way to distinct between regex and "folder" special
> characters.

    <brainstorming> Is there really a problem outside of "^"? Other
shortcuts are "! > < - !! ~ = + @". All of them expand only when in
first position. But even if backslashed. "~ = +" are directories, that
may have to be followed by more characters. Others are directly a
folder's name. What about expanding those last only when they are alone
in string? Like expand "^" to "/tmp/nomatchABC", but keep "^a" as
regexp?

    Otherwise: Why are they expanded if backslashed? They don't need to
(outside of ! because of ! negation)? "^a" could be expanded to
"/tmp/nomatchABCa", but "\^a" to "^a" regexp. Hum... One backslash is
stripped too early, right?

    They are not expanded if double\\ed. One backslash reaches the
regexp engine. This doesn't hurt normal chars, but of course hurts "^".
Grrr...

    BTW today "^^a" expands to "^a". But it's a false friend: That
"works" only at startup; At runtime it expands to "/tmp/nomatchABC^a"

    Why is "^" expanded at all in folder-hook patterns? It doesn't seem
to make much sense (because of empty expansion at startup). Hum... It
makes sense elsewhere, and crafting a special case for FHs would be
unclean, right? </brainstorming>


 On Thursday, August 3, 2006 at 13:05:01 +0200, Thomas Roessler wrote:

> Would we lose much by having hooks *either* do regular expression
> matching *or* mailbox shortcuts?

    We would lose "=IN.mutt-(users|dev)$", yes.


Bye!	Alain.
-- 
Mutt muttrc tip for mailing lists: set followup_to=yes and declare the list as
 - subscribe ^list@ddress$ if you are subscribed and don't want courtesy copy.
 - lists ^list@ddress$     if you are not subscribed or want a courtesy copy.
}}}

--------------------------------------------------------------------------------
2006-08-04 10:59:14 UTC Rado S <rado@math.uni-hamburg.de>
* Added comment:
{{{
=3D- Rocco Rutte wrote on Thu  3.Aug'06 at 17:45:03 +0200 -=3D
>  * Alain Bench [06-08-03 17:15:01 +0200] wrote:
>  > <brainstorming> Is there really a problem outside of "^"?
>  > Other shortcuts are "! > < - !! ~ =3D + @". All of them expand
>  > only when in first position.
> =20
>  Right.

If you want to match absolute path names to be sure it doesn't
match a subdir, then ^! or ^< for example wouldn't work.

>  Another idea: we simply rename '^' to something else {...}

I always wondered why that ^ was picked for this feature given its
other meaning it already has...

--=20
=A9 Rado S. -- You must provide YOUR effort for your goal!
Even if it seems insignificant, in fact EVERY effort counts
for a shared task, at least to show your deserving attitude.
}}}

--------------------------------------------------------------------------------
2006-08-05 00:11:54 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
{{{
Hi,

* Alain Bench [06-08-04 10:55:02 +0200] wrote:

>     At last! Found *the* working syntax. Tadaa!

> | folder-hook (^ABC$) yaddahyaddah

>     This expands to self:

> | Status of commands:

> | folder-hook
> |   (^ABC$)
> |     yaddahyaddah

>     ...and regexec seems to like it. No problem for negation:

> | folder-hook !(^ABC$) yaddahyaddah

>     ...expands to:

> | folder-hook
> |   !( (^ABC$) )
> |     yaddahyaddah

>     ...and works as expected. So it's maybe just a matter of documenting
> it. Something like: "To prevent the expansion of mailbox shortcut
> characters that you would want to use as special characters in the
> regexp, enclose the whole regexp in parenthesis. Example: If you want to
> match ^regexp$ exactly, without the default automagic expansion of "^"
> shortcut to the full name of the current folder, then use (^regexp$) as
> pattern. This is needed only for folder-hook patterns, the only ones to
> first endure folder expansion, and then pursue a regexp career."

This would be the cheapest way to fix the bug, I guess. However, I think 
when using:

   folder-hook ^foo

most people expect this to be a perfectly valid regexp (since the usage 
for folder-hook mentions regex). The minority (if at all), expects this 
to expand to the current folder.

So we should honor this by making the people's life harder who use the 
shortcut, and keep it as-is for the regex users.

   bye, Rocco
-- 
:wq!
}}}

--------------------------------------------------------------------------------
2006-08-05 00:37:24 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Thursday, August 3, 2006 at 17:45:03 +0200, Rocco Rutte wrote:

>> Grrr...
> So you tried it, too? :)

    At last! Found *the* working syntax. Tadaa!

| folder-hook (^ABC$) yaddahyaddah

    This expands to self:

| Status of commands:
|
| folder-hook
|   (^ABC$)
|     yaddahyaddah

    ...and regexec seems to like it. No problem for negation:

| folder-hook !(^ABC$) yaddahyaddah

    ...expands to:

| folder-hook
|   !( (^ABC$) )
|     yaddahyaddah

    ...and works as expected. So it's maybe just a matter of documenting
it. Something like: "To prevent the expansion of mailbox shortcut
characters that you would want to use as special characters in the
regexp, enclose the whole regexp in parenthesis. Example: If you want to
match ^regexp$ exactly, without the default automagic expansion of "^"
shortcut to the full name of the current folder, then use (^regexp$) as
pattern. This is needed only for folder-hook patterns, the only ones to
first endure folder expansion, and then pursue a regexp career."


 On Thursday, August 3, 2006 at 18:55:02 +0200, Brendan Cully wrote:

> On Thursday, 03 August 2006 at 17:45, Rocco Rutte wrote:
>> rename '^' to something else
> the only sensible thing to do. Let's use '.' instead :)

    And a nice enhancement would be to defer its expansion, so default
folder-hooks can continue to work... ;-)


> I don't think '^' has been in use long enough to warrant
> backward-compatibility hacks.

    Before its inclusion in Mutt, the "^" shortcut had a long career as
a feature patch, and was used by many people since years. Even included
in Debian packages.


 On Thursday, August 3, 2006 at 17:59:14 +0200, Rado Smiljanic wrote:

>> * Alain Bench [06-08-03 17:15:01 +0200] wrote:
>>> shortcuts are "! > < - !! ~ = + @". All of them expand only when in
>>> first position.
> If you want to match absolute path names to be sure it doesn't match a
> subdir, then ^! or ^< for example wouldn't work.

    Right: Strict regexp around a shortcut is not possible. And I can't
resist to think it's unfortunate. Hum... <BS> This could be done
automatically at expansion. When expanding shortcuts for wanabee
regexps, we could regexpify path names ^dir\.ext or ^file\.ext$ </BS>
Probable overkill. BS means here no br*inst*rming, but bullshit. ;-)


Bye!	Alain.
-- 
$ ln -s imap://veronatif@imap.free.fr/INBOX/ remote-inbox
create symbolic link remote-inbox to imap://veronatif@imap.free.fr/INBOX/
$ mutt-cvs -f remote-inbox
remote-inbox: No such file or directory (errno = 2)
}}}

--------------------------------------------------------------------------------
2015-07-29 16:13:46 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [75e398daa94c588cb94362abd4aca8278b6cbfb6]:
{{{
#!CommitTicketReference repository="" revision="75e398daa94c588cb94362abd4aca8278b6cbfb6"
Add error handling for ^ and other empty mailbox shortcuts.

(closes #2402) (closes #3735)

Explicitly mention the ^ example in the documentation added in 6d733cab6b45.

Add an error message for ^ when CurrentFolder is not set.  Add checks
for other mailbox shortcuts that expand to the empty string.  This
could happen if the @alias shortcut was accidentally used, or the value
referenced by a shortcut isn't set yet.
}}}

* resolution changed to fixed
* status changed to closed
