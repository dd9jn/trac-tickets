Ticket:  3132
Status:  closed
Summary: askfrom: prompt for from-address before editing the body of an outgoing message

Reporter: pebo
Owner:    mutt-dev

Opened:       2008-11-07 19:35:14 UTC
Last Updated: 2011-07-13 23:00:54 UTC

Priority:  major
Component: mutt
Keywords:  askfrom ask-from

--------------------------------------------------------------------------------
Description:
Hi,

when one has to deal with multiple from-addresses, `alternates` and `reverse_name` are a great time-saver. To make the situation perfect, there is just one component missing -- `askfrom`. When replying to a message `reverse_name` does its job very well, but when composing a new-one, you always have to remember changing the From: header through using `<ESC>-f` after editing the body. Obviously, I often forget to do this.

`askcc` & `askbcc` exist and there was even an `askfrom` patch back in 2002:

http://www.mail-archive.com/mutt-users@mutt.org/msg28714.html

but I couldn't figure out why it wasn't accepted back then.


Thanks,

Petar

--------------------------------------------------------------------------------
2011-07-13 23:00:54 UTC pebo
* resolution changed to invalid
* status changed to closed
