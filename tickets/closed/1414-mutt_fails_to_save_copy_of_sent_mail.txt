Ticket:  1414
Status:  closed
Summary: mutt fails to save copy of sent mail

Reporter: Martin Siegert <siegert@sfu.ca>
Owner:    mutt-dev

Opened:       2002-12-10 22:07:21 UTC
Last Updated: 2005-08-02 02:31:22 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: normal

-- Please type your report below this line
mutt fails to save a copy of a sent message, if the corresponding
folder is not recognized by mutt as a mailbox.

Example: copy=yes, save_name=yes, record="~/Mail/sent", folder="~/Mail"
Folder =xyz starts with an empty line (elm once in a while creates
such folders; but in principle it does not really matter how that
folder was created). When sending an email to xyz no copy will be
made, neither in =xyz, nor in =sent. No error messages is visible,
only the notice "Mail sent". Thus, the copy gets lost completely
and there is no way of recovering it. Furthermore, you do not notice
that the message was not saved.

The problem (as far as I can tell) that mx_get_magic is called when
saving a message and if it returns 0 the message is thrown away.
Possible solutions:

1) append to the specified folder anyway: if the users wants it that
   way why should mutt care. Nobody requires mutt to by able to parse
   those files afterwards. But at least the message is saved and
   can be recovered by other means.
2) spit out an error message and prompt for a different folder
   maybe suggesting something like
   =<folder>.<month><day>-<year>.<hour>:<minute>
   as in =xyz.Dec10-02.15:24
3) save to $record if set and recognized as a mailbox. But if that's
   not the case, you would use menthod 1 or 2 anyway.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc -R/usr/LOCAL/lib -R/usr/LOCAL/ssl/lib
Reading specs from /usr/LOCAL/lib/gcc-lib/sparc-sun-solaris2.6/2.95.1/specs
gcc version 2.95.1 19990816 (release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: SunOS 5.6 (sun4u) [using ncurses 4.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  +ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/LOCAL/bin/ispell"
SENDMAIL="/usr/lib/sendmail"
MAILPATH="/tmp"
PKGDATADIR="/usr/LOCAL/share/mutt"
SYSCONFDIR="/usr/LOCAL/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-12-10 22:07:21 UTC Martin Siegert <siegert@sfu.ca>
* Added comment:
{{{
Package: mutt
Version: 1.4i
Severity: normal

-- Please type your report below this line
mutt fails to save a copy of a sent message, if the corresponding
folder is not recognized by mutt as a mailbox.

Example: copy=yes, save_name=yes, record="~/Mail/sent", folder="~/Mail"
Folder =xyz starts with an empty line (elm once in a while creates
such folders; but in principle it does not really matter how that
folder was created). When sending an email to xyz no copy will be
made, neither in =xyz, nor in =sent. No error messages is visible,
only the notice "Mail sent". Thus, the copy gets lost completely
and there is no way of recovering it. Furthermore, you do not notice
that the message was not saved.

The problem (as far as I can tell) that mx_get_magic is called when
saving a message and if it returns 0 the message is thrown away.
Possible solutions:

1) append to the specified folder anyway: if the users wants it that
  way why should mutt care. Nobody requires mutt to by able to parse
  those files afterwards. But at least the message is saved and
  can be recovered by other means.
2) spit out an error message and prompt for a different folder
  maybe suggesting something like
  =<folder>.<month><day>-<year>.<hour>:<minute>
  as in =xyz.Dec10-02.15:24
3) save to $record if set and recognized as a mailbox. But if that's
  not the case, you would use menthod 1 or 2 anyway.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc -R/usr/LOCAL/lib -R/usr/LOCAL/ssl/lib
Reading specs from /usr/LOCAL/lib/gcc-lib/sparc-sun-solaris2.6/2.95.1/specs
gcc version 2.95.1 19990816 (release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: SunOS 5.6 (sun4u) [using ncurses 4.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  +ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/LOCAL/bin/ispell"
SENDMAIL="/usr/lib/sendmail"
MAILPATH="/tmp"
PKGDATADIR="/usr/LOCAL/share/mutt"
SYSCONFDIR="/usr/LOCAL/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.
}}}

--------------------------------------------------------------------------------
2002-12-23 23:38:01 UTC Martin Siegert <siegert@sfu.ca>
* Added comment:
{{{
On Sat, Dec 21, 2002 at 01:48:35PM -0800, Michael Elkins wrote:
> On 2002-12-10, Martin Siegert <siegert@sfu.ca> wrote:
> > mutt fails to save a copy of a sent message, if the corresponding
> > folder is not recognized by mutt as a mailbox.
> 
> Have you tried 1.5.3 yet?  I believe this problem should be fixed.  I
> added some code to make mutt not ignore it when it gets an error trying
> to fcc the message before sending.

I installed mutt-1.5.3 and (after fighting with configure once more -
it insists on installing mutt_dotlock as setgid mail although I specify
--disable-external-dotlock; when using mutt and imap the permissions
on /var/mail should be irrelevant - I guess I should submit a bug
report on that issue as well) it now says "/home/siegert/Mail/xyz is not
a mailbox" when I send mail to xyz and /home/siegert/Mail/xyz starts with
an empty line. The mail is not sent. This is better and probably ok for
the expert who knows what is going on. The average user is going to be
extremely confused though. What should (s)he do?
I very much would prefer, if mutt would just save the message to whatever
folder was requested without checking whether it is a mailbox or not
(if I want to append messages to my daily diary, why does mutt care?).

Thus, although the new behaviour is better than just silently not saving
the sent mail, it is nevertheless not going to solve my problem, which
is partially also a user support problem. Having mutt sit there with
the mail screen and displaying "xyz is not a mailbox" and leaving the
user no other way out other than aborting the message is not really
"user friendly". I guess the best solution would be to prompt for an
alternate mail folder to save the message in.

Cheers,
Martin
}}}

--------------------------------------------------------------------------------
2002-12-26 05:20:10 UTC Thomas Roessler <roessler-mobile@does-not-exist.net>
* Added comment:
{{{
On 2002-12-23 15:38:01 -0800, Martin Siegert wrote:

> I very much would prefer, if mutt would just save the message to
> whatever folder was requested without checking whether it is a
> mailbox or not (if I want to append messages to my daily diary,
> why does mutt care?).

Because mutt has to generate the proper mailbox delimiter: When
appending a message to an MMDF folder, you have to do something
different from what you'd do when saving to an mbox folder. Both
folder formats use plain files.

> Thus, although the new behaviour is better than just silently not
> saving the sent mail, it is nevertheless not going to solve my
> problem, which is partially also a user support problem. Having
> mutt sit there with the mail screen and displaying "xyz is not a
> mailbox" and leaving the user no other way out other than
> aborting the message is not really "user friendly". I guess the
> best solution would be to prompt for an alternate mail folder to
> save the message in.

You do have a point here.

-- 
Thomas Roessler	(mobile)	<roessler-mobile@does-not-exist.net>
}}}

--------------------------------------------------------------------------------
2005-08-02 20:31:22 UTC brendan
* Added comment:
{{{
FCC checking seems fine in CVS.
}}}

* resolution changed to fixed
* status changed to closed
