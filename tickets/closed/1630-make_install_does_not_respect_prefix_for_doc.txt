Ticket:  1630
Status:  closed
Summary: make install does not respect prefix for doc

Reporter: mutt+sjm217@cl.cam.ac.uk
Owner:    mutt-dev

Opened:       2003-09-11 09:14:23 UTC
Last Updated: 2005-08-02 04:58:46 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.1i
Severity: normal

-- Please type your report below this line

For installing applications using Stow
(http://www.gnu.org/software/stow/stow.html) it is necessary to
provide ./configure with a different "prefix" from that given to
install. Then stow makes symlinks from the ./configure target to the
make install target.

However mutt 1.4.1i does not fully respect the make install prefix,
so will install the doc directory in the incorrect place, since
configure seems to hard-code the prefix it is given to be the
doc install directory. Other directories seem to work properly.

For example:
sjm217@solway:mutt-1.4.1 719$ uname -a
Linux solway.cl.cam.ac.uk 2.4.19-3cl #1 SMP Mon Nov 4 12:20:37 GMT 2002 i686 i686 i386 GNU/Linux
sjm217@solway:mutt-1.4.1 719$ ./configure --prefix=/local/scratch/sjm217/build/local --with-homespool=.mail
[snip]
sjm217@solway:mutt-1.4.1 719$ make install prefix=/local/scratch/sjm217/build/local2
[snip]
sjm217@solway:mutt-1.4.1 721$ ls /local/scratch/sjm217/build/local
doc
sjm217@solway:mutt-1.4.1 722$ ls /local/scratch/sjm217/build/local2
bin  etc  man  share

So while bin, etc, man and share have been installed in the correct
place, doc has not.

This seems to be because in the Makefile, the docdir variable has
been hard-coded by ./configure, where with other locations, ./configure
has made them relative to ${prefix}

In order to get the correct behaviour from the Mutt install system I
had to leave --with-docdir at the default location (since the symlinks
will exist in the normal place, relative to --prefix). Normally for
the installation of a package using stow it is only necessary to
override "prefix", however with Mutt I also have to override "docdir"
when calling "make install".

If there is no specific reason not to, then I would have considered
the normal behaviour to set docdir in the Makefile to be relative to
prefix, rather than being hard coded by ./configure. This will allow
Stow to work correctly while still allowing users to override docdir
if needed.

Some relevant lines from the generated mutt-1.4.1/Makefile are
included below:

----------------------------
prefix = /usr/groups/security/local
exec_prefix = ${prefix}

bindir = ${exec_prefix}/bin
...
datadir = ${prefix}/share
sysconfdir = ${prefix}/etc
...
mandir = ${prefix}/man
...
docdir = /usr/groups/security/local/doc/mutt
----------------------------

Thank you,
Steven Murdoch.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/3.2.2/specs
Configured with: ../configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --enable-shared --enable-threads=posix --disable-checking --with-system-zlib --enable-__cxa_atexit --host=i386-redhat-linux
Thread model: posix
gcc version 3.2.2 20030222 (Red Hat Linux 3.2.2-5)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.19-3cl (i686) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH=".mail"
PKGDATADIR="/usr/groups/security/local/share/mutt"
SYSCONFDIR="/usr/groups/security/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 22:58:46 UTC brendan
* Added comment:
{{{
Fixed in CVS
}}}

* resolution changed to fixed
* status changed to closed
