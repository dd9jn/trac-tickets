Ticket:  3311
Status:  closed
Summary: Wrong parsing of MH dirs and sequences

Reporter: JohanD
Owner:    mutt-dev

Opened:       2009-07-31 11:48:18 UTC
Last Updated: 2009-07-31 11:56:07 UTC

Priority:  major
Component: mutt
Keywords:  MH unseen sequence

--------------------------------------------------------------------------------
Description:
As described in the debian bug tracking system (http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=538128), mutt version 1.5.20 blocked when opening a MH folder.

The mutt ticket #3308 seems to solve the opening of an MH folder.
But mutt still failed to hanlde correctly the MH sequences (especially the Unseen sequence) : when opening a MH folder, whatever the unseen sequence is, mutt does not find more than one new message.

I hope the following patch for the functions maildir_parse_dir and mh_read_sequence will solve this.

regards, 
Johan

--------------------------------------------------------------------------------
2009-07-31 11:56:07 UTC JohanD
* Added comment:
sorry, I mess with Trac and open a duplicate bug here #3312.

* resolution changed to duplicate
* status changed to closed
