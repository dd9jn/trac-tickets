Ticket:  3787
Status:  closed
Summary: out of bounds read in next_token

Reporter: hanno
Owner:    mutt-dev

Opened:       2015-10-10 06:49:22 UTC
Last Updated: 2015-10-18 12:08:11 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
The attached file will cause an out of bounds heap read (similar to #3776) when trying to read it with mutt with something like
mutt -H [input file] a

To see this one needs to use memory debugging tools like valgrind or asan. I'll attach the full output from asan.

--------------------------------------------------------------------------------
2015-10-10 06:49:51 UTC hanno
* Added attachment mutt-heap-oob-read-next_token
* Added comment:
sample input showing oob

--------------------------------------------------------------------------------
2015-10-10 06:50:37 UTC hanno
* Added attachment mutt-heap-oob-read-next_token.asan.txt
* Added comment:
address sanitizer error message

--------------------------------------------------------------------------------
2015-10-17 12:42:30 UTC kevin8t8
* Added comment:
I believe this patch should fix the problem, but I'm having trouble getting ASAN to trigger in my environment (perhaps I'm just not using it properly).

Would you mind testing this patch and seeing if it fixes the problem?

--------------------------------------------------------------------------------
2015-10-17 12:42:50 UTC kevin8t8
* Added attachment ticket-3787-oob-read.patch

--------------------------------------------------------------------------------
2015-10-18 02:18:42 UTC hanno
* Added comment:
Thanks, your patch fixes the issue in my tests.

I've been using clang for testing (or - to be more precise - afl's wrapper afl-clang-fast), maybe that's why you couldn't reproduce. Also afl adds some extra hardening CFLAGS, that may change memory layout as well.

--------------------------------------------------------------------------------
2015-10-18 03:30:52 UTC kevin8t8
* Added comment:
Thank you for confirming the patch.  It turns out I was just too tired last night, and when I enabled ASAN, I accidentally used the wrong input file!  :-)

After getting that straight, I was able to duplicate the ASAN error, and also confirm the patch fixes it.

I'll commit the patch later today and close the bug out, but I'm going to take a closer look at mutt's other uses of strchr.  I didn't realize '\0' will match, so it may be worth creating our own "mutt_strchr" that catches that case.

Thanks again for the bug report.  This was a tricky one!

--------------------------------------------------------------------------------
2015-10-18 12:08:11 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [a6919571eb593031738fc0a8794d5dcaf2260668]:
{{{
#!CommitTicketReference repository="" revision="a6919571eb593031738fc0a8794d5dcaf2260668"
Fix next_token() oob read.  (closes #3787)

With specially crafted input to 'mutt -H', the line "Return-Path:<() "
is read and passed to mutt_parse_rfc822_line(). "<() " is then passed
through to rfc822_parse_adrlist().

Eventually, inside next_token(), is_special(*s) is called when s
points to the end of the string ('\0').  This macro calls strchr,
which will actually match and return a pointer to the trailing '\0' in
RFC822Specials!  This causes "s + 1" to be returned, skipping past the
end of string inside parse_mailboxdomain().

This patch adds a check to make sure *s is non-null before calling
is_special(*s).
}}}

* resolution changed to fixed
* status changed to closed
