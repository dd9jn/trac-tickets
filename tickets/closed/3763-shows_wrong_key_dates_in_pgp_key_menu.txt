Ticket:  3763
Status:  closed
Summary: shows wrong key dates in pgp key menu

Reporter: Gregor_Zattler
Owner:    mutt-dev

Opened:       2015-07-18 15:53:12 UTC
Last Updated: 2015-07-19 23:00:19 UTC

Priority:  minor
Component: crypto
Keywords:  pgp key menu dates

--------------------------------------------------------------------------------
Description:
Mutt 1.5.23+102 (2ca89bed6448) (2014-03-12) shows totally wrong pgp key creation dates in pgp key menu when pgp_entry_format allows for key dates:

q:Exit  <Return>:Select  c:Check key  ?:Help
   1 ?  1024/0x03ECD6999737C0DD DSA  es  (1986-10-18 10:17:52)

   2 ?  4096/0x50A35937F4CBEF5D RSA  es  (1919-11-16 14:53:04)

   3 ?  1024/0xCAD51662F4CB86A6 DSA  es  (1909-02-26 03:49:36)

   4    2048/0xD2262944CE6AC6C1 RSA  es  (2024-11-13 01:26:40)

   5 ?R 1024/0x03ECD6999737C0DD DSA  es Thomas Roessler (mobile) <roessler-mobile@does-not-exist.info> (1986-10-18 10:17:52)

   6 ?R 1024/0x03ECD6999737C0DD DSA  es Thomas Roessler (mobile) <roessler-mobile@does-not-exist.net> (1986-10-18 10:17:52)

   7 ?R 1024/0x03ECD6999737C0DD DSA  es Thomas Roessler (mobile) <roessler-mobile@does-not-exist.org> (1986-10-18 10:17:52)

   8 ?  1024/0x03ECD6999737C0DD DSA  es Thomas Roessler <roessler@does-not-exist.org> (1986-10-18 10:17:52)

   9 ?  4096/0x50A35937F4CBEF5D RSA  es Thomas Roessler <roessler@does-not-exist.org> (1919-11-16 14:53:04)

  10 ?  1024/0xCAD51662F4CB86A6 DSA  es Thomas Roessler <roessler@does-not-exist.org> (1909-02-26 03:49:36)

  11    2048/0xD2262944CE6AC6C1 RSA  es Thomas Roessler <roessler@does-not-exist.org> (2024-11-13 01:26:40)

  12 ?  4096/0x50A35937F4CBEF5D RSA  es Thomas Roessler <roessler@gmail.com> (1919-11-16 14:53:04)

  13    2048/0xD2262944CE6AC6C1 RSA  es Thomas Roessler <roessler@guug.de> (2024-11-13 01:26:40)


This output shows key creation dates ranging from 1909 to 2024
while they range in fact from 1997 to 2013. (I added line breaks for better readability)

In order to ensure that this is no effect of my environment I started mutt like so:

env -i TERM=screen-256c-bce-s ./mutt -nF/dev/null -f nuk

the only customisations necessay to produce this output are:

:set pgp_list_pubring_command="env -i /usr/bin/gpg   --list-options no-show-photos --no-verbose --batch --quiet   --with-colons --list-keys %r"
:set pgp_entry_format="%4n %t%f %4l/0x%k %-4a %2c %u (%[%F %T])"

and having the respective keys in ones gpg public keyring.


Use case:  This settings and pgp_sort_keys=reverse-date helps me
picking the current pgpg key of my correspondent.  It would by
nice if the shown dates would be correct.


I have the same problem with the debian mutt packages (wheezy/jessie).

Below are infos on this specific build of mutt.


Thanks for your attention, Gregor


Mutt 1.5.23+102 (2ca89bed6448) (2014-03-12)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 3.16.0-4-686-pae (i686)
ncurses: ncurses 5.9.20140913 (compiled with 5.9)
libidn: 1.29 (compiled with 1.29)

Compiler:
Using built-in specs.
COLLECT_GCC=/usr/bin/gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/i586-linux-gnu/4.9/lto-wrapper
Target: i586-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Debian 4.9.2-10' --with-bugurl=file:///usr/share/doc/gcc-4.9/README.Bugs --enable-languages=c,c++,java,go,d,fortran,objc,obj-c++ --prefix=/usr --program-suffix=-4.9 --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --with-gxx-include-dir=/usr/include/c++/4.9 --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --enable-gnu-unique-object --disable-vtable-verify --enable-plugin --with-system-zlib --disable-browser-plugin --enable-java-awt=gtk --enable-gtk-cairo --with-java-home=/usr/lib/jvm/java-1.5.0-gcj-4.9-i386/jre --enable-java-home --with-jvm-root-dir=/usr/lib/jvm/java-1.5.0-gcj-4.9-i386 --with-jvm-jar-dir=/usr/lib/jvm-exports/java-1.5.0-gcj-4.9-i386 --with-arch-directory=i386 --with-ecj-jar=/usr/share/java/eclipse-ecj.jar --enable-objc-gc --enable-targets=all --enable-multiarch --with-arch-32=i586 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-checking=release --build=i586-linux-gnu --host=i586-linux-gnu --target=i586-linux-gnu
Thread model: posix
gcc version 4.9.2 (Debian 4.9.2-10)

Configure options:

Compilation CFLAGS: -Wall -pedantic -Wno-long-long -g -O2

Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  +USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_SMTP
-USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  -HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.



--------------------------------------------------------------------------------
2015-07-18 19:27:00 UTC kevin8t8
* Added comment:
So far I haven't been able to reproduce this.

Was is the output if you directly run:
{{{
env -i /usr/bin/gpg --list-options no-show-photos --no-verbose --batch --quiet --with-colons --list-keys 0x50A35937F4CBEF5D
}}}

For me, this gives:
{{{
tru::1:1437164744:1453409426:3:1:5
pub:-:4096:1:50A35937F4CBEF5D:2013-08-04:2017-08-04::-:Thomas Roessler <xxx@xxxx>::escaESCA:
fpr:::::::::07C710D08DCA6D687B6002CD50A35937F4CBEF5D:
uid:-::::2013-08-04::E7BE5D4362A8D65643033CCD52EB26470FFC02A2::Thomas Roessler <xxx@xxxx>:
sub:-:4096:1:460FE040A7956E88:2013-08-04:2017-08-04:::::esa:
}}}

--------------------------------------------------------------------------------
2015-07-18 23:25:09 UTC Gregor_Zattler
* Added comment:
Replying to [comment:1 kevin8t8]:
> So far I haven't been able to reproduce this.
> 
> Was is the output if you directly run:
> {{{
> env -i /usr/bin/gpg --list-options no-show-photos --no-verbose --batch --quiet --with-colons --list-keys 0x50A35937F4CBEF5D
> }}}
> 
> For me, this gives:
> {{{
> tru::1:1437164744:1453409426:3:1:5
> pub:-:4096:1:50A35937F4CBEF5D:2013-08-04:2017-08-04::-:Thomas Roessler <xxx@xxxx>::escaESCA:
> fpr:::::::::07C710D08DCA6D687B6002CD50A35937F4CBEF5D:
> uid:-::::2013-08-04::E7BE5D4362A8D65643033CCD52EB26470FFC02A2::Thomas Roessler <xxx@xxxx>:
> sub:-:4096:1:460FE040A7956E88:2013-08-04:2017-08-04:::::esa:
> }}}

$ env -i /usr/bin/gpg --list-options no-show-photos --no-verbose --batch  --quiet --with-colons --list-keys 0x50A35937F4CBEF5D
{{{
tru::1:1437234043:1442013515:5:2:3
pub:-:4096:1:50A35937F4CBEF5D:1375617754:1501848154::-:::escaESCA:
uid:-::::1375617858::602DABFB3D5C3486F37F0DAC75FEC77920B895AA::Thomas Roessler <roessler@gmail.com>:
uid:-::::1375617823::E7BE5D4362A8D65643033CCD52EB26470FFC02A2::Thomas Roessler <roessler@does-not-exist.org>:
sub:-:4096:1:460FE040A7956E88:1375617754:1501848154:::::esa:
}}}

This is astonishing, since different.  I have no idea why, isn't the with-colons output supposed to be everywhere the same?  This is gpg (GnuPG) 1.4.18

Thanks for looking into this, Gregor

--------------------------------------------------------------------------------
2015-07-18 23:37:49 UTC kevin8t8
* Added comment:
Your output is in the "fixed-list-mode" format.  In this format, the dates are output as seconds since epoch, and the first uid is not included in the pub record.  This would definitely explain the crazy dates, since mutt is expecting YYYY-MM-DD.

Would you check if you have "fixed-list-mode" option in your ~/.gnupg/gpg.conf file?  If so, I suggest you comment it out and try your tests again.

--------------------------------------------------------------------------------
2015-07-19 21:06:37 UTC Gregor_Zattler
* Added comment:
{{{
Hi Kevin,
* Mutt <fleas@mutt.org> [18. Jul. 2015]:

You are right.  I copied this setting from
https://we.riseup.net/riseuplabs+paow/openpgp-best-practices#update-your-gpg-defaults
obviously without knowing that this would impact mutt and then
forgot about it.

Sorry for this unnecessary bug report and thanks for enlightening
me.

I tried to document this behaviour, see below.  Ciao; gregor



# HG changeset patch
# User Gregor Zattler <grfz@gmx.de>
# Date 1437339519 -7200
#      Sun Jul 19 22:58:39 2015 +0200
# Node ID 00a95c6c7a3a2cef6ed70ef573ac82de5a32e0db
# Parent  2ca89bed64480780d0a435e89c13dba06c748094
Document imapct of gpg's "fixed-list-mode" option.

Using this option results in mutt showing crazy key generation dates.

diff -r 2ca89bed6448 -r 00a95c6c7a3a init.h
--- a/init.h	Sat Jul 11 14:36:51 2015 -0700
+++ b/init.h	Sun Jul 19 22:58:39 2015 +0200
@@ -1863,7 +1863,9 @@
   ** .te
   ** .pp
   ** This format is also generated by the \fCpgpring\fP utility which comes
-  ** with mutt.
+  ** with mutt.  Beware that when using gpg's "fixed-list-mode" option gpg
+  ** produces a different format which results in mutt showing crazy key
+  ** generation dates.
   ** .pp
   ** This is a format string, see the $$pgp_decode_command command for
   ** possible \fCprintf(3)\fP-like sequences.
@@ -1879,7 +1881,10 @@
   ** .te
   ** .pp
   ** This format is also generated by the \fCpgpring\fP utility which comes
-  ** with mutt.
+  ** with mutt.  Beware that when using gpg's "fixed-list-mode" option gpg
+  ** produces a different format which results in mutt showing crazy key
+  ** generation dates.
+
   ** .pp
   ** This is a format string, see the $$pgp_decode_command command for
   ** possible \fCprintf(3)\fP-like sequences.

}}}

--------------------------------------------------------------------------------
2015-07-19 23:00:19 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [100835c4a8acd8d3f43d00b05dd73f2f6ddc6722]:
{{{
#!CommitTicketReference repository="" revision="100835c4a8acd8d3f43d00b05dd73f2f6ddc6722"
Add note about gpg fixed-list-mode.  (closes #3763).

Thanks to Gregor Zattler for the original patch.
}}}

* resolution changed to fixed
* status changed to closed
