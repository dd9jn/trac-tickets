Ticket:  2383
Status:  closed
Summary: cvs head does not build on debian unstable

Reporter: michael.tatge@web.der
Owner:    mutt-dev

Opened:       2006-07-24 12:52:16 UTC
Last Updated: 2006-08-05 01:23:53 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
cvs doesn't build on my debian unstable machine.
config.log.gz attached.
$ make
make  all-recursive
make[1]: Entering directory `/home/t/build/mutt/mutt-cvs'
Making all in m4
make[2]: Entering directory `/home/t/build/mutt/mutt-cvs/m4'
make[2]: *** No rule to make target `Makefile.am.in', needed by `Makefile.am'.  Stop.
make[2]: Leaving directory `/home/t/build/mutt/mutt-cvs/m4'
make[1]: *** [all-recursive] Error 1
make[1]: Leaving directory `/home/t/build/mutt/mutt-cvs'
make: *** [all] Error 2

Since the 1.5.12 release builds fine i guess this is related to latest changes to the build system.
>How-To-Repeat:
check out cvs
prepare && make
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-08-05 19:23:53 UTC brendan
* Added comment:
{{{
Duplicate of closed bug 2384.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:14 UTC 
* Added attachment config.log.gz
* Added comment:
config.log.gz
