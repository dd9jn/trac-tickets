Ticket:  3426
Status:  closed
Summary: certificate validation fails with openssl

Reporter: ph030
Owner:    mutt-dev

Opened:       2010-06-29 14:50:05 UTC
Last Updated: 2013-01-17 02:45:38 UTC

Priority:  major
Component: mutt
Keywords:  openssl cert

--------------------------------------------------------------------------------
Description:
Connecting to IMAP with mutt built against openssl always asks to accept the certificate on restarting mutt, though "always accept" has been selected.

The same procedure works without problems when built against gnutls.

Installing the ca_cert doesn't solve the problem in mutt, but connecting directly via "openssl s_client ..." works.

Sticking to gnutls ain't an option here, so a fix would be very much appreciated.

I wrote down some more info (system, versions, ...) here:
https://forums.gentoo.org/viewtopic-t-834023.html

Attached are the certs generated with using gnutls/openssl + the output of "openssl s_client -connect $server:$port -CAfile $file".

Thanks,
ph

--------------------------------------------------------------------------------
2010-06-29 14:51:14 UTC ph030
* Added attachment certs.openssl
* Added comment:
cert generated when built against openssl

--------------------------------------------------------------------------------
2010-06-29 14:51:46 UTC ph030
* Added attachment openssl.connect
* Added comment:
output of `openssl s_client ...`

--------------------------------------------------------------------------------
2010-06-29 15:25:20 UTC ph030
* Added attachment certs.gnutls
* Added comment:
cert generated when built against gnutls

--------------------------------------------------------------------------------
2010-06-29 17:01:04 UTC julm
* Added attachment fix-mutt_ssl-check_host-mutt_strlen-subj_alt_name.patch
* Added comment:
Fix error in changeset 6016:dc09812e63a3.

--------------------------------------------------------------------------------
2010-06-29 17:21:28 UTC julm
* Added comment:
Hi. Had the same problem and fixed it with uploaded
fix-mutt_ssl-check_host-mutt_strlen-subj_alt_name.patch.
It is due to an error in changeset 6016:dc09812e63a3
where the subj_alt_name string is not taken in ->d.ia5->data.
Hope that helps.

--------------------------------------------------------------------------------
2010-06-29 18:10:29 UTC ph030
* Added comment:
Thank you, julm, I'll see if I can apply it.

--------------------------------------------------------------------------------
2010-06-29 18:28:17 UTC ph030
* Added comment:
Hm, I'm not experienced enough to backport it myself to the current version in Gentoo's portage, thus I filed a bug. https://bugs.gentoo.org/show_bug.cgi?id=326197

--------------------------------------------------------------------------------
2010-08-06 08:01:33 UTC Matthias Andree
* Added comment:
{{{
Am 29.06.2010, 20:10 Uhr, schrieb Mutt:


The patch appears to be in reverse, but is required to unbreak subject  
alternative name matching. Mutt, with Thomas Hoger's patch, mistakenly  
compares the strlen of the SSL structure (which is ill-defined and bogus)  
against the payload ->length.

I've just sent a new hg patch to mutt-dev@
}}}

--------------------------------------------------------------------------------
2010-08-06 17:05:55 UTC Matthias Andree <matthias.andree@gmx.de>
* Added comment:
(In [473fbe29f626]) Unbreak X.509 SubjAltName checks,

regression in 6016:dc09812e63a3 that calls strlen on an SSL sk rather than
its string payload.

closes #3426

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2013-01-17 02:45:38 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
(In [b58cdfacfb89]) change semantics of mutt_is_autoview() to return 1 if there is a matching mailcap entry and either $implicit_autoview is set or the MIME type matches the user's auto_view list.

closes #3496
closes #3426
