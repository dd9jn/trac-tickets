Ticket:  3719
Status:  closed
Summary: Segmentation fault

Reporter: sriharimanoj
Owner:    sriharimanoj@gmail.com

Opened:       2014-12-10 23:51:05 UTC
Last Updated: 2015-08-17 21:57:44 UTC

Priority:  critical
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,

I'm getting the below error while I try to send mail using mutt in my indimail server which runs with qmail as its base. It will be great if some one gets me out of this.

Error sending message, child exited 111 ().
Segmentation fault

Note: Have attached the screen-shot FYR

Regards,

R.Sriharimanoj.

--------------------------------------------------------------------------------
2014-12-10 23:51:26 UTC sriharimanoj
* Added attachment 53.png

--------------------------------------------------------------------------------
2015-02-18 12:28:57 UTC Lekensteyn
* Added comment:
This picture only tells that a member of a NULL pointer got dereferenced. Can you attach a gdb backtrace with debugging symbols installed?

--------------------------------------------------------------------------------
2015-08-17 21:57:44 UTC kevin8t8
* resolution changed to invalid
* status changed to closed
