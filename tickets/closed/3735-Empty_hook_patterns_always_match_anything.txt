Ticket:  3735
Status:  closed
Summary: Empty hook patterns always match anything

Reporter: Lekensteyn
Owner:    mutt-dev

Opened:       2015-02-18 12:33:13 UTC
Last Updated: 2015-07-29 16:13:46 UTC

Priority:  major
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
When a hook pattern expands to an empty value, it will always trigger the action. This is quite unexpected and the proposed patch reject such patterns and informs the user.

--------------------------------------------------------------------------------
2015-02-18 12:33:22 UTC Lekensteyn
* Added attachment reject-empty-hook-patterns.patch

--------------------------------------------------------------------------------
2015-07-29 16:12:31 UTC kevin8t8
* Added comment:
I'm committing a patch which gives an error for mailbox expansion that results in an empty regexp, and also for mistakenly using the '^' mailbox shortcut in the .muttrc.

It's not quite the exact same as your patch, but covers the same basic trigger due to mistaken mailbox expansion.

--------------------------------------------------------------------------------
2015-07-29 16:13:46 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [75e398daa94c588cb94362abd4aca8278b6cbfb6]:
{{{
#!CommitTicketReference repository="" revision="75e398daa94c588cb94362abd4aca8278b6cbfb6"
Add error handling for ^ and other empty mailbox shortcuts.

(closes #2402) (closes #3735)

Explicitly mention the ^ example in the documentation added in 6d733cab6b45.

Add an error message for ^ when CurrentFolder is not set.  Add checks
for other mailbox shortcuts that expand to the empty string.  This
could happen if the @alias shortcut was accidentally used, or the value
referenced by a shortcut isn't set yet.
}}}

* resolution changed to fixed
* status changed to closed
