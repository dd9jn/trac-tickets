Ticket:  2808
Status:  closed
Summary: changed / added %-expansion in save-hook

Reporter: utcke+mutt@informatik.uni-hamburg.de
Owner:    mutt-dev

Opened:       2007-03-01 09:28:13 UTC
Last Updated: 2007-09-11 08:28:33 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
when upgrading from 1.4.1 to 1.5.13 (mostly so that I would get header caching with IMAP) I noticed that the following line now triggers an error:

  fcc-save-hook karsten%phifo@germany.eu.net =karsten.ottenberg

  Error in /home/utcke/.mutt/fcc, line 132: error in pattern at: karsten%phifo@
germany.eu.net

However, the man-page makes no special mention of '%' as an active character in patterns, and the above is a valid address according to RFC822.  So either this is a software bug, or a duplicate of 2135 (hard to tell without a documentation of the intended behaviour).
>How-To-Repeat:
:fcc-save-hook karsten%phifo@germany.eu.net=karsten.ottenberg
>Fix:
If this is the intended behaviour, than it should be documented and an escape-character should be specified (neither \% nor %% work).
}}}

--------------------------------------------------------------------------------
2007-03-01 16:09:54 UTC Kyle Wheeler <kyle@memoryhole.net>
* Added attachment mutt.doc.patch
* Added comment:
mutt.doc.patch

* Added comment:
{{{
On Thursday, March  1 at 10:28 AM, quoth utcke+mutt@informatik.uni-hamburg.de:
>However, the man-page makes no special mention of '%' as an active 
>character in patterns, and the above is a valid address according to 
>RFC822.  So either this is a software bug, or a duplicate of 2135 
>(hard to tell without a documentation of the intended behaviour).
>>How-To-Repeat:
>:fcc-save-hook karsten%phifo@germany.eu.net=karsten.ottenberg
>>Fix:
>If this is the intended behaviour, than it should be documented and an escape-character should be specified (neither \% nor %% work).

Attached is a patch to CVS HEAD that adds better documentation.

~Kyle
-- 
Well, I've wrestled with reality for over thirty five years, doctor, 
and I'm happy to state I finally won out over it.
                                         -- Jimmy Stewart, in "Harvey"
}}}

--------------------------------------------------------------------------------
2007-09-11 08:28:33 UTC pdmef
* Added comment:
The documentation fix for groups got commmitted in [fa6128cf9cba].

* resolution changed to fixed
* status changed to closed
