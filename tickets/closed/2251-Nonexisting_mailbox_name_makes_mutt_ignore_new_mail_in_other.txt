Ticket:  2251
Status:  closed
Summary: Nonexisting mailbox name makes mutt ignore new mail in other mailboxes

Reporter: vlmarek@volny.cz
Owner:    mutt-dev

Opened:       2006-05-29 10:29:32 UTC
Last Updated: 2006-08-08 23:32:10 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
If you have nonexisting folder in your mailboxes in muttrc, mutt ignores new mail in all other mailboxes. For example (let's say that folder non-existing does not exist in imap):

set spoolfile = imap://vm156888@mail-emea.sun.com
mailboxes +INBOX +opn1-rpe-telco +x25-cte +non-existing

Now let's say there's new mail in +x25-cte. If you press 'c' (like for changing folder), then ' ' (space for suggestion of folder with new mail), nothing will be suggested to you. However the log attached shows that mutt still polls the mailboxes. In the log, the bt2-bd2-support mailinglist does not exist. You can see

...
a0028 STATUS "bt2-bd2-support" (UIDNEXT UIDVALIDITY UNSEEN)
...
< a0028 NO Mailbox does not exist
imap_exec: command failed: a0028 NO Mailbox does not exist
Error polling mailboxesmail_addr_is_user: no, all failed.
...

For other types of mailboxes, suggestion still works even if there are nonexisting folders in "mailboxes ..." line.
>How-To-Repeat:
Make mutt use imap. Make sure that "mailboxes ..." entry in your muttrc works (that means, if you press 'c' and ' ' that it suggests you mailbox with new mail). Now add to muttrc line "mailboxes +non-existing". Now mutt does not suggest anything.
>Fix:
Remove non-existing folder from muttrc.
}}}

--------------------------------------------------------------------------------
2006-08-09 17:32:10 UTC brendan
* Added comment:
{{{
Fixed in CVS, thanks.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:22 UTC 
* Added attachment muttdebug0
* Added comment:
muttdebug0
