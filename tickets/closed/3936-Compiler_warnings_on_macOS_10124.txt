Ticket:  3936
Status:  closed
Summary: Compiler warnings on macOS 10.12.4

Reporter: chdiza
Owner:    mutt-dev

Opened:       2017-04-28 14:39:04 UTC
Last Updated: 2017-04-30 23:09:57 UTC

Priority:  major
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
When building mutt from hg tip on macOS 10.12.4 and Xcode 8.3.2, I see these two compiler warnings fly by.  (Everything builds fine, though.)

{{{
gcc -DPKGDATADIR=\"/usr/local/share/mutt\" -DSYSCONFDIR=\"/usr/local/etc\" -DBINDIR=\"/usr/local/bin\" -DMUTTLOCALEDIR=\"/usr/local/share/locale\" -DHAVE_CONFIG_H=1 -I.  -I. -I. -I./imap  -Iintl -I/usr/local/ssl/include -I./intl  -Wall -pedantic -Wno-long-long -g -O2 -MT getdomain.o -MD -MP -MF .deps/getdomain.Tpo -c -o getdomain.o getdomain.c
init.c:944:12: warning: unused function 'parse_path_list' [-Wunused-function]
static int parse_path_list (BUFFER *buf, BUFFER *s, unsigned long data, ...
           ^
init.c:960:12: warning: unused function 'parse_path_unlist' [-Wunused-function]
static int parse_path_unlist (BUFFER *buf, BUFFER *s, unsigned long data...
           ^
}}}

--------------------------------------------------------------------------------
2017-04-28 16:37:06 UTC kevin8t8
* Added comment:
Yes, when it's built without the sidebar, those two functions are not used.
The two functions are not "sidebar specific" though, so I'm not inclined to put #ifdefs around them.

I'll try to see if there is some other way to squelsh the warnings.

--------------------------------------------------------------------------------
2017-04-28 21:17:15 UTC derekmartin
* Added comment:
You can remove the static keyword.  Or disable -Wunused-function, possibly for only that file if you want to be clever. All the other ways I can think of will just generate other warnings that need to be squelched.

--------------------------------------------------------------------------------
2017-04-28 21:22:59 UTC derekmartin
* Added comment:
FWIW though, since the functions are static (only visible in file scope) and are not used when the sidebar is enabled, it does seem that in actuality, they really *are* sidebar-specific. =8^)

--------------------------------------------------------------------------------
2017-04-30 21:27:15 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"c54ac874a32bc3a85f724b9475bc21db12d12caf" 7027:c54ac874a32b]:
{{{
#!CommitTicketReference repository="" revision="c54ac874a32bc3a85f724b9475bc21db12d12caf"
Fix unused function warnings when sidebar is disabled. (closes #3936)

parse_path_list/unlist are currently only used by the
un/sidebar_whitelist commands.  Add an ifdef around them to stop an
unused function warning.  Add a comment too, so it's clear why they
are ifdef'ed.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2017-04-30 22:02:44 UTC chdiza
* Added comment:
Unfortunately I can't test to make sure this works, because fa1192803257 broke building on macOS.  I just opened #3937 for this.

--------------------------------------------------------------------------------
2017-04-30 23:09:57 UTC chdiza
* Added comment:
I can confirm that this warning has now gone away.  Thanks!
