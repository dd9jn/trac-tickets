Ticket:  310
Status:  closed
Summary: flea ought to find sourced files in the muttrc

Reporter: Larry Rosenman <ler@lerctr.org>
Owner:    mutt-dev

Opened:       2000-10-16 16:42:25 UTC
Last Updated: 2006-09-21 23:16:14 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.10i
Severity: wishlist

-- Please type your report below this line

When flea is getting the muttrc file, it ought to follow the chain, and
send ALL of the sourced files.  This would prevent asking for them later.





-- Mutt Version Information

Mutt 1.3.10i (2000-10-11)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: UnixWare 5
Compile options:
DOMAIN="lerctr.org"
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  +ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/etc/mail/sendmail"
MAILPATH="/var/mail"
SHAREDIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /home/ler/.mutt/muttrc
  source ~ler/.mutt/rc.forall
  source ~ler/.mutt/gpg.rc
--- End /home/ler/.mutt/muttrc


--- Begin /usr/local/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /usr/local/etc/Muttrc


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-04 21:08:48 UTC brendan
* Added comment:
{{{
People often keep confidential settings in separate rc files sourced from the 
main .muttrc, so this could be a security problem.
}}}

* resolution changed to wontfix
* status changed to closed

--------------------------------------------------------------------------------
2006-09-22 17:16:14 UTC paul
* Added comment:
{{{
As Brendan says, this sounds like it has a high chance of
transmitting info people don't want sending. (Besides, flea
doesn't exist any more.)
}}}

* resolution changed to fixed
* status changed to closed
