Ticket:  3817
Status:  closed
Summary: [PATCH] Fedora: Fix crash in IMAP

Reporter: flatcap
Owner:    brendan

Opened:       2016-03-19 16:00:16 UTC
Last Updated: 2016-03-27 19:25:53 UTC

Priority:  major
Component: IMAP
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Downstream patch.

Fedora adds this patch to fix a crash in sync_helper() (imap/imap.c)

I can't find a matching bugzilla report, but the patch is can do no harm.

--------------------------------------------------------------------------------
2016-03-19 16:00:31 UTC flatcap
* Added attachment fedora2.patch

--------------------------------------------------------------------------------
2016-03-19 16:04:13 UTC flatcap
* keywords changed to patch

--------------------------------------------------------------------------------
2016-03-24 04:41:51 UTC brendan
* Added comment:
I don't think this patch is quite correct, since it converts an error condition (idata->ctx is null) into a successful result. It should at least return -1 if idata->ctx is null (and the caller should be handling that better than it currently is).

--------------------------------------------------------------------------------
2016-03-24 21:27:18 UTC kevin8t8
* Added comment:
Here's an alternative patch.  It returns -1 from sync_helper() if the ctx is NULL (mailbox closed somehow).  It adds missing checks for the sync_helper() return value.  We don't really need the sum, so it changes it to use bitwise-OR instead.  Lastly, if the imap_exec below fails, I noticed it wasn't setting rc=-1, so I added that.

--------------------------------------------------------------------------------
2016-03-24 21:27:51 UTC kevin8t8
* Added attachment ticket-3817.patch

--------------------------------------------------------------------------------
2016-03-27 01:48:33 UTC kevin8t8
* Added comment:
Attaching a second version of the patch.  This one uses the existing error handling for imap_exec below (which prompts when ctx->closing is set), and resets the sort order even if sync_helper() has an error.

--------------------------------------------------------------------------------
2016-03-27 01:48:51 UTC kevin8t8
* Added attachment ticket-3817-v2.patch

--------------------------------------------------------------------------------
2016-03-27 19:25:53 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"62e478a3f1c8856b3d4855fa287c8212e805e2a9"]:
{{{
#!CommitTicketReference repository="" revision="62e478a3f1c8856b3d4855fa287c8212e805e2a9"
Fix error handling in sync_helper() and imap_sync_mailbox(). (closes #3817)

This patch is based on the one Richard Russon found in the Fedora package.

If an error occurs during one of the imap_exec() calls in
imap_sync_mailbox(), the mailbox could end up being closed.  This
would cause idata->ctx to be NULL.  Add a check in sync_helper() for
the case where idata->ctx == NULL.

In imap_sync_mailbox(), check the return value of sync_helper().  To
keep the code simple, change rc from being the sum of the calls to the
bitwise-OR of the calls.  (We only need to know if a single flag needs
to be updated, and bitwise-OR will detect negatives.)

Below the calls to sync_helper(), if the call to imap_exec() fails,
make sure rc is set to -1.
}}}

* resolution changed to fixed
* status changed to closed
