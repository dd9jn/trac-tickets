Ticket:  3350
Status:  closed
Summary: inconsistent handling of key bindings/codes

Reporter: trespassing
Owner:    mutt-dev

Opened:       2009-11-04 09:02:12 UTC
Last Updated: 2010-04-04 01:36:51 UTC

Priority:  trivial
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
It is not possible to bind anything to ctrl+up or shift+up und such.

what-key reports those keys as simple Letters (eg. ctrl+up = 'A')
Binding to those letters won't work (so mutt *can* distinguish those
from the regular letters :-)

Two exceptions: It is possible to bind to shift+left/right which
are being reported as <F129> / <F128>

(I am using 1.5.18/debian lenny/i386)  + sidebar patch)

--------------------------------------------------------------------------------
2010-04-04 00:48:59 UTC me
* Added comment:
The problem is that the ncurses library doesn't have a keycode for ctrl+<UP> or ctrl+<DOWN>.  Instead you get a multibyte escape sequence.  The following seemed to work in gnome-terminal/screen:

bind index "\e[1;5A" previous-thread # ctrl+UP
bind index "\e[1;5B" next-thread        # ctrl+DOWN

* priority changed to trivial
* resolution changed to wontfix
* status changed to closed

--------------------------------------------------------------------------------
2010-04-04 01:36:51 UTC Thomas Dickey
* Added comment:
{{{
On Sun, 4 Apr 2010, Mutt wrote:


That's an extension (mutt could in principle ask ncurses for the binding
of kUP5 and kDN5).  Use "infocmp -x" to see some of the possibilities.
}}}
