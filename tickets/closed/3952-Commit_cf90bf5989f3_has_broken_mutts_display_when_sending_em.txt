Ticket:  3952
Status:  closed
Summary: Commit cf90bf5989f3 has broken mutt's display when sending email.

Reporter: chdiza
Owner:    mutt-dev

Opened:       2017-06-09 21:26:01 UTC
Last Updated: 2017-06-13 01:31:15 UTC

Priority:  major
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
Commit cf90bf5989f3 has broken mutt's display when sending email.

Prior to this commit, when I hit Ctrl-x to send an email, I see "Uploading message..." at the bottom, and then when that succeeds I see "Mail sent".  At no point does mutt leave the compose menu.

But after this commit, when I hit Ctrl-x to send an email, the compose menu vanishes and is replaced by what my terminal had been showing prior to starting mutt in the first place.  I see no "Uploading message..." message, or any evidence that I'm even still in mutt.  Then, once the mail is sent, mutt gains control over the screen and I can see the "Mail sent" message.

This is on macOS 10.12.5 with iTerm2.app.  It also happens in Terminal.app

I send my emails with:

set sendmail  = '/usr/local/bin/msmtp'

--------------------------------------------------------------------------------
2017-06-12 20:10:56 UTC kevin8t8
* Added comment:
This was a bad effect from #3951.  I forgot about the visual effect for msmtp users, and I predict it will make many people unhappy.

I will change the fix for #3951 to do a hard redraw instead.  

--------------------------------------------------------------------------------
2017-06-13 01:31:15 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"0e3730ed2c3cef32f18402b8725e2bbc4eb367ae" 7085:0e3730ed2c3c]:
{{{
#!CommitTicketReference repository="" revision="0e3730ed2c3cef32f18402b8725e2bbc4eb367ae"
Force hard redraw after $sendmail instead of calling mutt_endwin. (closes #3952) (see #3948)

Adding a mutt_endwin() seemed like a clean solution to allowing
ncurses pinentry for $sendmail, but it leaves other users watching a
blank screen.  This change is extremely likely to generate a large
number of complaints and bug reports.  So instead, force a hard
refresh afterwards.
}}}

* resolution changed to fixed
* status changed to closed
