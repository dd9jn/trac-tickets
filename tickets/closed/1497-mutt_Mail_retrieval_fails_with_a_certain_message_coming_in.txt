Ticket:  1497
Status:  closed
Summary: mutt: Mail retrieval fails with a certain message coming in

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2003-03-16 11:13:37 UTC
Last Updated: 2007-04-03 16:49:32 UTC

Priority:  minor
Component: POP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.28-2
Severity: important

[NOTE: this bug report has been submitted to the debian BTS as Bug#184918.
Please Cc all your replies to 184918@bugs.debian.org .]

From: "Chris Tillman" <tillman@voicetrak.com>
Subject: mutt: Mail retrieval fails with a certain message coming in
Date: Sat, 15 Mar 2003 09:04:32 -0700

This has happened to me a few time in the last month or two, so this
time I'm submitting a bug hoping it can be tracked down.

The symptoms are that I go to retrieve mail, one message is retrieved,
and then there is a beep and no further messages are retrieved. If I
retry, the same message is retrieved again and again.

I ran an strace, which I can send if interested. Here is what I suspect
is the interesting part:

write(1, "\rReading new messages (530\33[10@6"..., 51) = 51
time(NULL)                              = 1047742337
write(4, "RETR 1\r\n", 8)               = 8
read(4, "+OK 13852 octets\r\n", 1024)   = 18
read(4, "Return-path: <fromus0120@nyc.com"..., 1024) = 1024
read(4, "Content-Transfer-Encoding: quote"..., 1024) = 424
read(4, "AN=3D14> <a href=3D\"http://secur"..., 1024) = 1024
read(4, "://www.peternorth.=\r\ncom\" target"..., 1024) = 424
read(4, "dth=3D\"9\" ROWSPAN=3D8> <IMG SRC="..., 1024) = 1024
read(4, " width=3D\"177\" height=3D\"96\" val"..., 1024) = 424
write(5, "From toff Sat Mar 15 08:32:17 20"..., 4096) = 4096
read(4, "/TD>\r\n        </tr>\r\n      </tab"..., 1024) = 1024
write(5, "=\nh=3D\"77\" height=3D\"15\" align=3"..., 303) = 303
fsync(5)                                = 0
write(4, "DELE 1\r\n", 8)               = 8
write(1, "\7", 1)                       = 1
write(1, "\rDELE:     <TD height=3D\"95\"> <I"..., 89) = 89
fcntl64(5, F_SETLK, {type=F_UNLCK, whence=SEEK_SET, start=0, len=0}) = 0
rt_sigaction(SIGINT, {SIG_IGN}, {0x1005effc, [], SA_RESTART}, 8) = 0
rt_sigaction(SIGQUIT, {SIG_IGN}, {0x1005ef9c, [], 0}, 8) = 0
rt_sigprocmask(SIG_BLOCK, [CHLD], NULL, 8) = 0
rt_sigaction(SIGTSTP, {SIG_DFL}, {0x1005effc, [TSTP], SA_RESTART}, 8) = 0
rt_sigaction(SIGCONT, {SIG_DFL}, {0x1005effc, [TSTP], SA_RESTART}, 8) = 0
fork()                                  = 536
rt_sigprocmask(SIG_SETMASK, NULL, [CHLD], 8) = 0
rt_sigaction(SIGALRM, {0x1007e500, [], SA_INTERRUPT}, {0x1005effc, [TSTP], 0}, 8) = 0
alarm(0x384, 0x7fffdae4, 0, 0x8, 0x20)  = 0
wait4(536, [WIFEXITED(s) && WEXITSTATUS(s) == 0], 0, NULL) = 536
alarm(0, 0x7fffdcb8, 0, 0, 0x20)        = 900
rt_sigaction(SIGALRM, {0x1005effc, [TSTP], 0}, NULL, 8) = 0
rt_sigprocmask(SIG_SETMASK, [CHLD], NULL, 8) = 0
rt_sigaction(SIGCONT, {0x1005effc, [TSTP], SA_RESTART}, NULL, 8) = 0
rt_sigaction(SIGTSTP, {0x1005effc, [TSTP], SA_RESTART}, NULL, 8) = 0
rt_sigprocmask(SIG_UNBLOCK, [CHLD], NULL, 8) = 0
--- SIGCHLD (Child exited) ---
rt_sigaction(SIGQUIT, {0x1005ef9c, [], 0}, NULL, 8) = 0
rt_sigaction(SIGINT, {0x1005effc, [], SA_RESTART}, NULL, 8) = 0
close(5)                                = 0
munmap(0x30017000, 4096)                = 0
write(4, "QUIT\r\n", 6)                 = 6
close(4)                                = 0
stat("/var/mail/toff", {st_mode=S_IFREG|0660, st_size=537894, ...}) = 0
rt_sigprocmask(SIG_BLOCK, [HUP INT TERM TSTP WINCH], NULL, 8) = 0
fcntl64(3, F_SETLK, {type=F_RDLCK, whence=SEEK_SET, start=0, len=0}) = 0
rt_sigaction(SIGINT, {SIG_IGN}, {0x1005effc, [], SA_RESTART}, 8) = 0
rt_sigaction(SIGQUIT, {SIG_IGN}, {0x1005ef9c, [], 0}, 8) = 0
rt_sigprocmask(SIG_BLOCK, [CHLD], NULL, 8) = 0
rt_sigaction(SIGTSTP, {SIG_DFL}, {0x1005effc, [TSTP], SA_RESTART}, 8) = 0
rt_sigaction(SIGCONT, {SIG_DFL}, {0x1005effc, [TSTP], SA_RESTART}, 8) = 0
fork()                                  = 537
rt_sigprocmask(SIG_SETMASK, NULL, [HUP INT TERM CHLD TSTP WINCH], 8) = 0
rt_sigaction(SIGALRM, {0x1007e500, [], SA_INTERRUPT}, {0x1005effc, [TSTP], 0}, 8) = 0
alarm(0x384, 0x7fffdab4, 0, 0x8, 0x20)  = 0
wait4(537, [WIFEXITED(s) && WEXITSTATUS(s) == 0], 0, NULL) = 537
alarm(0, 0x7fffdc88, 0, 0, 0x20)        = 900
rt_sigaction(SIGALRM, {0x1005effc, [TSTP], 0}, NULL, 8) = 0
rt_sigprocmask(SIG_SETMASK, [HUP INT TERM CHLD TSTP WINCH], NULL, 8) = 0
rt_sigaction(SIGCONT, {0x1005effc, [TSTP], SA_RESTART}, NULL, 8) = 0
rt_sigaction(SIGTSTP, {0x1005effc, [TSTP], SA_RESTART}, NULL, 8) = 0
rt_sigprocmask(SIG_UNBLOCK, [CHLD], NULL, 8) = 0
--- SIGCHLD (Child exited) ---
rt_sigaction(SIGQUIT, {0x1005ef9c, [], 0}, NULL, 8) = 0
rt_sigaction(SIGINT, {0x1005effc, [], SA_RESTART}, NULL, 8) = 0
_llseek(0x3, 0, 0x82000, 0x7fffea18, 0) = 0
read(3, "D width=3D\"144\" height=3D\"96\" va"..., 1015) = 1015
read(3, "From toff Sat Mar 15 08:32:17 20"..., 4096) = 4096
_llseek(0x3, 0, 0x833f7, 0x7fffea18, 0) = 0
stat("/var/mail/toff", {st_mode=S_IFREG|0660, st_size=537894, ...}) = 0
access("/var/mail/toff", W_OK)          = 0
time(NULL)                              = 1047742338
write(1, "\rReading /var/mail/toff... 1 (99"..., 44) = 44


Not that it means anything to me. I can also send the message that keeps
getting re-retrieved, in its pure form copied from the mail server, if
need be. The mail server is MDaemon. I think there must be some parsing
error or something which causes the DELE statement not to be able to
delete correctly, and stops the process. Actually, looking at the message,
it looks like it's cut off in the middle of an IMG tag. The entire message
on the server is 14k, but only about 4.2k arrives in /var/mail/toff.

I also tried moving aside /var/mail/toff, creating a new one, and that
made no difference; so it's not other stuff in my mailbox.

As soon as I deleted that message at the server, the rest of my mail 
came in fine.

-- System Information
Debian Release: 3.0
Architecture: powerpc
Kernel: Linux iMacBlue 2.4.19-powerpc #1 Mon Sep 9 09:01:43 EDT 2002 ppc
Locale: LANG=C, LC_CTYPE=C

Versions of packages mutt depends on:
ii  libc6                    2.3.1-9         GNU C Library: Shared libraries an
ii  libncurses5              5.2.20020112a-7 Shared libraries for terminal hand
ii  libsasl7                 1.5.27-3        Authentication abstraction library
ii  ssmtp [mail-transport-ag 2.50.6          Extremely simple MTA to get mail o

-- 
"The way the Romans made sure their bridges worked is what 
we should do with software engineers. They put the designer 
under the bridge, and then they marched over it." 
-- Lawrence Bernstein, Discover, Feb 2003



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-03-31 23:15:18 UTC brendan
* component changed to POP
* Updated description:
{{{
Package: mutt
Version: 1.3.28-2
Severity: important

[NOTE: this bug report has been submitted to the debian BTS as Bug#184918.
Please Cc all your replies to 184918@bugs.debian.org .]

From: "Chris Tillman" <tillman@voicetrak.com>
Subject: mutt: Mail retrieval fails with a certain message coming in
Date: Sat, 15 Mar 2003 09:04:32 -0700

This has happened to me a few time in the last month or two, so this
time I'm submitting a bug hoping it can be tracked down.

The symptoms are that I go to retrieve mail, one message is retrieved,
and then there is a beep and no further messages are retrieved. If I
retry, the same message is retrieved again and again.

I ran an strace, which I can send if interested. Here is what I suspect
is the interesting part:

write(1, "\rReading new messages (530\33[10@6"..., 51) = 51
time(NULL)                              = 1047742337
write(4, "RETR 1\r\n", 8)               = 8
read(4, "+OK 13852 octets\r\n", 1024)   = 18
read(4, "Return-path: <fromus0120@nyc.com"..., 1024) = 1024
read(4, "Content-Transfer-Encoding: quote"..., 1024) = 424
read(4, "AN=3D14> <a href=3D\"http://secur"..., 1024) = 1024
read(4, "://www.peternorth.=\r\ncom\" target"..., 1024) = 424
read(4, "dth=3D\"9\" ROWSPAN=3D8> <IMG SRC="..., 1024) = 1024
read(4, " width=3D\"177\" height=3D\"96\" val"..., 1024) = 424
write(5, "From toff Sat Mar 15 08:32:17 20"..., 4096) = 4096
read(4, "/TD>\r\n        </tr>\r\n      </tab"..., 1024) = 1024
write(5, "=\nh=3D\"77\" height=3D\"15\" align=3"..., 303) = 303
fsync(5)                                = 0
write(4, "DELE 1\r\n", 8)               = 8
write(1, "\7", 1)                       = 1
write(1, "\rDELE:     <TD height=3D\"95\"> <I"..., 89) = 89
fcntl64(5, F_SETLK, {type=F_UNLCK, whence=SEEK_SET, start=0, len=0}) = 0
rt_sigaction(SIGINT, {SIG_IGN}, {0x1005effc, [], SA_RESTART}, 8) = 0
rt_sigaction(SIGQUIT, {SIG_IGN}, {0x1005ef9c, [], 0}, 8) = 0
rt_sigprocmask(SIG_BLOCK, [CHLD], NULL, 8) = 0
rt_sigaction(SIGTSTP, {SIG_DFL}, {0x1005effc, [TSTP], SA_RESTART}, 8) = 0
rt_sigaction(SIGCONT, {SIG_DFL}, {0x1005effc, [TSTP], SA_RESTART}, 8) = 0
fork()                                  = 536
rt_sigprocmask(SIG_SETMASK, NULL, [CHLD], 8) = 0
rt_sigaction(SIGALRM, {0x1007e500, [], SA_INTERRUPT}, {0x1005effc, [TSTP], 0}, 8) = 0
alarm(0x384, 0x7fffdae4, 0, 0x8, 0x20)  = 0
wait4(536, [WIFEXITED(s) && WEXITSTATUS(s) == 0], 0, NULL) = 536
alarm(0, 0x7fffdcb8, 0, 0, 0x20)        = 900
rt_sigaction(SIGALRM, {0x1005effc, [TSTP], 0}, NULL, 8) = 0
rt_sigprocmask(SIG_SETMASK, [CHLD], NULL, 8) = 0
rt_sigaction(SIGCONT, {0x1005effc, [TSTP], SA_RESTART}, NULL, 8) = 0
rt_sigaction(SIGTSTP, {0x1005effc, [TSTP], SA_RESTART}, NULL, 8) = 0
rt_sigprocmask(SIG_UNBLOCK, [CHLD], NULL, 8) = 0
--- SIGCHLD (Child exited) ---
rt_sigaction(SIGQUIT, {0x1005ef9c, [], 0}, NULL, 8) = 0
rt_sigaction(SIGINT, {0x1005effc, [], SA_RESTART}, NULL, 8) = 0
close(5)                                = 0
munmap(0x30017000, 4096)                = 0
write(4, "QUIT\r\n", 6)                 = 6
close(4)                                = 0
stat("/var/mail/toff", {st_mode=S_IFREG|0660, st_size=537894, ...}) = 0
rt_sigprocmask(SIG_BLOCK, [HUP INT TERM TSTP WINCH], NULL, 8) = 0
fcntl64(3, F_SETLK, {type=F_RDLCK, whence=SEEK_SET, start=0, len=0}) = 0
rt_sigaction(SIGINT, {SIG_IGN}, {0x1005effc, [], SA_RESTART}, 8) = 0
rt_sigaction(SIGQUIT, {SIG_IGN}, {0x1005ef9c, [], 0}, 8) = 0
rt_sigprocmask(SIG_BLOCK, [CHLD], NULL, 8) = 0
rt_sigaction(SIGTSTP, {SIG_DFL}, {0x1005effc, [TSTP], SA_RESTART}, 8) = 0
rt_sigaction(SIGCONT, {SIG_DFL}, {0x1005effc, [TSTP], SA_RESTART}, 8) = 0
fork()                                  = 537
rt_sigprocmask(SIG_SETMASK, NULL, [HUP INT TERM CHLD TSTP WINCH], 8) = 0
rt_sigaction(SIGALRM, {0x1007e500, [], SA_INTERRUPT}, {0x1005effc, [TSTP], 0}, 8) = 0
alarm(0x384, 0x7fffdab4, 0, 0x8, 0x20)  = 0
wait4(537, [WIFEXITED(s) && WEXITSTATUS(s) == 0], 0, NULL) = 537
alarm(0, 0x7fffdc88, 0, 0, 0x20)        = 900
rt_sigaction(SIGALRM, {0x1005effc, [TSTP], 0}, NULL, 8) = 0
rt_sigprocmask(SIG_SETMASK, [HUP INT TERM CHLD TSTP WINCH], NULL, 8) = 0
rt_sigaction(SIGCONT, {0x1005effc, [TSTP], SA_RESTART}, NULL, 8) = 0
rt_sigaction(SIGTSTP, {0x1005effc, [TSTP], SA_RESTART}, NULL, 8) = 0
rt_sigprocmask(SIG_UNBLOCK, [CHLD], NULL, 8) = 0
--- SIGCHLD (Child exited) ---
rt_sigaction(SIGQUIT, {0x1005ef9c, [], 0}, NULL, 8) = 0
rt_sigaction(SIGINT, {0x1005effc, [], SA_RESTART}, NULL, 8) = 0
_llseek(0x3, 0, 0x82000, 0x7fffea18, 0) = 0
read(3, "D width=3D\"144\" height=3D\"96\" va"..., 1015) = 1015
read(3, "From toff Sat Mar 15 08:32:17 20"..., 4096) = 4096
_llseek(0x3, 0, 0x833f7, 0x7fffea18, 0) = 0
stat("/var/mail/toff", {st_mode=S_IFREG|0660, st_size=537894, ...}) = 0
access("/var/mail/toff", W_OK)          = 0
time(NULL)                              = 1047742338
write(1, "\rReading /var/mail/toff... 1 (99"..., 44) = 44


Not that it means anything to me. I can also send the message that keeps
getting re-retrieved, in its pure form copied from the mail server, if
need be. The mail server is MDaemon. I think there must be some parsing
error or something which causes the DELE statement not to be able to
delete correctly, and stops the process. Actually, looking at the message,
it looks like it's cut off in the middle of an IMG tag. The entire message
on the server is 14k, but only about 4.2k arrives in /var/mail/toff.

I also tried moving aside /var/mail/toff, creating a new one, and that
made no difference; so it's not other stuff in my mailbox.

As soon as I deleted that message at the server, the rest of my mail 
came in fine.

-- System Information
Debian Release: 3.0
Architecture: powerpc
Kernel: Linux iMacBlue 2.4.19-powerpc #1 Mon Sep 9 09:01:43 EDT 2002 ppc
Locale: LANG=C, LC_CTYPE=C

Versions of packages mutt depends on:
ii  libc6                    2.3.1-9         GNU C Library: Shared libraries an
ii  libncurses5              5.2.20020112a-7 Shared libraries for terminal hand
ii  libsasl7                 1.5.27-3        Authentication abstraction library
ii  ssmtp [mail-transport-ag 2.50.6          Extremely simple MTA to get mail o

-- 
"The way the Romans made sure their bridges worked is what 
we should do with software engineers. They put the designer 
under the bridge, and then they marched over it." 
-- Lawrence Bernstein, Discover, Feb 2003



>How-To-Repeat:
>Fix:
}}}
* keywords changed to 

--------------------------------------------------------------------------------
2007-04-03 16:49:32 UTC myon
* Added comment:
The corresponding Debian bug was closed ages ago. I also vaguely remember a semi-recent fix for messages containing NUL bytes (though I can't find it in the changelog.) Closing.

* resolution changed to fixed
* status changed to closed
