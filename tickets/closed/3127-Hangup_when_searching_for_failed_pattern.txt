Ticket:  3127
Status:  closed
Summary: Hangup when searching for failed pattern

Reporter: vinc17
Owner:    mutt-dev

Opened:       2008-10-14 14:01:16 UTC
Last Updated: 2008-10-30 03:51:38 UTC

Priority:  major
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
With Mutt from trunk (and no patches):

1. View a mail message.

2. Search for a word that doesn't exist (e.g. type "/blahblah"[RET]). One gets a "Not found." message.

3. Search for the same word (type "/"[RET]). One gets a "Search wrapped to top." message and Mutt takes 100% CPU. Ctrl-C has no effect.

Note: I can't reproduce this problem with Debian's package mutt 1.5.18-4. So, either this bug has been added recently or Debian has a fix.

--------------------------------------------------------------------------------
2008-10-17 16:15:35 UTC blacktrash
* Added comment:
Replying to [ticket:3127 vinc17]:
> With Mutt from trunk (and no patches):
> 
> 1. View a mail message.
> 
> 2. Search for a word that doesn't exist (e.g. type "/blahblah"[RET]). One gets a "Not found." message.
> 
> 3. Search for the same word (type "/"[RET]). One gets a "Search wrapped to top." message and Mutt takes 100% CPU. Ctrl-C has no effect.

FWIW, I can confirm this with latest tip.

--------------------------------------------------------------------------------
2008-10-18 23:11:23 UTC TAKAHASHI Tamotsu
* Added comment:
{{{
* Tue Oct 14 2008 Mutt <fleas@mutt.org>

Apparently b6d07a662c7f is the cause of your infinite loop.

changeset:   5525:b6d07a662c7f
branch:      HEAD
user:        Rocco Rutte <pdmef@gmx.net>
date:        Sun Aug 31 23:36:36 2008 +0200
summary:     Fix pager to respect $wrap_search
}}}

--------------------------------------------------------------------------------
2008-10-19 04:31:02 UTC TAKAHASHI Tamotsu
* Added comment:
{{{
Perhaps this is what Rocco meant:

diff -r 10a1f06bc8aa pager.c
--- a/pager.c	Tue Oct 07 19:22:53 2008 -0700
+++ b/pager.c	Sun Oct 19 13:19:48 2008 +0900
@@ -1511,7 +1511,7 @@
   struct q_class_t *QuoteList = NULL;
   int i, j, ch = 0, rc = -1, hideQuoted = 0, q_level = 0, force_redraw = 0;
   int lines = 0, curline = 0, topline = 0, oldtopline = 0, err, first = 1;
-  int r = -1;
+  int r = -1, wrapped = 0;
   int redraw = REDRAW_FULL;
   FILE *fp = NULL;
   LOFF_T last_pos = 0, last_offset = 0;
@@ -1967,12 +1967,14 @@
       case OP_SEARCH_OPPOSITE:
 	if (SearchCompiled)
 	{
+	  wrapped = 0;
+
 search_next:
 	  if ((!SearchBack && ch==OP_SEARCH_NEXT) ||
 	      (SearchBack &&ch==OP_SEARCH_OPPOSITE))
 	  {
 	    /* searching forward */
-	    for (i = topline + 1; i < lastLine; i++)
+	    for (i = wrapped ? 0 : topline + 1; i < lastLine; i++)
 	    {
 	      if ((!hideQuoted || lineInfo[i].type != MT_COLOR_QUOTED) && 
 		    !lineInfo[i].continuation && lineInfo[i].search_cnt > 0)
@@ -1981,19 +1983,19 @@
 
 	    if (i < lastLine)
 	      topline = i;
-	    else if (!option (OPTWRAPSEARCH))
+	    else if (wrapped || !option (OPTWRAPSEARCH))
 	      mutt_error _("Not found.");
 	    else
 	    {
 	      mutt_message _("Search wrapped to top.");
-	      topline = 1;
+	      wrapped = 1;
 	      goto search_next;
 	    }
 	  }
 	  else
 	  {
 	    /* searching backward */
-	    for (i = topline - 1; i >= 0; i--)
+	    for (i = wrapped ? lastLine : topline - 1; i >= 0; i--)
 	    {
 	      if ((!hideQuoted || (has_types && 
 		    lineInfo[i].type != MT_COLOR_QUOTED)) && 
@@ -2003,12 +2005,12 @@
 
 	    if (i >= 0)
 	      topline = i;
-	    else if (!option (OPTWRAPSEARCH))
+	    else if (wrapped || !option (OPTWRAPSEARCH))
 	      mutt_error _("Not found.");
 	    else
 	    {
 	      mutt_message _("Search wrapped to bottom.");
-	      topline = lastLine - 1;
+	      wrapped = 1;
 	      goto search_next;
 	    }
 	  }
@@ -2038,6 +2040,7 @@
 	    else
 	      ch = OP_SEARCH_OPPOSITE;
 
+	    wrapped = 0;
 	    goto search_next;
 	  }
 	}
}}}

--------------------------------------------------------------------------------
2008-10-27 13:20:10 UTC tamo
* Added attachment pager.diff
* Added comment:
A diff to restart the search from the top (of message) if it is called twice. (It fails with "Not found" at first.) Avoiding infinite loop.

--------------------------------------------------------------------------------
2008-10-27 13:20:46 UTC tamo
* keywords changed to patch

--------------------------------------------------------------------------------
2008-10-27 22:41:44 UTC vinc17
* Added comment:
I confirm that this patch solves the problem. Thanks.

--------------------------------------------------------------------------------
2008-10-29 16:53:34 UTC blacktrash
* Added comment:
Replying to [comment:4 tamo]:

Can confirm as well that the patch solves the problem. Thank you tamo.

--------------------------------------------------------------------------------
2008-10-30 03:51:38 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
(In [c2439fc68cd6]) Restart pager search from top if called twice. Do not loop infinitely.
Closes #3127.

* resolution changed to fixed
* status changed to closed
