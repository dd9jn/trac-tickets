Ticket:  2135
Status:  closed
Summary: % expansion in hooks needs documentation

Reporter: rado@math.uni-hamburg.de
Owner:    mutt-dev

Opened:       2005-11-16 19:15:41 UTC
Last Updated: 2007-11-20 18:47:07 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Obviously some %-codes are being expanded to useful value when
specified in hooks, but this is nowhere documented.

Many people in dire need of the cool stuff can't learn about it.
>How-To-Repeat:
>Fix:
Unknown since I have no idea where this comes from,
i.e. which codes work and when they do (are eval'ed).
}}}

--------------------------------------------------------------------------------
2007-11-20 18:47:07 UTC pdmef
* Added comment:
(In [96f931ae0b22]) Document that save-hook/fcc-hook/fcc-save-hook use $index_format expandos. Closes #2135.

* resolution changed to fixed
* status changed to closed
