Ticket:  3219
Status:  closed
Summary: send-hooks don't work in batch mode

Reporter: agriffis
Owner:    mutt-dev

Opened:       2009-04-21 03:07:03 UTC
Last Updated: 2009-07-15 03:25:17 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
As far as I can tell, it's a long-standing bug that send-hooks don't work in batch mode.  I sure would like to see this patch applied since my setup depends on it.

--------------------------------------------------------------------------------
2009-04-21 03:09:12 UTC agriffis
* Added attachment run-sendhooks-for-batch

--------------------------------------------------------------------------------
2009-04-21 03:13:47 UTC agriffis
* Added comment:
This patch was discussed on the ML to generally favorable response.  See http://thread.gmane.org/gmane.mail.mutt.devel/15723

--------------------------------------------------------------------------------
2009-07-15 03:25:17 UTC Aron Griffis <agriffis@n01se.net>
* Added comment:
(In [084fb086a0e7]) Make send-hooks work for batch mode. Closes #3219.

Handle SENDBATCH inline with the normal sending code, instead of handling it
separately.  This allows send-hooks to run, along with removing a number of
unnecessarily lines of code.

* resolution changed to fixed
* status changed to closed
