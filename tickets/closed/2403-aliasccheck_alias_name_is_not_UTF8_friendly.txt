Ticket:  2403
Status:  closed
Summary: alias.c:check_alias_name() is not UTF-8 friendly

Reporter: Alain Bench <veronatif@free.fr>
Owner:    mutt-dev

Opened:       2006-08-03 12:42:31 UTC
Last Updated: 2008-05-15 15:42:46 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{

My dear ALL,

    In an UTF-8 environment, entering an alphanumeric alias name at the
<create-alias> prompt suffers from an unexpected interference:

| Alias as: RenéClerc
| Warning: This alias name may not work.  Fix it? ([yes]/no): yes
| Alias as: Ren__Clerc

    The "fix" does replace the 2 bytes constituting René's e acute by an
underscore. Note that if I refuse the fix (replying no), such accented
alias name seemingly works everywhere: Expansion, completion, Aliases
menu, $reverse_alias, save, source... saved within single quotes though:

| alias 'RenéClerc' René Clerc <rene@example.com>

    In a Latin-1 environment, there is no fix proposal, such accented
alias also works everywhere, and saving it does not add single quotes.


Bye!    Alain.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2008-05-15 15:42:46 UTC pdmef
* Added comment:
(In [132474d6dd3b]) Make mutt_check_alias_name() multibyte-aware (closes #2403).

* resolution changed to fixed
* status changed to closed
