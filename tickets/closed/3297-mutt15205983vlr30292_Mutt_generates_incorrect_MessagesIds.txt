Ticket:  3297
Status:  closed
Summary: mutt-1.5.20-5983-vl-r30292: Mutt generates incorrect Messages-Ids

Reporter: vinc17
Owner:    mutt-dev

Opened:       2009-07-20 13:23:01 UTC
Last Updated: 2009-07-20 18:07:11 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{
Package: mutt
Version: 1.5.20-2
Severity: important

-- Please type your report below this line

Mutt generates incorrect Message-Id's such as:

  Message-ID: <20090712205905.GA7803@xvii.home>
  Message-ID: <20090720083128.GA6861@xvii>

Mutt should use the FQDN, which is xvii.vinc17.org on my machine.
For instance, the Perl script

------------------------------------------------------------
#!/usr/bin/env perl

use strict;
use POSIX;

my $nodename = (POSIX::uname)[1];
print "Nodename: $nodename\n";
print "FQDN: ", (gethostbyname $nodename)[0], "\n";
------------------------------------------------------------

gives:

xvii% fqdn
Nodename: xvii
FQDN: xvii.vinc17.org

Note: "mutt -D | grep hostname" gives:

  hostname="@"

(ditto if I add "-F /dev/null"). This is strange because I don't
set $hostname and the manual says that the default value is the
empty string.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Using built-in specs.
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Debian 4.3.3-14' --with-bugurl=file:///usr/share/doc/gcc-4.3/README.Bugs --enable-languages=c,c++,fortran,objc,obj-c++ --prefix=/usr --enable-shared --enable-multiarch --enable-linker-build-id --with-system-zlib --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --enable-nls --with-gxx-include-dir=/usr/include/c++/4.3 --program-suffix=-4.3 --enable-clocale=gnu --enable-libstdcxx-debug --enable-objc-gc --enable-mpfr --with-tune=generic --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 4.3.3 (Debian 4.3.3-14) 

- CFLAGS
-Wall -pedantic -Wno-long-long -g -O2

-- Mutt Version Information

Mutt 1.5.20-5983-vl-r30292 (2009-07-14)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.30-1-amd64 (x86_64)
ncurses: ncurses 5.7.20090613 (compiled with 5.7)
libidn: 1.15 (compiled with 1.15)
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_IMAP  -USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH=""
PKGDATADIR="/home/vinc17/share/mutt"
SYSCONFDIR="/home/vinc17/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.20hg.pdmef.progress.vl.1
patch-1.5.20hg.tamo.namequot.vl.1
patch-1.5.20hg.tamovl.patterns.1
patch-1.5.15hg.cd.trash_folder.vl.1
patch-1.5.20hg.jlh-vl.parentchildmatch.2

--- Begin /home/vinc17/etc/Muttrc
ignore *
unignore from: subject to cc date x-mailer x-url user-agent
hdr_order date from to cc subject
macro index \eb "<search>~b " "search in message bodies"
macro index,pager,attach,compose \cb "\
<enter-command> set my_pipe_decode=\$pipe_decode pipe_decode<Enter>\
<pipe-message> urlview<Enter>\
<enter-command> set pipe_decode=\$my_pipe_decode; unset my_pipe_decode<Enter>" \
"call urlview to extract URLs out of a message"
macro generic,pager <F1> "<shell-escape> less /home/vinc17/share/doc/mutt/manual.txt<Enter>" "show Mutt documentation"
macro index,pager y "<change-folder>?<toggle-mailboxes>" "show incoming mailboxes list"
bind browser y exit
mime_lookup application/octet-stream
attachments   +A */.*
attachments   -A text/x-vcard application/pgp.*
attachments   -A application/x-pkcs7-.*
attachments   +I text/plain
attachments   -A message/external-body
attachments   -I message/external-body
--- End /home/vinc17/etc/Muttrc
}}}

--------------------------------------------------------------------------------
2009-07-20 13:23:01 UTC 
* Added comment:

This message has 0 attachment(s)


* id changed to 3297

--------------------------------------------------------------------------------
2009-07-20 18:06:40 UTC vinc17
* Added comment:
This bug is marked

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2009-07-20 18:07:11 UTC vinc17
* Added comment:
(I did this because it was marked as new and fixed at the same time!)
