Ticket:  2891
Status:  closed
Summary: color index rule causes messages to be marked read.

Reporter: docwhat
Owner:    mutt-dev

Opened:       2007-05-16 14:10:19 UTC
Last Updated: 2009-06-12 18:55:56 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
= What Happens: =

Add these two lines to your .muttrc
{{{
color  index red       default '~h "Subject: .*l"'
imap_peek=no
}}}

Start mutt by going to a folder that has new messages in it:
{{{
$ mutt -f =foo
}}}

Notice that if you move up or down in the index, the new messages are
marked as read.  If you quit and come back, all the new messages are
marked read.

= What I expected: =

The messages should not be marked read until actually viewed.

= Notes: =

It looks like the color rule above is actually causing the messages
to be marked read.  This causes the index to temporarily show the
unread state, but then as you move around the index, it gets the
current state: read.

My original rule was:
{{{
color  index red       default '~h "X-Spam-Status:.*score=[2-3]\."'
}}}

I tried playing with the imap_headers, but that didn't fix the
problem.  So I tried it with something that was supposed to be read
by the index: Subject.  This implies that something bad is happening
in either mutt or dovecot.  I'm guessing mutt, since it works right
the rest of the time. :-/


== System: ==

||  || Version ||
||OS|| Ubuntu Edgy Eft 6.10||
||mutt|| 1.5.12-1ubuntu1.1||
||dovecot-imap|| 1.0.rc17-1ubuntu1~6.10prevu1||

I'm using imap-local, a shell script, that runs dovecot locally via
this muttrc line:
{{{
account-hook imap://localhost/ 'set tunnel="/usr/local/bin/imap-local"'
}}}


--------------------------------------------------------------------------------
2007-05-20 22:46:10 UTC brendan
* Added comment:
~h causes a full message fetch, regardless of the header, and message fetches cause IMAP servers to mark messages as read. Headers that are known to be in the index mostly have their own patterns (eg ~s, ~f) which are safe to use for index coloring. You can also use server side searches (eg =h) to avoid the message fetch.

* priority changed to minor
* type changed to enhancement

--------------------------------------------------------------------------------
2009-06-12 18:55:56 UTC pdmef
* Added comment:
Closing, not a bug. See also $imap_headers.

* resolution changed to invalid
* status changed to closed
