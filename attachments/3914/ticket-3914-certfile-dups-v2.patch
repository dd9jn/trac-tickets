# HG changeset patch
# User Kevin McCarthy <kevin@8t8.us>
# Date 1488673484 28800
#      Sat Mar 04 16:24:44 2017 -0800
# Node ID ed645a36ba0f758297554d67a19dc60eabff6459
# Parent  5873e42a42f497c24dd7bf6d737588f2c641843e
Don't allow storing duplicate certs for OpenSSL interactive prompt. (closes #3914)

Check to make sure the certificate is not already in the
$certificate_file before offering the (a)ccept always option.

diff --git a/mutt_ssl.c b/mutt_ssl.c
--- a/mutt_ssl.c
+++ b/mutt_ssl.c
@@ -790,68 +790,89 @@
     {
       return 1;
     }
   }
 
   return 0;
 }
 
-static int check_certificate_by_digest (X509 *peercert)
+static int check_certificate_expiration (X509 *peercert, int silent)
+{
+  if (option (OPTSSLVERIFYDATES) != MUTT_NO)
+  {
+    if (X509_cmp_current_time (X509_get_notBefore (peercert)) >= 0)
+    {
+      if (!silent)
+      {
+        dprint (2, (debugfile, "Server certificate is not yet valid\n"));
+        mutt_error (_("Server certificate is not yet valid"));
+        mutt_sleep (2);
+      }
+      return 0;
+    }
+    if (X509_cmp_current_time (X509_get_notAfter (peercert)) <= 0)
+    {
+      if (!silent)
+      {
+        dprint (2, (debugfile, "Server certificate has expired\n"));
+        mutt_error (_("Server certificate has expired"));
+        mutt_sleep (2);
+      }
+      return 0;
+    }
+  }
+
+  return 1;
+}
+
+static int check_certificate_file (X509 *peercert)
 {
   unsigned char peermd[EVP_MAX_MD_SIZE];
   unsigned int peermdlen;
   X509 *cert = NULL;
   int pass = 0;
   FILE *fp;
 
-  /* expiration check */
-  if (option (OPTSSLVERIFYDATES) != MUTT_NO)
-  {
-    if (X509_cmp_current_time (X509_get_notBefore (peercert)) >= 0)
-    {
-      dprint (2, (debugfile, "Server certificate is not yet valid\n"));
-      mutt_error (_("Server certificate is not yet valid"));
-      mutt_sleep (2);
-      return 0;
-    }
-    if (X509_cmp_current_time (X509_get_notAfter (peercert)) <= 0)
-    {
-      dprint (2, (debugfile, "Server certificate has expired\n"));
-      mutt_error (_("Server certificate has expired"));
-      mutt_sleep (2);
-      return 0;
-    }
-  }
+  if (!SslCertFile)
+    return 0;
 
   if ((fp = fopen (SslCertFile, "rt")) == NULL)
     return 0;
 
   if (!X509_digest (peercert, EVP_sha1(), peermd, &peermdlen))
   {
     safe_fclose (&fp);
     return 0;
   }
 
   while (PEM_read_X509 (fp, &cert, NULL, NULL) != NULL)
   {
-    pass = compare_certificates (cert, peercert, peermd, peermdlen) ? 0 : 1;
-
-    if (pass)
+    if ((compare_certificates (cert, peercert, peermd, peermdlen) == 0) &&
+        check_certificate_expiration (cert, 1))
+    {
+      pass = 1;
       break;
+    }
   }
   /* PEM_read_X509 sets an error on eof */
   if (!pass)
     ERR_clear_error();
   X509_free (cert);
   safe_fclose (&fp);
 
   return pass;
 }
 
+static int check_certificate_by_digest (X509 *peercert)
+{
+  return check_certificate_expiration (peercert, 0) &&
+    check_certificate_file (peercert);
+}
+
 /* port to mutt from msmtp's tls.c */
 static int hostname_match (const char *hostname, const char *certname)
 {
   const char *cmp1, *cmp2;
 
   if (strncmp(certname, "*.", 2) == 0)
   {
     cmp1 = certname + 2;
@@ -1156,20 +1177,19 @@
 
   /* L10N:
    * These four letters correspond to the choices in the next four strings:
    * (r)eject, accept (o)nce, (a)ccept always, (s)kip.
    * These prompts are the interactive certificate confirmation prompts for
    * an OpenSSL connection.
    */
   menu->keys = _("roas");
-  if (SslCertFile
-      && (option (OPTSSLVERIFYDATES) == MUTT_NO
-	  || (X509_cmp_current_time (X509_get_notAfter (cert)) >= 0
-	      && X509_cmp_current_time (X509_get_notBefore (cert)) < 0)))
+  if (SslCertFile &&
+      check_certificate_expiration (cert, 1) &&
+      !check_certificate_file (cert))
   {
     allow_always = 1;
     if (allow_skip)
       menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always, (s)kip");
     else
       menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always");
   }
   else
