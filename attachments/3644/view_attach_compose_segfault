# HG changeset patch
# Parent 0beba37c336b80e6e793b597f51bd9f7fc7e3918
# User Michael Elkins <me@sigpipe.org>
# Date 1380909020 0

fix bug introduced by http://dev.mutt.org/trac/changeset/b9f9e3147eb4/ where viewing an attachment from the compose menu caused a segfault.

this is an alternate version of the fix proposed here http://dev.mutt.org/trac/attachment/ticket/3644/simple-fix.patch

closes #3644


diff --git a/attach.c b/attach.c
--- a/attach.c
+++ b/attach.c
@@ -504,27 +504,39 @@
 
     if (flag == M_AS_TEXT)
     {
-      /* just let me see the raw data.
-       *
-       * Don't use mutt_save_attachment() because we want to perform charset
-       * conversion since this will be displayed by the internal pager.
-       */
-      STATE decode_state;
+      /* just let me see the raw data */
+      if (fp)
+      {
+	/* Viewing from a received message.
+	 *
+	 * Don't use mutt_save_attachment() because we want to perform charset
+	 * conversion since this will be displayed by the internal pager.
+	 */
+	STATE decode_state;
 
-      memset(&decode_state, 0, sizeof(decode_state));
-      decode_state.fpout = safe_fopen(pagerfile, "w");
-      if (!decode_state.fpout)
+	memset(&decode_state, 0, sizeof(decode_state));
+	decode_state.fpout = safe_fopen(pagerfile, "w");
+	if (!decode_state.fpout)
+	{
+	  dprint(1, (debugfile, "mutt_view_attachment:%d safe_fopen(%s) errno=%d %s\n", __LINE__, pagerfile, errno, strerror(errno)));
+	  mutt_perror(pagerfile);
+	  mutt_sleep(1);
+	  goto return_error;
+	}
+	decode_state.fpin = fp;
+	decode_state.flags = M_CHARCONV;
+	mutt_decode_attachment(a, &decode_state);
+	if (fclose(decode_state.fpout) == EOF)
+	  dprint(1, (debugfile, "mutt_view_attachment:%d fclose errno=%d %s\n", __LINE__, pagerfile, errno, strerror(errno)));
+      }
+      else
       {
-	dprint(1, (debugfile, "mutt_view_attachment:%d safe_fopen(%s) errno=%d %s\n", __LINE__, pagerfile, errno, strerror(errno)));
-	mutt_perror(pagerfile);
-	mutt_sleep(1);
-	goto return_error;
+	/* in compose mode, just copy the file.  we can't use
+	 * mutt_decode_attachment() since it assumes the content-encoding has
+	 * already been applied
+	 */
+	mutt_save_attachment(fp, a, pagerfile, 0, NULL);
       }
-      decode_state.fpin = fp;
-      decode_state.flags = M_CHARCONV;
-      mutt_decode_attachment(a, &decode_state);
-      if (fclose(decode_state.fpout) == EOF)
-	dprint(1, (debugfile, "mutt_view_attachment:%d fclose errno=%d %s\n", __LINE__, pagerfile, errno, strerror(errno)));
     }
     else
     {
