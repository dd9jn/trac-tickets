Mutt 1.5.23 (2014-03-12)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 3.16.0-4-amd64 (x86_64)
ncurses: ncurses 5.9.20140913 (compiled with 5.9)
libidn: 1.29 (compiled with 1.29)
hcache backend: tokyocabinet 1.4.48

Compiler:
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/4.9/lto-wrapper
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Debian 4.9.2-4' --with-bugurl=file:///usr/share/doc/gcc-4.9/README.Bugs --enable-languages=c,c++,java,go,d,fortran,objc,obj-c++ --prefix=/usr --program-suffix=-4.9 --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --with-gxx-include-dir=/usr/include/c++/4.9 --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --enable-gnu-unique-object --disable-vtable-verify --enable-plugin --with-system-zlib --disable-browser-plugin --enable-java-awt=gtk --enable-gtk-cairo --with-java-home=/usr/lib/jvm/java-1.5.0-gcj-4.9-amd64/jre --enable-java-home --with-jvm-root-dir=/usr/lib/jvm/java-1.5.0-gcj-4.9-amd64 --with-jvm-jar-dir=/usr/lib/jvm-exports/java-1.5.0-gcj-4.9-amd64 --with-arch-directory=amd64 --with-ecj-jar=/usr/share/java/eclipse-ecj.jar --enable-objc-gc --enable-multiarch --with-arch-32=i586 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 4.9.2 (Debian 4.9.2-4) 

Configure options: '--prefix=/usr' '--sysconfdir=/etc' '--mandir=/usr/share/man' '--with-docdir=/usr/share/doc' '--with-mailpath=/var/mail' '--disable-dependency-tracking' '--enable-compressed' '--enable-debug' '--enable-fcntl' '--enable-hcache' '--enable-gpgme' '--enable-imap' '--enable-smtp' '--enable-pop' '--with-curses' '--with-gnutls' '--with-gss' '--with-idn' '--with-mixmaster' '--with-sasl' '--without-gdbm' '--without-bdb' '--without-qdbm' '--build' 'x86_64-linux-gnu' 'build_alias=x86_64-linux-gnu' 'CFLAGS=-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wall' 'LDFLAGS=-Wl,-z,relro' 'CPPFLAGS=-D_FORTIFY_SOURCE=2 -I/usr/include/qdbm'

Compilation CFLAGS: -g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wall

Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_IMAP  +USE_SMTP  
-USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  +CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

misc/am-maintainer-mode.patch
features/ifdef.patch
features/xtitles.patch
features/trash-folder.patch
features/purge-message.patch
features/imap_fast_trash.patch
features/sensible_browser_position.patch
features-old/patch-1.5.4.vk.pgp_verbose_mime.patch
features/compressed-folders.patch
features/compressed-folders.debian.patch
debian-specific/Muttrc.patch
debian-specific/Md.etc_mailname_gethostbyname.patch
debian-specific/use_usr_bin_editor.patch
debian-specific/correct_docdir_in_man_page.patch
debian-specific/dont_document_not_present_features.patch
debian-specific/document_debian_defaults.patch
debian-specific/assumed_charset-compat.patch
debian-specific/467432-write_bcc.patch
debian-specific/566076-build_doc_adjustments.patch
misc/define-pgp_getkeys_command.patch
misc/gpg.rc-paths.patch
misc/smime.rc.patch
misc/fix-configure-test-operator.patch
upstream/531430-imapuser.patch
upstream/543467-thread-segfault.patch
upstream/542817-smimekeys-tmpdir.patch
upstream/548577-gpgme-1.2.patch
upstream/553321-ansi-escape-segfault.patch
upstream/547980-smime_keys-chaining.patch
upstream/528233-readonly-open.patch
upstream/228671-pipe-mime.patch
upstream/383769-score-match.patch
upstream/603288-split-fetches.patch
upstream/611410-no-implicit_autoview-for-text-html.patch
upstream/path_max.patch
translations/update_german_translation.patch
upstream/771125-CVE-2014-9116-jessie.patch
__separator__mutt.org.patch
