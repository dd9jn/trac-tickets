/* Prints the portable name for the current locale's charset.
 * Build with gcc -o locale_charset locale_charset.c -lcharset
 * to catch /usr/local/lib/libcharset.so.1
 * Or -liconv to catch /usr/local/lib/libiconv.so.2
 * Or -lintl  to catch /usr/local/lib/libintl.so.2
 */

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <localcharset.h>

int main ()
{
  setlocale(LC_ALL, "");
  printf("%s\n", locale_charset());
  exit(0);
}
