# HG changeset patch
# User Kevin McCarthy <kevin@8t8.us>
# Date 1488076335 28800
#      Sat Feb 25 18:32:15 2017 -0800
# Node ID df8f3d48f855253967c507ce06182e3ba2946ac7
# Parent  66056fb5f434dbbf5c3588f1ac4d8efd96401e5c
Add "skip" option when confirming OpenSSL certificates.  (closes #3916)

The reworked OpenSSL certificate validation took a way a "feature" of
the previous implementation: the ability to reject a node in the chain
and yet continue to the next node.

To supply something roughly equivalent, this patch adds a (s)kip
option.  When set, preverify_ok passed in by OpenSSL is ignored,
forcing mutt to perform checks as if it were not pre-verified.
(e.g. checking the session certs, certificate file, and prompting).

Once a following cert in the chain is accepted, this mode is turned
off and preverify_ok is used again.

diff --git a/mutt_ssl.c b/mutt_ssl.c
--- a/mutt_ssl.c
+++ b/mutt_ssl.c
@@ -52,16 +52,22 @@
 #else
 static int entropy_byte_count = 0;
 /* OpenSSL fills the entropy pool from /dev/urandom if it exists */
 #define HAVE_ENTROPY()	(!access(DEVRANDOM, R_OK) || entropy_byte_count >= 16)
 #endif
 
 /* index for storing hostname as application specific data in SSL structure */
 static int HostExDataIndex = -1;
+
+/* Index for storing the "skip mode" state in SSL structure.
+ * When the user skips a certificate in the chain, the slot
+ * will be changed from NULL to point to this address. */
+static int SkipModeExDataIndex = -1;
+
 /* keep a handle on accepted certificates in case we want to
  * open up another connection to the same server in this session */
 static STACK_OF(X509) *SslSessionCerts = NULL;
 
 typedef struct
 {
   SSL_CTX *ctx;
   SSL *ssl;
@@ -77,17 +83,17 @@
 static int ssl_socket_write (CONNECTION* conn, const char* buf, size_t len);
 static int ssl_socket_open (CONNECTION * conn);
 static int ssl_socket_close (CONNECTION * conn);
 static int tls_close (CONNECTION* conn);
 static void ssl_err (sslsockdata *data, int err);
 static void ssl_dprint_err_stack (void);
 static int ssl_cache_trusted_cert (X509 *cert);
 static int ssl_verify_callback (int preverify_ok, X509_STORE_CTX *ctx);
-static int interactive_check_cert (X509 *cert, int idx, int len);
+static int interactive_check_cert (X509 *cert, int idx, int len, SSL *ssl);
 static void ssl_get_client_cert(sslsockdata *ssldata, CONNECTION *conn);
 static int ssl_passwd_cb(char *buf, int size, int rwflag, void *userdata);
 static int ssl_negotiate (CONNECTION *conn, sslsockdata*);
 
 /* ssl certificate verification can behave strangely if there are expired
  * certs loaded into the trusted store.  This function filters out expired
  * certs.
  * Previously the code used this form:
@@ -484,16 +490,28 @@
   }
 
   if (! SSL_set_ex_data (ssldata->ssl, HostExDataIndex, conn->account.host))
   {
     dprint (1, (debugfile, "failed to save hostname in SSL structure\n"));
     return -1;
   }
 
+  if ((SkipModeExDataIndex = SSL_get_ex_new_index (0, "skip", NULL, NULL, NULL)) == -1)
+  {
+    dprint (1, (debugfile, "failed to get index for application specific data\n"));
+    return -1;
+  }
+
+  if (! SSL_set_ex_data (ssldata->ssl, SkipModeExDataIndex, NULL))
+  {
+    dprint (1, (debugfile, "failed to save skip mode in SSL structure\n"));
+    return -1;
+  }
+
   SSL_set_verify (ssldata->ssl, SSL_VERIFY_PEER, ssl_verify_callback);
   SSL_set_mode (ssldata->ssl, SSL_MODE_AUTO_RETRY);
   ERR_clear_error ();
 
   if ((err = SSL_connect (ssldata->ssl)) != 1)
   {
     switch (SSL_get_error (ssldata->ssl, err))
     {
@@ -941,83 +959,88 @@
  * certificate is trusted, returning 0 immediately aborts the SSL connection */
 static int ssl_verify_callback (int preverify_ok, X509_STORE_CTX *ctx)
 {
   char buf[STRING];
   const char *host;
   int len, pos;
   X509 *cert;
   SSL *ssl;
+  int skip_mode;
 
   if (! (ssl = X509_STORE_CTX_get_ex_data (ctx, SSL_get_ex_data_X509_STORE_CTX_idx ())))
   {
     dprint (1, (debugfile, "ssl_verify_callback: failed to retrieve SSL structure from X509_STORE_CTX\n"));
     return 0;
   }
   if (! (host = SSL_get_ex_data (ssl, HostExDataIndex)))
   {
     dprint (1, (debugfile, "ssl_verify_callback: failed to retrieve hostname from SSL structure\n"));
     return 0;
   }
+  skip_mode = (SSL_get_ex_data (ssl, SkipModeExDataIndex) != NULL);
 
   cert = X509_STORE_CTX_get_current_cert (ctx);
   pos = X509_STORE_CTX_get_error_depth (ctx);
   len = sk_X509_num (X509_STORE_CTX_get_chain (ctx));
 
-  dprint (1, (debugfile, "ssl_verify_callback: checking cert chain entry %s (preverify: %d)\n",
-              X509_NAME_oneline (X509_get_subject_name (cert),
-                                 buf, sizeof (buf)), preverify_ok));
+  dprint (1, (debugfile,
+              "ssl_verify_callback: checking cert chain entry %s (preverify: %d skipmode: %d)\n",
+              X509_NAME_oneline (X509_get_subject_name (cert), buf, sizeof (buf)),
+              preverify_ok, skip_mode));
 
   /* check session cache first */
   if (check_certificate_cache (cert))
   {
     dprint (2, (debugfile, "ssl_verify_callback: using cached certificate\n"));
+    SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
     return 1;
   }
 
   /* check hostname only for the leaf certificate */
   buf[0] = 0;
   if (pos == 0 && option (OPTSSLVERIFYHOST) != MUTT_NO)
   {
     if (!check_host (cert, host, buf, sizeof (buf)))
     {
       mutt_error (_("Certificate host check failed: %s"), buf);
       mutt_sleep (2);
-      return interactive_check_cert (cert, pos, len);
+      return interactive_check_cert (cert, pos, len, ssl);
     }
     dprint (2, (debugfile, "ssl_verify_callback: hostname check passed\n"));
   }
 
-  if (!preverify_ok)
+  if (!preverify_ok || skip_mode)
   {
     /* automatic check from user's database */
     if (SslCertFile && check_certificate_by_digest (cert))
     {
       dprint (2, (debugfile, "ssl_verify_callback: digest check passed\n"));
+      SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
       return 1;
     }
 
 #ifdef DEBUG
     /* log verification error */
     {
       int err = X509_STORE_CTX_get_error (ctx);
       snprintf (buf, sizeof (buf), "%s (%d)",
          X509_verify_cert_error_string (err), err);
       dprint (2, (debugfile, "X509_verify_cert: %s\n", buf));
     }
 #endif
 
     /* prompt user */
-    return interactive_check_cert (cert, pos, len);
+    return interactive_check_cert (cert, pos, len, ssl);
   }
 
   return 1;
 }
 
-static int interactive_check_cert (X509 *cert, int idx, int len)
+static int interactive_check_cert (X509 *cert, int idx, int len, SSL *ssl)
 {
   static const int part[] =
     { NID_commonName,             /* CN */
       NID_pkcs9_emailAddress,     /* Email */
       NID_organizationName,       /* O */
       NID_organizationalUnitName, /* OU */
       NID_localityName,           /* L */
       NID_stateOrProvinceName,    /* ST */
@@ -1026,16 +1049,17 @@
   X509_NAME *x509_issuer;
   char helpstr[LONG_STRING];
   char buf[STRING];
   char title[STRING];
   MUTTMENU *menu = mutt_new_menu (MENU_GENERIC);
   int done, row, i;
   unsigned u;
   FILE *fp;
+  int allow_always = 0;
 
   menu->max = mutt_array_size (part) * 2 + 10;
   menu->dialog = (char **) safe_calloc (1, menu->max * sizeof (char *));
   for (i = 0; i < menu->max; i++)
     menu->dialog[i] = (char *) safe_calloc (1, SHORT_STRING * sizeof (char));
 
   row = 0;
   strfcpy (menu->dialog[row], _("This certificate belongs to:"), SHORT_STRING);
@@ -1067,28 +1091,38 @@
   buf[0] = '\0';
   x509_fingerprint (buf, sizeof (buf), cert, EVP_md5);
   snprintf (menu->dialog[row++], SHORT_STRING, _("MD5 Fingerprint: %s"), buf);
 
   snprintf (title, sizeof (title),
 	    _("SSL Certificate check (certificate %d of %d in chain)"),
 	    len - idx, len);
   menu->title = title;
+  /* L10N:
+   * These four letters correspond to the choices in the next four strings:
+   * (r)eject, accept (o)nce, (a)ccept always, (s)kip.
+   */
+  menu->keys = _("roas");
   if (SslCertFile
       && (option (OPTSSLVERIFYDATES) == MUTT_NO
 	  || (X509_cmp_current_time (X509_get_notAfter (cert)) >= 0
 	      && X509_cmp_current_time (X509_get_notBefore (cert)) < 0)))
   {
-    menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always");
-    menu->keys = _("roa");
+    allow_always = 1;
+    if (idx == 0)
+      menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always");
+    else
+      menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always, (s)kip");
   }
   else
   {
-    menu->prompt = _("(r)eject, accept (o)nce");
-    menu->keys = _("ro");
+    if (idx == 0)
+      menu->prompt = _("(r)eject, accept (o)nce");
+    else
+      menu->prompt = _("(r)eject, accept (o)nce, (s)kip");
   }
 
   helpstr[0] = '\0';
   mutt_make_help (buf, sizeof (buf), _("Exit  "), MENU_GENERIC, OP_EXIT);
   safe_strcat (helpstr, sizeof (helpstr), buf);
   mutt_make_help (buf, sizeof (buf), _("Help"), MENU_GENERIC, OP_HELP);
   safe_strcat (helpstr, sizeof (helpstr), buf);
   menu->help = helpstr;
@@ -1100,16 +1134,18 @@
     switch (mutt_menuLoop (menu))
     {
       case -1:			/* abort */
       case OP_MAX + 1:		/* reject */
       case OP_EXIT:
         done = 1;
         break;
       case OP_MAX + 3:		/* accept always */
+        if (!allow_always)
+          break;
         done = 0;
         if ((fp = fopen (SslCertFile, "a")))
 	{
 	  if (PEM_write_X509 (fp, cert))
 	    done = 1;
 	  safe_fclose (&fp);
 	}
 	if (!done)
@@ -1120,18 +1156,27 @@
 	else
         {
 	  mutt_message (_("Certificate saved"));
 	  mutt_sleep (0);
 	}
         /* fall through */
       case OP_MAX + 2:		/* accept once */
         done = 2;
+        SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
 	ssl_cache_trusted_cert (cert);
         break;
+      case OP_MAX + 4:          /* skip */
+        if (idx == 0)
+          break;
+        done = 2;
+        /* This turns on "skip mode", forcing the following certficate
+         * nodes to be checked. */
+        SSL_set_ex_data (ssl, SkipModeExDataIndex, &SkipModeExDataIndex);
+        break;
     }
   }
   unset_option(OPTIGNOREMACROEVENTS);
   mutt_menuDestroy (&menu);
   set_option (OPTNEEDREDRAW);
   dprint (2, (debugfile, "ssl interactive_check_cert: done=%d\n", done));
   return (done == 2);
 }
